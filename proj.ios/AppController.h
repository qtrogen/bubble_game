#import <UIKit/UIKit.h>
#import "GCViewController.h"
#import "GameCenterManager.h"
#import <GameKit/GameKit.h>
#import "Chartboost.h"
#import "GADBannerView.h"
#import "LStoreManagerIOS.h"
#import "LLoadingView.h"
#import <AVFoundation/AVFoundation.h>
#import "BaasGameClient.h"
#import "NTWGetAppINFO.h"
#import "iRate.h"

@class RootViewController;


#ifdef PRO_VERSION


#define CHARTBOOST_ID   @"522c95f317ba470a7b000011"
#define CHARTBOOST_SIGN @"e6ef7e25a1a60c64aa1e616702561a6b4466a449"

#define REVMOB_ID       @"522c86720492eb32f0000016"
#define ADMOB_ID        @"ca-app-pub-7986442841539994/1432210265"

#define kCandyLeaderboard @"com.flopstudios.cupcakejumppro.score"

#else // FREE version


#define CHARTBOOST_ID   @"522c8e4417ba47ab7800000f"
#define CHARTBOOST_SIGN @"d26c12ca0ff4fb4b05ac507231d88805ee89df76"

#define REVMOB_ID       @"522c87a30492eb32f0000025"
#define ADMOB_ID        @"f7315e9f4b1e43f6"

#define kCandyLeaderboard @"com.flopstudios.cupcakejump.score"

#endif //PRO_VERSION

@interface AppController : NSObject <UIApplicationDelegate, ChartboostDelegate, GameCenterManagerDelegate, GKLeaderboardViewControllerDelegate, LPurchaseDelegateIOS, AVAudioPlayerDelegate, BaasGameDelegate, NTWGetAppINFO, UIAlertViewDelegate> {
    UIWindow *window;
    RootViewController    *viewController;

    GCViewController* viewController2;
    GameCenterManager* gameCenterManager;
    
    NSMutableDictionary* soundPlayers;

    bool        isRemoveAds;
}

@property (nonatomic, retain) GADBannerView* bannerView;
@property (nonatomic, retain) LLoadingView* loadingView;

-(void) sendToCocos:(int) nType strParam:(NSString*) strParam intParam:(int) nParam;
-(bool) receivedFromCocos2dx:(int) nType strParam:(NSString*) strParam intParam:(int) nParam;

-(void) removeAds;
- (void)showLeaderboard;
- (void) submitScore:(int) score forLevel:(int) level;
- (void) showApplovin;
- (void) showRevmob;
//commented by KUH in 2014-09-25
- (void) showChartboost;
- (void) showPlayHaven;
- (void) showMoreApps;
@end


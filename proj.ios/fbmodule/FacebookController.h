/*
 * Copyright 2012 Facebook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "type_defs.h"

#import "RootViewController.h"
#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Accounts/Accounts.h>

enum eGameAchievements
{
    kACHIEVEMENT_SCORE50 = 0,
    kACHIEVEMENT_SCORE100,
    kACHIEVEMENT_SCORE150,
    kACHIEVEMENT_SCORE200,
    kACHIEVEMENT_SCOREx3,
    kACHIEVEMENT_MAX
};

@protocol FacebookDelegate <NSObject>
-(void) didLogIn:(bool) bLoggedIn;
-(void) didLogout:(bool) bLoggedOut;
-(void) didFetchUserDetails:(bool) bSuccess;
-(void) didProcessFBURL:(NSString*) challengeName fbid:(NSString*) challengeFBID;
-(void) didGetScores:(NSArray*) array;
-(void) receivedProfilePicture:(id) data;
@end

@interface FacebookController : NSObject
{
    bool ms_bIsLoggedIn;
    
    FBFrictionlessRecipientCache* ms_friendCache;
    
    NSOperationQueue *fbQueue;
};

@property (nonatomic, assign) id<FacebookDelegate> delegate;
@property (nonatomic, retain) NSString* strUserName;
@property (nonatomic) u64 ms_uPlayerFBID;

-(void) CreateNewSession;
-(bool) IsLoggedIn;
-(void) SetLoggedIn:(bool) bLoggedIn;
-(void) OpenSession:(id) target selector:(SEL) selector;
-(void) Login:(id) target selector:(SEL) selector;
-(void) Logout:(id) target selector:(SEL) selector;
-(void) FetchUserDetails:(id) target selector:(SEL) selector;
-(void) ProcessIncomingURL:(NSURL*) targetURL target:(id) target selector:(SEL) Callback;
-(void) ProcessIncomingRequest:(NSURL*) targetURL target:(id) target selector:(SEL) Callback;
-(void) ProcessIncomingFeed:(NSURL*) targetURL target:(id) target selector:(SEL) Callback;
-(void) SendRequest:(int) nScore;
-(void) SendFilteredRequest:(int) nScore;
-(void) SendFriendRequest:(id) target selector:(SEL)callback;
-(void) SendBrag:(int) nScore;
-(void) SendScore:(int) nScore level:(int) nLevel;
-(void) SendAchievement:(eGameAchievements) achievement;
-(void) GetScores;
-(void) SendOG:(const u64) uFriendID;
-(void) requestPhotoForId:(NSString*) fbid;

-(void) checkIncommingRequests:(id) target selector:(SEL) Callback;
-(void) askLiveToFriends;
-(void) giveLive:(NSString*) requestId fbid:(NSString*) fbid target:(id) target selector:(SEL) Callback;
-(void) collectLive:(NSString*) requestId target:(id) target selector:(SEL) Callback;
-(void) notificationClear:(NSString *)requestid target:(id) target selector:(SEL) Callback;

@end


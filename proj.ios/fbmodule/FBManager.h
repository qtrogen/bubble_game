/*
 * FB Manager class
 */

#ifndef FBMANAGER_H
#define FBMANAGER_H

#include "type_defs.h"
#include <vector>

typedef struct _tagUserInfo
{
    const char* name;
    u64         nid;
    u32         score; // score for ranking
    u16         level; //achievement
    const char* photo; // image url or image binary data
} UserInfo;

typedef std::vector<UserInfo> TUserList;

class FBManager
{
public:
    static FBManager* sharedInstance();
    
    bool isLoggedIn();
    void login();
    void prepare();
    
    void sendScore(int nLevel, int nScore);
    void sendAchievement(int nLevel);
    const TUserList& getFriendList();
    void reqFriendList();
    void updateScore();
    
    void ProcessIncomingURL(NSURL* url);
    // callback
    void DidChangeFBLoginState(bool bState);
    void DidFetchFBUserInfo(bool bSuccess);
    void DidProcessIncomingURL(NSString* challengeName, NSString* challengeFBID);

protected:
    UserInfo* ownInfo;
    
private:
    FBManager();
    static FBManager* _instance;
    TUserList       userList;
    
};
//typedef struct _tagUserInfo
//{
//    const char* name;
//    u64         nid;
//    u32         score; // score for ranking
//    u16         level; //achievement
//    const char* photo; // image url or image binary data
//} UserInfo;
//
//typedef std::vector<UserInfo> TUserList;
//
//@class FBManager;
//
//@interface FBManager
//{
//    UserInfo* ownInfo;
//    
//    TUserList       userList;
//}
//
//+(FBManager*) sharedInstance;
//
//-(bool) isLoggedIn;
//-(void) login;
//-(void) prepare;
//
//-(void) sendScore:(int) nScore level:(int) nLevel;
//-(void) sendAchievement:(int) nAchieve;
//-(TUserList*) getFriendList;
//-(void) reqFriendList;
//
//
//-(void) ProcessIncomingURL:(NSURL*) url;
//// callback
//-(void) DidChangeFBLoginState:(bool) bState;
//-(void) DidFetchFBUserInfo:(bool) bSuccess;
//-(void) DidProcessIncomingURL:(NSString*) challengeName fbid:(NSString*) challengeFBID;
//@end

#endif

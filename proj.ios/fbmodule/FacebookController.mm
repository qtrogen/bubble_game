//
//  FacebookController.cpp
//  friendSmasher
//
//  Created by Ali Parr on 23/10/2013.
//
//

#import "FacebookController.h"

#import <Social/Social.h>
#import <Social/SLComposeViewController.h>
#import "SBJson.h"

static u64 kuFBAppID;// = 190698341093965;
static const int kScoreMask = 0xFFFFFF;
#define FB_ACCESS_TOKEN_DATA @"fb_access_token"

@implementation FacebookController

-(id) init
{
    if (self = [super init])
    {
        kuFBAppID = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"] longLongValue];
        fbQueue = [[NSOperationQueue alloc] init];
    }
    
    return self;
}

-(void) CreateNewSession
{
    FBSession* session = [[FBSession alloc] initWithAppID:[NSString stringWithFormat:@"%llu", kuFBAppID] permissions:nil defaultAudience:FBSessionDefaultAudienceFriends urlSchemeSuffix:nil tokenCacheStrategy:[FBSessionTokenCachingStrategy nullCacheInstance]];
    [FBSession setActiveSession: session];
        NSLog(@"session state3 = %d", [FBSession activeSession].state);
}

-(void) saveAccessTokenData:(FBSession*) session
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[session.accessTokenData dictionary] forKey:FB_ACCESS_TOKEN_DATA];
    [defaults synchronize];
}

-(bool) IsLoggedIn
{
    return ms_bIsLoggedIn;
}

-(void) SetLoggedIn:(bool) bLoggedIn
{
    ms_bIsLoggedIn = bLoggedIn;
}

-(void) OpenSession:(id) target selector:(SEL) selector
{
    NSLog(@"session state4 = %d", [FBSession activeSession].state);
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary* fbdic = [defaults objectForKey:FB_ACCESS_TOKEN_DATA];
    FBAccessTokenData* token = [FBAccessTokenData createTokenFromDictionary:fbdic];
    NSLog(@"session state5 = %d", [FBSession activeSession].state);

    [[FBSession activeSession] openFromAccessTokenData:token completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
        // Did something go wrong during login? I.e. did the user cancel?
        
        if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateClosed || status == FBSessionStateCreatedOpening) {
            ms_bIsLoggedIn = false;
            [self.delegate didLogIn:false];
        }
        else {
            ms_bIsLoggedIn = true;
            [self.delegate didLogIn:true];
        }
    }];
}

-(void) Login:(id) target selector:(SEL) selector
{
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"basic_info",
                            nil];
    // Attempt to open the session. If the session is not open, show the user the Facebook login UX
    [FBSession openActiveSessionWithPermissions:permissions allowLoginUI:true completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
        
        // Did something go wrong during login? I.e. did the user cancel?
        
        if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateClosed || status == FBSessionStateCreatedOpening) {
            
            // If so, just send them round the loop again
            [[FBSession activeSession] closeAndClearTokenInformation];
            [FBSession setActiveSession:nil];
            ms_bIsLoggedIn = false;
            [self.delegate didLogIn:false];
        }
        else {
            ms_bIsLoggedIn = true;
            [self saveAccessTokenData:session];
            [self.delegate didLogIn:true];
        }
    }];
}

-(void) Logout:(id) target selector:(SEL) selector
{
    // Log out of Facebook and reset our session
    [[FBSession activeSession] closeAndClearTokenInformation];
    [FBSession setActiveSession:nil];
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:FB_ACCESS_TOKEN_DATA];
    [defaults synchronize];
    ms_bIsLoggedIn = false;
    [self.delegate didLogout:true];
}

-(void) FetchUserDetails:(id) target selector:(SEL) selector
{
    //
    // Start the facebook request
    [[FBRequest requestForMe]
     startWithCompletionHandler:
     ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *result, NSError *error)
     {
         // Did everything come back okay with no errors?
         if (!error && result) {
             NSLog(@"User info : %@", result);
             // If so we can extract out the player's Facebook ID and first name
             self.strUserName = [[NSString alloc] initWithString:result.first_name];
             self.ms_uPlayerFBID =  [result.id longLongValue];
             
             [self.delegate didFetchUserDetails:true];
         }
         else {
             [self.delegate didFetchUserDetails:false];
         }
     }];
}


-(void) ProcessIncomingURL:(NSURL*) targetURL target:(id) target selector:(SEL) selector
{
    // Process the incoming url and see if it's of value...
    
    NSRange range = [targetURL.query rangeOfString:@"notif" options:NSCaseInsensitiveSearch];
    
    // If the url's query contains 'notif', we know it's coming from a notification - let's process it
    if(targetURL.query && range.location != NSNotFound)
    {
        // Yes the incoming URL was a notification
        [self ProcessIncomingRequest:targetURL target:target selector:selector];
    }
    
    range = [targetURL.path rangeOfString:@"challenge_brag" options:NSCaseInsensitiveSearch];
    
    // If the url's path contains 'challenge_brag', we know it comes from a feed post
    if(targetURL.path && range.location != NSNotFound)
    {
        // Yes the incoming URL was a notification
        [self ProcessIncomingFeed:targetURL target:target selector:selector];
    }
}

-(void) ProcessIncomingRequest:(NSURL*) targetURL target:(id) target selector:(SEL) selector
{
    // Extract the notification id
    NSArray *pairs = [targetURL.query componentsSeparatedByString:@"&"];
    NSMutableDictionary *queryParams = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs)
    {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val = [[kv objectAtIndex:1]
                         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [queryParams setObject:val forKey:[kv objectAtIndex:0]];
        
    }
    
    NSString *requestIDsString = [queryParams objectForKey:@"request_ids"];
    NSArray *requestIDs = [requestIDsString componentsSeparatedByString:@","];
    
    FBRequest *req = [[FBRequest alloc] initWithSession:[FBSession activeSession] graphPath:[requestIDs objectAtIndex:0]];
    
    [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         if (!error)
         {
             
             if ([result objectForKey:@"from"])
             {
                 NSString *from = [[result objectForKey:@"from"] objectForKey:@"name"];
                 NSString *fid = [[result objectForKey:@"from"] objectForKey:@"id"];
                 
                 [target performSelector:selector withObject:from withObject:fid];
             }
             
         }
         
     }];
}

-(void) ProcessIncomingFeed:(NSURL*) targetURL target:(id) target selector:(SEL) selector
{
    // Here we process an incoming link that has launched the app via a feed post
    
    // Here is extract out the FBID component at the end of the brag, so 'challenge_brag_123456' becomes just 123456
    NSString* val = [[targetURL.path componentsSeparatedByString:@"challenge_brag_"] lastObject];
    
    FBRequest *req = [[FBRequest alloc] initWithSession:[FBSession activeSession] graphPath:val];
    
    // With the FBID extracted, we have enough information to go ahead and request the user's profile picture
    // But we also need their name, so here we make a request to http://graph.facebook.com/USER_ID to get their basic information
    [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         // If the result came back okay with no errors...
         if (result && !error)
         {
             NSString *from = [result objectForKey:@"first_name"];
             [target performSelector:selector withObject:from withObject:val];
         }
     }];
}

-(void) SendRequest:(int) nScore
{
    // Normally this won't be hardcoded but will be context specific, i.e. players you are in a match with, or players who recently played the game etc
    
//    NSArray *suggestedFriends = [[NSArray alloc] initWithObjects:
//                                 @"223400030", @"286400088", @"767670639", @"516910788",
//                                 nil];
    
    SBJsonWriter *jsonWriter = [SBJsonWriter new];
    NSDictionary *challenge =  [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat:@"%d", nScore], @"challenge_score", nil];
    NSString *challengeStr = [jsonWriter stringWithObject:challenge];
    
    
    // Create a dictionary of key/value pairs which are the parameters of the dialog
    
    // 1. No additional parameters provided - enables generic Multi-friend selector
    NSMutableDictionary* params =   [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     // 2. Optionally provide a 'to' param to direct the request at a specific user
                                     //@"286400088", @"to", // Ali
                                     // 3. Suggest friends the user may want to request, could be game context specific?
                                     //[suggestedFriends componentsJoinedByString:@","], @"suggestions",
                                     challengeStr, @"data",
                                     nil];
    
    
    
    if (ms_friendCache == NULL) {
        ms_friendCache = [[FBFrictionlessRecipientCache alloc] init];
    }
    
    [ms_friendCache prefetchAndCacheForSession:nil];
    
    [FBWebDialogs presentRequestsDialogModallyWithSession:nil
                                                  message:[NSString stringWithFormat:@"I just smashed %d friends! Can you beat it?", nScore]
                                                    title:@"Smashing!"
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // Case A: Error launching the dialog or sending request.
                                                          NSLog(@"Error sending request.");
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // Case B: User clicked the "x" icon
                                                              NSLog(@"User canceled request.");
                                                          } else {
                                                              NSLog(@"Request Sent.");
                                                          }
                                                      }}
                                              friendCache:ms_friendCache];
}

-(void) SendFilteredRequest:(int) nScore
{
    // Okay, we're going to filter our friends by their device, we're looking for friends with an iPhone or iPad
    
    // We're going to place these friends into this container
    NSMutableArray *deviceFilteredFriends = [[NSMutableArray alloc] init];
    
    // We request a list of our friends' names and devices
    [[FBRequest requestForGraphPath:@"me/friends?fields=name,devices"]
     startWithCompletionHandler:
     ^(FBRequestConnection *connection,
       NSDictionary *result,
       NSError *error)
     {
         // If we received a result with no errors...
         if (!error && result)
         {
             // Get the result
             NSArray *resultData = [result objectForKey:@"data"];
             
             // Check we have some friends. If the player doesn't have any friends, they probably need to put down the demo app anyway, and go outside...
             if ([resultData count] > 0)
             {
                 // Loop through the friends returned
                 for (NSDictionary *friendObject in resultData)
                 {
                     // Check if devices info available
                     if ([friendObject objectForKey:@"devices"])
                     {
                         // Yep, we know what devices this friend has.. let's extract them
                         NSArray *deviceData = [friendObject objectForKey:@"devices"];
                         
                         // Loop through the list of devices this friend has...
                         for (NSDictionary *deviceObject in deviceData)
                         {
                             // Check if there is a device match, in this case we're looking for iOS
                             if ([@"iOS" isEqualToString: [deviceObject objectForKey:@"os"]])
                             {
                                 // If there is a match, add it to the list - this friend has an iPhone or iPad. Hurrah!
                                 [deviceFilteredFriends addObject: [friendObject objectForKey:@"id"]];
                                 break;
                             }
                         }
                     }
                 }
             }
             
             // Now we have a list of friends with an iOS device, we can send requests to them
             
             // We create our parameter dictionary as we did before
             NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:nil];
             
             // We have the same list of suggested friends
             NSArray *suggestedFriends = [[NSArray alloc] initWithObjects:
                                          @"695755709", @"685145706", @"569496010", @"7963306",
                                          nil];
             
             
             // Of course, not all of our suggested friends will have iPhones or iPads - we need to filter them down
             NSMutableArray *validSuggestedFriends = [[NSMutableArray alloc] init];
             
             // So, we loop through each suggested friend
             for (NSString* suggestedFriend in suggestedFriends)
             {
                 // If they are on our device filtered list, we know they have an iOS device
                 if ([deviceFilteredFriends containsObject:suggestedFriend])
                 {
                     // So we can call them valid
                     [validSuggestedFriends addObject:suggestedFriend];
                 }
             }
             
             // If at least one of our suggested friends had an iOS device...
             if ([deviceFilteredFriends count] > 0)
             {
                 // We add them to the suggest friend param of the dialog
                 NSString *selectIDsStr = [validSuggestedFriends componentsJoinedByString:@","];
                 [params setObject:selectIDsStr forKey:@"suggestions"];
             }
             
             if (ms_friendCache == NULL) {
                 ms_friendCache = [[FBFrictionlessRecipientCache alloc] init];
             }
             
             [FBWebDialogs presentRequestsDialogModallyWithSession:nil
                                                           message:[NSString stringWithFormat:@"I just smashed %d friends! Can you beat it?", nScore]
                                                             title:nil
                                                        parameters:params
                                                           handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                               if (error) {
                                                                   NSLog(@"Error sending request.");
                                                               } else {
                                                                   if (result == FBWebDialogResultDialogNotCompleted) {
                                                                       NSLog(@"User canceled request.");
                                                                   } else {
                                                                       NSLog(@"Request Sent.");
                                                                   }
                                                               }}
                                                       friendCache:ms_friendCache];
             
         }
     }];
}

-(void) SendFriendRequest:(id) target selector:(SEL)callback
{
    [[FBRequest requestForGraphPath:@"me/friends?fields=name,scores,photos,achievements"]
     startWithCompletionHandler:
     ^(FBRequestConnection *connection,
       NSDictionary *result,
       NSError *error)
     {
         // If we received a result with no errors...
         if (!error && result)
         {
             [target performSelector:callback withObject:result];
         }
     }];
}

-(void) SendBrag:(int) nScore
{
    // This function will invoke the Feed Dialog to post to a user's Timeline and News Feed
    // It will attemnt to use the Facebook Native Share dialog
    // If that's not supported we'll fall back to the web based dialog.
    NSString *linkURL = [NSString stringWithFormat:@"https://www.friendsmash.com/challenge_brag_%llu", self.ms_uPlayerFBID];
    NSString *pictureURL = @"http://www.friendsmash.com/images/logo_large.jpg";
    
    // Prepare the native share dialog parameters
    FBShareDialogParams *shareParams = [[FBShareDialogParams alloc] init];
    shareParams.link = [NSURL URLWithString:linkURL];
    shareParams.name = @"Checkout my Friend Smash greatness!";
    shareParams.caption= @"Come smash me back!";
    shareParams.picture= [NSURL URLWithString:pictureURL];
    shareParams.description =
    [NSString stringWithFormat:@"I just smashed %d friends! Can you beat my score?", nScore];
    
    if ([FBDialogs canPresentShareDialogWithParams:shareParams]){
        
        [FBDialogs presentShareDialogWithParams:shareParams
                                    clientState:nil
                                        handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                            if(error) {
                                                NSLog(@"Error publishing story.");
                                            } else if (results[@"completionGesture"] && [results[@"completionGesture"] isEqualToString:@"cancel"]) {
                                                NSLog(@"User canceled story publishing.");
                                            } else {
                                                NSLog(@"Story published.");
                                            }
                                        }];
        
    } else {
        
        // Prepare the web dialog parameters
        NSDictionary *params = @{
                                 @"name" : shareParams.name,
                                 @"caption" : shareParams.caption,
                                 @"description" : shareParams.description,
                                 @"picture" : pictureURL,
                                 @"link" : linkURL
                                 };
        
        // Invoke the dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:
         ^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
             if (error) {
                 NSLog(@"Error publishing story.");
             } else {
                 if (result == FBWebDialogResultDialogNotCompleted) {
                     NSLog(@"User canceled story publishing.");
                 } else {
                     NSLog(@"Story published.");
                 }
             }}];
    }
}

-(void) realSendScore:(NSDictionary*) dic
{
    int nNewScore = [[dic objectForKey:@"score"] intValue];
    int nNewLevel = [[dic objectForKey:@"level"] intValue];
    
    int nScore = nNewLevel << 24 | nNewScore;
    
    NSMutableDictionary* params =   [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     [NSString stringWithFormat:@"%d", nScore], @"score",
                                     nil];
    
    NSLog(@"Fetching current score");
    
    // Get the score, and only send the updated score if it's highter
    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%llu/scores", self.ms_uPlayerFBID] parameters:params HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        
        if (result && !error) {
            
            NSLog(@"Get Score is %@", result);
            
            NSArray* data = [result objectForKey:@"data"];
            int nCurrentScore;
            
            if (data == nil || [data count] == 0)
                nCurrentScore = 0;
            else
                nCurrentScore = [[[data objectAtIndex:0] objectForKey:@"score"] intValue];
            
            int nCurLevel = (nCurrentScore >> 24) & 0xFF;
            int nCurScore = (nCurrentScore & 0xFFFFFF);
            
            NSLog(@"Current score is %d", nCurrentScore);
            
            if ((nNewLevel > nCurLevel) || (nNewScore > nCurScore)) {
                int finalScore = MAX(nNewScore, nCurScore);
                int finalLevel = MAX(nNewLevel, nCurLevel);
                
                int forPub = finalLevel << 24 | finalScore;
                NSMutableDictionary* params =   [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 [NSString stringWithFormat:@"%d", forPub], @"score",
                                                 nil];
                [self performSelector:@selector(publishScore:) withObject:params afterDelay:0.1f];
            }
            else {
                NSLog(@"Existing score is higher - not posting new score");
            }
        }
    }];
}

-(void) publishScore:(NSDictionary*) params
{
    NSLog(@"Posting new score of %@", params);
    NSString* strPath = [NSString stringWithFormat:@"%llu/scores", self.ms_uPlayerFBID];
    [FBRequestConnection startWithGraphPath:strPath parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        
        NSLog(@"Score Post result: %@", result);
        NSLog(@"Score posted with error : %@", error);
    }];
}

-(void) SendScore:(int) nScore level:(int) nLevel
{
    NSMutableDictionary* dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                [NSString stringWithFormat:@"%d", nLevel], @"level",
                                [NSString stringWithFormat:@"%d", nScore], @"score",
                                nil
                                ];
    
    // Make sure we have write permissions
    [self RequestWritePermissionsWithTarget:self callback:@selector(realSendScore:) param:dic];
    
}

-(void) SendAchievement:(eGameAchievements) achievement
{
    // Make sure we have write permissions
    //    RequestWritePermissions();
    
    NSArray *achievementURLs = [NSArray arrayWithObjects:   @"http://www.friendsmash.com/opengraph/achievement_50.html",
                                @"http://www.friendsmash.com/opengraph/achievement_100.html",
                                @"http://www.friendsmash.com/opengraph/achievement_150.html",
                                @"http://www.friendsmash.com/opengraph/achievement_200.html",
                                @"http://www.friendsmash.com/opengraph/achievement_x3.html",
                                nil];
    
    NSMutableDictionary* params =   [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     [NSString stringWithFormat:@"%@", [achievementURLs objectAtIndex:achievement]], @"achievement",
                                     nil];
    
    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%llu/achievements", self.ms_uPlayerFBID] parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
    }];
}

-(void) GetScores
{
    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%llu/scores?fields=score,user", kuFBAppID] parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        
        if (result && !error)
        {
            NSArray* dic = [result objectForKey:@"data"];
            
            [self.delegate didGetScores:dic];
        }
        
    }];
}

-(void) SendOG:(const u64) uFriendID
{
    FBRequest* newAction = [[FBRequest alloc]initForPostWithSession:[FBSession activeSession] graphPath:[NSString stringWithFormat:@"me/friendsmashsample:smash?profile=%llu", uFriendID] graphObject:nil];
    
    FBRequestConnection* conn = [[FBRequestConnection alloc] init];
    
    [conn addRequest:newAction completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if(error) {
            NSLog(@"Sending OG Story Failed: %@", result[@"id"]);
            return;
        }
        
        NSLog(@"OG action ID: %@", result[@"id"]);
    }];
    [conn start];
}

-(void) RequestWritePermissionsWithTarget:(id) target callback:(SEL) selector param:(id) param
{
    // We need to request write permissions from Facebook
//    static bool bHaveRequestedPublishPermissions = false;
    
    NSArray* arr = [[FBSession activeSession] permissions];
    
    bool bHavePublishPermissions = false;
    for (NSString* str in arr) {
        if ([str isEqualToString:@"publish_actions"])
        {
            bHavePublishPermissions = true;
            break;
        }
    }

    if (bHavePublishPermissions)
    {
        [target performSelector:selector withObject:param];
    }
    else
    {
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"publish_actions", nil];
        
        [[FBSession activeSession] requestNewPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceFriends completionHandler:^(FBSession *session, NSError *error) {
            NSLog(@"Reauthorized with publish permissions.");
            [target performSelector:selector withObject:param];
        }];
    }
}

-(void) requestPhotoForId:(NSString*) fbid
{
    NSString *resourceAddress = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=128&height=128", fbid];
    NSLog(@"requested resource for %@", resourceAddress);
    
    NSURL *url = [NSURL URLWithString:resourceAddress];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];

    [NSURLConnection sendAsynchronousRequest:urlRequest queue:fbQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSMutableDictionary* dic = [[NSMutableDictionary dictionaryWithObjectsAndKeys: fbid, @"fbid", data, @"data", nil] retain];
        [self.delegate receivedProfilePicture:dic];
    }];
}

-(void) checkIncommingRequests:(id)target selector:(SEL)Callback
{
    NSLog(@"Sending apprequests");
    [FBRequestConnection startWithGraphPath:@"/me/apprequests"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                              /* handle the result */

                              if (!error)
                              {
                                  NSLog(@"AppRequest Data = %@", result);
                                  
                                  [target performSelector:Callback withObject:result];
                              }
                              else
                              {
                                  NSLog(@"received apprequests with error %@", error);
                              }
                          }];
}

- (void) notificationClear:(NSString *)requestid target:(id) target selector:(SEL) Callback {
    // Delete the request notification
    [FBRequestConnection startWithGraphPath:requestid
                                 parameters:nil
                                 HTTPMethod:@"DELETE"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error) {
                              if (!error) {
                                  [target performSelector:Callback withObject:requestid];
                              }
                          }];
}

- (void) askLiveToFriends {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:@{
                                             @"social_karma": @"5",
                                             @"badge_of_awesomeness": @"1"}
                        options:0
                        error:&error];
    if (!jsonData) {
        NSLog(@"JSON error: %@", error);
        return;
    }
    
    NSString *giftStr = [[NSString alloc]
                         initWithData:jsonData
                         encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary* params = [@{@"data" : giftStr} mutableCopy];
    
    // Display the requests dialog
    [FBWebDialogs
     presentRequestsDialogModallyWithSession:nil
     message:@"I'm out of lives. Can you send me one?"
     title:nil
     parameters:params
     handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
         if (error) {
             // Error launching the dialog or sending the request.
             NSLog(@"Error sending request.");
         } else {
             if (result == FBWebDialogResultDialogNotCompleted) {
                 // User clicked the "x" icon
                 NSLog(@"User canceled request.");
             } else {
                 // Handle the send request callback
                 NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                 if (![urlParams valueForKey:@"request"]) {
                     // User clicked the Cancel button
                     NSLog(@"User canceled request.");
                 } else {
                     // User clicked the Send button
                     NSString *requestID = [urlParams valueForKey:@"request"];
                     NSLog(@"Request ID: %@", requestID);
                 }
             }
         }
     }];
}

-(void) giveLive:(NSString*) requestId fbid:(NSString*) fbid target:(id) target selector:(SEL) Callback {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:@{
                                             @"social_karma": @"5",
                                             @"badge_of_awesomeness": @"2"}
                        options:0
                        error:&error];
    if (!jsonData) {
        NSLog(@"JSON error: %@", error);
        return;
    }
    
    NSString *giftStr = [[NSString alloc]
                         initWithData:jsonData
                         encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary* params = [@{@"data" : giftStr} mutableCopy];
    
//    params[@"suggestions"] = fbid;
    params[@"to"] = fbid;
    
    // Display the requests dialog
    [FBWebDialogs
     presentRequestsDialogModallyWithSession:nil
     message:@"Here is a heart. Have a great day!"
     title:@"Response!!!"
     parameters:params
     handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
         if (error) {
             // Error launching the dialog or sending the request.
             NSLog(@"Error sending request.");
         } else {
             if (result == FBWebDialogResultDialogNotCompleted) {
                 // User clicked the "x" icon
                 NSLog(@"User canceled request.");
             } else {
                 // Handle the send request callback
                 NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                 if (![urlParams valueForKey:@"request"]) {
                     // User clicked the Cancel button
                     NSLog(@"User canceled request.");
                 } else {
                     // User clicked the Send button
                     NSString *requestID = [urlParams valueForKey:@"request"];
                     NSLog(@"Request ID: %@", requestID);
                     [self notificationClear:requestId target:target selector:Callback];
                 }
             }
         }
     }];
}

-(void) collectLive:(NSString *)requestId target:(id) target selector:(SEL) Callback
{
    [self notificationClear:requestId target:target selector:Callback];
}

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}


@end
//
//  FBManager.mm
//  friendSmasher
//
//  Created by Chen on 30/01/2014.
//
//

#include "FBManager.h"
#include "FacebookController.h"

FBManager* FBManager::_instance = NULL;

FBManager* FBManager::sharedInstance()
{
    if (_instance == NULL)
        _instance = new FBManager();
    return _instance;
}
static void didProcessFriends(NSDictionary* dic);
static void didFetchUserDetails(bool bSuccess);

static void didLogIn(bool bLoggedIn) {
    NSLog(@"didLogIn %d", bLoggedIn);
    FBManager::sharedInstance()->DidChangeFBLoginState(bLoggedIn);

    if (bLoggedIn)
    {
        FacebookController::FetchUserDetails(didFetchUserDetails);
//        FBManager::sharedInstance()->reqFriendList();
        
//        FBManager::sharedInstance()->sendScore(2, 100);
//        FBManager::sharedInstance()->sendScore(3, 100);
    }
}

static void didLogout(bool bLoggedOut) {
    FBManager::sharedInstance()->DidChangeFBLoginState(!bLoggedOut);
}

static void didFetchUserDetails(bool bSuccess) {
    NSLog(@"Fetched User Details");
    FBManager::sharedInstance()->DidFetchFBUserInfo(bSuccess);
}

static void didProcessFBURL(NSString* challengeName, NSString* challengeFBID) {
    FBManager::sharedInstance()->DidProcessIncomingURL(challengeName, challengeFBID);
}

static void didProcessFriends(NSDictionary* dic) {
    NSEnumerator* en = [dic keyEnumerator];
    for (NSString* key in en)
    {
        NSObject* obj = [dic objectForKey:key];
        NSLog(@"%@, %@", key, obj);
    }
}

FBManager::FBManager()
{
    
}


bool FBManager::isLoggedIn()
{
    return FacebookController::IsLoggedIn();
}

void FBManager::prepare()
{
    NSLog(@"Prepare");
    if (!FacebookController::IsLoggedIn()) {
        FacebookController::CreateNewSession();
        FacebookController::OpenSession(didLogIn);
    }
    else {
        DidChangeFBLoginState(true);
    }
}

void FBManager::login()
{
    NSLog(@"Login");
    FacebookController::Login(didLogIn);
}

void FBManager::sendScore(int nLevel, int nScore)
{
    FacebookController::SendScore(nLevel, nScore);
}

void FBManager::sendAchievement(int nLevel)
{
}

void FBManager::reqFriendList()
{
    FacebookController::SendFriendRequest(didProcessFriends);
}


const TUserList& FBManager::getFriendList()
{
    return userList;
}

void FBManager::DidChangeFBLoginState(bool bState)
{
    
}

void FBManager::DidFetchFBUserInfo(bool bSuccess)
{
    
}

void FBManager::DidProcessIncomingURL(NSString* challengeName, NSString* challengeFBID)
{
    
}

void FBManager::ProcessIncomingURL(NSURL* url)
{
    FacebookController::ProcessIncomingFeed(url, didProcessFBURL);
}

//
//  NTWDefinitions.h
//  NetTest
//
//  Created by netnod on 23/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#ifdef DEBUG
#define DLog(...) NSLog(@"%s:%d %@", __PRETTY_FUNCTION__, __LINE__, [NSString stringWithFormat:__VA_ARGS__])
#define ALog(...) [[NSAssertionHandler currentHandler] handleFailureInFunction:[NSString stringWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] file:[NSString stringWithCString:__FILE__ encoding:NSUTF8StringEncoding] lineNumber:__LINE__ description:__VA_ARGS__]
#else
#define DLog(...) do { } while (0)
#ifndef NS_BLOCK_ASSERTIONS
#define NS_BLOCK_ASSERTIONS
#endif
#define ALog(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#endif


// From: http://orangejuiceliberationfront.com/safe-key-value-coding/
#if DEBUG
#define PROPERTY(propName)    NSStringFromSelector(@selector(propName))
#else
#define PROPERTY(propName)    @#propName
#endif

// Used for optimization around if statements
#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

#define NTWReturnRA(object) return( [[object retain] autorelease] )
#define NTWAllocIA(aClass) [[[aClass alloc] init] autorelease]

#define MINIMUM_DELAY_BETWEEN_NAGS				60 //in seconds
#define DELAY_BEFORE_SHOWING_NAG_SCREEN         4.0f

/// ***********************    NTWStore requires the following frameworks:

//SystemConfiguration.framework
//MobileCoreServices.framework
//libz.1.2.5.dylib
//CFNetwork.framework
//QuartzCore.framework
//UIKit.framework
//CoreGraphics.framework

//add "-ObjC" and "-all_load" to "Other Linker Flags" in their project.


//include #import "NTWDefinitions.h" inside the project.pch file


///*********************************************   APP SETTINGS  ************************************************

//app configuration - marketing DB location --- CHANGE THIS IF AND ONLY IF THE NETWORKING SERVER IS DIFFERENT !!!!
#define appInfoPlistURL         @"http://flopstudios.com/marketing/output/data.php"
#define appRemoteMediaURL       @"http://flopstudios.com/marketing/media/"
//app configuration - marketing DB location --- CHANGE THIS IF AND ONLY IF THE NETWORKING SERVER IS DIFFERENT !!!!



// SETUP AND DOUBLE CHECK ALL PARAMETERS BEFORE SUBMITTING TO APP STORE !!!!!!

#define FLURRY_API_KEY                          @"XVKY7XT4DKXMKGJ36ZKH"
#define RATE_THE_APP_ID                         @"686300836" // this is the ApplicationID from iTunes
//#define FLURRY_API_KEY                          @"6MUSIL34CG1GGDNC18R7"
//#define RATE_THE_APP_ID                         @"465887678" // this is the ApplicationID from iTunes

//#define FLURRY_API_KEY                          @"6MUSIL34CG1GGDNC18R7"
//#define RATE_THE_APP_ID                         @"444304133" // this is the ApplicationID from iTunes
//#define FLURRY_API_KEY                          @"YH2TFALEQRFL6F5IHNBN"
//#define RATE_THE_APP_ID                         @"646962708" // this is the ApplicationID from iTunes
///*********************************************   END APP SETTINGS  ********************************************

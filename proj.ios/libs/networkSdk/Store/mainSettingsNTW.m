//
//  mainSettingsNTW.m
//  NetworkTest
//
//  Created by netnod on 10/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "mainSettingsNTW.h"

@implementation mainSettingsNTW
- (id)init
{
	self = [super init];
	
	return self;
}


-(NSString *)valInfoPlistURL{

    return appInfoPlistURL;
}

-(NSString *)valRemoteMediaURL{

    return appRemoteMediaURL;
}

-(int)valMINIMUM_DELAY_BETWEEN_NAGS{
    
    return MINIMUM_DELAY_BETWEEN_NAGS;
}

-(float)valDELAY_BEFORE_SHOWING_NAG_SCREEN{
    
    return DELAY_BEFORE_SHOWING_NAG_SCREEN;
}

-(NSString *)valFLURRY_API_KEY{
    
    return FLURRY_API_KEY;
}

-(NSString *)valRATE_THE_APP_ID{
    
    return RATE_THE_APP_ID;
}

               
-(BOOL)isaniPad{
    BOOL isiPad = NO;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        isiPad = YES;
    }
    return isiPad;
}


- (void)dealloc
{
	[super dealloc];
}
@end

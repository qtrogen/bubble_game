//http://www.johnwordsworth.com/2010/04/iphone-code-snippet-the-singleton-pattern/

#import <Foundation/Foundation.h>
#import  "NTWModalPanel.h"
#import "NTWBlackThemeModalPanel.h"


@class NTWGetAppINFO;

@protocol NTWGetAppINFO
@optional
- (void) didFailShowMorePage:(NSString *)Error;
@end

@interface NTWGetAppINFO : NSObject<NTWModalPanelDelegate> {
    NSObject<NTWGetAppINFO> * delegate;

    int numberOfDownloadAttempts;
    NSMutableDictionary *nagScreenData;    
    NSMutableArray *imageLinksData;
    NSMutableArray *bannerListData;    
    NSString *pathToAppTempDirectory;
    NSString *infopageType;
    NSString *nagType;
    
    NSString *morePageTitle;
    NSString *morePageColor;
    
    NSString *POPupMorePageEnabled;
    NSString *POPupMorePageDelay;
    NSString *POPupMorePageRepeatInterval;

    BOOL marketingInfoStatusReady;
    
    BOOL isAniPad;    
    
    BOOL infoDataTimeStampCompareStatus; // used to indicate is there change in the downloaded .plist
    int numberOfNagScreenForDOWNLOADFromDownloadQueue;
    int numberOfNagScreenImagesProcessedFromDownloadQueue;
    
    int numberOfBadgesImagesProcessedFromDownloadQueue;
    int numberOfBadgesImagesForDOWNLOADFromDownloadQueue;

    int numberOfBannerImagesForDOWNLOADFromDownloadQueue;
    int numberOfBannerImagesProcessedFromDownloadQueue;

    int numberOfIconsImagesForDOWNLOADFromDownloadQueue;
    int numberOfIconsImagesProcessedFromDownloadQueue;
    
    //////////////////////////*************************************rating and nag ******************************************    
	NSURL         * nagITunesStoreURL;
    NSString      * nagRefreshInterval;
    //////////////////////////*************************************rating and nag ******************************************
    
    NTWBlackThemeModalPanel *modalPanel;
}

//////////////////////////*************************************rating and nag ******************************************    
@property (nonatomic, retain)   NSURL         * nagITunesStoreURL;
@property (nonatomic, retain)   NSString      * nagRefreshInterval;
//////////////////////////*************************************rating and nag ******************************************

@property (nonatomic, assign)   NSObject<NTWGetAppINFO> * delegate;

@property (nonatomic, assign) NSMutableDictionary *nagScreenData;    
@property (nonatomic, assign) NSMutableArray *imageLinksData;   
@property (nonatomic, assign) NSMutableArray *bannerListData; 
@property (nonatomic, retain) NSString *pathToAppTempDirectory;

@property (nonatomic, copy) NSString *morePageTitle;
@property (nonatomic, copy) NSString *morePageColor;

@property (nonatomic, copy) NSString *POPupMorePageEnabled;
@property (nonatomic, copy) NSString *POPupMorePageDelay;
@property (nonatomic, copy) NSString *POPupMorePageRepeatInterval;

+ (id)sharedInstance;
-(void)downloadAndSaveApplicationInfoPlist:(NSString*)InfoPlist;
- (BOOL) compareappMarketingInfoPlists_OldvsNew; // returns YES if there is an update on the app info
-(void)downloadTheNewMediaDataFromTheServer;

-(void)showTHEStore;
- (void) showNTWTEXTStore;
- (void) showNTWGRAPHICALStore;
- (void) showNTWGRAPHICALNag;
- (void) showNTWNoInfoNAG;

-(void)showPOPupMorePage; // new more page

-(BOOL) checkIsThereNewInfoDataSoIcanShowTheBadge;

- (void) showNTWPanelBlackTheme:(NSString *)panelTitle;
- (void)didCloseModalPanel:(NTWModalPanel *)modalPanel;
@end
//
//  NTWNagStoreViewController.h
//  NTWStoreKit
//
//  Created by netnod on 31/10/11.
//  Copyright (c) 2011 Anystone Technologies, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NTWNagStoreViewController : UIViewController{
        BOOL statusbarPrevStatus;
}

@property (nonatomic, retain) IBOutlet UIButton *nagImage;
- (IBAction)closeNag;
- (IBAction)openNagURL;
@end

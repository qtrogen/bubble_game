


#import <UIKit/UIKit.h>

@protocol NTWStoreViewControllerDelegate
- (void)astStoreViewControllerDidFinish:(UIViewController *)controller;
@end


@interface NTWStoreViewController : UIViewController
    <
    UITableViewDataSource, 
    UITableViewDelegate
    >
{
    UIView *tableContainerView_;
    UITableView *tableView_;
    UITableViewCell *storeCell_;
  
   
    BOOL isAniPad;
    
    BOOL statusbarPrevStatus;    
}

@property (retain) IBOutlet UIView *tableContainerView;
@property (retain) IBOutlet UITableView *tableView;
@property (retain) IBOutlet UITableViewCell *storeCell;

@property (nonatomic,retain) UIColor *cellBackgroundColor1;
@property (nonatomic,retain) UIColor *cellBackgroundColor2;

@end



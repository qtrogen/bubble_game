

#import <UIKit/UIKit.h>

typedef enum
{
    NTWStoreViewControllerTableViewCellTagImageView = 1,
    NTWStoreViewControllerTableViewCellTagTitleLabel = 2,
    NTWStoreViewControllerTableViewCellTagDescriptionLabel = 3,
    NTWStoreViewControllerTableViewCellTagExtraInfoLabel = 4,
    NTWStoreViewControllerTableViewCellTagPriceLabel = 5,
    NTWStoreViewControllerTableViewCellTagTopLineView = 6,
    NTWStoreViewControllerTableViewCellTagBottomLineView = 7,
    NTWStoreViewControllerTableViewCellTagDropShadowView = 8,
    NTWStoreViewControllerTableViewCellTagPurchaseButton = 9
} NTWStoreViewControllerTableViewCellTags;


void updateCellBackgrounds(UITableViewCell* cell, NSIndexPath *indexPath, UIColor *cellBackgroundColor1, UIColor *cellBackgroundColor2);
void setLabelForExpiresDate(NSDate *expiresDate, UILabel *expiresLabel, int isPurchased);
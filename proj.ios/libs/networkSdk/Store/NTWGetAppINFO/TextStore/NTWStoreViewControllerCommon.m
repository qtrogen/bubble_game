

#include "NTWStoreViewControllerCommon.h"

void updateCellBackgrounds(UITableViewCell* cell, NSIndexPath *indexPath, UIColor *cellBackgroundColor1, UIColor *cellBackgroundColor2)
{
    UIView *backgroundView = cell.backgroundView;
    
    if((indexPath.row % 2) == 0 )
    {
        backgroundView.backgroundColor = cellBackgroundColor1;
    }
    else
    {
        backgroundView.backgroundColor = cellBackgroundColor2;
    }
    
    
    if(( backgroundView.frame.size.height != 0.0 ) &&
       ( backgroundView.frame.size.width != 0.0 ))
    {
        UIView *topLineView = [backgroundView viewWithTag:NTWStoreViewControllerTableViewCellTagTopLineView];
        UIView *bottomLineView = [backgroundView viewWithTag:NTWStoreViewControllerTableViewCellTagBottomLineView];
        
        if( nil == topLineView )
        {   
            CGRect frame = backgroundView.frame;
            frame.size.height = 1;
            
            topLineView = [[[UIView alloc] initWithFrame:frame] autorelease];
            topLineView.tag = NTWStoreViewControllerTableViewCellTagTopLineView;
            topLineView.backgroundColor = [UIColor whiteColor];
            
            topLineView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
            UIViewAutoresizingFlexibleTopMargin |
            UIViewAutoresizingFlexibleWidth;
            
            [backgroundView addSubview:topLineView];
        }
        
        if( nil == bottomLineView )
        {   
            CGRect frame = backgroundView.frame;
            frame.origin.y = frame.size.height - 1.0;
            frame.size.height = 1.0;
            
            bottomLineView = [[[UIView alloc] initWithFrame:frame] autorelease];
            bottomLineView.tag = NTWStoreViewControllerTableViewCellTagBottomLineView;
            bottomLineView.backgroundColor = [UIColor blackColor];
            
            bottomLineView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
            UIViewAutoresizingFlexibleTopMargin |
            UIViewAutoresizingFlexibleWidth;
            
            [backgroundView addSubview:bottomLineView];
        }
        
        topLineView.alpha = 0.3;
        bottomLineView.alpha = 0.3;
        
        if( indexPath.row == 0 )
        {
            topLineView.alpha = 0.0;
        }
    }
}


void setLabelForExpiresDate(NSDate *expiresDate, UILabel *expiresLabel, int isPurchased)
{
    NSString *expiryDateAsString = [NSDateFormatter localizedStringFromDate:expiresDate 
                                                                  dateStyle:kCFDateFormatterMediumStyle 
                                                                  timeStyle:kCFDateFormatterShortStyle];
    NSString *expiresString = nil;
    
    expiresLabel.textColor = [UIColor blackColor];
    
    if( nil == expiryDateAsString )
    {
        expiresLabel.text = NSLocalizedString(@"Not Subscribed", nil);
        return;
    }
    
    if( isPurchased )
    {
        expiresString = NSLocalizedString(@"Expires: ", nil);
    }
    else
    {
        expiresLabel.textColor = [UIColor redColor];
        expiresString = NSLocalizedString(@"Expired: ", nil);
    }
    
    expiresLabel.text = [NSString stringWithFormat:@"%@%@", expiresString, expiryDateAsString];
}


#import <QuartzCore/QuartzCore.h>

#import "NTWStoreViewController.h"
#import "NTWStoreViewControllerCommon.h"
#import "NTWGetAppINFO.h"

enum NTWStoreViewControllerSections 
{
    NTWStoreViewControllerSectionButtons = 0,
    NTWStoreViewControllerSectionConsumables,
    NTWStoreViewControllerSectionAutoRenewables,
    NTWStoreViewControllerSectionNonconsumables,
    NTWStoreViewControllerSectionMax
};

enum NTWStoreViewControllerButtonsRows 
{
    NTWStoreViewControllerButtonsRowsRestore = 0,
    //    NTWStoreViewControllerButtonsRowsReceiveVoucher,
    NTWStoreViewControllerButtonsRowsMax
};




@implementation NTWStoreViewController

#pragma mark Synthesis

@synthesize tableContainerView = tableContainerView_;
@synthesize tableView = tableView_;
@synthesize storeCell = storeCell_;
@synthesize cellBackgroundColor1 = cellBackgroundColor1_;
@synthesize cellBackgroundColor2 = cellBackgroundColor2_;




- (UIColor*)cellBackgroundColor1
{
    if( nil == cellBackgroundColor1_ )
    {
        self.cellBackgroundColor1 = [UIColor lightGrayColor];
    }
    
    NTWReturnRA(cellBackgroundColor1_);
}

- (UIColor*)cellBackgroundColor2
{
    if( nil == cellBackgroundColor2_ )
    {
        self.cellBackgroundColor2 = [UIColor colorWithWhite:0.6 alpha:1.0];
    }
    
    NTWReturnRA(cellBackgroundColor2_);
    
}



#pragma mark - Table View Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NTWGetAppINFO* sharedNTWGetAppINFO = [NTWGetAppINFO sharedInstance];
    int numberOfLinks = [sharedNTWGetAppINFO.imageLinksData count];
   
    return numberOfLinks;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{    
    updateCellBackgrounds(cell, indexPath, self.cellBackgroundColor1, self.cellBackgroundColor2);
}


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath 
{    
    NTWGetAppINFO* sharedNTWGetAppINFO = [NTWGetAppINFO sharedInstance];
    NSMutableDictionary *appDataToDisplay = [[[NSMutableDictionary alloc] init] autorelease];
    
    UIImageView *imageView = (UIImageView*) [cell viewWithTag:NTWStoreViewControllerTableViewCellTagImageView];
    UILabel *title = (UILabel*) [cell viewWithTag:NTWStoreViewControllerTableViewCellTagTitleLabel];
    UILabel *description = (UILabel*) [cell viewWithTag:NTWStoreViewControllerTableViewCellTagDescriptionLabel];
    UILabel *extraInfo = (UILabel*) [cell viewWithTag:NTWStoreViewControllerTableViewCellTagExtraInfoLabel];
    UILabel *price = (UILabel*) [cell viewWithTag:NTWStoreViewControllerTableViewCellTagPriceLabel];
    
    description.textColor = [UIColor blackColor];
   
    appDataToDisplay = [sharedNTWGetAppINFO.imageLinksData objectAtIndex:indexPath.row];
    
        
    title.text = [appDataToDisplay valueForKey:@"Name"];
    extraInfo.text = nil;
    price.text = nil;
    description.text = [appDataToDisplay valueForKey:@"Description"];
    
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    imageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",sharedNTWGetAppINFO.pathToAppTempDirectory,[appDataToDisplay valueForKey:@"Filename"]]];
    DLog(@"filepath:%@",[NSString stringWithFormat:@"%@/%@",sharedNTWGetAppINFO.pathToAppTempDirectory,[appDataToDisplay valueForKey:@"Filename"]]);
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NTWStoreTableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        [[NSBundle mainBundle] loadNibNamed:@"NTWStoreTableViewCell" owner:self options:nil];
        cell = storeCell_;
        self.storeCell = nil;
        cell.backgroundView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
        
        cell.backgroundView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
        UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin |
        UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        // Setup rounded corners
        UIImageView *imageView = (UIImageView*) [cell viewWithTag:NTWStoreViewControllerTableViewCellTagImageView];
        imageView.layer.cornerRadius = 10.0; // Same as the radius that iOS uses
        imageView.layer.masksToBounds = YES;
        
        UIView *dropShadowView = [cell viewWithTag:NTWStoreViewControllerTableViewCellTagDropShadowView];
        dropShadowView.layer.cornerRadius = 10.0;
        dropShadowView.layer.masksToBounds = NO;
        dropShadowView.layer.shadowColor = [[UIColor blackColor] CGColor];
        dropShadowView.layer.shadowOffset = CGSizeMake(0,2);
        dropShadowView.layer.shadowRadius = 1;
        dropShadowView.layer.shadowOpacity = 1;
        dropShadowView.layer.shouldRasterize = YES;
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - Table View Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ( 71.0 );
}
       
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NTWGetAppINFO* sharedNTWGetAppINFO = [NTWGetAppINFO sharedInstance];
   
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[sharedNTWGetAppINFO.imageLinksData objectAtIndex:indexPath.row] valueForKey:@"Link"]]]];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];	    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)astStoreViewControllerDidFinish:(UIViewController *)controller
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)dismissView:(id)sender 
{
    [self dismissModalViewControllerAnimated:YES];
//    [self.delegate astStoreViewControllerDidFinish:self];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
      
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]
                                               initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                               target:self
                                               action:@selector(dismissView:)] autorelease];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //self.storeController.delegate = nil;    
}

-(void)viewDidDisappear:(BOOL)animated{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
    }else{
        if (!statusbarPrevStatus){
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];    
        }
    }
}

// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil 
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) 
    {
        // Custom initialization
        self.title = @"More Amazing Apps";
		isAniPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        }else{
                statusbarPrevStatus = [[UIApplication sharedApplication] isStatusBarHidden];            
            if(![[UIApplication sharedApplication] isStatusBarHidden]){
                [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];

            }     
        }        
    }
    return self;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.tableContainerView = nil;
    self.tableView = nil;
    self.storeCell = nil;
}

#pragma  mark - Memory Management

- (void)dealloc
{
    [tableContainerView_ release];
    tableContainerView_ = nil;
    
    [tableView_ release];
    tableView_ = nil;
    
    [storeCell_ release];
    storeCell_ = nil;
   
    
    [cellBackgroundColor1_ release], cellBackgroundColor1_ = nil;
    [cellBackgroundColor2_ release], cellBackgroundColor2_ = nil;
 
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end

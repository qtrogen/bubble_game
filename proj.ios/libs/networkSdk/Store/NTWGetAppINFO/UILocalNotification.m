//
//  UILocalNotification.m
//  SupplementTracker
//
//  Created by Chris Sheldrick on 6/12/11.
//  Copyright 2011 Chris Sheldrick. All rights reserved.
//

#import "UILocalNotification.h"

@implementation UILocalNotification (incrementBadge)

//+(void)setBadge 
//{
//    NSArray *notifArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
//    int count = [notifArray count];
//    for (int x = 0; x < count; x++) 
//    {
//        if (count > 0 )
//        {
//            UILocalNotification *localNotif = [notifArray objectAtIndex:x];
//            [[UIApplication sharedApplication] cancelLocalNotification:localNotif];
//            localNotif.applicationIconBadgeNumber = x + 1;
//            [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
//            NSLog(@"notif: %@ badge %i", localNotif, localNotif.applicationIconBadgeNumber);
//        }
//    }
//
//}
//@end

+(void)setBadge
{
// clear the badge on the icon
[[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

// first get a copy of all pending notifications (unfortunately you cannot 'modify' a pending notification)
NSArray *pendingNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];

// if there are any pending notifications -> adjust their badge number
if (pendingNotifications.count != 0)
{
    // clear all pending notifications
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    // the for loop will 'restore' the pending notifications, but with corrected badge numbers
    // note : a more advanced method could 'sort' the notifications first !!!
    NSUInteger badgeNbr = 1;
    
    for (UILocalNotification *notification in pendingNotifications)
    {
        // modify the badgeNumber
        notification.applicationIconBadgeNumber = badgeNbr++;
        
        // schedule 'again'
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
}

}
@end
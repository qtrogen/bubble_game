//
//  NTWGraphicalStoreViewController.h
//  NTWStoreKit
//
//  Created by netnod on 26/10/11.
//  Copyright (c) 2011 Anystone Technologies, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NTWbannerRotatorViewController.h"
#import "NTWiconRotatorBottomController.h"
#import "NTWiconRotatorTopController.h"

@interface NTWGraphicalStoreViewController : UIViewController{
    UIView *tableContainerView_;
    NTWbannerRotatorViewController *bannerRotator;
    NTWiconRotatorBottomController *iconBottomRotator; 
    NTWiconRotatorTop *iconTopRotator;    
    BOOL statusbarPrevStatus;
    
    
    NSTimer *timer;
    NSTimer *timerTopIcons;
    NSTimer *timerBotoomIcons;     
}
    @property (retain) IBOutlet UIView *tableContainerView;
@property (nonatomic, retain) IBOutlet NTWbannerRotatorViewController *bannerRotator;
@property (nonatomic, retain) IBOutlet NTWiconRotatorBottomController *iconBottomRotator;
@property (nonatomic, retain) IBOutlet NTWiconRotatorTop *iconTopRotator;
@end

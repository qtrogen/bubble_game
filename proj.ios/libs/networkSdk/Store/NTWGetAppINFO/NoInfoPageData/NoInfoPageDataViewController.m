//
//  NoInfoPageDataViewController.m
//  NTWStoreKit
//
//  Created by netnod on 21/11/11.
//  Copyright (c) 2011 Anystone Technologies, Inc. All rights reserved.
//

#import "NoInfoPageDataViewController.h"

@implementation NoInfoPageDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        }else{
                statusbarPrevStatus = [[UIApplication sharedApplication] isStatusBarHidden];            
            if(![[UIApplication sharedApplication] isStatusBarHidden]){
                [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
            }     
        }        
    }
    return self;
}

- (IBAction)closeNag{
  
    [self dismissModalViewControllerAnimated:YES]; 
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}
-(void)viewDidDisappear:(BOOL)animated{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
    }else{
        if (!statusbarPrevStatus){
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];    
        }
    }
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


@end

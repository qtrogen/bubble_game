//
//  NTWGraphicalStoreViewController.m
//  NTWStoreKit
//
//  Created by netnod on 26/10/11.
//  Copyright (c) 2011 Anystone Technologies, Inc. All rights reserved.
//

#import "NTWGraphicalStoreViewController.h"

@implementation NTWGraphicalStoreViewController
@synthesize tableContainerView = tableContainerView_;
@synthesize bannerRotator,iconBottomRotator, iconTopRotator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        bannerRotator = [[NTWbannerRotatorViewController alloc]init];
        iconBottomRotator = [[NTWiconRotatorBottomController alloc]init];
        iconTopRotator = [[NTWiconRotatorTop alloc]init];
            statusbarPrevStatus = [[UIApplication sharedApplication] isStatusBarHidden];            
        if(![[UIApplication sharedApplication] isStatusBarHidden]){
             [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
        }        

        self.navigationItem.title = @"More Amazing Apps";
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/
-(void)viewDidDisappear:(BOOL)animated{

    if (!statusbarPrevStatus){
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];    
    }
    [bannerRotator.carousel release];
    [iconTopRotator.carousel release];
    [iconBottomRotator.carousel release];
    
 

	[timer invalidate];
	[timer release];    
    
    [timerTopIcons invalidate];
	[timerTopIcons release];    
    
    [timerBotoomIcons invalidate];  
   	[timerBotoomIcons release];    
    
    
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
 		
    bannerRotator.carousel.type = iCarouselTypeLinear;
    iconTopRotator.carousel.type = iCarouselTypeLinear;
    iconBottomRotator.carousel.type = iCarouselTypeLinear;    
    iconTopRotator.carousel.centerItemWhenSelected = NO;
    iconBottomRotator.carousel.centerItemWhenSelected = NO;
    
    timer =  [[NSTimer
                        timerWithTimeInterval:16
                        target:self
                        selector:@selector(stepNextBanner)
                        userInfo:nil
                        repeats:YES
                        ] retain];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];

    timerTopIcons =  [[NSTimer
                        timerWithTimeInterval:13
                        target:self
                        selector:@selector(stepNextTopIcon)
                        userInfo:nil
                        repeats:YES
                        ] retain];
    [[NSRunLoop currentRunLoop] addTimer:timerTopIcons forMode:NSDefaultRunLoopMode];
    
    timerBotoomIcons =  [[NSTimer
                                timerWithTimeInterval:12
                                target:self
                                selector:@selector(stepNextBottomIcon)
                                userInfo:nil
                                repeats:YES
                                ] retain];
    [[NSRunLoop currentRunLoop] addTimer:timerBotoomIcons forMode:NSDefaultRunLoopMode];
    
    
    UIBarButtonItem *myBarButton = [[[UIBarButtonItem alloc]
                                    initWithImage:[UIImage imageNamed:@"NTWclose.png"] 
                                    style:UIBarButtonItemStylePlain target:self action:@selector(dismissView)] autorelease];
    
    self.navigationItem.rightBarButtonItem=myBarButton ;
    
//    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]
//                                               initWithBarButtonSystemItem:UIBarButtonSystemItemDone
//                                               target:self
//                                               action:@selector(dismissView:)] autorelease];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
}

- (void)stepNextBanner
{
    NSInteger numberOfItems = [bannerRotator.carousel numberOfItems];
    NSInteger currentIndex =  [bannerRotator.carousel currentItemIndex];

    if (currentIndex < numberOfItems){
        [bannerRotator.carousel scrollToItemAtIndex:currentIndex+1 animated:YES];
    }else{
        [bannerRotator.carousel scrollToItemAtIndex:0 animated:YES];        
    }

}


- (void)stepNextTopIcon
{
    NSInteger numberOfItems = [iconTopRotator.carousel numberOfItems];
    NSInteger currentIndex =  [iconTopRotator.carousel currentItemIndex];
    
    if (currentIndex ==0){
        [iconTopRotator.carousel scrollToItemAtIndex:numberOfItems-1 animated:YES];
    }else{
        [iconTopRotator.carousel scrollToItemAtIndex:currentIndex-1 animated:YES];    
    }
    
}

- (void)stepNextBottomIcon
{
    
    NSInteger numberOfItems = [iconBottomRotator.carousel numberOfItems];
    NSInteger currentIndex =  [iconBottomRotator.carousel currentItemIndex];
    
    if (currentIndex < numberOfItems){
        [iconBottomRotator.carousel scrollToItemAtIndex:currentIndex+1 animated:YES];
    }else{
        [iconBottomRotator.carousel scrollToItemAtIndex:0 animated:YES];        
    }
    
}


- (void)dismissView 
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

@end

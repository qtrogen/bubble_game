

#import "NTWNagStoreViewController.h"
#import "NTWGetAppINFO.h"
#ifdef FLURRY_API_KEY
#import "Flurry.h"
#endif

@implementation NTWNagStoreViewController
@synthesize nagImage;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        }else{
            statusbarPrevStatus = [[UIApplication sharedApplication] isStatusBarHidden];                        
        if(![[UIApplication sharedApplication] isStatusBarHidden]){
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:YES];
        }     
        }
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewDidDisappear:(BOOL)animated{
    
   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
   }else{
    if (!statusbarPrevStatus){
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];    
    }
   }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    NTWGetAppINFO* sharedNTWGetAppINFO = [NTWGetAppINFO sharedInstance];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            NSString *downloadedNagImage = [sharedNTWGetAppINFO.nagScreenData valueForKey:@"iPadImage"];
            [nagImage setBackgroundImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",sharedNTWGetAppINFO.pathToAppTempDirectory, downloadedNagImage]] forState:UIControlStateNormal];        
    }else{
        if((self.interfaceOrientation == UIDeviceOrientationLandscapeLeft) || (self.interfaceOrientation == UIDeviceOrientationLandscapeRight)){        
            NSString *downloadedNagImage = [sharedNTWGetAppINFO.nagScreenData valueForKey:@"LandscapeImage"];
            [nagImage setBackgroundImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",sharedNTWGetAppINFO.pathToAppTempDirectory, downloadedNagImage]] forState:UIControlStateNormal];        

        }else{
            NSString *downloadedNagImage = [sharedNTWGetAppINFO.nagScreenData valueForKey:@"PortraitImage"];            
            [nagImage setBackgroundImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",sharedNTWGetAppINFO.pathToAppTempDirectory, downloadedNagImage]] forState:UIControlStateNormal];        

        }
            
    }

    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction)closeNag{

#ifdef FLURRY_API_KEY
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // iPad-specific code
        [Flurry logEvent:@"Dismissed GraphicalNag iPad"];
    }else{
        // iPhone-specific code                
        [Flurry logEvent:@"Dismissed GraphicalNag iPhone"];
    }
#endif  
    [self dismissModalViewControllerAnimated:YES]; 
}

- (IBAction)openNagURL{
#ifdef FLURRY_API_KEY
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // iPad-specific code
        [Flurry logEvent:@"Yes on GraphicalNag iPad"];
    }else{
        // iPhone-specific code                
        [Flurry logEvent:@"Yes on GraphicalNag iPhone"];
    }
#endif
    NTWGetAppINFO* sharedNTWGetAppINFO = [NTWGetAppINFO sharedInstance];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[sharedNTWGetAppINFO.nagScreenData objectForKey:@"Link"]]];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    NTWGetAppINFO* sharedNTWGetAppINFO = [NTWGetAppINFO sharedInstance];
	if((self.interfaceOrientation == UIDeviceOrientationLandscapeLeft) || (self.interfaceOrientation == UIDeviceOrientationLandscapeRight)){

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            NSString *downloadedNagImage = [sharedNTWGetAppINFO.nagScreenData valueForKey:@"iPadImage"];
            [nagImage setBackgroundImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",sharedNTWGetAppINFO.pathToAppTempDirectory, downloadedNagImage]] forState:UIControlStateNormal];        

        }else{
            NSString *downloadedNagImage = [sharedNTWGetAppINFO.nagScreenData valueForKey:@"LandscapeImage"];
            [nagImage setBackgroundImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",sharedNTWGetAppINFO.pathToAppTempDirectory, downloadedNagImage]] forState:UIControlStateNormal];        

        }
    }else{

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            NSString *downloadedNagImage = [sharedNTWGetAppINFO.nagScreenData valueForKey:@"iPadImage"];
            [nagImage setBackgroundImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",sharedNTWGetAppINFO.pathToAppTempDirectory, downloadedNagImage]] forState:UIControlStateNormal];        

        }else{
            NSString *downloadedNagImage = [sharedNTWGetAppINFO.nagScreenData valueForKey:@"PortraitImage"];            
            [nagImage setBackgroundImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",sharedNTWGetAppINFO.pathToAppTempDirectory, downloadedNagImage]] forState:UIControlStateNormal];        

            }
    }
        
}

@end

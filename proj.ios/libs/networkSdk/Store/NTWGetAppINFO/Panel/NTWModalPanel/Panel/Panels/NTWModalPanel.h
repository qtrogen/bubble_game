

#import <Foundation/Foundation.h>
#import "NTWRoundedRectView.h"
#import "NTWCustomBadge.h"

// Logging Helpers
//#ifdef UAMODALVIEW_DEBUG
//#define UADebugLog( s, ... ) NSLog( @"<%@:%d> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__,  [NSString stringWithFormat:(s), ##__VA_ARGS__] )
//#else
//#define UADebugLog( s, ... ) 
//#endif

@class NTWModalPanel;

@protocol NTWModalPanelDelegate
@optional
- (void)willShowModalPanel:(NTWModalPanel *)modalPanel;
- (void)didShowModalPanel:(NTWModalPanel *)modalPanel;
- (BOOL)shouldCloseModalPanel:(NTWModalPanel *)modalPanel;
- (void)willCloseModalPanel:(NTWModalPanel *)modalPanel;
- (void)didCloseModalPanel:(NTWModalPanel *)modalPanel;
@end

typedef void (^UAModalDisplayPanelEvent)(NTWModalPanel* panel);
typedef void (^UAModalDisplayPanelAnimationComplete)(BOOL finished);

@interface NTWModalPanel : UIView {	
	NSObject<NTWModalPanelDelegate>	*delegate;
	
    NTWCustomBadge *customBadge1; 
	UIView		*contentContainer;
	UIView		*roundedRect;
	UIButton	*closeButton;
	UIButton	*poweredByButton;    
	UIView		*contentView;
	
	CGPoint		startEndPoint;
	
	CGFloat		outerMargin;
	CGFloat		innerMargin;
	UIColor		*borderColor;
	CGFloat		borderWidth;
	CGFloat		cornerRadius;
	UIColor		*contentColor;
	BOOL		shouldBounce;
	
}

@property (nonatomic, assign) NSObject<NTWModalPanelDelegate>	*delegate;

@property (nonatomic, retain) UIView		*contentContainer;
@property (nonatomic, retain) UIView		*roundedRect;
@property (nonatomic, retain) UIButton		*closeButton;
@property (nonatomic, retain) UIView		*contentView;

// Margin between edge of container frame and panel. Default = 20.0
@property (nonatomic, assign) CGFloat		outerMargin;
// Margin between edge of panel and the content area. Default = 20.0
@property (nonatomic, assign) CGFloat		innerMargin;
// Border color of the panel. Default = [UIColor whiteColor]
@property (nonatomic, retain) UIColor		*borderColor;
// Border width of the panel. Default = 1.5f
@property (nonatomic, assign) CGFloat		borderWidth;
// Corner radius of the panel. Default = 4.0f
@property (nonatomic, assign) CGFloat		cornerRadius;
// Color of the panel itself. Default = [UIColor colorWithWhite:0.0 alpha:0.8]
@property (nonatomic, retain) UIColor		*contentColor;
// Shows the bounce animation. Default = YES
@property (nonatomic, assign) BOOL			shouldBounce;

@property (readwrite, copy)	UAModalDisplayPanelEvent onClosePressed;


- (void)show;
- (void)showFromPoint:(CGPoint)point;
- (void)hide;
- (void)hideWithOnComplete:(UAModalDisplayPanelAnimationComplete)onComplete;
- (void)notificationReceivedCloseMe;

- (CGRect)roundedRectFrame;
- (CGRect)closeButtonFrame;
- (CGRect)contentViewFrame;


- (CGRect)poweredByButtonFrame;

- (UIImage *)radialGradientImage:(CGSize)size rgbHEXvalue:(NSString*)rgbHEXvalue centre:(CGPoint)centre radius:(float)radius;

- (void)setContentColor:(UIColor *)newColor;
@end

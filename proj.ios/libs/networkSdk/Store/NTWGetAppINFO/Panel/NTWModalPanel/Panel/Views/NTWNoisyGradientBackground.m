

#import "NTWNoisyGradientBackground.h"
#import "UIView+NTWNoise.h"

//#import <QuartzCore/QuartzCore.h>

#define NOISE_DEFAULT_OPACITY 0.1

@implementation NTWNoisyGradientBackground

@synthesize noiseOpacity, blendMode;


- (id)initWithFrame:(CGRect)frame style:(UAGradientBackgroundStyle)aStyle color:(CGFloat *)components lineMode:(UAGradientLineMode)lineModes noiseOpacity:(CGFloat)opacity blendMode:(CGBlendMode)mode {
	if ((self = [self initWithFrame:frame style:aStyle color:components lineMode:lineModes])) {
		self.noiseOpacity = opacity;
		self.blendMode = mode;
	}
	return self;
}
- (id)initWithFrame:(CGRect)frame noiseOpacity:(CGFloat)opacity {
	if (self = [self initWithFrame:frame]) {
		self.noiseOpacity = opacity;
	}
	return self;
}
- (id)initWithFrame:(CGRect)frame blendMode:(CGFloat)mode {
	if ((self = [self initWithFrame:frame])) {
		self.blendMode = blendMode;
	}
	return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.noiseOpacity = NOISE_DEFAULT_OPACITY;
		self.blendMode = kCGBlendModeNormal;
    }
    return self;
}



+ (id)gradientWithFrame:(CGRect)frame style:(UAGradientBackgroundStyle)aStyle color:(CGFloat *)components lineMode:(UAGradientLineMode)lineModes noiseOpacity:(CGFloat)opacity blendMode:(CGBlendMode)mode {
	return  [[[NTWNoisyGradientBackground alloc] initWithFrame:frame
														style:aStyle
														color:components
													 lineMode:lineModes
												 noiseOpacity:opacity
													blendMode:mode] autorelease];
}
+ (id)gradientWithFrame:(CGRect)frame noiseOpacity:(CGFloat)noiseOpacity {
	return [[[NTWNoisyGradientBackground alloc] initWithFrame:frame noiseOpacity:noiseOpacity] autorelease];
}
+ (id)gradientWithFrame:(CGRect)frame blendMode:(CGFloat)mode {
	return [[[NTWNoisyGradientBackground alloc] initWithFrame:frame blendMode:mode] autorelease];
}



- (void)drawRect:(CGRect)rect {
	[super drawRect:rect];
	
	[self dbsdrawCGNoiseWithOpacity:self.noiseOpacity blendMode:self.blendMode];
}

@end


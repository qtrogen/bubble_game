
#import <UIKit/UIKit.h>

@interface UIView (NTWNoise)

// Can be used directly on UIView
- (void)dbsapplyNoise;
- (void)dbsapplyNoiseWithOpacity:(CGFloat)opacity atLayerIndex:(NSUInteger) layerIndex;
- (void)dbsapplyNoiseWithOpacity:(CGFloat)opacity;

// Can be invoked from a drawRect() method
- (void)dbsdrawCGNoise;
- (void)dbsdrawCGNoiseWithOpacity:(CGFloat)opacity;
- (void)dbsdrawCGNoiseWithOpacity:(CGFloat)opacity blendMode:(CGBlendMode)blendMode;

@end

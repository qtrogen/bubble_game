

#import "NTWTitledModalPanel.h"
#import <QuartzCore/QuartzCore.h>

#define DEFAULT_TITLE_BAR_HEIGHT	40.0f

@implementation NTWTitledModalPanel

@synthesize titleBarHeight, titleBar, pageControlBar, headerLabel;

@synthesize buttonLeft;
@synthesize buttonRight;


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.titleBar = nil;
    self.pageControlBar = nil;    
	self.headerLabel = nil;
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceivedleftallowedToGoRight) name:@"allowedToGoRight" object:NULL];
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceivedleftnotAllowedToGoRight) name:@"notAllowedToGoRight" object:NULL];
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceivedleftallowedToGoLeft) name:@"allowedToGoLeft" object:NULL];
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceivedleftnotAllowedToGoLeft) name:@"notAllowedToGoLeft" object:NULL];        
        
        
		self.titleBarHeight = DEFAULT_TITLE_BAR_HEIGHT;
		
		CGFloat colors[8] = { 1, 1, 1, 1, 0, 0, 0, 1 };
        CGFloat colors2[8] = { 1, 1, 1, 0, 0, 0, 0, 0 };
		self.titleBar = [NTWNoisyGradientBackground gradientWithFrame:CGRectZero
															   style:UAGradientBackgroundStyleLinear
															   color:colors
															lineMode:UAGradientLineModeTop
														noiseOpacity:0.2
														   blendMode:kCGBlendModeNormal];
        
		self.pageControlBar = [NTWNoisyGradientBackground gradientWithFrame:CGRectZero
                                                                style:UAGradientBackgroundStyleLinear
                                                                color:colors2
                                                             lineMode:UAGradientLineModeBottom
                                                         noiseOpacity:0.2
                                                            blendMode:kCGBlendModeNormal];
		
		[self.roundedRect addSubview:self.titleBar];

        
        self.buttonLeft = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [self.buttonLeft setImage:[UIImage imageNamed:@"goLeft.png"] forState:UIControlStateNormal];
        [self.buttonLeft addTarget:self action:@selector(buttonLeftTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        
        self.buttonRight = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [self.buttonRight setImage:[UIImage imageNamed:@"goRight.png"] forState:UIControlStateNormal];
        [self.buttonRight addTarget:self action:@selector(buttonRightTapped:) forControlEvents:UIControlEventTouchUpInside];

		
		self.headerLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
		self.headerLabel.font = [UIFont systemFontOfSize:24];
		self.headerLabel.backgroundColor = [UIColor clearColor];
		self.headerLabel.textColor = [UIColor whiteColor];
		self.headerLabel.shadowColor = [UIColor blackColor];
		self.headerLabel.shadowOffset = CGSizeMake(0, -1);
		self.headerLabel.textAlignment = UITextAlignmentCenter;
		[self.titleBar addSubview:self.headerLabel];

		
    }
    return self;
}


- (void)notificationReceivedleftallowedToGoRight{

    [self.roundedRect addSubview:self.buttonRight];
}
- (void)notificationReceivedleftnotAllowedToGoRight{
    [self.buttonRight removeFromSuperview];
}

- (void)notificationReceivedleftallowedToGoLeft{
    [self.roundedRect addSubview:self.buttonLeft];
}

- (void)notificationReceivedleftnotAllowedToGoLeft{
    [self.buttonLeft removeFromSuperview];
}


- (void)buttonLeftTapped:(UIButton *)sender
{
      [[NSNotificationCenter defaultCenter] postNotificationName:@"leftButtonTapped" object:nil];
//    [self startScrollTimerWithOffset:-1];
    
}

- (void)buttonRightTapped:(UIButton *)sender
{
     [[NSNotificationCenter defaultCenter] postNotificationName:@"rightButtonTapped" object:nil];
//    [self startScrollTimerWithOffset:1];
    
}
- (CGRect)leftButtonFrame {
	CGRect frame = [self.roundedRect bounds];
//    NSLog(@"SIZE:%f", frame.size.height);
    
	return CGRectMake(frame.origin.x+10,
					  frame.size.height - self.titleBarHeight-5,
					  30,
					  30);
}

- (CGRect)rightButtonFrame {
	CGRect frame = [self.roundedRect bounds];
//    NSLog(@"SIZE:%f", frame.size.height);
	return CGRectMake(frame.size.width-50,
					  frame.size.height - self.titleBarHeight-5,
					  30,
					  30);

}


- (CGRect)titleBarFrame { 
	CGRect frame = [self.roundedRect bounds];
	return CGRectMake(frame.origin.x,
					  frame.origin.y + self.roundedRect.layer.borderWidth,
					  frame.size.width,
					  self.titleBarHeight - self.roundedRect.layer.borderWidth);
}

- (CGRect)pageControlBarFrame {
	CGRect frame = [self.roundedRect bounds];

//        NSLog(@"SIZE:%f", frame.size.height);
    return CGRectMake(frame.origin.x,
					  frame.size.height - self.titleBarHeight-8,
					  frame.size.width,
					  self.titleBarHeight+8 - self.roundedRect.layer.borderWidth);
    
    
    
}

// overriding the subclass to make room for the title bar
- (CGRect)contentViewFrame {
	CGRect titleBarFrame = [self titleBarFrame];
	CGRect roundedRectFrame = [self roundedRectFrame];
	CGFloat y = titleBarFrame.origin.y + titleBarFrame.size.height;
	CGRect rect = CGRectMake(self.outerMargin + self.innerMargin,
							 self.outerMargin + self.innerMargin + y - 30,
							 roundedRectFrame.size.width - 2*self.innerMargin,
							 roundedRectFrame.size.height - y - self.innerMargin);

	
    
    
    CGFloat heightY = roundedRectFrame.size.height - titleBarFrame.size.height - 2*self.innerMargin;
    
if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        heightY = roundedRectFrame.size.height - 2*y - 3*self.innerMargin;
}else{
    if (heightY > 400){
        heightY = roundedRectFrame.size.height - 2*y - 3*self.innerMargin;
    }
}
    
    
    
    NSValue *myValue = [NSValue valueWithCGRect:CGRectMake(self.outerMargin + self.innerMargin,
                                                           self.outerMargin + self.innerMargin + y,
                                                           roundedRectFrame.size.width - 2*self.innerMargin,
                                                           heightY)];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:myValue];
    
    [[NSUserDefaults standardUserDefaults] setValue:data forKey:@"rectangleFrame"];
	[[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    return rect;
}


- (void)layoutSubviews {
	[super layoutSubviews];
	
	self.titleBar.frame = [self titleBarFrame];
	self.headerLabel.frame = self.titleBar.bounds;
    
     [self.pageControlBar removeFromSuperview];
     [self.buttonLeft removeFromSuperview];
     [self.buttonRight removeFromSuperview];

   
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if ((orientation == UIInterfaceOrientationLandscapeLeft) || (orientation == UIInterfaceOrientationLandscapeRight)){
    }else{
        [self.roundedRect addSubview:self.pageControlBar];        
        self.pageControlBar.frame = [self pageControlBarFrame];
    }


    
    self.buttonRight.frame = [self rightButtonFrame];
    self.buttonLeft.frame = [self leftButtonFrame];
}


// Overrides

- (void)showAnimationStarting {
	self.contentView.alpha = 0.0;
	self.titleBar.alpha = 0.0;
    self.pageControlBar.alpha = 0.3;
}

- (void)showAnimationFinished {
//	UADebugLog(@"Fading in content for modalPanel: %@", self);
	[UIView animateWithDuration:0.2
						  delay:0.0
						options:UIViewAnimationCurveEaseIn
					 animations:^{
						 self.contentView.alpha = 1.0;
						 self.titleBar.alpha = 1.0;
						 self.pageControlBar.alpha = 1.0;
					 }
					 completion:nil];
}


@end

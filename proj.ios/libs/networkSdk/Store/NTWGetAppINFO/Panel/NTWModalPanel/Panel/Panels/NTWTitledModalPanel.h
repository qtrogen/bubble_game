

#import <Foundation/Foundation.h>
#import "NTWModalPanel.h"
#import "NTWNoisyGradientBackground.h"	

@interface NTWTitledModalPanel : NTWModalPanel {

	CGFloat						titleBarHeight;
	NTWNoisyGradientBackground	*titleBar;
    NTWNoisyGradientBackground	*pageControlBar;    
	UILabel						*headerLabel;

}

// Height of the title view. Default = 40.0f
@property (nonatomic, assign) CGFloat					titleBarHeight;
// The gradient bacground of the title
@property (nonatomic, retain) NTWNoisyGradientBackground	*titleBar;

// The gradient bacground of the pageControl
@property (nonatomic, retain) NTWNoisyGradientBackground	*pageControlBar;

// The title label
@property (nonatomic, retain) UILabel					*headerLabel;

@property (nonatomic, assign) UIButton *buttonLeft;
@property (nonatomic, assign) UIButton *buttonRight;


- (CGRect)titleBarFrame;
- (CGRect)pageControlBarFrame;

- (CGRect)leftButtonFrame;
- (CGRect)rightButtonFrame;
@end

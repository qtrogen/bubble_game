

#import "NTWTitledModalPanel.h"
#import "NTWLauncherViewDelegate.h"
#import "NTWLauncherData.h"
@class NTWLauncherParentView;
@class NTWLauncherService;
@class NTWLauncherView;

@interface NTWBlackThemeModalPanel : NTWTitledModalPanel <UITableViewDataSource, NTWLauncherViewDelegate> {
	UIView			*v;
	IBOutlet UIView	*viewLoadedFromXib;
    NTWLauncherView *launcherViewLeft;
    NTWLauncherData *launcherDataLeft;
}

@property (nonatomic, retain) IBOutlet UIView *viewLoadedFromXib;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title;
- (IBAction)buttonPressed:(id)sender;
- (NTWLauncherParentView*) launcherParentView;
//- (id) initWithLauncherService:(NTWLauncherService*) inLauncherService;

@property (nonatomic, retain) NTWLauncherService *launcherService;
@property (nonatomic, assign) NTWLauncherView *currentDraggingView;
@end

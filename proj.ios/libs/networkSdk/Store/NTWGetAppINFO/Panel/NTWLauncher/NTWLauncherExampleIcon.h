

#import "NTWLauncherIcon.h"

@interface NTWLauncherExampleIcon : NTWLauncherIcon
@property (nonatomic, retain) UIImage *iconImage;
@property (nonatomic, retain) UIImage *closeImage;
@property (nonatomic, assign) CGRect closeRect;

@end

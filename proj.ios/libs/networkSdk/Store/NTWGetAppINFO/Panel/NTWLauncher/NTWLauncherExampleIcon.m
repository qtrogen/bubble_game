

#import "NTWLauncherExampleIcon.h"
#import "NTWLauncherItem.h"

@interface NTWLauncherExampleIcon(private)
- (void) setIconImageFromIconPath:(NSString *)iconPath;
- (UIImage*) mergeBackgroundImage:(UIImage*) bottomImage withTopImage:(UIImage*) image;
@end

@implementation NTWLauncherExampleIcon
@synthesize iconImage;
@synthesize closeImage;
@synthesize closeRect;

- (BOOL) hitCloseButton:(CGPoint)point {
    return (CGRectContainsPoint(self.closeRect, point));
}

- (void) drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat availableWidth = self.iconImage.size.width;
    
    // Icon
    CGFloat x = floor((self.bounds.size.width - availableWidth) / 2);
    CGFloat y = 10;
    CGRect buttonRect = CGRectMake(x, y, self.iconImage.size.width, self.iconImage.size.height);
    
    
//    //BezierPath
//    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, 100, 100)];  
//    
//    // Create an image context containing the original UIImage.
//    UIGraphicsBeginImageContext(_imgTemp.image.size);
//    
//    // Clip to the bezier path and clear that portion of the image.
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextAddPath(context,bezierPath.CGPath);
//    CGContextClip(context);
//    
//    // Draw here when the context is clipped
//    [_imgTemp.image drawAtPoint:CGPointZero];
//    
//    // Build a new UIImage from the image context.
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    self._imgTemp.image = newImage;
    
    
//    [[UIColor redColor] setFill];
    UIBezierPath *outerPath = [UIBezierPath bezierPathWithRoundedRect:buttonRect cornerRadius:12.0];
    CGContextSaveGState(context);
    CGContextAddPath(context,outerPath.CGPath);
    CGContextClip(context);
    
    
//    CGContextSaveGState(context);
//    [outerPath addClip];
//    CGContextFillRect(context, buttonRect);
//    CGContextRestoreGState(context);    
    

    // Highlighted
    if (self.highlighted && (self.canBeTapped || self.canBeDragged)) {
        [[UIColor darkGrayColor] setFill];
        UIBezierPath *outerPath = [UIBezierPath bezierPathWithRoundedRect:buttonRect cornerRadius:11.0];
        CGContextSaveGState(context);
        [outerPath addClip];
        CGContextFillRect(context, buttonRect);
        CGContextRestoreGState(context);
    }
    CGFloat alpha = 1.0;
    if (self.canBeDragged == NO) {
        alpha = 0.3;
    }
    [self.iconImage drawInRect:buttonRect blendMode:kCGBlendModeOverlay alpha:alpha];
    CGContextRestoreGState(context);     
    
    // Close Button
    if (!self.hideDeleteImage) {
        [self.closeImage drawInRect:self.closeRect];
    } 
    // Text
    y += buttonRect.size.height + 3;
    UIFont *font = [UIFont boldSystemFontOfSize:10.0];
    
//    NSString *textColor = [[NSUserDefaults standardUserDefaults] stringForKey:@"NTWthemeColor"];    
//    if ([textColor isEqualToString:@"black"]){
            [[UIColor whiteColor] setFill];
//    }else if ([textColor isEqualToString:@"white"]){ 
//        [[UIColor blackColor] setFill];
//    }
    

    
    NSString *text = self.launcherItem.titleText;
    
    NSString *shortString;
    
    if ([text length]<=20){
            shortString = text;
    }else{ 
            shortString = [text substringToIndex:16];
            shortString = [NSString stringWithFormat:@"%@...",shortString];
    }
    

    
    CGSize maxTextSize = CGSizeMake(availableWidth, self.bounds.size.height - y+5);
    CGSize textSize = [shortString sizeWithFont:font 
                       constrainedToSize:maxTextSize 
                           lineBreakMode:UILineBreakModeTailTruncation];
    
    x = floor((self.bounds.size.width - textSize.width) / 2);
    CGRect textRect = CGRectMake(x, y, textSize.width, textSize.height);
    CGContextSetAlpha(context, alpha);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetShadowWithColor(ctx, CGSizeMake(1.0f, 1.0f), 1.0f, [[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:.6] CGColor]);
    [shortString drawInRect:textRect withFont:font lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentCenter];
    

    
}


- (void) setIconImageFromIconPath:(NSString *)iconPath {
//    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
//    NSString *imagePath = [bundle pathForResource:self.launcherItem.iconPath ofType:nil]; 
//    UIImage *aIconImage = [UIImage imageWithContentsOfFile:iconPath];
    
    // I've used the static imageNamed function before, but imageNamed doesn't work in unittests.
    //
    // UIImage *aIconImage = [UIImage imageNamed:self.launcherItem.iconPath];
    // UIImage *aBackgroundImage = [UIImage imageNamed:self.launcherItem.iconBackgroundPath];
//    NSString *aBackgroundImagePath = [bundle pathForResource:self.launcherItem.iconBackgroundPath ofType:nil];
//    UIImage *aBackgroundImage = [UIImage imageWithContentsOfFile:aBackgroundImagePath];
    
//    NSParameterAssert(aIconImage != nil);
//    self.iconImage = [self mergeBackgroundImage:aBackgroundImage  withTopImage:aIconImage];
    self.iconImage =     [UIImage imageWithContentsOfFile:iconPath];
}

- (UIImage*) mergeBackgroundImage:(UIImage*) bottomImage withTopImage:(UIImage*) image {
    CGSize newSize = bottomImage.size;   
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
		if ([[UIScreen mainScreen] scale] == 2.0) {
			UIGraphicsBeginImageContextWithOptions(newSize, NO, 2.0);
		} else {
			UIGraphicsBeginImageContext(newSize);
		}
	} else {
		UIGraphicsBeginImageContext(newSize);
	}

    
    [bottomImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    int x = (int)((bottomImage.size.width - image.size.width) / 2);
    int y = (int)((bottomImage.size.height- image.size.height) / 2);
    
    [image drawInRect:CGRectMake(x, y, image.size.width, image.size.height) blendMode:kCGBlendModeNormal alpha:1.0];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (id) initWithLauncherItem:(NTWLauncherItem *)launcherItem {
    if (self = [super initWithLauncherItem:launcherItem]) {
        closeRect = CGRectMake(50
                               , 0, 30, 30);
        if ([self.launcherItem.badgeImage isEqualToString:@""]){
            [self setCloseImage: [UIImage imageNamed:@"emptyBadge.png"]];
        }else{
            [self setCloseImage: [UIImage imageWithContentsOfFile:self.launcherItem.badgeImage]];
        }

        [self setIconImageFromIconPath:self.launcherItem.iconPath];
//        NSLog(@"imagePath : %@", self.launcherItem.iconPath);
        NSParameterAssert(self.iconImage != nil);
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void) dealloc {
    [iconImage release], iconImage = nil;
    [closeImage release], closeImage = nil;
    [super dealloc];
}

@end



#import "NTWLauncherParentView.h"
#import "NTWLauncherView.h"

#define BUTTON_SPACER 30
//#define PAGECONTROL_HEIGHT 20

@interface NTWLauncherParentView(private)
- (CGRect) centerRectForLauncherView:(NTWLauncherView*) launcherView parentRect:(CGRect) parentRect;
@end

@implementation NTWLauncherParentView
@synthesize launcherViewLeft;


- (void) drawRect:(CGRect)rect {
//    CGContextRef c = UIGraphicsGetCurrentContext();
//    [[UIColor blackColor] setFill];
//    CGContextFillRect(c, self.bounds);
    
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClearRect(context, rect);
}

- (void) layoutSubviews {
    int x_offset;
    UIInterfaceOrientation o = [[UIApplication sharedApplication] statusBarOrientation];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        switch (o) {
            case UIInterfaceOrientationLandscapeLeft: 
                x_offset = 205;
                PAGECONTROL_HEIGHT = 20;
                break;
                
            case UIInterfaceOrientationLandscapeRight: 
                x_offset = 205;
                PAGECONTROL_HEIGHT = 20;                
                break;
                
            case UIInterfaceOrientationPortraitUpsideDown: 
                
                x_offset = 145;
                PAGECONTROL_HEIGHT = 20;                
                break;
                
                
            case UIInterfaceOrientationPortrait: 
                
                x_offset = 145;
                PAGECONTROL_HEIGHT = 20;                
                break;
                
            default: break;
        }
    }else{
        switch (o) {
            case UIInterfaceOrientationLandscapeLeft: 
                x_offset = 110;
                PAGECONTROL_HEIGHT = -20;
                break;
                
            case UIInterfaceOrientationLandscapeRight: 
                x_offset = 110;
                PAGECONTROL_HEIGHT = -20;
                break;
                
            case UIInterfaceOrientationPortraitUpsideDown: 
                
                x_offset = 60;
                PAGECONTROL_HEIGHT = -10;
                break;
                
                
            case UIInterfaceOrientationPortrait: 
                
                x_offset = 60;
                PAGECONTROL_HEIGHT = -10;
                break;
                
            default: break;
        }
    }    
    
    
    CGRect leftHalfRect = CGRectMake(x_offset, 0, self.bounds.size.width / 2, self.bounds.size.height);
    CGRect launcherViewLeftRect = [self centerRectForLauncherView:launcherViewLeft parentRect:leftHalfRect];
    [self.launcherViewLeft setFrame:launcherViewLeftRect];
    
    
}

- (CGRect) centerRectForLauncherView:(NTWLauncherView*) launcherView parentRect:(CGRect) parentRect {
    int spacerButton;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        spacerButton = 30;
    }else {
        spacerButton = 5;
    }
    
    CGSize buttonDimensionsInLauncherView = [launcherView.dataSource buttonDimensionsInLauncherView:launcherView];
    
    int rows = [launcherView.dataSource numberOfRowsInLauncherView:launcherView];
    int cols = [launcherView.dataSource numberOfColumnsInLauncherView:launcherView];
    
    CGRect launcherViewRect = CGRectMake(parentRect.origin.x,
                                         parentRect.origin.y, 
                                         cols * (buttonDimensionsInLauncherView.width + spacerButton) ,
                                         (rows * buttonDimensionsInLauncherView.height) + PAGECONTROL_HEIGHT);
    
    CGRect centeredLauncherViewRect = launcherViewRect;
    centeredLauncherViewRect.origin.x = parentRect.origin.x + (parentRect.size.width / 2  - launcherViewRect.size.width / 2);
    centeredLauncherViewRect.origin.y = parentRect.origin.y + (parentRect.size.height / 2  - launcherViewRect.size.height / 2 +10);

    return centeredLauncherViewRect;
}



#pragma mark - lifecycle
- (id) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
         self.opaque = NO;
        [self setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        self.launcherViewLeft = [[[NTWLauncherView alloc] initWithFrame:CGRectZero] autorelease];
//        self.launcherViewLeft.backgroundColor = [UIColor redColor];
        [self addSubview:self.launcherViewLeft];

    }
    return self;
}

- (void) dealloc {
    [launcherViewLeft release], launcherViewLeft = nil;
    [super dealloc];
}

@end

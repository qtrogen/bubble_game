

#import "NTWLauncherViewController.h"
#import "NTWLauncherParentView.h"
#import "NTWLauncherService.h"
#import "NTWLauncherData.h"
#import "NTWLauncherView.h"

@implementation NTWLauncherViewController
@synthesize launcherService;
@synthesize currentDraggingView;

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (NTWLauncherParentView*) launcherParentView {
    return (NTWLauncherParentView*) self.view;
}

#pragma mark - View lifecycle
- (void)loadView {
    NTWLauncherParentView *launcherParentView = [[NTWLauncherParentView alloc] initWithFrame:CGRectZero];
    self.view = launcherParentView;
    [launcherParentView release];
}


- (void)viewDidLoad {
    NSParameterAssert(launcherService != nil);
    [launcherService launcherDataLeft];
    NTWLauncherData *launcherDataLeft = launcherService.launcherDataLeft;
    NTWLauncherView *launcherViewLeft = self.launcherParentView.launcherViewLeft;
    
    NSParameterAssert(launcherDataLeft != nil);
    [launcherViewLeft setPersistKey:launcherDataLeft.persistKey];
    [launcherViewLeft setDataSource:launcherService];
    [launcherViewLeft setDelegate:self];
    [launcherViewLeft reloadData];
    
    
//    NTWLauncherData *launcherDataRight = launcherService.launcherDataRight;
//    NTWLauncherView *launcherViewRight = self.launcherParentView.launcherViewRight;
//    NSParameterAssert(launcherViewRight != nil);
//    [launcherViewRight setPersistKey:launcherDataRight.persistKey];
//    [launcherViewRight setDataSource:launcherService];
//    [launcherViewRight setDelegate:self];
//    [launcherViewRight reloadData];
    
    [super viewDidLoad];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}
#pragma mark - NTWLauncherViewDelegate 
- (void) launcherView:(NTWLauncherView *)launcherView didTapLauncherIcon:(NTWLauncherIcon *)icon {
//    NSLog(@"didTapLauncherIcon: %@", icon);
}

- (void) launcherView:(NTWLauncherView *)launcherView didStartDragging:(NTWLauncherIcon *)icon {
    self.currentDraggingView = launcherView;
}

- (void) launcherView:(NTWLauncherView *)launcherView didStopDragging:(NTWLauncherIcon *)icon {
    self.currentDraggingView = nil;
}

- (void) launcherView:(NTWLauncherView*) launcherView willAddIcon:(NTWLauncherIcon*) addedIcon {
    
}


- (void) launcherView:(NTWLauncherView*) launcherView didDeleteIcon:(NTWLauncherIcon*) deletedIcon {
    
}

- (void) launcherViewDidAppear:(NTWLauncherView *)launcherView {
    
}

- (void) launcherViewDidDisappear:(NTWLauncherView *)launcherView {
    
}

- (void) launcherViewDidStartEditing:(NTWLauncherView*) launcherView {
    if (!self.launcherParentView.launcherViewLeft.editing) {
        [self.launcherParentView.launcherViewLeft startEditing];          
    }
    
//    if (!self.launcherParentView.launcherViewRight.editing) {
//        [self.launcherParentView.launcherViewRight startEditing];
//    }
}

- (void) launcherViewDidStopEditing:(NTWLauncherView*) launcherView {
    if (self.launcherParentView.launcherViewLeft.editing) {
        [self.launcherParentView.launcherViewLeft stopEditing];          
    }
    
//    if (self.launcherParentView.launcherViewRight.editing) {
//        [self.launcherParentView.launcherViewRight stopEditing];
//    }
}

- (NTWLauncherView*) targetLauncherViewForIcon:(NTWLauncherIcon *) icon {
    
    CGRect leftLauncherViewRectInKeyView = [icon.superview convertRect:self.launcherParentView.launcherViewLeft.frame
                                                              fromView:self.launcherParentView.launcherViewLeft.superview];
    
    BOOL inLeftLauncherView = (CGRectContainsPoint(leftLauncherViewRectInKeyView, icon.center));
    
    if (inLeftLauncherView ) {
        // both launcherviews are overlapping. this is not intended. 
        // in order to prevent a crash, the current draggingView will be returned.
    } else {
        if (inLeftLauncherView) {
            return self.launcherParentView.launcherViewLeft;
        }
        
    }
    return self.currentDraggingView;
}

#pragma mark - Lifecycle
- (id) initWithLauncherService:(NTWLauncherService*) inLauncherService {
    if (self = [super init]) {
        self.launcherService = inLauncherService;
    }
    return self;
    
}

- (void) dealloc {
    [launcherService release], launcherService = nil;
    [super dealloc];
}


@end

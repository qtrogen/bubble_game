

#import <UIKit/UIKit.h>
#import "NTWLauncherViewDelegate.h"
@class NTWLauncherParentView;
@class NTWLauncherService;
@class NTWLauncherView;

@interface NTWLauncherViewController : UIViewController<NTWLauncherViewDelegate>

- (NTWLauncherParentView*) launcherParentView;
- (id) initWithLauncherService:(NTWLauncherService*) inLauncherService;

@property (nonatomic, retain) NTWLauncherService *launcherService;
@property (nonatomic, assign) NTWLauncherView *currentDraggingView;
@end


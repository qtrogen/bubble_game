

#import <Foundation/Foundation.h>
#import "NTWLauncherDataSource.h"
@class NTWLauncherData;

@interface NTWLauncherService : NSObject<NTWLauncherDataSource>

- (void) loadLauncherData;

@property (nonatomic, retain) NTWLauncherData *launcherDataLeft;
@property (nonatomic, retain) NTWLauncherData *launcherDataRight;

@end



#import <UIKit/UIKit.h>
#import "NTWLauncherView.h"
#import "NTWLauncherDataSource.h"
#import "NTWLauncherViewDelegate.h"

@interface NTWLauncherView : UIView <UIScrollViewDelegate, UIAlertViewDelegate>

- (void) reloadData;
- (void) startEditing;
- (void) stopEditing;

// Adds the icon to the view. Please note that the icon has to be added to the datasource before.
- (void) addIcon:(NTWLauncherIcon*) icon;

// Removes the icon from the view. Please note that the icon will not be removed from the datasource.
- (void) removeIcon:(NTWLauncherIcon*) icon;
- (void) removeIconAnimated:(NTWLauncherIcon*) icon completion:(void (^) (void)) block;
- (void) layoutIconsAnimated;
- (void) layoutIcons;
- (void) notifyButtonsState;


@property (nonatomic, assign) BOOL shouldLayoutDragButton;
@property (nonatomic, readonly) BOOL editing;
@property (nonatomic, retain) NSIndexPath *targetPath;
@property (nonatomic, retain) NSObject<NTWLauncherDataSource> *dataSource;
@property (nonatomic, assign) NSObject<NTWLauncherViewDelegate> *delegate;
@property (nonatomic, retain) NSString *persistKey;



@end
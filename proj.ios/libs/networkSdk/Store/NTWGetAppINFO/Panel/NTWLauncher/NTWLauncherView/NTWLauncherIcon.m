

#import "NTWLauncherIcon.h"
#import "NTWLauncherItem.h"

@interface NTWLauncherIcon(private)
@end

@implementation NTWLauncherIcon
@synthesize canBeDeleted;
@synthesize canBeDragged;
@synthesize canBeTapped;
@synthesize hideDeleteImage;
@synthesize identifier;
@synthesize originIndexPath;
@synthesize launcherItem;
@synthesize badgeImage;


@synthesize iconStoreURL;



- (BOOL) hitCloseButton:(CGPoint)point {
    NSAssert(NO, @"this method must be overridden");
    return NO;
}
- (void) drawRect:(CGRect) rect {
    NSAssert(NO, @"this method must be overridden");
}

- (void) setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    [self setNeedsDisplay];
}

- (NSString*) description {
    return [NSString stringWithFormat:@"%@ identifier:%@", launcherItem.titleText, self.identifier];
}

- (NSString*) applicationID {
    return self.badgeImage;
}


#pragma mark - lifecycle
- (id) initWithLauncherItem: (NTWLauncherItem*) inLauncherItem {
    if (self = [super initWithFrame:CGRectZero]) {
        self.launcherItem = inLauncherItem;
        self.badgeImage = inLauncherItem.badgeImage;
        self.identifier = inLauncherItem.identifier;
        self.iconStoreURL = inLauncherItem.appStoreURL;
        self.hideDeleteImage = NO;
        [self setClipsToBounds:NO];
        [self setContentMode:UIViewContentModeRedraw];
        [self setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        
//        NSLog(@"identifier:%@", self.identifier);
    }
    return self;
}

-(void) dealloc {
    [originIndexPath release], originIndexPath = nil;
    [identifier release], identifier = nil;
    [badgeImage release], badgeImage = nil;
    [launcherItem release], launcherItem = nil;
    [super dealloc];
}
@end



#import <UIKit/UIKit.h>
@class NTWLauncherView;
@class NTWLauncherItem;

// Base class for an Icon. You've to extend from this class.
// See LauncherExampleIcon.h for an example.
@interface NTWLauncherIcon : UIControl {
}
@property (nonatomic, assign) BOOL canBeDeleted;
@property (nonatomic, assign) BOOL canBeDragged;
@property (nonatomic, assign) BOOL canBeTapped;
@property (nonatomic, assign) BOOL hideDeleteImage;
@property (nonatomic, retain) NSString *identifier;
@property (nonatomic, retain) NSString *badgeImage;
@property (nonatomic, retain) NSString *iconStoreURL;
@property (nonatomic, retain) NSIndexPath *originIndexPath;
@property (nonatomic, retain) NTWLauncherItem *launcherItem;

// Should return YES, if the close button contains the given point. 
- (BOOL) hitCloseButton:(CGPoint) point;

- (id) initWithLauncherItem: (NTWLauncherItem*) launcherItem;

@end

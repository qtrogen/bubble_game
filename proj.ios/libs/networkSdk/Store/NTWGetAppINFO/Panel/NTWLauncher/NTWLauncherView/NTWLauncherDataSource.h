

#import <Foundation/Foundation.h>
@class NTWLauncherView;
#import "NTWLauncherIcon.h"

@protocol NTWLauncherDataSource <NSObject>

@required

// Size of the icon. This includes the padding.
- (CGSize) buttonDimensionsInLauncherView:(NTWLauncherView *) launcherView;

- (NSUInteger) numberOfRowsInLauncherView:(NTWLauncherView *) launcherView;

- (NSUInteger) numberOfColumnsInLauncherView:(NTWLauncherView*) launcherView;

// The total number of pages to be shown in the launcher view.
- (NSUInteger) numberOfPagesInLauncherView:(NTWLauncherView *) launcherView;

// Counts all contained icons in the given launcher view
- (NSUInteger) numberOfIconsInLauncherView:(NTWLauncherView *)launcherView;

// The total number of buttons in a given page.
- (NSUInteger) launcherView:(NTWLauncherView *)launcherView 
        numberOfIconsInPage:(NSUInteger)page;


- (BOOL) launcherView:(NTWLauncherView *) launcherView contains:(NTWLauncherIcon*) icon;

- (NSArray*) launcherView:(NTWLauncherView*) launcherView findIconsByIdentifier:(NSString*) identifier;

// Retrieve the button to be displayed at a given page and index.
- (NTWLauncherIcon *) launcherView: (NTWLauncherView *)launcherView
                      iconForPage: (NSUInteger)pageIndex
                          atIndex: (NSUInteger)iconIndex;


// Writing operations
- (void) launcherView:(NTWLauncherView*) launcherView 
              addIcon:(NTWLauncherIcon*) icon;

- (void) launcherView:(NTWLauncherView*) launcherView
              addIcon:(NTWLauncherIcon*) icon
            pageIndex:(NSUInteger) pageIndex
            iconIndex:(NSUInteger) iconIndex;

- (void) launcherView:(NTWLauncherView*) launcherView
             moveIcon:(NTWLauncherIcon*) icon 
               toPage:(NSUInteger) pageIndex
              toIndex:(NSUInteger) iconIndex;

- (void) launcherView:(NTWLauncherView*) launcherView
           removeIcon:(NTWLauncherIcon*) icon;

// Adds a new page to the launcher view. The new created page is returned for further usage.
- (NSMutableArray*) addPageToLauncherView:(NTWLauncherView*) launcherView;


// Removes all pages which does not contain any icon.
- (void) removeEmptyPages:(NTWLauncherView*) launcherView;


@end

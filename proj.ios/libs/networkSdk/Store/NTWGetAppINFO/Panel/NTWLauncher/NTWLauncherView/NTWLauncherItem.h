

#import <Foundation/Foundation.h>

// The item represents the "data" of an icon and can be serialized.
@interface NTWLauncherItem : NSObject
@property (nonatomic, retain) NSString *identifier;
@property (nonatomic, retain) NSString *iconPath;
@property (nonatomic, retain) NSString *iconBackgroundPath;
@property (nonatomic, retain) NSString *titleText;
@property (nonatomic, retain) NSString *badgeImage;
@property (nonatomic, retain) NSString *appStoreURL;
@end

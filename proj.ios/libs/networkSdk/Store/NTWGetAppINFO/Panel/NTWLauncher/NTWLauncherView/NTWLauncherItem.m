

#import "NTWLauncherItem.h"

@implementation NTWLauncherItem
@synthesize identifier;
@synthesize iconPath;
@synthesize iconBackgroundPath;
@synthesize titleText;
@synthesize badgeImage;
@synthesize appStoreURL;


-(void) encodeWithCoder:(NSCoder*) coder {
	[coder encodeObject: identifier forKey: @"identifier"];
	[coder encodeObject: iconPath forKey: @"iconPath"];
	[coder encodeObject: titleText forKey: @"titleText"];
    [coder encodeObject: iconBackgroundPath forKey:@"iconBackgroundPath"];
    [coder encodeObject: appStoreURL forKey:@"appStoreURL"];
}

-(id) initWithCoder:(NSCoder*) decoder {
	if (self = [super init]) {
        self.badgeImage = [decoder decodeObjectForKey:@"badgeFileName"];
        self.identifier = [decoder decodeObjectForKey:@"identifier"];
        self.iconPath   = [decoder decodeObjectForKey:@"iconPath"];
        self.titleText  = [decoder decodeObjectForKey:@"titleText"];
        self.iconBackgroundPath = [decoder decodeObjectForKey:@"iconBackgroundPath"];
        self.appStoreURL = [decoder decodeObjectForKey:@"appStoreURL"];
    }
	return self;
}

- (void) dealloc {
    [badgeImage release], badgeImage = nil;    
    [identifier release], identifier = nil;
    [iconPath release], iconPath = nil;
    [iconBackgroundPath release], iconBackgroundPath = nil;
    [titleText release], titleText = nil;
    [appStoreURL release], appStoreURL = nil;
    [super dealloc];
}
@end

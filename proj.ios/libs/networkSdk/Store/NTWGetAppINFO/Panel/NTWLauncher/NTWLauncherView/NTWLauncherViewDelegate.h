

#import <Foundation/Foundation.h>

@class NTWLauncherView;
@class NTWLauncherIcon;
@protocol NTWLauncherViewDelegate <NSObject>

- (void) launcherView:(NTWLauncherView*) launcherView didStartDragging:(NTWLauncherIcon*) icon;

- (void) launcherView:(NTWLauncherView*) launcherView didStopDragging:(NTWLauncherIcon*) icon;

- (void) launcherView:(NTWLauncherView*) launcherView didTapLauncherIcon:(NTWLauncherIcon*) icon;

- (void) launcherView:(NTWLauncherView*) launcherView willAddIcon:(NTWLauncherIcon*) icon;

- (void) launcherView:(NTWLauncherView*) launcherView didDeleteIcon:(NTWLauncherIcon*) icon;

- (void) launcherViewDidAppear:(NTWLauncherView *)launcherView;

- (void) launcherViewDidDisappear:(NTWLauncherView *)launcherView;

- (void) launcherViewDidStartEditing:(NTWLauncherView*) launcherView;

- (void) launcherViewDidStopEditing:(NTWLauncherView*) launcherView;


// Returns the NTWLauncherView, which will embed the icon, when the dragging ends.
- (NTWLauncherView*) targetLauncherViewForIcon:(NTWLauncherIcon*) icon;
@end

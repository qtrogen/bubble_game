

#import <Foundation/Foundation.h>
#import "NTWLauncherIcon.h"


// NTWLauncherData contains a list of pages. Each page is represented by a NSMutableArray.
@interface NTWLauncherData : NSObject

@property (nonatomic, assign) NSInteger maxRows;
@property (nonatomic, assign) NSInteger maxColumns;
@property (nonatomic, retain) NSString *persistKey;
@property (nonatomic, retain) NSMutableArray *launcherIconPages;


- (void)  addIcon:(NTWLauncherIcon*) icon;

- (void)      addIcon:(NTWLauncherIcon*) icon
            pageIndex:(NSUInteger) pageIndex
            iconIndex:(NSUInteger) iconIndex;

- (void)     moveIcon: (NTWLauncherIcon*) icon 
               toPage: (NSUInteger) pageIndex
              toIndex: (NSUInteger) iconIndex;

- (void) removeIcon:(NTWLauncherIcon*) icon;

- (NSMutableArray*) addPage;

- (void) removeEmptyPages;

- (NSArray*) findIconsByIdentifier:(NSString*) identifier;

- (NSMutableArray*) pageOfIcon:(NTWLauncherIcon*) icon;

- (NSUInteger) iconCount;

- (NSUInteger) pageCount;

@end

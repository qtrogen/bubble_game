

#import "NTWLauncherService.h"
#import "NTWLauncherItem.h"
#import "NTWLauncherIcon.h"
#import "NTWLauncherExampleIcon.h"
#import "NTWLauncherView.h"
#import "NTWLauncherData.h"


#import "NTWGetAppINFO.h"
#import "NTWNSString+HTML.h"

@interface NTWLauncherService() 
- (NTWLauncherData*) launcherDataFor:(NTWLauncherView*) launcherView;
- (NTWLauncherIcon*) launcherIconForTitle:(NSString*) titleText 
                               imagePath:(NSString*) imagePath
                     badgeImage:(NSString*) badgeImage
                    appStoreURL:(NSString*) appStoreURL;


@end

@implementation NTWLauncherService
@synthesize launcherDataLeft;
@synthesize launcherDataRight;

- (NSMutableArray*) addPageToLauncherView:(NTWLauncherView*) launcherView {
    NSParameterAssert(launcherView != nil);
    NTWLauncherData *data = [self launcherDataFor:launcherView];
    return [data addPage];
}

- (NTWLauncherData*) launcherDataFor:(NTWLauncherView*) launcherView {
    NSParameterAssert(launcherView.persistKey != nil);
    if ([launcherView.persistKey isEqualToString:self.launcherDataLeft.persistKey]) {
        return self.launcherDataLeft;
    }
    if ([launcherView.persistKey isEqualToString:self.launcherDataRight.persistKey]) {
        return self.launcherDataRight;
    }
    return nil;
}

#pragma mark - NTWLauncherDataSource
- (NSUInteger) numberOfRowsInLauncherView:(NTWLauncherView *) launcherView {
    NTWLauncherData *data = [self launcherDataFor:launcherView];
    return data.maxRows;
}

- (NSUInteger) numberOfColumnsInLauncherView:(NTWLauncherView*) launcherView {
    NTWLauncherData *data = [self launcherDataFor:launcherView];
    return data.maxColumns;
}

- (CGSize) buttonDimensionsInLauncherView:(NTWLauncherView *)launcherView {
    return CGSizeMake(80, 120);
}

- (NSUInteger)numberOfPagesInLauncherView:(NTWLauncherView *)launcherView {
    NSParameterAssert(launcherView != nil);
    NTWLauncherData *data = [self launcherDataFor:launcherView];
    return [data.launcherIconPages count];
}

- (NSUInteger)numberOfIconsInLauncherView:(NTWLauncherView *)launcherView {
    NTWLauncherData *data = [self launcherDataFor:launcherView];
    NSUInteger count = 0;
    for (NSMutableArray *page in data.launcherIconPages) {
        count += [page count];
    }
    return count;
}

- (NSUInteger)launcherView:(NTWLauncherView *)launcherView numberOfIconsInPage:(NSUInteger) page {
    NSParameterAssert(launcherView != nil);
    NSParameterAssert(page < 5000);
    NTWLauncherData *data = [self launcherDataFor:launcherView];
    NSMutableArray *buttons = [data.launcherIconPages objectAtIndex:page];
    return [buttons count];
}

- (BOOL) launcherView:(NTWLauncherView *) launcherView contains:(NTWLauncherIcon*) icon {
    NSParameterAssert(launcherView != nil);
    NSParameterAssert(icon != nil);
    NTWLauncherData *data = [self launcherDataFor:launcherView];    
    if ([data pageOfIcon:icon] == nil) {
        return NO;
    }
    return YES;
}

- (NTWLauncherIcon *) launcherView: (NTWLauncherView *)launcherView
                      iconForPage: (NSUInteger)pageIndex
                          atIndex: (NSUInteger)iconIndex {
    NTWLauncherData *data = [self launcherDataFor:launcherView];    
    NSMutableArray *buttons = [data.launcherIconPages objectAtIndex:pageIndex];
    return [buttons objectAtIndex:iconIndex];
}

- (void) launcherView:(NTWLauncherView *) launcherView
              addIcon:(NTWLauncherIcon*) icon {
    NTWLauncherData *data = [self launcherDataFor:launcherView];    
    [data addIcon:icon];
}

- (void) launcherView:(NTWLauncherView*) launcherView
              addIcon:(NTWLauncherIcon*) icon
            pageIndex:(NSUInteger) pageIndex
            iconIndex:(NSUInteger) iconIndex {
    NTWLauncherData *data = [self launcherDataFor:launcherView];     
    [data addIcon:icon pageIndex:pageIndex iconIndex:iconIndex];
}

- (void) launcherView:(NTWLauncherView *)launcherView removeIcon:(NTWLauncherIcon *)icon {
    NSParameterAssert(launcherView != nil);
    NTWLauncherData *data = [self launcherDataFor:launcherView];
    [data removeIcon:icon];
}

- (void) launcherView:(NTWLauncherView*) launcherView
             moveIcon:(NTWLauncherIcon*) icon 
               toPage:(NSUInteger) pageIndex
              toIndex:(NSUInteger) iconIndex {
    NTWLauncherData *data = [self launcherDataFor:launcherView];
    NSParameterAssert(data != nil);
    [data moveIcon:icon toPage:pageIndex toIndex:iconIndex];
}

- (void) removeEmptyPages:(NTWLauncherView*) launcherView {
    NSParameterAssert(launcherView != nil);
    NTWLauncherData *data = [self launcherDataFor:launcherView];
    [data removeEmptyPages];
}

- (NSArray*) launcherView:(NTWLauncherView*) launcherView findIconsByIdentifier:(NSString*) identifier {
    NSParameterAssert(launcherView != nil);
    NSParameterAssert(identifier != nil);
    NTWLauncherData *data = [self launcherDataFor:launcherView];
    return [data findIconsByIdentifier:identifier];
}

- (void) loadLauncherData {
    NSParameterAssert(self.launcherDataLeft != nil);
//    NSString *lorem = @"Lorem ipsum dolor sit amet consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut";
//    NSArray *loremArray = [lorem componentsSeparatedByString:@" "];
    
//    NSString *imageBackgroundLightPath = @"icon_border_bold_light.png";
    
    
    NTWGetAppINFO* sharedNTWRing = [NTWGetAppINFO sharedInstance];    
    int iconsCount = [sharedNTWRing.imageLinksData count];

    NSMutableDictionary *appDataToDisplay = [[[NSMutableDictionary alloc] init] autorelease];

    
    // Add some dummy icons for launcherview on the left side.
    for (int i=0;i<iconsCount;i++) {
            appDataToDisplay = [sharedNTWRing.imageLinksData objectAtIndex:i];
//        UIView *view = [[[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@%@.png",sharedNTWRing.pathToAppTempDirectory,[appDataToDisplay valueForKey:@"IconName"],sharedNTWRing.scaleExtention]]] autorelease];        
        
        NSString *imagePath = [NSString stringWithFormat:@"%@/%@",sharedNTWRing.pathToAppTempDirectory,[appDataToDisplay valueForKey:@"Filename"]];
        
        NSString *badgeImagePath = [NSString stringWithFormat:@"%@/%@",sharedNTWRing.pathToAppTempDirectory,[appDataToDisplay valueForKey:@"BadgeFileName"]];
        
        NSString *titleText = [[appDataToDisplay valueForKey:@"Name"] dbsstringByConvertingHTMLToPlainText];
        
        NSString *applicationStoreURL = [appDataToDisplay valueForKey:@"Link"];

//        NSLog(@"appName:%@",[[appDataToDisplay valueForKey:@"AppName"] dbsstringByConvertingHTMLToPlainText]);
        NTWLauncherIcon *icon = [self launcherIconForTitle:titleText
                                      imagePath:imagePath
                                      badgeImage:badgeImagePath
                                              appStoreURL:applicationStoreURL];
        if (i %2 == 0) {
            [icon setCanBeDeleted:YES];
        }
        
        [self.launcherDataLeft addIcon:icon];          
        
    }

//    NSString *imageBackgroundDarkPath = @"icon_border_bold_dark.png";
    
//    // Add some dummy icons for launcherview on the right side.
//    char start = 'a';
//    for (int i=1;i<14;i++) {
//        NSString *titleText = [loremArray objectAtIndex:i];
//        NSString *imagePath = [NSString stringWithFormat:@"%c.png", start];
//        NTWLauncherIcon *icon = [self launcherIconForTitle:titleText
//                                      imagePath:imagePath
//                                      imageBackgroundPath:imageBackgroundDarkPath];
//        if (i %2 == 1) {
//            [icon setCanBeDeleted:YES];
//        }        
//        [self.launcherDataRight addIcon:icon];
//        start++;
//    }
}

- (NTWLauncherIcon*) launcherIconForTitle:(NSString*) titleText
                               imagePath:(NSString*) imagePath 
                     badgeImage:(NSString*) badgeImage
                    appStoreURL:(NSString*)applicationStoreURL{
    NSParameterAssert(titleText != nil);
    NSString *titleLowercase  = [titleText lowercaseString];
    NSString *titleReplaced = [titleLowercase stringByReplacingOccurrencesOfString:@" " withString:@"_"];    
    NSString *identifier = [NSString stringWithFormat:@"static_%@", titleReplaced];
    
    NTWLauncherItem *launcherItem = [[NTWLauncherItem alloc] init];
    [launcherItem setBadgeImage:badgeImage];
    [launcherItem setIdentifier:identifier];
    [launcherItem setTitleText:titleText];
    [launcherItem setIconPath:imagePath];
    [launcherItem setAppStoreURL:applicationStoreURL];
//    [launcherItem setIconBackgroundPath:imageBackgroundPath];
    NTWLauncherExampleIcon *launcherIcon = [[NTWLauncherExampleIcon alloc] initWithLauncherItem:launcherItem];
    [launcherIcon setCanBeTapped:YES];
    [launcherIcon setCanBeDragged:YES];   

    
    [launcherItem release];
    return [launcherIcon autorelease]; 
}

- (BOOL) launcherItem:(NTWLauncherItem*) item inData:(NTWLauncherData*) data {
    NSParameterAssert(item != nil);
    NSArray *array = [data findIconsByIdentifier:item.identifier];
    return [array count] > 0;
}

#pragma mark - lifecycle
- (id) init {
    if (self = [super init]) {
        
        UIInterfaceOrientation o = [[UIApplication sharedApplication] statusBarOrientation];


        
        self.launcherDataLeft  = [[[NTWLauncherData alloc] init] autorelease];
        
        self.launcherDataLeft.persistKey = @"LauncherDataLeft";
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            switch (o) {
                case UIInterfaceOrientationLandscapeLeft: 
                    self.launcherDataLeft.maxRows = 4;
                    self.launcherDataLeft.maxColumns = 7;
                    break;
                    
                case UIInterfaceOrientationLandscapeRight: 
                    self.launcherDataLeft.maxRows = 4;
                    self.launcherDataLeft.maxColumns = 7;
                    break;
                    
                case UIInterfaceOrientationPortraitUpsideDown: 
                    
                    self.launcherDataLeft.maxRows = 6;
                    self.launcherDataLeft.maxColumns = 4;
                    break;
                    
                    
                case UIInterfaceOrientationPortrait: 
                    
                    self.launcherDataLeft.maxRows = 6;
                    self.launcherDataLeft.maxColumns = 4;
                    break;
                    
                default: break;
            }
        }else{
            switch (o) {
                case UIInterfaceOrientationLandscapeLeft: 
                    self.launcherDataLeft.maxRows = 2;
                    self.launcherDataLeft.maxColumns = 4;
                    break;
                    
                case UIInterfaceOrientationLandscapeRight: 
                    self.launcherDataLeft.maxRows = 2;
                    self.launcherDataLeft.maxColumns = 4;
                    break;
                    
                case UIInterfaceOrientationPortraitUpsideDown: 
                    
                    self.launcherDataLeft.maxRows = 3;
                    self.launcherDataLeft.maxColumns = 3;
                    break;
                    
                    
                case UIInterfaceOrientationPortrait: 
                    
                    self.launcherDataLeft.maxRows = 3;
                    self.launcherDataLeft.maxColumns = 3;
                    break;
                    
                default: break;
            }
        }
        

        self.launcherDataRight = [[[NTWLauncherData alloc] init] autorelease];
        self.launcherDataRight.persistKey = @"LauncherDataRight"; 
        self.launcherDataRight.maxRows = 2;
        self.launcherDataRight.maxColumns = 4;
    }
    return self;
}

- (void) dealloc {
    [launcherDataLeft release], launcherDataLeft = nil;
    [launcherDataRight release], launcherDataRight = nil;
    [super dealloc];
}

@end

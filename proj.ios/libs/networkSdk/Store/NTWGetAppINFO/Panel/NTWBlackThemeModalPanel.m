

#import "NTWBlackThemeModalPanel.h"

#import "NTWLauncherParentView.h"
#import "NTWLauncherService.h"
#import "NTWLauncherView.h"
#import "NTWGetAppINFO.h"

#define BLACK_BAR_COMPONENTS				{ 0.0, 0.0, 0.0, 0.0, 0.00, 0.00, 0.00, 0.0 }

@implementation NTWBlackThemeModalPanel

@synthesize viewLoadedFromXib;
@synthesize launcherService;
@synthesize currentDraggingView;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title {
	if ((self = [super initWithFrame:frame])) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rotationNotification:) name:UIDeviceOrientationDidChangeNotification object:nil];
        
        
        ///launcher 
        launcherService = [[NTWLauncherService alloc] init];
        [launcherService loadLauncherData];
        ///launcher         

        NTWLauncherParentView *launcherParentView = [[NTWLauncherParentView alloc] initWithFrame:CGRectZero];
        v = launcherParentView;
        //        [launcherParentView release];      		
       
        NSParameterAssert(launcherService != nil);
        [launcherService launcherDataLeft];
        launcherDataLeft = launcherService.launcherDataLeft;
        launcherViewLeft = self.launcherParentView.launcherViewLeft;
        
        NSParameterAssert(launcherDataLeft != nil);
        [launcherViewLeft setPersistKey:launcherDataLeft.persistKey];
        [launcherViewLeft setDataSource:launcherService];
        [launcherViewLeft setDelegate:self];
        [launcherViewLeft reloadData];
        
          
        
		CGFloat colors[8] = BLACK_BAR_COMPONENTS;
		[self.titleBar setColorComponents:colors];
		self.headerLabel.text = title;
		
		
//		////////////////////////////////////
//		// RANDOMLY CUSTOMIZE IT
//		////////////////////////////////////
//		// Show the defaults mostly, but once in awhile show a completely random funky one
//		if (arc4random() % 4 == 0) {
//			// Funky time.
//			UADebugLog(@"Showing a randomized panel for modalPanel: %@", self);
//			
//			// Margin between edge of container frame and panel. Default = 20.0
//			self.outerMargin = ((arc4random() % 4) + 1) * 20.0f;
//			
//			// Margin between edge of panel and the content area. Default = 20.0
//			self.innerMargin = ((arc4random() % 4) + 1) * 10.0f;
//			
//			// Border color of the panel. Default = [UIColor whiteColor]
//			self.borderColor = [UIColor colorWithRed:(arc4random() % 2) green:(arc4random() % 2) blue:(arc4random() % 2) alpha:1.0];
//
			// Border width of the panel. Default = 1.5f;
			self.borderWidth = 0;
//			
			// Corner radius of the panel. Default = 4.0f
			self.cornerRadius = 20.0f;
//			
//			// Color of the panel itself. Default = [UIColor colorWithWhite:0.0 alpha:0.8]
			self.contentColor = [UIColor colorWithRed:(arc4random() % 2) green:(arc4random() % 2) blue:(arc4random() % 2) alpha:1.0];
//			
//			// Shows the bounce animation. Default = YES
			self.shouldBounce = NO;
//			
//			// Height of the title view. Default = 40.0f
//			[self setTitleBarHeight:((arc4random() % 5) + 2) * 20.0f];
			[self setTitleBarHeight:30.0f];        
//			
//			// The background color gradient of the title
//			CGFloat colors[8] = {
//				(arc4random() % 2), (arc4random() % 2), (arc4random() % 2), 1,
//				(arc4random() % 2), (arc4random() % 2), (arc4random() % 2), 1
//			};
//			[[self titleBar] setColorComponents:colors];
//			
//			// The gradient style (Linear, linear reversed, radial, radial reversed, center highlight). Default = UAGradientBackgroundStyleLinear
			[[self titleBar] setGradientStyle:UAGradientBackgroundStyleCenterHighlight];
//			
//			// The line mode of the gradient view (top, bottom, both, none). Top is a white line, bottom is a black line.
//			[[self titleBar] setLineMode: pow(2, (arc4random() % 3))];
//			
//			// The noise layer opacity. Default = 0.4
			[[self titleBar] setNoiseOpacity:0];
//			
//			// The header label, a UILabel with the same frame as the titleBar
			[self headerLabel].font = [UIFont boldSystemFontOfSize:floor(self.titleBarHeight / 2.0)];
//		}

	
//		//////////////////////////////////////
//		// SETUP RANDOM CONTENT
//		//////////////////////////////////////
//		UIWebView *wv = [[[UIWebView alloc] initWithFrame:CGRectZero] autorelease];
//		[wv loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://urbanapps.com/product_list"]]];
//		
//		UITableView *tv = [[[UITableView alloc] initWithFrame:CGRectZero] autorelease];
//		[tv setDataSource:self];
//		
//		UIImageView *iv = [[[UIImageView alloc] initWithFrame:CGRectZero] autorelease];
//		[iv setImage:[UIImage imageNamed:@"UrbanApps.png"]];
//		[iv setContentMode:UIViewContentModeScaleAspectFit];
//		
//		[[NSBundle mainBundle] loadNibNamed:@"UAExampleView" owner:self options:nil];
//		
//		NSArray *contentArray = [NSArray arrayWithObjects:wv, tv, iv, viewLoadedFromXib, nil];
//		
//		int i = arc4random() % [contentArray count];
//		v = [[contentArray objectAtIndex:i] retain];
//        [v setFrame:self.contentView.bounds];
		[self.contentView addSubview:v];
		
	}	
	return self;
}

- (NTWLauncherParentView*) launcherParentView {
    return (NTWLauncherParentView*) v;
}

- (void)dealloc {
    [v release];
	[viewLoadedFromXib release];
    [launcherService release], launcherService = nil;    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    [super dealloc];
}


- (void)rotationNotification:(NSNotification *)notification
{
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if ((orientation == UIInterfaceOrientationLandscapeLeft) || (orientation == UIInterfaceOrientationLandscapeRight)){
        [v removeFromSuperview];
        [launcherService release], launcherService = nil;   
        
        
        ///launcher 
        launcherService = [[NTWLauncherService alloc] init];
        [launcherService loadLauncherData];
        ///launcher         
        
        NTWLauncherParentView *launcherParentView = [[NTWLauncherParentView alloc] initWithFrame:CGRectZero];
        v = launcherParentView;
        //        [launcherParentView release];      		
        
        NSParameterAssert(launcherService != nil);
        [launcherService launcherDataLeft];
        launcherDataLeft = launcherService.launcherDataLeft;
        launcherViewLeft = self.launcherParentView.launcherViewLeft;
        
        NSParameterAssert(launcherDataLeft != nil);
        [launcherViewLeft setPersistKey:launcherDataLeft.persistKey];
        [launcherViewLeft setDataSource:launcherService];
        [launcherViewLeft setDelegate:self];
        [launcherViewLeft reloadData];
        [v setFrame:self.contentView.bounds];
[self.contentView addSubview:v];        
    }else{
        [v removeFromSuperview];
        [launcherService release], launcherService = nil;           
        
        ///launcher 
        launcherService = [[NTWLauncherService alloc] init];
        [launcherService loadLauncherData];
        ///launcher         
        
        NTWLauncherParentView *launcherParentView = [[NTWLauncherParentView alloc] initWithFrame:CGRectZero];
        v = launcherParentView;
        //        [launcherParentView release];      		
        
        NSParameterAssert(launcherService != nil);
        [launcherService launcherDataLeft];
        launcherDataLeft = launcherService.launcherDataLeft;
        launcherViewLeft = self.launcherParentView.launcherViewLeft;
        
        NSParameterAssert(launcherDataLeft != nil);
        [launcherViewLeft setPersistKey:launcherDataLeft.persistKey];
        [launcherViewLeft setDataSource:launcherService];
        [launcherViewLeft setDelegate:self];
        [launcherViewLeft reloadData];
        [v setFrame:self.contentView.bounds];
[self.contentView addSubview:v];
    }
}


- (void)layoutSubviews {
	[super layoutSubviews];
	
    [v setFrame:self.contentView.bounds];
//    NSLog(@"bounds: %f,%f",self.contentView.bounds.size.height, self.contentView.bounds.size.width);
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

	NSString *cellIdentifier = @"UAModalPanelCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
	}
	
	[cell.textLabel setText:[NSString stringWithFormat:@"Row %d", indexPath.row]];
	
	return cell;
}

#pragma mark - Actions
- (IBAction)buttonPressed:(id)sender {
	// The button was pressed. Lets do something with it.
	
	// Maybe the delegate wants something to do with it...
	if ([delegate respondsToSelector:@selector(superAwesomeButtonPressed:)]) {
		[delegate performSelector:@selector(superAwesomeButtonPressed:) withObject:sender];
	
	// Or perhaps someone is listening for notifications 
	} else {
		[[NSNotificationCenter defaultCenter] postNotificationName:@"SuperAwesomeButtonPressed" object:sender];
	}
		
//	NSLog(@"Super Awesome Button pressed!");
}


#pragma mark - NTWLauncherViewDelegate 
- (void) launcherView:(NTWLauncherView *)launcherView didTapLauncherIcon:(NTWLauncherIcon *)icon {
//    NSLog(@"didTapLauncherIcon: %@", icon.appID);
    
//    NTWRing* sharedNTWRing = [NTWRing sharedInstance];

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[icon.iconStoreURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ]];

}

- (void) launcherView:(NTWLauncherView *)launcherView didStartDragging:(NTWLauncherIcon *)icon {
    self.currentDraggingView = launcherView;
}

- (void) launcherView:(NTWLauncherView *)launcherView didStopDragging:(NTWLauncherIcon *)icon {
    self.currentDraggingView = nil;
}

- (void) launcherView:(NTWLauncherView*) launcherView willAddIcon:(NTWLauncherIcon*) addedIcon {
    
}


- (void) launcherView:(NTWLauncherView*) launcherView didDeleteIcon:(NTWLauncherIcon*) deletedIcon {
    
}

- (void) launcherViewDidAppear:(NTWLauncherView *)launcherView {
    
}

- (void) launcherViewDidDisappear:(NTWLauncherView *)launcherView {
    
}

- (void) launcherViewDidStartEditing:(NTWLauncherView*) launcherView {
    if (!self.launcherParentView.launcherViewLeft.editing) {
        [self.launcherParentView.launcherViewLeft startEditing];          
    }
    
    //    if (!self.launcherParentView.launcherViewRight.editing) {
    //        [self.launcherParentView.launcherViewRight startEditing];
    //    }
}

- (void) launcherViewDidStopEditing:(NTWLauncherView*) launcherView {
    if (self.launcherParentView.launcherViewLeft.editing) {
        [self.launcherParentView.launcherViewLeft stopEditing];          
    }
    
    //    if (self.launcherParentView.launcherViewRight.editing) {
    //        [self.launcherParentView.launcherViewRight stopEditing];
    //    }
}

- (NTWLauncherView*) targetLauncherViewForIcon:(NTWLauncherIcon *) icon {
    
    CGRect leftLauncherViewRectInKeyView = [icon.superview convertRect:self.launcherParentView.launcherViewLeft.frame
                                                              fromView:self.launcherParentView.launcherViewLeft.superview];
    
    BOOL inLeftLauncherView = (CGRectContainsPoint(leftLauncherViewRectInKeyView, icon.center));
    
    if (inLeftLauncherView ) {
        // both launcherviews are overlapping. this is not intended. 
        // in order to prevent a crash, the current draggingView will be returned.
    } else {
        if (inLeftLauncherView) {
            return self.launcherParentView.launcherViewLeft;
        }
        
    }
    return self.currentDraggingView;
}

@end

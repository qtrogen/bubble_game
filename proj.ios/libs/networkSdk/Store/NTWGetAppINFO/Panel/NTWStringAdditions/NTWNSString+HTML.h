

#import <Foundation/Foundation.h>

// Dependant upon NTWGTMNSString+HTML

@interface NSString (NTWHTML)

// Strips HTML tags & comments, removes extra whitespace and decodes HTML character entities.
- (NSString *)dbsstringByConvertingHTMLToPlainText;

// Decode all HTML entities using GTM.
- (NSString *)dbsstringByDecodingHTMLEntities;

// Encode all HTML entities using GTM.
- (NSString *)dbsstringByEncodingHTMLEntities;

// Minimal unicode encoding will only cover characters from table
// A.2.2 of http://www.w3.org/TR/xhtml1/dtds.html#a_dtd_Special_characters
// which is what you want for a unicode encoded webpage.
- (NSString *)dbsstringByEncodingHTMLEntities:(BOOL)isUnicode;

// Replace newlines with <br /> tags.
- (NSString *)dbsstringWithNewLinesAsBRs;

// Remove newlines and white space from string.
- (NSString *)dbsstringByRemovingNewLinesAndWhitespace;

// Wrap plain URLs in <a href="..." class="linkified">...</a>
//  - Ignores URLs inside tags (any URL beginning with =")
//  - HTTP & HTTPS schemes only
//  - Only works in iOS 4+ as we use NSRegularExpression (returns self if not supported so be careful with NSMutableStrings)
//  - Expression: (?<!=")\b((http|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)
//  - Adapted from http://regexlib.com/REDetails.aspx?regexp_id=96
- (NSString *)dbsstringByLinkifyingURLs;

// DEPRECIATED - Please use NSString dbsstringByConvertingHTMLToPlainText
- (NSString *)dbsstringByStrippingTags __attribute__((deprecated));

@end
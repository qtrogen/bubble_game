//
//  mainSettingsNTW.h
//  NetworkTest
//
//  Created by netnod on 10/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface mainSettingsNTW : NSObject

-(NSString *)valInfoPlistURL;

-(NSString *)valRemoteMediaURL;

-(int)valMINIMUM_DELAY_BETWEEN_NAGS;

-(float)valDELAY_BEFORE_SHOWING_NAG_SCREEN;

-(NSString *)valFLURRY_API_KEY;

-(NSString *)valRATE_THE_APP_ID;

-(BOOL)isaniPad;
@end

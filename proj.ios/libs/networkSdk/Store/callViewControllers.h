

#import <Foundation/Foundation.h>

@interface callViewControllers : NSObject

- (void) showNTWTEXTStore;
- (void) showNTWGRAPHICALStore;
- (void) showNTWGRAPHICALNag;
- (void) showNTWNoInfoNAG;
@end


#import "callViewControllers.h"
#import "NTWStoreViewController.h"
#import "NTWNagStoreViewController.h"
#import "NTWGraphicalStoreViewController.h"
#import "NoInfoPageDataViewController.h"


@implementation callViewControllers
- (id)init
{
	self = [super init];
	
	return self;
}
- (void) showNTWTEXTStore {
        BOOL isAniPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
        if (isAniPad) {
            NTWStoreViewController * vc = [[NTWStoreViewController alloc] initWithNibName:@"NTWStoreViewController-iPad" bundle:nil];
            UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:vc];
            navController.modalPresentationStyle = UIModalPresentationFormSheet; // Like 1/2 the size
            
            UIWindow * window = [UIApplication sharedApplication].keyWindow;
            UIViewController * rootViewController = window.rootViewController;
            
            [rootViewController presentModalViewController:navController animated:YES];
            [vc release];
        } else {
            NTWStoreViewController * vc = [[NTWStoreViewController alloc] initWithNibName:@"NTWStoreViewController" bundle:nil];
            UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:vc];
            UIWindow * window = [UIApplication sharedApplication].keyWindow;
            UIViewController * rootViewController = window.rootViewController;
            [rootViewController presentModalViewController:navController animated:YES];
            [vc release];
        }
}

- (void) showNTWGRAPHICALStore {
        BOOL isAniPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);    
        if (isAniPad) {
            NTWGraphicalStoreViewController * vc = [[NTWGraphicalStoreViewController alloc] initWithNibName:@"NTWGraphicalStoreViewController-iPad" bundle:nil];
            UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:vc];
            navController.modalPresentationStyle = UIModalPresentationFormSheet; // Like 1/2 the size
            UIWindow * window = [UIApplication sharedApplication].keyWindow;
            UIViewController * rootViewController = window.rootViewController;
            [rootViewController presentModalViewController:navController animated:YES];
            [vc release];
        } else {
            NTWGraphicalStoreViewController * vc = [[NTWGraphicalStoreViewController alloc] initWithNibName:@"NTWGraphicalStoreViewController" bundle:nil];
            UINavigationController * navController = [[[UINavigationController alloc] initWithRootViewController:vc] autorelease];
            UIWindow * window = [UIApplication sharedApplication].keyWindow;
            UIViewController * rootViewController = window.rootViewController;
            [rootViewController presentModalViewController:navController animated:YES];
            [vc release];
        }
}

- (void) showNTWGRAPHICALNag {
        BOOL isAniPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);    
        if (isAniPad) {
            NTWNagStoreViewController * vc = [[NTWNagStoreViewController alloc] initWithNibName:@"NTWNagStoreViewController-iPad" bundle:nil];
            
            vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:vc];
            navController.modalPresentationStyle = UIModalPresentationFormSheet; // Like 1/2 the size
            UIWindow * window = [UIApplication sharedApplication].keyWindow;
            UIViewController * rootViewController = window.rootViewController;
            
            [rootViewController presentModalViewController:navController animated:YES];
            
            [vc release];
        } else {
            NTWNagStoreViewController * vc = [[NTWNagStoreViewController alloc] initWithNibName:@"NTWNagStoreViewController" bundle:nil];
            vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            UINavigationController * navController = [[[UINavigationController alloc] initWithRootViewController:vc] autorelease];
            UIWindow * window = [UIApplication sharedApplication].keyWindow;
            UIViewController * rootViewController = window.rootViewController;
            
            [rootViewController presentModalViewController:navController animated:YES];
            [vc release];
        }
}


- (void) showNTWNoInfoNAG {
    BOOL isAniPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);    
    if (isAniPad) {
        NoInfoPageDataViewController * vc = [[NoInfoPageDataViewController alloc] initWithNibName:@"NoInfoPageDataViewController-iPad" bundle:nil];
        UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:vc];
        navController.modalPresentationStyle = UIModalPresentationFormSheet; // Like 1/2 the size
        
        UIWindow * window = [UIApplication sharedApplication].keyWindow;
        UIViewController * rootViewController = window.rootViewController;
        
        [rootViewController presentModalViewController:navController animated:YES];
        [vc release];
    } else {
        NoInfoPageDataViewController * vc = [[NoInfoPageDataViewController alloc] initWithNibName:@"NoInfoPageDataViewController" bundle:nil];
        UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:vc];
        navController.modalPresentationStyle = UIModalPresentationFormSheet; // Like 1/2 the size
        
        UIWindow * window = [UIApplication sharedApplication].keyWindow;
        UIViewController * rootViewController = window.rootViewController;
        
        [rootViewController presentModalViewController:navController animated:YES];
        [vc release];
        
    }
}

- (void)dealloc
{
	[super dealloc];
}
@end

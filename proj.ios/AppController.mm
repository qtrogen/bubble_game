#include <sys/types.h>
#include <sys/sysctl.h>

#import "AppController.h"
#import "EAGLView.h"
//#import "cocos2d.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "LJniMediator.h"

//#import <RevMobAds/RevMobAds.h>
// commented by KUH in 2014-0925
//#import "Chartboost.h"
#import "Flurry.h"
#import "NTWDefinitions.h"

//AppLovin
#import "ALSdk.h"
#import "ALInterstitialAd.h"
//#import "AskingPoint/AskingPoint.h"

// facebook link
#import "FacebookController.h"

// mix panel
#import "Mixpanel.h"

#import "PlayHavenSDK.h"
#import "IAPHelper.h"

#define KEY_REMOVE_ADS @"_ctl_isRemoveAds_"


enum
{
    MSG_REMOVE_ADS	=	200,
    MSG_SUBMIT_ACHIEVE,
    MSG_SUBMIT_SCORE,
    MSG_SUBMIT_LEADERBOARD,
    MSG_SHOW_LDB,
    MSG_NOTIF_ACHIEVE,
    MSG_AD_HEIGHT,
    MSG_APP_VERSION,

    MSG_SHOW_INDICATOR,
    MSG_HIDE_INDICATOR,

    MSG_LOWSPEC_DEVICE,

    MSG_BUY_REQUEST,
    MSG_BUY_RESULT,
    MSG_RESTORE_REQ,
    MSG_RESTORE_RES,

    MSG_SHOW_REVMOB,
    MSG_SHOW_CHARTBOOST,
    MSG_SHOW_MOREAPPS,
    MSG_SHOW_APPLOVIN,
    MSG_SHOW_PLAYHAVEN,

    MSG_SHOW_FB,
    MSG_SHOW_TW,
    MSG_SHOW_RATE,
	MSG_RATED_APP,

    MSG_CONNECT_FB,
    MSG_FB_CONNECTED,
    MSG_FRIEND_LIST_REQ,
    MSG_FRIEND_LIST,
    MSG_UPDATE_MYINFO,
    MSG_PROFILE_PHOTO,

    MSG_FB_CHECK_REQUESTS,
    MSG_FB_ASK_LIVE,
    MSG_FB_GIVE_LIVE,
    MSG_FB_LIVEREQ_LIST,
    MSG_FB_COLLECT_LIVE_LIST,
    MSG_FB_COLLECT_LIVE,
    MSG_FB_COLLECTLIVE_SUC,
    MSG_FB_GIVLIVE_SUC,

    MSG_SND_PLAY,
    MSG_SND_STOP,

//    MSG_UD_LOGIN_REQ,
//    MSG_UD_LOGIN_RES,
    MSG_UD_UPLOAD_REQ,
    MSG_UD_UPLOAD_RES,
    MSG_UD_DOWNLOAD_REQ,
    MSG_UD_DOWNLOAD_RES,
    
    MSG_PUSH_NOTIF,
    MSG_SEND_FUSE_EVENT,
    MSG_SEND_FUSE_EVENT_LEVEL,
    MSG_SEND_FUSE_PURCAHSE,

    MSG_ACHIEVE_RESET,
};

NSString * const iRateiOSAppStoreURLFormat = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@";

#define PARAM_SEPERATOR '|'
#define RECORD_SEPERATOR @"^"

#define M_SUCCESS	1
#define M_FAIL		2
#define M_ERROR		3

@interface TCocosParam : NSObject
{
    
}
@property (nonatomic)           int nType;
@property (nonatomic, retain)   NSString* strParam;
@property (nonatomic)           int nParam;
@end
@implementation TCocosParam
@synthesize nType;
@synthesize strParam;
@synthesize nParam;
@end

@interface AppController () <FacebookDelegate, PHIAPDelegate>
@property (nonatomic) bool isShowIndicator;
@property (nonatomic) int  indicatorFrame;

@property (nonatomic, retain) FacebookController* facebookController;
@property (nonatomic, retain) NSMutableDictionary* friendList;

@property (nonatomic) NSTimeInterval lastNotifTime;
@property (nonatomic) bool isInForground;

@property (nonatomic, retain) NSArray* inactiveMessages;
@property (nonatomic, retain) NSArray* activeMessages;

@end

@implementation AppController

// cocos2d application instance
static AppDelegate s_sharedApplication;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    _isShowIndicator = false;
    soundPlayers = [[NSMutableDictionary dictionary] retain];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    NTWGetAppINFO* sharedNTWGetAppINFO = [NTWGetAppINFO sharedInstance];
    [sharedNTWGetAppINFO downloadAndSaveApplicationInfoPlist:appInfoPlistURL];

    // Override point for customization after application launch.

    NSUserDefaults* def = [NSUserDefaults standardUserDefaults];
    isRemoveAds = [def boolForKey: KEY_REMOVE_ADS];
#ifdef PRO_VERSION
    isRemoveAds = YES;
#endif

    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    
    // Init the EAGLView
    EAGLView *__glView = [EAGLView viewWithFrame: [window bounds]
                                     pixelFormat: kEAGLColorFormatRGB565
                                     depthFormat: GL_DEPTH24_STENCIL8_OES
                              preserveBackbuffer: NO
                                      sharegroup: nil
                                   multiSampling: NO
                                 numberOfSamples: 0];

    // Use RootViewController manage EAGLView 
    viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    viewController.wantsFullScreenLayout = YES;
    viewController.view = __glView;

    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:viewController];
    }
    
    self.loadingView = [[LLoadingView alloc] initWithFrame:[window bounds]];
    [viewController.view addSubview:self.loadingView];
    [self.loadingView setHidden:YES];
    
    int adHeight = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)? 90 : 50;
    if (isRemoveAds)
        adHeight = 0;
    
    [self sendToCocos:MSG_AD_HEIGHT strParam:@"" intParam:adHeight * [window screen].scale];
    [self sendCurrentVersion];
    
    [self checkLowSpecDevice];
    [Flurry startSession:FLURRY_API_KEY];
//    [RevMobAds startSessionWithAppID:REVMOB_ID];
//    [[RevMobAds session] showFullscreen];
    
    /* commented by KUH in 2014-09-25
    Chartboost* chartboost = [Chartboost sharedChartboost];
    chartboost.appId = CHARTBOOST_ID;
    chartboost.appSignature = CHARTBOOST_SIGN;
    chartboost.delegate = self;*/

    if (!isRemoveAds)
    {
#ifndef PRO_VERSION
        // add admob
        CGRect rcBound = [window bounds];
        CGRect rc = CGRectMake(0, rcBound.size.height - adHeight, rcBound.size.width, adHeight);
        _bannerView = [[GADBannerView alloc] initWithFrame:rc];
        
        // Specify the ad's "unit identifier". This is your AdMob Publisher ID.
        _bannerView.adUnitID = ADMOB_ID;
        
        // Let the runtime know which UIViewController to restore after taking
        // the user wherever the ad goes and add it to the view hierarchy.
        
        _bannerView.rootViewController = viewController;
        [viewController.view addSubview:_bannerView];
        
        // Initiate a generic request to load it with an ad.
        GADRequest* req = [GADRequest request];
        req.testDevices = [NSArray arrayWithObjects:GAD_SIMULATOR_ID, @"58f5ad61cf4cb930466012111a45f646", nil];
        [_bannerView loadRequest:req];
#endif //PRO_VERSION
        
        // add rebmov
        [ALSdk initializeSdk];
        
    }
    
    [self initGameCenter];
    
    [Mixpanel sharedInstanceWithToken:@"aac18f5d12d81a97327d4e4815bb454f"];
    
    [window makeKeyAndVisible];

//    NTWGetAppINFO* sharedNTWGetAppINFO = [NTWGetAppINFO sharedInstance];
//    [sharedNTWGetAppINFO downloadAndSaveApplicationInfoPlist:appInfoPlistURL appNAGsEnabled:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:true];
    
    [[LStoreManagerIOS sharedInstance] setDelegate:self];
    
    cocos2d::CCApplication::sharedApplication()->run();
    
    [self prepareFacebook];

//    [ASKPManager startup:@"_QBaAEUGUyYTNf7_Q4MqS2ddNBmW0L6mR0yXRaroBZY="];

    [self initPlayhaven];

    [self initIdleNotification];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    _lastNotifTime = [NSDate timeIntervalSinceReferenceDate];
    _isInForground = NO;
    cocos2d::CCDirector::sharedDirector()->pause();
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    cocos2d::CCDirector::sharedDirector()->resume();
    /* commented by KUH in 2014-09-25
    [[Chartboost sharedChartboost] startSession];
    [self showChartboost];
    [self showApplovin];*/

    _isInForground = YES;
    [FBAppCall handleDidBecomeActive];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    cocos2d::CCApplication::sharedApplication()->applicationDidEnterBackground();
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    cocos2d::CCApplication::sharedApplication()->applicationWillEnterForeground();
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    bool isHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication fallbackHandler:^(FBAppCall *call) {
        if (call.appLinkData && call.appLinkData.targetURL) {
            [[NSNotificationCenter defaultCenter] postNotificationName:APP_HANDLED_URL object:call.appLinkData.targetURL];
        }
        
    }];
    
    return isHandled;
}
- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
    [[FBSession activeSession] close];
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}

-(void) application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    NSLog(@"received notification");
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

-(void) sendToCocos:(int) nType strParam:(NSString*) strParam intParam:(int) nParam
{
    TCocosParam* cocosParam = [[TCocosParam alloc] init];
    [cocosParam setNType:nType];
    [cocosParam setStrParam:strParam];
    [cocosParam setNParam:nParam];
    [self performSelectorOnMainThread:@selector(sendToCocosOnMain:) withObject:cocosParam waitUntilDone:YES];
}

-(void) sendToCocosOnMain:(TCocosParam*) cocosParam
{
    LJniMediator::sharedInstance()->sendToCocos(cocosParam.nType, [cocosParam.strParam UTF8String], cocosParam.nParam);
    [cocosParam release];
}

-(bool) receivedFromCocos2dx:(int) nType strParam:(NSString*) strParam intParam:(int) nParam
{
    TCocosParam* cocosParam = [[TCocosParam alloc] init];
    [cocosParam setNType:nType];
    [cocosParam setStrParam:strParam];
    [cocosParam setNParam:nParam];
    [self performSelectorOnMainThread:@selector(receivedFromCocos2dxOnMain:) withObject:cocosParam waitUntilDone:YES];
    
    return YES;
}

-(bool) receivedFromCocos2dxOnMain:(TCocosParam*) cocosParam
{
    int nType = [cocosParam nType];
    NSString* strParam = [cocosParam strParam];
    int nParam = [cocosParam nParam];
    
    bool bRet = true;
    switch (nType) {
        case MSG_REMOVE_ADS:
            [self removeAds];
            break;
        case MSG_SUBMIT_SCORE:
            [self submitScore: nParam forLevel:[strParam intValue]];
            break;
        case MSG_SUBMIT_ACHIEVE:
            [self submitAchievement: strParam];
            break;
        case MSG_SUBMIT_LEADERBOARD:
            [self submitLeaderboard:strParam value:nParam];
            break;
        case MSG_SHOW_LDB:
            [self showLDB];
            break;
        /* commented by KUH in 2014-09-25
        case MSG_SHOW_CHARTBOOST:
            [[Chartboost sharedChartboost] showInterstitial];
            break;*/
        case MSG_SHOW_MOREAPPS:
            [self showMoreApps];
            break;
        case MSG_SHOW_REVMOB:
            [self showRevmob];
            break;
        case MSG_SHOW_APPLOVIN:
            [self showApplovin];
            break;
        case MSG_SHOW_PLAYHAVEN:
            [self showPlayHaven];
            break;
        case MSG_SHOW_FB:
            break;
        case MSG_SHOW_TW:
            break;
        case MSG_SHOW_RATE:
            [self showRateApp];
            break;
        case MSG_BUY_REQUEST:
//            bRet = [[LStoreManagerIOS sharedInstance] buyProduct:strParam];
            [self buyProductViaPH:strParam];
            break;
        case MSG_RESTORE_REQ:
            bRet = [[LStoreManagerIOS sharedInstance] restore];
            break;
        case MSG_SHOW_INDICATOR:
            [self showIndicator];
            break;
        case MSG_HIDE_INDICATOR:
            [self hideIndicator];
            break;
        case MSG_CONNECT_FB:
            if (nParam)
                [self connectFacebook];
            else
                [self logoutFacebook];
            break;
        case MSG_FRIEND_LIST_REQ:
            [_facebookController GetScores];
            break;
        case MSG_FB_CHECK_REQUESTS:
            [self checkIncomingRequest];
            break;
        case MSG_FB_ASK_LIVE:
            [_facebookController askLiveToFriends];
            break;
        case MSG_FB_GIVE_LIVE:
            [self giveLive:strParam];
            break;
        case MSG_FB_COLLECT_LIVE:
            [self collectLive:strParam];
            break;
        case MSG_SND_PLAY:
            [self playSound:strParam volume:nParam];
            break;
        case MSG_SND_STOP:
            [self stopSound];
            break;
        case MSG_UD_DOWNLOAD_REQ:
            [[BaasGameClient sharedClient] setFbid:strParam];
            [[BaasGameClient sharedClient] requestGameData];
            break;
        case MSG_UD_UPLOAD_REQ:
            [[BaasGameClient sharedClient] uploadData:strParam];
            break;
        case MSG_NOTIF_ACHIEVE:
            [self showGKNotification:strParam];
            break;
        case MSG_PUSH_NOTIF:
            [self showPushNotification:strParam];
            break;
        case MSG_SEND_FUSE_EVENT:
        {
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:strParam properties:nil];
        }
            break;
        case MSG_SEND_FUSE_EVENT_LEVEL:
        {
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:strParam properties:@{@"Level":[NSNumber numberWithInt:nParam]}];
        }
            break;
        case MSG_SEND_FUSE_PURCAHSE:
        {
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:strParam properties:@{@"Count":[NSNumber numberWithInt:nParam]}];
        }
            break;
        case MSG_ACHIEVE_RESET:
            [self resetAchievements];
            break;

    }
    return bRet;
}

-(void) removeAds
{
    if (_bannerView)
    {
        [_bannerView removeFromSuperview];
        
        [_bannerView release];
        _bannerView = nil;
    }
    
    isRemoveAds = YES;
    NSUserDefaults* def = [NSUserDefaults standardUserDefaults];
    [def setBool:YES forKey: KEY_REMOVE_ADS];
    [def synchronize];
}

-(void) checkLowSpecDevice
{
    NSString* strHWVersion = [self getModel];
    
    BOOL isLowSpecDevice = NO;
    
    if ([strHWVersion hasPrefix:@"iPhone"])
    {
        int version = [[strHWVersion stringByReplacingOccurrencesOfString:@"iPhone" withString:@""] intValue];
        if (version <= 3) isLowSpecDevice = YES;
    }
    else if ([strHWVersion hasPrefix:@"iPod"])
    {
        int version = [[strHWVersion stringByReplacingOccurrencesOfString:@"iPod" withString:@""] intValue];
        if (version <= 4) isLowSpecDevice = YES;
    }

    [self sendToCocos:MSG_LOWSPEC_DEVICE strParam:@"" intParam:isLowSpecDevice];
}

- (NSString *)getModel {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *model = (char*) malloc(size);
    sysctlbyname("hw.machine", model, &size, NULL, 0);
    NSString *sDeviceModel = [NSString stringWithCString:model encoding:NSUTF8StringEncoding];
    free(model);
    
    return sDeviceModel;

//    if ([sDeviceModel isEqual:@"i386"])      return @"Simulator";  //iPhone Simulator
//    if ([sDeviceModel isEqual:@"iPhone1,1"]) return @"iPhone1G";   //iPhone 1G
//    if ([sDeviceModel isEqual:@"iPhone1,2"]) return @"iPhone3G";   //iPhone 3G
//    if ([sDeviceModel isEqual:@"iPhone2,1"]) return @"iPhone3GS";  //iPhone 3GS
//    if ([sDeviceModel isEqual:@"iPhone3,1"]) return @"iPhone4 AT&T";  //iPhone 4 - AT&T
//    if ([sDeviceModel isEqual:@"iPhone3,2"]) return @"iPhone4 Other";  //iPhone 4 - Other carrier
//    if ([sDeviceModel isEqual:@"iPhone3,3"]) return @"iPhone4";    //iPhone 4 - Other carrier
//    if ([sDeviceModel isEqual:@"iPhone4,1"]) return @"iPhone4S";   //iPhone 4S
//    if ([sDeviceModel isEqual:@"iPhone5,1"]) return @"iPhone5";    //iPhone 5 (GSM)
//    if ([sDeviceModel isEqual:@"iPod1,1"])   return @"iPod1stGen"; //iPod Touch 1G
//    if ([sDeviceModel isEqual:@"iPod2,1"])   return @"iPod2ndGen"; //iPod Touch 2G
//    if ([sDeviceModel isEqual:@"iPod3,1"])   return @"iPod3rdGen"; //iPod Touch 3G
//    if ([sDeviceModel isEqual:@"iPod4,1"])   return @"iPod4thGen"; //iPod Touch 4G
//    if ([sDeviceModel isEqual:@"iPad1,1"])   return @"iPadWiFi";   //iPad Wifi
//    if ([sDeviceModel isEqual:@"iPad1,2"])   return @"iPad3G";     //iPad 3G
//    if ([sDeviceModel isEqual:@"iPad2,1"])   return @"iPad2";      //iPad 2 (WiFi)
//    if ([sDeviceModel isEqual:@"iPad2,2"])   return @"iPad2";      //iPad 2 (GSM)
//    if ([sDeviceModel isEqual:@"iPad2,3"])   return @"iPad2";      //iPad 2 (CDMA)
//    
//    NSString *aux = [[sDeviceModel componentsSeparatedByString:@","] objectAtIndex:0];
//    
//    //If a newer version exist
//    if ([aux rangeOfString:@"iPhone"].location!=NSNotFound) {
//        int version = [[aux stringByReplacingOccurrencesOfString:@"iPhone" withString:@""] intValue];
//        if (version == 3) return @"iPhone4";
//        if (version >= 4) return @"iPhone4s";
//        
//    }
//    if ([aux rangeOfString:@"iPod"].location!=NSNotFound) {
//        int version = [[aux stringByReplacingOccurrencesOfString:@"iPod" withString:@""] intValue];
//        if (version >=4) return @"iPod4thGen";
//    }
//    if ([aux rangeOfString:@"iPad"].location!=NSNotFound) {
//        int version = [[aux stringByReplacingOccurrencesOfString:@"iPad" withString:@""] intValue];
//        if (version ==1) return @"iPad3G";
//        if (version >=2) return @"iPad2";
//    }
//    //If none was found, send the original string
//    return sDeviceModel;
}

- (BOOL)shouldRequestInterstitialsInFirstSession {
    return NO;
}

-(void) initGameCenter
{
    viewController2 = [GCViewController alloc];
	if ([GameCenterManager isGameCenterAvailable])
	{
        
		gameCenterManager = [[[GameCenterManager alloc] init] autorelease];
		[gameCenterManager setDelegate:self];
		[gameCenterManager authenticateLocalUser];
	}
    else
    {
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"" message:@"Game Center is not avaliable." delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil] autorelease];
        [alert show];
    }
    
}

- (void)showLeaderboard{
    //	[[UIApplication sharedApplication] setStatusBarOrientation: UIInterfaceOrientationLandscapeRight];
    if ([GameCenterManager isGameCenterAvailable])
	{
        [viewController2.view setHidden:NO];
        [self.window addSubview:viewController2.view];
        [self showLDB];
    }
    else
    {
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"" message:@"Game Center is not avaliable." delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil] autorelease];
        [alert show];
    }
    
}
- (void) showLDB
{
	GKLeaderboardViewController *leaderboardController = [[GKLeaderboardViewController alloc] init];
	if (leaderboardController != NULL) {
//		leaderboardController.category = @"com.gamesfun.fruitbubbleblast.score";//kCandyLeaderboard;
//		leaderboardController.timeScope = GKLeaderboardTimeScopeAllTime;
		leaderboardController.leaderboardDelegate = self;
        leaderboardController.viewState = GKGameCenterViewControllerStateDefault;
        [viewController presentViewController:leaderboardController animated:YES completion:nil];
//		[viewController presentModalViewController: leaderboardController animated: YES ];
	}
}

- (void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController1{
	[viewController1 dismissModalViewControllerAnimated: YES];
	[viewController2.view removeFromSuperview];
	[viewController2.view setHidden:YES];
	[viewController1 release];
}

- (void) submitScore:(int) score forLevel:(int) level
{
    [_facebookController SendScore:score level:level];
//	if(score <= 0)
//	{
//        return;
//    }
//    if ([GameCenterManager isGameCenterAvailable])
//	{
//		[gameCenterManager reportScore:score forCategory: kCandyLeaderboard];
//    }
}

- (void) submitLeaderboard:(NSString*) category value:(int) nVal
{
	if(nVal <= 0)
	{
        return;
    }
    if ([GameCenterManager isGameCenterAvailable])
	{
		[gameCenterManager reportScore:nVal forCategory: category];
    }
}
- (void) submitAchievement:(NSString*) strId
{
    if ([GameCenterManager isGameCenterAvailable])
	{
		GKAchievement* achievement= [[[GKAchievement alloc] initWithIdentifier: strId] autorelease];
		achievement.percentComplete= 100.0f;
		//Add achievement to achievement cache...

		[achievement reportAchievementWithCompletionHandler: ^(NSError *error)
		{
		}];

    }
}

-(void) showGKNotification:(NSString*) msg
{
    NSString* strTitle = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Bundle display name"];

    [GKNotificationBanner showBannerWithTitle:strTitle message:msg duration:1.0f completionHandler:^{
    }];
}

-(void) achievementSubmitted:(NSError*) err
{
}

- (void) showApplovin
{
//    [ALInterstitialAd showOver:viewController.view.window];
}

- (void) showRevmob
{
//    [[RevMobAds session] showFullscreen];
}

- (void) showChartboost
{
    [[Chartboost sharedChartboost] showInterstitial];
}

- (void) showPlayHaven
{
    NSLog(@"== showPlayHaven");
}

- (void) showMoreApps
{
    NTWGetAppINFO* sharedNTWGetAppINFO = [NTWGetAppINFO sharedInstance];
    [sharedNTWGetAppINFO setDelegate:self];
    [sharedNTWGetAppINFO showTHEStore];
}

- (void) didFailShowMorePage:(NSString *)Error
{
    NSLog(@"didFailShowMorePage : %@", Error);
}

-(void) dealloc {
    [window release];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

-(void) purchaseSuccessed:(NSString*) productId
{
    [self sendToCocos:MSG_BUY_RESULT strParam:productId intParam:M_SUCCESS];
}
-(void) purchaseFailed:(NSString*) productId message:(NSString*) errMsg
{
    [self sendToCocos:MSG_BUY_RESULT strParam:productId intParam:M_FAIL];
}
-(void) restoreSuccessed:(NSArray*) productIdList
{
    for (NSString* productId in productIdList)
    {
        [self sendToCocos:MSG_RESTORE_RES strParam:productId intParam:M_SUCCESS];
    }
}
-(void) restoreFailed:(NSString*) errMsg
{
    [self sendToCocos:MSG_RESTORE_RES strParam:errMsg intParam:M_FAIL];
}

-(void) showIndicator
{
    _isShowIndicator = true;
    [self.loadingView showIndicator];
}

-(void) hideIndicator
{
    _isShowIndicator = false;
    [self.loadingView stopIndicator];
}

//--------------------  facebook  ---------------------

-(void) didLogIn:(bool) bLoggedIn
{
    LJniMediator::sharedInstance()->sendToCocos(MSG_FB_CONNECTED, "", bLoggedIn);
    if (bLoggedIn)
    {
        [_facebookController FetchUserDetails:self selector:@selector(didFetchUserDetails)];
    }
}
-(void) didLogout:(bool) bLoggedOut
{
    LJniMediator::sharedInstance()->sendToCocos(MSG_FB_CONNECTED, "", 0);
    [[BaasGameClient sharedClient] logout:NO];
}
-(void) didFetchUserDetails:(bool) bSuccess
{
    NSString* strMyId = [NSString stringWithFormat:@"%llu", [_facebookController ms_uPlayerFBID]];
    LJniMediator::sharedInstance()->sendToCocos(MSG_UPDATE_MYINFO, [strMyId UTF8String], 1);
    [[BaasGameClient sharedClient] setDelegate:self];
    [[BaasGameClient sharedClient] setFbid:strMyId];
}
-(void) didProcessFBURL:(NSString*) challengeName fbid:(NSString*) challengeFBID
{
    
}
-(void) didGetScores:(NSArray*) array
{
    NSString* outStr = [NSString new];
    
    int index = 0;
    for (NSDictionary *dict in array)
    {
        NSDictionary* dictU = [dict objectForKey:@"user"];
        NSString *name = [dictU objectForKey:@"name"];
        NSString *fid = [dictU objectForKey:@"id"];
        NSString *strScore = [dict objectForKey:@"score"];
        
        if (index > 0)
            outStr = [outStr stringByAppendingString:RECORD_SEPERATOR];
        
        NSString* record = [NSString stringWithFormat:@"%@%c%@%c%@",
                            fid, PARAM_SEPERATOR,
                            name, PARAM_SEPERATOR,
                            strScore
                            ];
        outStr = [outStr stringByAppendingString:record];
        
//        if ([_friendList objectForKey:fid] == nil)
//        {
            [_facebookController requestPhotoForId:fid];
//        }
        
        if (++index > 5) break;
    }

    [self sendToCocos:MSG_FRIEND_LIST strParam:outStr intParam:index];
}

-(void) receivedProfilePicture:(NSMutableDictionary*) dic
{
    [self performSelectorOnMainThread:@selector(sendProfilePicture:) withObject:dic waitUntilDone:YES];
}

-(void) sendProfilePicture:(NSMutableDictionary*) dic
{
    NSString* fbid = [dic objectForKey:@"fbid"];
    NSData* dat = [dic objectForKey:@"data"];
    const char* strId = [fbid cStringUsingEncoding:NSUTF8StringEncoding];
    int lenId = [fbid length] + 1;
    int lenImgBuf = [dat length];
    
    int nTotalLen = 4 + lenId + lenImgBuf;
    char* buf = new char[nTotalLen];
    
    memset(buf, 0, nTotalLen);
    memcpy(buf, &lenId, 4);
    memcpy(buf + 4, strId, lenId);
    buf[4 + lenId - 1] = 0;
    [dat getBytes:buf + 4 + lenId length:lenImgBuf];
    
    [_friendList  setObject:@"loaded" forKey:fbid];
    
    LJniMediator::sharedInstance()->sendToCocos(MSG_PROFILE_PHOTO, (const char*)buf, nTotalLen);
}

-(void) prepareFacebook
{
    _friendList = [[NSMutableDictionary dictionary] retain];
    
    _facebookController = [[[FacebookController alloc] init] retain];
    [_facebookController setDelegate:self];
    NSLog(@"prepareFacebook");
    if ([_facebookController IsLoggedIn]) {
        [self didLogIn:true];
    } else {
        [_facebookController CreateNewSession];
        [_facebookController OpenSession:self selector:@selector(didLogIn)];
    }
}

-(void) connectFacebook
{
   [_facebookController Login:self selector:@selector(didLogIn:)];
}

-(void) logoutFacebook
{
    [_facebookController Logout:self selector:@selector(didLogout:)];
}

-(void) checkIncomingRequest
{
    [_facebookController checkIncommingRequests:self selector:@selector(didReceivedIncomingRequest:)];
}

-(void) didReceivedIncomingRequest:(id) result
{
    NSLog(@"didReceivedIncomingRequest == %@", result);
    NSString* strCollectList = @"";
    NSString* strGiveReq = @"";
    NSArray* data = [result objectForKey:@"data"];
    
    for (NSDictionary* dic in data)
    {
        NSLog(@"dic = %@", dic);
        NSString* datastr = [dic objectForKey:@"data"];
        NSData* indata = [datastr dataUsingEncoding:NSUTF8StringEncoding];
        if (!indata)
        {
            NSLog(@"JSON decode error--");
            continue;
        }
        
        
        NSError* jsonError = nil;
        NSDictionary* emdata = [NSJSONSerialization JSONObjectWithData:indata options:0 error:&jsonError];
        if (jsonError)
        {
            NSLog(@"JSON decode error: %@", jsonError);
            continue;
        }
        
        NSString* strType = [emdata objectForKey:@"badge_of_awesomeness"];
        NSString* reqId = [dic objectForKey:@"id"];
        NSDictionary* from = [dic objectForKey:@"from"];
        NSString* fromId = [from objectForKey:@"id"];
        NSString* fromName = [from objectForKey:@"name"];
        
        NSString* resStr = [NSString stringWithFormat:@"%@|%@|%@", reqId, fromId, fromName];

        if ([strType integerValue] == 1)
        {
            if ([strGiveReq length] > 0)
                strGiveReq = [strGiveReq stringByAppendingString:@"^"];
            strGiveReq = [strGiveReq stringByAppendingString:resStr];
        }
        else if ([strType integerValue] == 2)
        {
            if ([strCollectList length] > 0)
                strCollectList = [strCollectList stringByAppendingString:@"^"];
            strCollectList = [strCollectList stringByAppendingString:resStr];
        }
    }
    
    if ([strCollectList length] > 0)
        [self sendToCocos:MSG_FB_COLLECT_LIVE_LIST strParam:strCollectList intParam:[strCollectList length]];

    if ([strGiveReq length] > 0)
        [self sendToCocos:MSG_FB_LIVEREQ_LIST strParam:strGiveReq intParam:[strGiveReq length]];
}

-(void) collectLive:(NSString*) requestId
{
    [_facebookController collectLive:requestId target:self selector:@selector(collectLiveSuc:)];
}

-(void) collectLiveSuc:(NSString*) requestId
{
    [self sendToCocos:MSG_FB_COLLECTLIVE_SUC strParam:requestId intParam:0];
}

-(void) giveLive:(NSString*) requestMix
{
    NSArray* arr = [requestMix componentsSeparatedByString:@"|"];
    NSString* reqId = [arr objectAtIndex:0];
    NSString* receiverId = [arr objectAtIndex:1];
    [_facebookController giveLive:reqId fbid:receiverId target:self selector:@selector(giveLiveSuc:)];
}

-(void) giveLiveSuc:(NSString*) requestId
{
    [self sendToCocos:MSG_FB_GIVLIVE_SUC strParam:requestId intParam:0];
}


- (void) playSound:(NSString*)strSound volume:(int)nVolume
{
    NSString* strChannel = @"1";
    NSString* fullpath = [[NSBundle mainBundle] pathForResource:strSound
                                                         ofType:nil ];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:fullpath];
    
    NSError *error;
    
    [self stopSound] ;//]:strChannel];

    AVAudioPlayer* audioPlayer = [[AVAudioPlayer alloc]
                     initWithContentsOfURL:soundFileURL
                     error:&error];
    
    audioPlayer.delegate = self;
    if (error)
        NSLog(@"Error: %@",
              [error localizedDescription]);
    [soundPlayers setObject:audioPlayer forKey:strChannel];
    
    [audioPlayer play];
    [audioPlayer setVolume:nVolume * 0.01f];
}

- (void) stopSound//:(NSString*) strChannel
{
    NSString* strChannel = @"1";
    AVAudioPlayer* audioPlayer = [soundPlayers objectForKey:strChannel];
    
    if (audioPlayer) {
        if ([audioPlayer isPlaying]) {
            [audioPlayer stop];
        }
        [soundPlayers removeObjectForKey:strChannel];
    }
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    NSEnumerator* enums = [soundPlayers keyEnumerator];
    for (NSString* key in enums)
    {
        AVAudioPlayer* audioPlayer = [soundPlayers objectForKey:key];
        if (audioPlayer == player)
        {
            [soundPlayers removeObjectForKey:key];
        }
    }
}

// BaasGameDelegate protocol

-(void) loggedInToBaas:(NSString *)username success:(bool)bSuccess
{
}
-(void) receivedGameData:(NSString*) gamedata
{
    [self sendToCocos:MSG_UD_DOWNLOAD_RES strParam:gamedata intParam:0];
}

-(void) showRateApp
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"RATE APP"
													message:@"To earn Magic Credits, rate the app on app store and then return to the game."
												   delegate:self
										  cancelButtonTitle:@"No Thanks"
										  otherButtonTitles:@"Rate it now", nil];
    [alert show];
	[alert release];

//    [[iRate sharedInstance] setDelegate:self];
//    [[iRate sharedInstance] promptForRating];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSURL* ratingsURL = [NSURL URLWithString:[NSString stringWithFormat:iRateiOSAppStoreURLFormat, RATE_THE_APP_ID]];
        [[UIApplication sharedApplication] openURL:ratingsURL];
        [self sendToCocos:MSG_RATED_APP strParam:@"" intParam:0];
    }
    NSLog(@"clicked %d", buttonIndex);
}

-(void) sendCurrentVersion
{
    NSString* curVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSArray* verNum = [curVersion componentsSeparatedByString:@"."];
    
    int nVer = 0;
    for (NSString* item in verNum)
    {
        nVer *= 100;
        nVer += [item intValue];
    }
    [self sendToCocos:MSG_APP_VERSION strParam:@"" intParam:nVer];
    
}

-(void) showPushNotification:(NSString*) message
{
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:3];
    localNotification.alertBody = message;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

#define NOTIF_LAST_TIME @"NOTIF_LAST_TIME"
#define NOTIF_NEXT_INACTIVE_ID @"NOTIF_NEXT_INACTIVE_ID"
#define NOTIF_NEXT_ACTIVE_ID @"NOTIF_LAST_ACTIVE_ID"

-(void) checkIdleNotification
{
    if (_isInForground) return;
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];

    NSTimeInterval now = [NSDate timeIntervalSinceReferenceDate];
    const NSTimeInterval oneDay = 3600;

    if (now - _lastNotifTime > oneDay * 7) // inactive player
    {
        int nLastInactiveIndex = [defaults integerForKey:NOTIF_NEXT_INACTIVE_ID];
        
        NSString* msg = [_inactiveMessages objectAtIndex:nLastInactiveIndex];
        nLastInactiveIndex++;
        if (nLastInactiveIndex >= _inactiveMessages.count - 1)
            nLastInactiveIndex = 0;
        
        [self showPushNotification:msg];
        _lastNotifTime = now;
        [defaults setInteger:nLastInactiveIndex forKey:NOTIF_NEXT_INACTIVE_ID];
        [defaults synchronize];
        
    } else if (now - _lastNotifTime > oneDay * 3) // active player
    {
        int nLastActiveIndex = [defaults integerForKey:NOTIF_NEXT_ACTIVE_ID];
        NSString* msg = [_activeMessages objectAtIndex:nLastActiveIndex];
        nLastActiveIndex++;
        if (nLastActiveIndex >= _activeMessages.count - 1)
            nLastActiveIndex = 0;
        
        [self showPushNotification:msg];
        
        _lastNotifTime = now;
        [defaults setInteger:nLastActiveIndex forKey:NOTIF_NEXT_ACTIVE_ID];
        [defaults synchronize];
    }
    
}

-(void) initIdleNotification
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSTimeInterval now = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval lastNotifTime = [defaults doubleForKey:NOTIF_LAST_TIME];
    if (lastNotifTime == 0)
        lastNotifTime = now;
    [defaults setDouble:lastNotifTime forKey:NOTIF_LAST_TIME];
    [defaults synchronize];

    [NSTimer scheduledTimerWithTimeInterval:3600 target:self selector:@selector(checkIdleNotification) userInfo:nil repeats:YES];

    _inactiveMessages = [NSArray arrayWithObjects:
                         @"Please come and save your friends!",
                         @"We miss our friend, come back soon",
                         @"Don’t you miss us? ",
                         @"We want to play!",
                         @"We haven’t seen you in a while; we are forgetting what you look like!",
                         @"You should be playing with us!",
                         @"Stop! Drop what you are doing! We need you to save our friends!",
                         @"Is it time to play with us yet?",
                         @"I think you are the best person for the job of saving my friends, come help me!",
                         @"What are you doing today? Playing with us I hope!",
                         @"Pingu is bored he wants you to play with him!",
                         @"Kali is bored she wants you to play with her!",
                         @"Roo is bored he wants you to play with him!",
                         @"Jack is bored he wants you to play with him!",
                         @"MoMo is bored she wants you to play with her!",
                         @"Piggles is bored she wants you to play with her!",
                         @"Come help Jack drop some bubbles",
                         @"Come help Kali drop some bubbles",
                         @"Come help Pingu drop some bubbles",
                         @"Come help Piggles drop some bubbles",
                         @"Come help Roo drop some bubbles",
                         @"Come help MoMo drop some bubbles",
                         @"Kali is ready to save her friends, she needs your help!",
                         @"Pingu is ready to save his friends, he needs your help!",
                         @"Roo is ready to save his friends, he needs your help!",
                         @"Jack is ready to save his friends, he needs your help!",
                         @"MoMo is ready to save her friends, she needs your help!",
                         @"Piggles is ready to save her friends, she needs your help!",
                         @"Jack the Monkey is going bananas without you!",
                         @"Roo the Rabbit is ready to hop to the next world, come play with him!",
                         @"Pingu the Penguin is frozen until you come back to play!",
                         @"Kali the Cat think you are a purrrfect friend, don’t you want to play with her?",
                         @"MoMo the Panda wants you to bring her some bamboo please!",
                         @"Piggles the Pig is playing in the mud, don’t you want to join in?",
                         @"Jack the Monkey is swinging through the trees looking for you!",
                         @"Roo the Rabbit thinks some carrots sound good, do you have any?",
                         @"Pingu the Penguin loves to share with his friends, want some fish?",
                         @"Kali the Cat is getting grumpy without you.",
                         @"MoMo the Panda says its time to play, don’t let her down.",
                         @"Piggles the Pig prefers to play come back and make her day!",
                         nil];
    _activeMessages = [NSArray arrayWithObjects:
                       @"Your lives are full, you should come use them!",
                       @"Do you like free stuff? Well we like giving it! Spin and you could win!",
                       @"Your daily challenge is ready for you. Come earn some free magic!",
                       @"You are full of energy, don’t you want to use some of it playing with us?",
                       @"Your daily spin is available. Don’t miss out!",
                       @"Do you like free stuff? Well we like giving it! Earn free magic today!",
                       @"Don’t let your lives pass you buy, use it to help your friends.",
                       @"Spin to win power ups and magic!",
                       @"Do you want to buy some magic or do you want to win it? Complete your daily challenge today!",
                       @"1-2-3-4-5 whose got all their lives? YOU DO!",
                       @"Be a winner and use your daily spin!",
                       @"Play your daily challenge and earn some free",
                       @"Your friends want you to win with your daily spin!",
                       nil];
}

-(void) initPlayhaven
{
    [[IAPHelper sharedIAPHelper] restorePurchases];

    [[IAPHelper sharedIAPHelper] setApplicationToken:@"889a18e890204200a6c9537e07b9af73"];
    [[IAPHelper sharedIAPHelper] setApplicationSecret:@"5c67d4dc4ed64f788eab73606d8fc48d"];
}
-(void) buyProductViaPH:(NSString*) identifier
{
    cocos2d::CCDirector::sharedDirector()->pause();
    PHPurchase *purchase = [PHPurchase new];
    
    purchase.productIdentifier = identifier;
    purchase.quantity          = 1;
    
    [[IAPHelper sharedIAPHelper] setDelegate:self];
    [[IAPHelper sharedIAPHelper] startPurchase:purchase];
    
    [purchase release];
}
// playhaven IAP tracking
-(void) didPurchaseWithSuccess:(NSString*) productId
{
    cocos2d::CCDirector::sharedDirector()->resume();
    [self sendToCocos:MSG_BUY_RESULT strParam:productId intParam:M_SUCCESS];
    
}
-(void) didPurchaseWithFail
{
    cocos2d::CCDirector::sharedDirector()->resume();
    [self sendToCocos:MSG_BUY_RESULT strParam:@"" intParam:M_FAIL];
}

-(void)resetAchievements {
    [GKAchievement resetAchievementsWithCompletionHandler: ^(NSError *error)
    {
        if (!error)
        {
            // overwrite any previously stored file
            [self sendToCocos:MSG_ACHIEVE_RESET strParam:@"" intParam:0];
        } else
        {
            // Error clearing achievements.
        }
    }];
}

@end

//
//  LLoadingView.h
//  EmojiBubbleBlast
//
//  Created by Luke Phang on 1/19/14.
//
//

#import <UIKit/UIKit.h>

@interface LLoadingView : UIView
-(void) showIndicator;
-(void) stopIndicator;
@end

//
//  LLoadingView.m
//  EmojiBubbleBlast
//
//  Created by Luke Phang on 1/19/14.
//
//

#import "LLoadingView.h"

@interface LLoadingView()
@property (nonatomic, retain) UIImageView* imgLoading1;
@property (nonatomic, retain) UIImageView* imgLoading2;
@property (nonatomic, retain) UIImageView* imgLoadingLetter;

@property (nonatomic, retain) UIImageView* imgZ1;
@property (nonatomic, retain) UIImageView* imgZ2;
@property (nonatomic, retain) UIImageView* imgZ3;

@property (nonatomic, retain) UIImage* imgDot;

@property (nonatomic, retain) UIView* imgDotFrame1;
@property (nonatomic, retain) UIView* imgDotFrame2;
@property (nonatomic) CGRect dotsRect;

@property (nonatomic, retain) UILabel*     lblLoading;
@property (nonatomic) CGRect rcBubble;
@property (nonatomic) CGPoint zPos1;
@property (nonatomic) CGPoint zPos2;
@property (nonatomic) CGPoint zPos3;

@property (nonatomic) float fScale;

@property (nonatomic) int nFrameId;
@property (nonatomic) bool isBubbleExpand;

@property (nonatomic) bool isShowIndicator;

@end

@implementation LLoadingView

#define DOT_COUNT 5
-(id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        _fScale = frame.size.width / 768.0f;

        UIImage* img1 = [self loadImage:@"Image/home/cat.png"];
        _imgLoading1 = [[UIImageView alloc] initWithImage:img1];
        float iw = img1.size.width * _fScale;
        float ih = img1.size.height * _fScale;
        [_imgLoading1 setFrame:CGRectMake(frame.size.width - iw - 10,
                                          frame.size.height - ih - 10,
                                          iw, ih)];
        [self addSubview:_imgLoading1];

        UIImage* img4 = [self loadImage:@"Image/home/loading.png"];
        _imgLoadingLetter = [[UIImageView alloc] initWithImage:img4];
        float loadingw = img4.size.width * _fScale;
        float loadingh = img4.size.height * _fScale;
        [_imgLoadingLetter setFrame:CGRectMake(25 * _fScale, frame.size.height - loadingh - 25 * _fScale, loadingw, loadingh)];
        [self addSubview:_imgLoadingLetter];
        
        _imgDotFrame1 = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
        _imgDotFrame2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];

        [self addSubview:_imgDotFrame1];
        [_imgDotFrame1 addSubview:_imgDotFrame2];

        _imgDot = [self loadImage:@"Image/home/loading_dot.png"];
        iw = _imgDot.size.width * _fScale;
        ih = _imgDot.size.height * _fScale;
        float dotInterval = 20 * _fScale;

        int xx = 0;
        for (int i = 0; i < DOT_COUNT; i++)
        {
            UIImageView* imgview = [[UIImageView alloc] initWithImage:_imgDot];
            [imgview setFrame:CGRectMake(xx, 0, iw, ih)];
            [_imgDotFrame2 addSubview:imgview];
            xx += dotInterval;
        }

        _dotsRect.origin.x = CGRectGetMaxX([_imgLoadingLetter frame]) + dotInterval;
        _dotsRect.origin.y = CGRectGetMaxY([_imgLoadingLetter frame]) - 10 * _fScale;
        _dotsRect.size.width = xx + iw;
        _dotsRect.size.height = ih * 2;

        [_imgDotFrame1 setClipsToBounds:YES];

        UIImage* img2 = [self loadImage:@"Image/home/bubble.png"];
        _imgLoading2 = [[UIImageView alloc] initWithImage:img2];
        _rcBubble = CGRectMake(_imgLoading1.frame.origin.x + 160 * _fScale,
                               _imgLoading1.frame.origin.y + 155 * _fScale,
                               img2.size.width * _fScale,
                               img2.size.height * _fScale);
        [_imgLoading2 setFrame:_rcBubble];
        [self addSubview:_imgLoading2];
        
        UIImage* img3 = [self loadImage:@"Image/home/z.png"];
        CGRect rcz = CGRectMake(100, 100, img3.size.width * _fScale, img3.size.height * _fScale);

        _imgZ1 = [[UIImageView alloc] initWithImage:img3];
        [_imgZ1 setFrame:rcz];
        [_imgZ1 setTransform:CGAffineTransformMakeScale(0.7, 0.7)];
        [self addSubview:_imgZ1];
        
        _imgZ2 = [[UIImageView alloc] initWithImage:img3];
        [_imgZ2 setFrame:rcz];
        [_imgZ2 setTransform:CGAffineTransformMakeScale(0.4, 0.4)];
        [self addSubview:_imgZ2];
        
        _imgZ3 = [[UIImageView alloc] initWithImage:img3];
        [_imgZ3 setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
        [_imgZ3 setFrame:rcz];
        [self addSubview:_imgZ3];
        
        _zPos1 = CGPointMake(_imgLoading1.center.x - 150 * _fScale, _imgLoading1.center.y - 120 * _fScale);
        _zPos2 = CGPointMake(_imgLoading1.center.x - 100 * _fScale, _imgLoading1.center.y - 80 * _fScale);
        _zPos3 = CGPointMake(_imgLoading1.center.x - 40 * _fScale, _imgLoading1.center.y - 140 * _fScale);
    }
    
    return self;
}

-(UIImage*) loadImage:(NSString*) fileName
{
    NSString* fullPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
    return [UIImage imageWithContentsOfFile:fullPath];
}
-(void) showIndicator
{
    _isShowIndicator = YES;
    [self setHidden:NO];
    [self animateLetter];
    [self animateBubble];
    [self animateZ];
}

-(void) animateLetter
{
    int dx = _dotsRect.size.width;
    [_imgDotFrame1 setFrame:_dotsRect];
    [_imgDotFrame2 setFrame:CGRectMake(0, 0, _dotsRect.size.width, _dotsRect.size.width)];

    CGPoint pt1 = _imgDotFrame1.center;
    CGPoint pt2 = _imgDotFrame2.center;

    _imgDotFrame1.center = CGPointMake(pt1.x - dx, pt1.y);
    _imgDotFrame2.center = CGPointMake(pt2.x + dx, pt2.y);

    [UIView animateWithDuration:1.0 delay:0.2f options:UIViewAnimationOptionRepeat animations:^{
        _imgDotFrame1.center = pt1;
        _imgDotFrame2.center = pt2;
    } completion:^(BOOL finished) {
    }];
}

-(void) animateBubble
{
    _imgLoading2.layer.anchorPoint = CGPointMake(0, 1.0f);
    _imgLoading2.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0f, 1.0f);
    
    [UIView animateWithDuration:2.0 delay:0.2f options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
        _imgLoading2.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.2f, 1.2f);
    } completion:^(BOOL finished) {
    }];
}

-(void) animateZ
{
    _imgZ1.center = _zPos1;
    _imgZ1.alpha = 0;
    
    [UIView animateWithDuration:2.0f animations:^{
    } completion:^(BOOL finished) {
        _imgZ1.alpha = 1.0f;
        [UIView animateWithDuration:2.0 delay:0.0 options:UIViewAnimationOptionRepeat animations:^{
            _imgZ1.center = CGPointMake(_zPos1.x, _zPos1.y - 100 * _fScale);
            _imgZ1.alpha = 0;
        } completion:^(BOOL finished) {
            _imgZ1.center = _zPos1;
            _imgZ1.alpha = 1.0f;
        }];
    }];
    _imgZ2.center = _zPos2;
    _imgZ2.alpha = 0;
    
    [UIView animateWithDuration:2.0f animations:^{
    } completion:^(BOOL finished) {
        _imgZ2.alpha = 1.0f;
        [UIView animateWithDuration:2.0 delay:0.5 options:UIViewAnimationOptionRepeat animations:^{
            _imgZ2.center = CGPointMake(_zPos2.x, _zPos2.y - 100 * _fScale);
            _imgZ2.alpha = 0;
        } completion:^(BOOL finished) {
            _imgZ2.center = _zPos2;
            _imgZ2.alpha = 1.0f;
        }];
    }];
    _imgZ3.center = _zPos3;
    _imgZ3.alpha = 0;
    
    [UIView animateWithDuration:2.0f animations:^{
    } completion:^(BOOL finished) {
        _imgZ3.alpha = 1.0f;
        [UIView animateWithDuration:2.0 delay:0.3 options:UIViewAnimationOptionRepeat animations:^{
            _imgZ3.center = CGPointMake(_zPos3.x, _zPos3.y - 100 * _fScale);
            _imgZ3.alpha = 0;
        } completion:^(BOOL finished) {
            _imgZ3.center = _zPos3;
            _imgZ3.alpha = 1.0f;
        }];
    }];
}
-(void) stopIndicator
{
    _isShowIndicator = NO;
    [self.layer removeAllAnimations];
    
    [_imgLoading2.layer removeAllAnimations];
    [_imgZ1.layer removeAllAnimations];
    [_imgZ2.layer removeAllAnimations];
    [_imgZ3.layer removeAllAnimations];

    [_imgDotFrame1.layer removeAllAnimations];
    [_imgDotFrame2.layer removeAllAnimations];
    
    [self setHidden:YES];
}

@end

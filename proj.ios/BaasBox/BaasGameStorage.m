//
//  BaasGameStorage.m
//  EmojiBubbleBlast
//
//  Created by Pink on 3/31/14.
//
//

#import "BaasGameStorage.h"

@implementation BaasGameStorage

@synthesize savedata;

- (instancetype) initWithDictionary:(NSDictionary *)dictionary {
    
    self = [super initWithDictionary:dictionary];
    
    if (self) {
        
        savedata = dictionary[@"savedata"];
    }
    
    return self;
    
}

- (NSString *)collectionName {
    
    return @"document/ebbtest1";
    
}

@end

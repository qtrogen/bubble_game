//
//  BaasGameClient.h
//  EmojiBubbleBlast
//
//  Created by Pink on 3/31/14.
//
//

#import <Foundation/Foundation.h>
@protocol BaasGameDelegate
-(void) loggedInToBaas:(NSString*) username success:(bool) bSuccess;
-(void) receivedGameData:(NSString*) gamedata;
@end
@interface BaasGameClient : NSObject
{
    
}

typedef void (^BaasGameResultBlock)(BOOL bSuccess, id object);

@property (nonatomic, assign) id<BaasGameDelegate> delegate;
@property (nonatomic, retain) NSString* objectId;
@property (nonatomic, retain) NSString* fbid;

+(instancetype) sharedClient;
-(void) logout:(BOOL) bKeepObjectId;
-(void) uploadData:(NSString*) gamedata;
-(void) requestGameData;
-(void) login;
@end

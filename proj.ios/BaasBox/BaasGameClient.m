//
//  BaasGameClient.m
//  EmojiBubbleBlast
//
//  Created by Pink on 3/31/14.
//
//

#import "BaasGameClient.h"
#import "BaasBox.h"
#import "BAAClient.h"
#import "BaasGameStorage.h"

#define BASE_URL        @"http://5.79.68.27:9000"
#define APP_CODE        @"20140409210600"
#define BAAS_PWD_FMT    @"bapwd_%@"

#define MAX_TRY_COUNT   3

@interface BaasGameClient ()
//@property (nonatomic, retain) BaasGameStorage* gameStorage;
@property (nonatomic) int trycount;
@end

@implementation BaasGameClient

-(id) init
{
    if (self = [super init])
    {
        [BaasBox setBaseURL:BASE_URL appCode:APP_CODE];
        _trycount = 0;
    }
    
    return self;
}

+ (instancetype)sharedClient {
    
    static BaasGameClient *sharedBAAClient = nil;
    static dispatch_once_t onceBAAToken;
    dispatch_once(&onceBAAToken, ^{
        sharedBAAClient = [[BaasGameClient alloc] init];
    });
    
    return sharedBAAClient;
}

-(void) login
{
}

-(void) loginWithCompletion:(BAABooleanResultBlock) block
{
    NSString* username = [self fbid];
    [[BAAClient sharedClient] authenticateUser:username password:[self getPasswordFor:username] completion:^(BOOL success, NSError *error) {
        if (success)
        {
            _trycount = 0;
            NSLog(@"success to login via baas");
            block(success, error);
//            [_delegate loggedInToBaas:username success:YES];
        }
        else
        {
            _trycount++;
            NSLog(@"failed to login(%d)", _trycount);
            if (_trycount > MAX_TRY_COUNT)
            {
                _trycount = 0;
                block(success, error);
//                [_delegate loggedInToBaas:username success:NO];
            }
            else
            {
                [self signup:username password:[self getPasswordFor:username] completion:block];
            }
        }
    }];
}
-(void) logout:(BOOL) bKeepObjectId
{
    if (![[BAAClient sharedClient] isAuthenticated]) return;

    if (!bKeepObjectId)
        self.objectId = nil;
    [[BAAClient sharedClient] logoutWithCompletion:^(BOOL success, NSError *error) {
        if (success)
        {
            NSLog(@"succeed to logout");
        }
        else
        {
            NSLog(@"failed to logout");
        }
    }];
}
-(void) uploadData:(NSString*) gamedata
{
//    if (![[BAAClient sharedClient] isAuthenticated]) return;
//
//    if (_gameStorage == nil)
//    {
//        NSLog(@"Game storage shouldn't be nil.");
//        return;
//    }
    
    [self loginWithCompletion:^(BOOL success, NSError *error) {
        if (success)
        {
            BaasGameStorage* gameStorage = [[BaasGameStorage alloc] init];
            gameStorage.objectId = self.objectId;
            gameStorage.savedata = gamedata;
            
            [gameStorage saveObjectWithCompletion:^(BaasGameStorage* gstorage, NSError *error) {
                NSLog(@"Saved game data = %@", gstorage);
                self.objectId = [gstorage.objectId copy];
                
                [self logout:YES];
            }];
        }
    }];
}
-(void) requestGameData
{
    NSLog(@"requestGameData");
    [self loginWithCompletion:^(BOOL success, NSError *error) {
        if (success)
        {
            [BaasGameStorage getObjectsWithCompletion:^(NSArray *objects, NSError *error) {
                if (objects != nil && [objects count] > 0)
                {
                    NSLog(@"received message");
                    BaasGameStorage* gameStorage = [objects objectAtIndex:0];
                    self.objectId = [gameStorage.objectId copy];
                    NSLog(@"received message = %@", [gameStorage savedata]);
                    [_delegate receivedGameData:gameStorage.savedata];
                }
                else
                {
                    NSLog(@"There is no data on baas");
                    [_delegate receivedGameData:@""];
                }
                
                [self logout:YES];
            }];
        }
    }];
}

-(void) signup:(NSString*) username password:(NSString*) password completion:(BAABooleanResultBlock) block
{
    [[BAAClient sharedClient] createUserWithUsername:username password:password completion:^(BOOL success, NSError *error) {
        if (success)
        {
            NSLog(@"succeed to signup");
            [self performSelector:@selector(loginWithCompletion:) withObject:block];
        }
        else
        {
            NSLog(@"Failed to sign-up");
        }
    }];

}

-(void) clearTryCount
{
    _trycount = 0;
}

-(NSString*) getPasswordFor:(NSString*) username
{
    return [NSString stringWithFormat:BAAS_PWD_FMT, username] ;
}

@end

package org.cocos.fbtutorial;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.text.NumberFormat;
import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.android.Facebook;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.parse.ParseInstallation;
import com.redrhino.bubble.bubble_game;
import com.redrhino.bubble.constants;
import com.redrhino.bubble.constants.msgType;

import com.redrhino.bubble.R;


public class FacebookConnectPlugin {
	
	private static final String TAG = "FacebookConnectPlugin";
	private static FacebookConnectPlugin instance;
	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback statusCallback = new SessionStatusCallback();
	private Activity activity;
	private static int callIndex;
	
	public FacebookConnectPlugin(Activity activity) {
		this.activity = activity;
		instance = this;
		uiHelper = new UiLifecycleHelper(activity, statusCallback);
	}

	public static native void nativeCallback(int cbIndex, String params, int nParam, byte[] buf);
	
	public void onCreate(Bundle savedInstanceState) {
		uiHelper.onCreate(savedInstanceState);
	}

	public static void login(int cbIndex, String scope) {
		callIndex = cbIndex;
		instance.login_();
	}

	public void login_() {
		
		Session session = Session.getActiveSession();
		if (!session.isOpened() && !session.isClosed()) {
			session.openForRead(new Session.OpenRequest(activity)
					//.setPermissions(Arrays.asList("basic_info")).setCallback(
					.setPermissions(Arrays.asList("public_profile", "user_friends")).setCallback(
							statusCallback));
		} else {
			Session.openActiveSession(activity, true, statusCallback);
		}
	}
	
	public static void logout(int cbIndex) {		
		instance.logout_();
		callIndex = cbIndex;
	}
	
	public void logout_() {
		Session session = Session.getActiveSession();
        
		if (!session.isClosed()) {
            session.closeAndClearTokenInformation();
        }
        
        // make facebook user ID and name empty
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("facebookUserID", "");
        installation.put("facebookUserName", "");
        installation.saveInBackground();
	}
	
	public static String getStatus(int cbIndex) {
		
		callIndex = cbIndex;
		
		return instance.getStatus_();
	}
	
	public String getStatus_() {
		
		Session session = Session.getActiveSession();
	
		if (session.getState().equals(SessionState.CREATED))
		{
			return "CREATED";
		}
		else if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED))
		{
			return "CREATED_TOKEN_LOADED";
		}
		else if (session.getState().equals(SessionState.OPENING))
		{
			return "OPENING";
		}
		else if (session.getState().equals(SessionState.OPENED))
		{
			return "OPENED";
		}
		else if (session.getState().equals(SessionState.OPENED_TOKEN_UPDATED))
		{
			return "OPENED_TOKEN_UPDATED";
		}
		else if (session.getState().equals(SessionState.CLOSED_LOGIN_FAILED))
		{
			return "CLOSED_LOGIN_FAILED";
		}
		else if (session.getState().equals(SessionState.CLOSED))
		{
			return "CLOSED";
		}
		
		return "";
	}
	
	public void onResume() {
		// For scenarios where the main activity is launched and user
		// session is not null, the session state change notification
		// may not be triggered. Trigger it if it's open/closed.
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}
		uiHelper.onResume();
	}

	public void onPause() {
		uiHelper.onPause();
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	public void onSaveInstanceState(Bundle outState) {
		uiHelper.onSaveInstanceState(outState);
	}

	public void onDestory() {
		uiHelper.onDestroy();
	}

	

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {
			Log.i(TAG, "Logged in...");
		} else if (state.isClosed()) {
			Log.i(TAG, "Logged out...");
		}
	}
	
	private class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			// Respond to session state changes, ex: updating the view
			onSessionStateChange(session, state, exception);
			
			int nState = state.isOpened() ? 1 : 0;
			
			Log.i(TAG, "State changed :" + nState);
			
			nativeCallback(msgType.MSG_FB_CONNECTED.value(), "login", nState, null);
			
			if(nState == 1)
				didFetchUserDetails();
			
		}
	}
	
	public static void didFetchUserDetails() {
		
		instance.didFetchUserDetails_();
	}
	
	public void didFetchUserDetails_() {
		
		final Session session = Session.getActiveSession();
	    if (session != null && session.isOpened()) {
	        // If the session is open, make an API call to get user data
	        // and define a new callback to handle the response
	        Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
	            @Override
	            public void onCompleted(GraphUser user, Response response) {	            	
	                // If the response is successful
	                if (session == Session.getActiveSession()) {
	                    if (user != null) {
	                        final String user_ID = user.getId();//user id
	                        String profileName = user.getName();//user's profile name
	                        
	                        // Store facebook user ID and name
	                        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
	                        installation.put("facebookUserID", user_ID);
	                        installation.put("facebookUserName", profileName);
	                        installation.saveInBackground();
	                        
	                        new Thread(new Runnable() {
	        					public void run() {
	        						nativeCallback(msgType.MSG_UPDATE_MYINFO.value(), user_ID, 1, null);
	        					}
	        					
	        				}).start();
	                    }   
	                }   
	            }   
	        }); 
	        Request.executeBatchAsync(request);
	    }  
	}
	
	public static void getScores() {
		
		instance.getScores_();
	}
	
	public void getScores_()	{
			
		final Session session = Session.getActiveSession();
	    if (session != null && session.isOpened()) {
	        
	    	// Create a RequestBatch and add a callback once the batch of requests completes
	        RequestBatch requestBatch = new RequestBatch();
	            	
	        Request scoresGraphPathRequest = Request.newGraphPathRequest(session, activity.getResources().getString(R.string.facebook_app_id) + "/scores", new Request.Callback() {
	            @Override
	            public void onCompleted(Response response) {
	                // If the response is successful
	            	FacebookRequestError error = response.getError();
	            	
	                if (error != null) {
	                    Log.e(FacebookConnectPlugin.TAG, error.toString());
	                } else  if (session == Session.getActiveSession()) {
	                	if (response != null) {
	                		
	                		try {
		                		// Get the result
		                        GraphObject graphObject = response.getGraphObject();
		                        JSONArray dataArray = (JSONArray)graphObject.getProperty("data");
		                        
		                        String strResult = "";
		                        
		                        String fid = "";
		                        
		                        for (int i = 0; i < Math.min(5, dataArray.length()); i++) {
		                        	if(i > 0)
		                        		strResult += constants.RECORD_SEPERATOR;
		                        	
		                        	JSONObject jsonObj = dataArray.getJSONObject(i);
		                        	
		                        	String strScore = jsonObj.has("score") ? jsonObj.getString("score") : "";
		                        	
		                        	JSONObject jsonUserObj = jsonObj.getJSONObject("user");
		                        	String name = jsonUserObj.has("name") ? jsonUserObj.getString("name") : "";
		                        	fid = jsonUserObj.has("id") ? jsonUserObj.getString("id") : "";
		                        	
		                        	String record = fid + constants.PARAM_SEPERATOR + name + constants.PARAM_SEPERATOR + strScore;
		                        	
		                        	strResult += record;
		                        	
		                        	requestPhotoForId_(fid);
		                        }
		                        
		                        nativeCallback(msgType.MSG_FRIEND_LIST.value(), strResult, Math.min(5, dataArray.length()), null);
		                        
//		                        requestPhotoForId(fid);
		                        
	                		}catch (Exception e) {
	            				// TODO: handle exception
	            				e.printStackTrace();
	            				Log.e("FacebookConnectPlugin:getScores_", "parse error");
	            			}
	                	}
	                }   
	            }   
	        });
	        
	        Bundle scoreParams = new Bundle();
	        scoreParams.putString("fields", "score,user");
	        scoresGraphPathRequest.setParameters(scoreParams);
	        requestBatch.add(scoresGraphPathRequest);
	        
	        // Execute the batch of requests asynchronously
	        requestBatch.executeAndWait();
	    }
	}
	
	public static void requestPhotoForId_(final String fbid) {
		
		new Thread(new Runnable() {
			public void run() {
				instance.requestPhotoForId(fbid);
			}
			
		}).start();
	}

	public void requestPhotoForId(String fbid)	{
		
		try {
			
			String resourceAddress =  String.format("https://graph.facebook.com/%s/picture?width=128&height=128", fbid);
//			String resourceAddress =  String.format("http://192.168.2.117:3000/test.png", fbid);
			Bitmap userCertificateBitmap = DownloadImage(resourceAddress);
			
			//Create image file.
			final String root = Environment.getExternalStorageDirectory().toString();
			File myDir = new File(root + "/bubblegame");
			myDir.mkdirs();

			final String fileName = "temp.png";

			File file = new File(myDir, fileName);
			FileOutputStream out = new FileOutputStream(file);
			userCertificateBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
			out.flush();
			out.close();

			
			File inFile = new File(myDir, fileName);
			int nLength = (int)inFile.length();
			ByteBuffer buf = ByteBuffer.allocateDirect(nLength);
			InputStream is = new FileInputStream(inFile);
			byte[] bytes = new byte[(int)nLength];
			is.read(bytes);
			buf.put(bytes);
			buf.position(0);
			
//			int nCertificateIamgeWidth = userCertificateBitmap.getWidth();
//			int nCertificateFileIamgeHeight = userCertificateBitmap.getHeight();
//			ByteBuffer certificateImageBuff = ByteBuffer.allocate(4 * nCertificateIamgeWidth * nCertificateFileIamgeHeight);
//			
//			userCertificateBitmap.copyPixelsToBuffer(certificateImageBuff);
//			certificateImageBuff.position(0);
//			
//			int lenImgBuf = 4 * nCertificateIamgeWidth * nCertificateFileIamgeHeight;
			
			int lenId = fbid.length() + 1;
			int nTotalLen = 4 + lenId + nLength;
			
			ByteBuffer resultBuf = ByteBuffer.allocate(nTotalLen);
			resultBuf.putInt(lenId).put(fbid.getBytes()).put((byte)0).put(buf);
			
			nativeCallback(msgType.MSG_PROFILE_PHOTO.value(), "", nTotalLen, resultBuf.array());
			
		} catch (Exception e) {
			System.out.println("I/O Error: " + e.getMessage());
		}
	}
	
	private InputStream OpenHttpConnection(String urlString) throws IOException
    {
        InputStream in = null;
        int response = -1;
               
        URL url = new URL(urlString); 
        URLConnection conn = url.openConnection();
                 
       if (!(conn instanceof HttpURLConnection))                     
            throw new IOException("Not an HTTP connection");
        
        try{
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect(); 

            response = httpConn.getResponseCode();                 
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();                                 
            }                     
        }
        catch (Exception ex)
        {
            throw new IOException("Error connecting");            
        }
        return in;     
    }
	
	private Bitmap DownloadImage(String URL)
    {
        Bitmap bitmap = null;
        InputStream in = null;        
        try {
            in = OpenHttpConnection(URL);
            bitmap = BitmapFactory.decodeStream(in);
            in.close();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return bitmap;                
    }
	
	public static void checkIncommingRequests() {
		
		instance.checkIncommingRequests_();
	}
	
	public void checkIncommingRequests_()
	{
		final Session session = Session.getActiveSession();
	    if (session != null && session.isOpened()) {
	        
	    	// Create a RequestBatch and add a callback once the batch of requests completes
	        RequestBatch requestBatch = new RequestBatch();
	            	
	        Request incomingRequestGraphPathRequest = Request.newGraphPathRequest(session, "/me/apprequests", new Request.Callback() {
	            @Override
	            public void onCompleted(Response response) {
	                // If the response is successful
	            	FacebookRequestError error = response.getError();
	            	
	                if (error != null) {
	                    Log.e(FacebookConnectPlugin.TAG, error.toString());
	                } else  if (session == Session.getActiveSession()) {
	                	if (response != null) {
	                		
	                		try {
		                		// Get the result
		                        GraphObject graphObject = response.getGraphObject();
		                        JSONArray dataArray = (JSONArray)graphObject.getProperty("data");
		                        
		                        String strCollectList = "";
		                        String strGiveReq = "";
		                        
		                        for (int i = 0; i < dataArray.length(); i++) {
		                        	
		                        	JSONObject obj = dataArray.getJSONObject(i);
		                        	if(!obj.has("data")) continue;
		                        	
		                        	JSONObject data = new JSONObject(obj.getString("data"));
		                        	
		                        	String strType = data.has("badge_of_awesomeness") ? data.getString("badge_of_awesomeness") : "";
		                        	
		                        	String reqId = obj.has("id") ? obj.getString("id") : "";
		                        	
		                        	if(!obj.has("from")) continue;

	                        		JSONObject from = obj.getJSONObject("from");
	                        		String fromId = from.has("id") ? from.getString("id") : "";
	                        		String fromName = from.has("name") ? from.getString("name") : "";
	                        		
	                        		String resStr = reqId + constants.PARAM_SEPERATOR + fromId + constants.PARAM_SEPERATOR + fromName;
	                        		
	                        		if(strType.equals("1")) {
	                        			
	                        			if(strGiveReq.length() > 0)
	                        				strGiveReq += constants.RECORD_SEPERATOR;
	                        			
	                        			strGiveReq += resStr;
	                        			
	                        		}else if(strType.equals("2")) {
	                        			
	                        			if(strCollectList.length() > 0)
	                        				strCollectList += constants.RECORD_SEPERATOR;
	                        			
	                        			strCollectList += resStr;
	                        		}
		                        }
		                        
		                        if(strCollectList.length() > 0)
		                        	nativeCallback(msgType.MSG_FB_COLLECT_LIVE_LIST.value(), strCollectList, strCollectList.length(), null);
		                        
		                        if(strGiveReq.length() > 0)
		                        	nativeCallback(msgType.MSG_FB_LIVEREQ_LIST.value(), strGiveReq, strCollectList.length(), null);
		                        	
		                        
	                		}catch (Exception e) {
	            				// TODO: handle exception
	            				e.printStackTrace();
	            				Log.e("FacebookConnectPlugin:checkIncommingRequests_", "parse error");
	            			}
	                	}
	                }   
	            }   
	        });
	        
	        Bundle scoreParams = new Bundle();
	        incomingRequestGraphPathRequest.setParameters(scoreParams);
	        requestBatch.add(incomingRequestGraphPathRequest);
	        
	        // Execute the batch of requests asynchronously
	        requestBatch.executeAndWait();
	    }
	}
	
	public static void notificationClear(String requestId, int nType) {
		
		instance.notificationClear_(requestId, nType);
	}
	
	public void notificationClear_(final String requestId, final int nType)
	{
		final Session session = Session.getActiveSession();
	    if (session != null && session.isOpened()) {
	        
	    	// Create a RequestBatch and add a callback once the batch of requests completes
	        RequestBatch requestBatch = new RequestBatch();
	        
	    	Request deleteFBRequestRequest = new Request(Session.getActiveSession(), requestId, new Bundle(), HttpMethod.DELETE, new Request.Callback() {
                @Override
                public void onCompleted(Response response) {
                    FacebookRequestError error = response.getError();
                    if (error != null) {
                        Log.e(TAG, "Deleting consumed Request failed: " + error.getErrorMessage());
                    } else {
                        Log.i(TAG, "Consumed Request deleted");
                        
                        if(nType == 1)
                        {//delete giveLiveRequest
                        	nativeCallback(msgType.MSG_FB_GIVLIVE_SUC.value(), requestId, 0, null);
                        }
                        else if(nType == 2)
                        {//delete collectLiveRequest
                        	nativeCallback(msgType.MSG_FB_COLLECTLIVE_SUC.value(), requestId, 0, null);
                        }
                    }
                }
	    	});
	    	
	    	requestBatch.add(deleteFBRequestRequest);
	    	
	    	// Execute the batch of requests asynchronously
	        requestBatch.executeAndWait();
	    }
	}
}

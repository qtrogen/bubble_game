package org.cocos.fbtutorial;

import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONObject;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.FacebookDialog.PendingCall;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.redrhino.bubble.constants.msgType;


public class FacebookSendRequestsPlugin {
	private static final String TAG = "FacebookPostPlugin";
	private static FacebookSendRequestsPlugin instance;
	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback statusCallback = new SessionStatusCallback();
	private Activity activity;
	private Handler handler;

	public FacebookSendRequestsPlugin(Activity activity) {
		this.activity = activity;
		instance = this;
		uiHelper = new UiLifecycleHelper(activity, statusCallback);
		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				
				final Session session = Session.getActiveSession();
				if (session != null && session.isOpened()) {
					
					switch (msg.what) {
						case 1:
							sendRequests_();
							break;
						case 2:
							askLive_();
							break;
						case 3:
							{
								Bundle bundle = msg.getData();
								String strRequestId = bundle.getString("RequestId");
								String strFBId = bundle.getString("FBId");
								giveLive_(strRequestId, strFBId);
							}
							break;
						case 4:
							{
								Bundle bundle = msg.getData();
								String strRequestId = bundle.getString("RequestId");
								collectLive_(strRequestId);
							}
							break;
						default:
							break;
					}
				}
			}
		};
	}
	
	public static void sendRequests(int cbIndex) {
		Message message = Message.obtain();
		message.what = 1;
		instance.handler.sendMessage(message);
	}
	
	public static void askLive(int cbIndex) {
		Message message = Message.obtain();
		message.what = 2;
		instance.handler.sendMessage(message);
	}
	
	public static void giveLive(int cbIndex, String requestId, String fbid) {
		
		Message message = Message.obtain();
		message.what = 3;
		
		Bundle bundle = new Bundle();
		bundle.putString("RequestId", requestId);
		bundle.putString("FBId", fbid);
		message.setData(bundle);
		
		instance.handler.sendMessage(message);
	}
	
	public static void collectLive(int cbIndex, String requestId) {
		
		Message message = Message.obtain();
		message.what = 4;
		
		Bundle bundle = new Bundle();
		bundle.putString("RequestId", requestId);
		message.setData(bundle);
		
		instance.handler.sendMessage(message);
	}

	public void sendRequests_() {
		
		Bundle params = new Bundle();
	    params.putString("message", "Learn how to make your Android apps social");

	    WebDialog requestsDialog = (
	        new WebDialog.RequestsDialogBuilder(activity,
	            Session.getActiveSession(),
	            params))
	            .setOnCompleteListener(new OnCompleteListener() {

	                @Override
	                public void onComplete(Bundle values,
	                    FacebookException error) {
	                    if (error != null) {
	                        if (error instanceof FacebookOperationCanceledException) {
	                            Toast.makeText(activity.getApplicationContext(), 
	                                "Request cancelled", 
	                                Toast.LENGTH_SHORT).show();
	                        } else {
	                            Toast.makeText(activity.getApplicationContext(), 
	                                "Network Error", 
	                                Toast.LENGTH_SHORT).show();
	                        }
	                    } else {
	                        final String requestId = values.getString("request");
	                        if (requestId != null) {
	                            Toast.makeText(activity.getApplicationContext(), 
	                                "Request sent",  
	                                Toast.LENGTH_SHORT).show();
	                        } else {
	                            Toast.makeText(activity.getApplicationContext(), 
	                                "Request cancelled", 
	                                Toast.LENGTH_SHORT).show();
	                        }
	                    }   
	                }

	            })
	            .build();
	    requestsDialog.show();
		
	}
	
	public void askLive_() {
		
		Log.d(TAG, "askLive_");
		
		Bundle params = new Bundle();
		
		params.putString("message", "I'm out of lives. Can you send me one?");
		
		try
		{
			JSONObject json = new JSONObject();
			json.put("social_karma", "5");
			json.put("badge_of_awesomeness", "1");
			
			params.putString("data", json.toString());
			
		}catch (Exception e) {
			
		}	
		
	    WebDialog requestsDialog = (
	        new WebDialog.RequestsDialogBuilder(activity,
	            Session.getActiveSession(),
	            params))
	            .setOnCompleteListener(new OnCompleteListener() {

	                @Override
	                public void onComplete(Bundle values,
	                    FacebookException error) {
	                    if (error != null) {
	                        if (error instanceof FacebookOperationCanceledException) {
	                            Toast.makeText(activity.getApplicationContext(), 
	                                "Request cancelled", 
	                                Toast.LENGTH_SHORT).show();
	                        } else {
	                            Toast.makeText(activity.getApplicationContext(), 
	                                "Network Error", 
	                                Toast.LENGTH_SHORT).show();
	                        }
	                    } else {
	                        final String requestId = values.getString("request");
	                        
	                        ArrayList<String> selectedFriendsId = new ArrayList<String>();
	                        int i = 0;
                            String to;

                            do {

                                to = values.getString("to[" +i + "]");  

                                if(!TextUtils.isEmpty(to)) 
                                {
                                	selectedFriendsId.add(to);
                                }

                                i++;

                            } while (to != null);
	                        
                            
	                        if (requestId != null) {
	                            Toast.makeText(activity.getApplicationContext(), 
	                                "Request sent",  
	                                Toast.LENGTH_SHORT).show();
	                            
	                            // get current installation
	                            ParseInstallation installation = ParseInstallation.getCurrentInstallation();
	                            String strID = installation.getString("facebookUserID");
	                            String strName = installation.getString("facebookUserName");
	                            
	                            try{
	                            	JSONObject data = new JSONObject();
	                            	data.put("title", "Bubble Game");
	                            	data.put("action","com.redrhino.bubble.UPDATE_STATUS");
	                            	data.put("alert", strName + " asked you lives!");
	                            	
	                            	// Send push notification to query
		                            ParsePush push = new ParsePush();
		                            
		                            // Create our Installation query
		                            ParseQuery pushQuery = ParseInstallation.getQuery();
//		                            pushQuery.whereEqualTo("facebookUserID", strID);
		                            pushQuery.whereContainedIn("facebookUserID", selectedFriendsId);
		                            
		                            push.setQuery(pushQuery);
		                            push.setData(data);
		                            
//		                            // Set our Installation query
//		                            push.setMessage(strName + " asked you lives!");
		                            
		                            push.sendInBackground();
		                            
	                            }catch (Exception e){
	                            	
	                            }
	                            
	                        } else {
	                            Toast.makeText(activity.getApplicationContext(), 
	                                "Request cancelled", 
	                                Toast.LENGTH_SHORT).show();
	                        }
	                    }   
	                }

	            })
	            .build();
	    requestsDialog.show();
	}
	
	public void giveLive_(final String requestId, final String fbid) {
		
		Log.d(TAG, "giveLive_");
		
		Bundle params = new Bundle();
		
		params.putString("message", "Here is a heart. Have a great day!");
		
		try
		{
			JSONObject json = new JSONObject();
			json.put("social_karma", "5");
			json.put("badge_of_awesomeness", "2");
			
			params.putString("data", json.toString());
			
			params.putString("to", fbid);
			
		}catch (Exception e) {
			
		}	
		
	    WebDialog requestsDialog = (
	        new WebDialog.RequestsDialogBuilder(activity,
	            Session.getActiveSession(),
	            params))
	            .setOnCompleteListener(new OnCompleteListener() {

	                @Override
	                public void onComplete(Bundle values,
	                    FacebookException error) {
	                    if (error != null) {
	                        if (error instanceof FacebookOperationCanceledException) {
	                            Toast.makeText(activity.getApplicationContext(), 
	                                "Request cancelled", 
	                                Toast.LENGTH_SHORT).show();
	                        } else {
	                            Toast.makeText(activity.getApplicationContext(), 
	                                "Network Error", 
	                                Toast.LENGTH_SHORT).show();
	                        }
	                    } else {
	                        String val = values.getString("request");
	                        
	                        if (val != null) {
	                            Toast.makeText(activity.getApplicationContext(), 
	                                "Request sent",  
	                                Toast.LENGTH_SHORT).show();
	                            
	                            
                        	// get current installation
	                            ParseInstallation installation = ParseInstallation.getCurrentInstallation();
	                            String strID = installation.getString("facebookUserID");
	                            String strName = installation.getString("facebookUserName");
	                            
	                            try{
	                            	JSONObject data = new JSONObject();
	                            	data.put("title", "Bubble Game");
	                            	data.put("action","com.redrhino.bubble.UPDATE_STATUS");
	                            	data.put("alert", strName + " gave you lives!");
	                            	
	                            	// Send push notification to query
		                            ParsePush push = new ParsePush();
		                            
		                            // Create our Installation query
		                            ParseQuery pushQuery = ParseInstallation.getQuery();
		                            pushQuery.whereEqualTo("facebookUserID", fbid);
		                            
		                            push.setQuery(pushQuery);
		                            push.setData(data);
		                            
		                            push.sendInBackground();
		                            
	                            }catch (Exception e){
	                            	
	                            }	                            
	                            
	                            new Thread(new Runnable() {
	                    			public void run() {
	                    				FacebookConnectPlugin.notificationClear(requestId, 1/*delete giveLiveRequest*/);
	                    			}
	                    		}).start();	
	                            
	                        } else {
	                            Toast.makeText(activity.getApplicationContext(), 
	                                "Request cancelled", 
	                                Toast.LENGTH_SHORT).show();
	                        }
	                    }   
	                }

	            })
	            .build();
	    requestsDialog.show();
	}
	
	public void collectLive_(final String requestId) {

		new Thread(new Runnable() {
			public void run() {
				FacebookConnectPlugin.notificationClear(requestId, 2/*delete collectLiveRequest*/);
			}
		}).start();		
	}

	
	private class SessionStatusCallback implements Session.StatusCallback,
			FacebookDialog.Callback {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			// Respond to session state changes, ex: updating the view
			onSessionStateChange(session, state, exception);
		}

		@Override
		public void onComplete(PendingCall pendingCall, Bundle data) {
			// TODO Auto-generated method stub
			Log.i(TAG, "ShareDialog onComplete");
			boolean didCancel = FacebookDialog.getNativeDialogDidComplete(data);
			String completionGesture = FacebookDialog
					.getNativeDialogCompletionGesture(data);
			String postId = FacebookDialog.getNativeDialogPostId(data);
			Log.i(TAG, "didCancel:" + didCancel + "completionGesture:"
					+ completionGesture + "postId:" + postId);
		}

		@Override
		public void onError(PendingCall pendingCall, Exception error,
				Bundle data) {
			// TODO Auto-generated method stub
			Log.i(TAG, "ShareDialog onError");
			boolean didCancel = FacebookDialog.getNativeDialogDidComplete(data);
			String completionGesture = FacebookDialog
					.getNativeDialogCompletionGesture(data);
			String postId = FacebookDialog.getNativeDialogPostId(data);
			Log.i(TAG, "didCancel:" + didCancel + "completionGesture:"
					+ completionGesture + "postId:" + postId);

		}
	}

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {
			Log.i(TAG, "Logged in...");
		} else if (state.isClosed()) {
			Log.i(TAG, "Logged out...");
		}
	}

	public void onCreate(Bundle savedInstanceState) {
		uiHelper.onCreate(savedInstanceState);
	}
	
	public void onResume() {
		// For scenarios where the main activity is launched and user
		// session is not null, the session state change notification
		// may not be triggered. Trigger it if it's open/closed.
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}
		uiHelper.onResume();
	}

	public void onPause() {
		uiHelper.onPause();
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
	}

	public void onSaveInstanceState(Bundle outState) {
		uiHelper.onSaveInstanceState(outState);
	}

	public void onDestory() {
		uiHelper.onDestroy();
	}

	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
		@Override
		public void onError(FacebookDialog.PendingCall pendingCall,
				Exception error, Bundle data) {
			Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
		}

		@Override
		public void onComplete(FacebookDialog.PendingCall pendingCall,
				Bundle data) {
			Log.d("HelloFacebook", "Success!");
		}
	};
}

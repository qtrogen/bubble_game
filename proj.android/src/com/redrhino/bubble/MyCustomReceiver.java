package com.redrhino.bubble;

import java.util.Iterator;

import org.cocos.fbtutorial.FacebookConnectPlugin;
import org.json.JSONException;
import org.json.JSONObject;

import com.redrhino.bubble.constants.msgType;

import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

public class MyCustomReceiver extends BroadcastReceiver {
	private static final String TAG = "MyCustomReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			if (intent == null)
			{
				Log.d(TAG, "Receiver intent null");
			}
			else
			{
				String action = intent.getAction();
				Log.d(TAG, "got action " + action );
				
				if (action.equals("com.redrhino.bubble.UPDATE_STATUS"))
				{
					JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
					
					Toast.makeText(bubble_game.me.getApplicationContext(), 
							json.getString("alert"), 
		                    Toast.LENGTH_SHORT).show();
					
//					FacebookConnectPlugin.nativeCallback(msgType.MSG_PUSH_NOTIFICATION.value(), "", 0, null);
				}
			}

		} catch (Exception e) {
			Log.d(TAG, "MyCustomReceiver: " + e.getMessage());
		}
	}
}

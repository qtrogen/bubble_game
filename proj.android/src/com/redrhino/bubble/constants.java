package com.redrhino.bubble;

import java.util.Map;
import java.util.TreeMap;

public class constants {	
	
	public enum sceneType {
	    
		SCENE_MAINMENU,
		SCENE_SELCHARA,
		SCENE_WORLDMAP,
		SCENE_DAILYSPIN,
		SCENE_SELBOOST,
		SCENE_GAME;

	    private static Map<Integer, sceneType> ss = new TreeMap<Integer,sceneType>();
	    private static final int START_VALUE = 1000;
	    private int value;

	    static {
	        for(int i=0;i<values().length;i++)
	        {
	            values()[i].value = START_VALUE + i;
	            ss.put(values()[i].value, values()[i]);
	        }
	    }

	    public static sceneType fromInt(int i) {
	        return ss.get(i);
	    }

	    public int value() {
	    	return value;
	    }
	}
	
	public enum msgType {

		MSG_REMOVE_ADS,
		MSG_SUBMIT_ACHIEVE,
		MSG_SUBMIT_SCORE,
		MSG_SUBMIT_LEADERBOARD,
	    MSG_SHOW_LDB,
		MSG_NOTIF_ACHIEVE,
	    MSG_AD_HEIGHT,
	    MSG_APP_VERSION,
	    
	    MSG_SHOW_INDICATOR,
	    MSG_HIDE_INDICATOR,
	    
	    MSG_LOWSPEC_DEVICE,
	    
	    MSG_BUY_REQUEST,
	    MSG_BUY_RESULT,
	    MSG_RESTORE_REQ,
	    MSG_RESTORE_RES,
	    
	    MSG_SHOW_REVMOB,
	    MSG_SHOW_CHARTBOOST,
	    MSG_SHOW_MOREAPPS,
	    MSG_SHOW_APPLOVIN,
	    MSG_SHOW_PLAYHAVEN,
	    
	    MSG_SHOW_FB,
		MSG_SHOW_TW,
		MSG_SHOW_RATE,
		MSG_RATED_APP,
	    
	    MSG_CONNECT_FB,
	    MSG_FB_CONNECTED,
	    MSG_FRIEND_LIST_REQ,
	    MSG_FRIEND_LIST,
	    MSG_UPDATE_MYINFO,
	    MSG_USER_PHOTO_REQ,
	    MSG_PROFILE_PHOTO,
	    
	    MSG_FB_CHECK_REQUESTS,
	    MSG_FB_ASK_LIVE,
	    MSG_FB_GIVE_LIVE,
	    MSG_FB_LIVEREQ_LIST,
	    MSG_FB_COLLECT_LIVE_LIST,
	    MSG_FB_COLLECT_LIVE,
	    MSG_FB_COLLECTLIVE_SUC,
	    MSG_FB_GIVLIVE_SUC,
	    
	    MSG_PUSH_NOTIFICATION,
	    
	    MSG_SND_PLAY,
	    MSG_SND_STOP,
	    
	    //    MSG_UD_LOGIN_REQ,
	    //    MSG_UD_LOGIN_RES,
	    MSG_UD_UPLOAD_REQ,
	    MSG_UD_UPLOAD_RES,
	    MSG_UD_DOWNLOAD_REQ,
	    MSG_UD_DOWNLOAD_RES,
	    
	    MSG_PUSH_NOTIF,
	    MSG_SEND_FUSE_EVENT,
	    MSG_SEND_FUSE_EVENT_LEVEL,
	    MSG_SEND_FUSE_PURCAHSE,
	    
	    MSG_ACHIEVE_RESET,
	    
	    MSG_SCENE_TYPE,
	    MSG_SCREEN_ORIENTATION_CHANGED;

	    private static Map<Integer, msgType> ss = new TreeMap<Integer,msgType>();
	    private static final int START_VALUE = 200;
	    private int value;

	    static {
	        for(int i=0;i<values().length;i++)
	        {
	            values()[i].value = START_VALUE + i;
	            ss.put(values()[i].value, values()[i]);
	        }
	    }

	    public static msgType fromInt(int i) {
	        return ss.get(i);
	    }

	    public int value() {
	    	return value;
	    }
	}
	
	public static String RECORD_SEPERATOR = "^";
	public static String PARAM_SEPERATOR = "|";
	
}

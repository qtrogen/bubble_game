/****************************************************************************
Copyright (c) 2010-2011 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 ****************************************************************************/
package com.redrhino.bubble;

import com.crashlytics.android.Crashlytics;
import java.util.Map;
import java.util.TreeMap;

import org.cocos.fbtutorial.FacebookConnectPlugin;
import org.cocos.fbtutorial.FacebookPickFriendPlugin;
import org.cocos.fbtutorial.FacebookPostPlugin;
import org.cocos.fbtutorial.FacebookSendRequestsPlugin;
import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import com.google.android.gms.games.Games;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;
import com.redrhino.bubble.constants.msgType;
import com.redrhino.bubble.constants.sceneType;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

public class bubble_game extends /* BaseGameActivity */Cocos2dxActivity {

	static final String TAG = "BubbleGame";

	public static bubble_game me;

	ParseUser pareUser;

	protected FacebookConnectPlugin facebookLoginPlugin = null;
	protected FacebookPickFriendPlugin facebooFriendPlugin = null;
	protected FacebookPostPlugin facebookPostPlugin = null;
	protected FacebookSendRequestsPlugin facebookSendRequestsPlugin = null;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		
		me = this;

		Parse.initialize(this, "a1vunyR6Bw9tJXfRlAGg7qYvWAveUAHCIeqmFKjh",
				"T049CYB40DpeNbaNEgmBUU4vPtAHsSZNXzuRIOuE");

		PushService.setDefaultPushCallback(this, bubble_game.class);
		
		// Save the current Installation to Parse.
		ParseInstallation.getCurrentInstallation().saveInBackground();
		

		facebookLoginPlugin = new FacebookConnectPlugin(this);
		facebooFriendPlugin = new FacebookPickFriendPlugin(this);
		facebookPostPlugin = new FacebookPostPlugin(this);
		facebookSendRequestsPlugin = new FacebookSendRequestsPlugin(this);

		facebookLoginPlugin.onCreate(savedInstanceState);
		facebooFriendPlugin.onCreate(savedInstanceState);
		facebookPostPlugin.onCreate(savedInstanceState);
		facebookSendRequestsPlugin.onCreate(savedInstanceState);

		// //Leaderboard part------------
		// new Thread(new Runnable() {
		// public void run() {
		// if (!me.isSignedIn())
		// gameServicesSignIn();
		// }
		// }).start();
		// //------------Leaderboard part

	}

	public Cocos2dxGLSurfaceView onCreateView() {
		Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);
		// bubble_game should create stencil buffer
		glSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 8);
		// glSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 8);

		return glSurfaceView;
	}

	static {
		System.loadLibrary("cocos2dcpp");
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);

	    // Checks the orientation of the screen
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//	        Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
	    	FacebookConnectPlugin.nativeCallback(msgType.MSG_SCREEN_ORIENTATION_CHANGED.value(), "", 0, null);
	    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
//	        Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
	    	FacebookConnectPlugin.nativeCallback(msgType.MSG_SCREEN_ORIENTATION_CHANGED.value(), "", 1, null);
	    }
	}
	
	public static boolean receivedFromCocos2dx(int nType, String arg, int nParam) {

		Log.d(TAG, nType + " : " + arg + " : " + nParam);

		final msgType type = msgType.fromInt(nType);
		final String strArg = arg;
		final int param = nParam;

		new Thread(new Runnable() {
			public void run() {

				switch (type) {
					case MSG_CONNECT_FB:
	
						if (param == 1)
							FacebookConnectPlugin.login(0, "login");
						else if (param == 0)
							FacebookConnectPlugin.logout(0);
	
						break;
	
					case MSG_FRIEND_LIST_REQ:
	
						FacebookConnectPlugin.getScores();
	
						break;
	
					case MSG_SUBMIT_LEADERBOARD:
	
						// //Leaderboard part------------
						// updateTopScoreLeaderboard(strArg, param);
	
						break;
	
					case MSG_SHOW_LDB:
	
						// //Leaderboard part------------
						// showLeaderboards();
	
						break;
	
					case MSG_FB_ASK_LIVE:
	
						FacebookSendRequestsPlugin.askLive(0);
	
						break;
	
					case MSG_FB_CHECK_REQUESTS:
	
						FacebookConnectPlugin.checkIncommingRequests();
	
						break;
	
					case MSG_FB_GIVE_LIVE:
	
						String[] arr = strArg.split("\\|");
						String reqId = arr[0];
						String receiverId = arr[1];
	
						FacebookSendRequestsPlugin.giveLive(0, reqId, receiverId);
	
						break;
	
					case MSG_FB_COLLECT_LIVE:
	
						String requestId = strArg;
	
						FacebookSendRequestsPlugin.collectLive(0, requestId);
	
						break;
						
					case MSG_USER_PHOTO_REQ:
	
						String fbid = strArg;
	
						FacebookConnectPlugin.requestPhotoForId_(fbid);
						
						break;
	
					case MSG_SCENE_TYPE:
						
						me.setAutoOrientationEnabled(me.getContext(), param);
						
						break;
						
					default:
						break;
				}
			}

		}).start();

		return true;
	}

	public static void setAutoOrientationEnabled(Context context, int nSceneType)
    {
		sceneType type = sceneType.fromInt(nSceneType);
		
		switch(type)
		{
			case SCENE_MAINMENU:
			case SCENE_SELCHARA:
			case SCENE_WORLDMAP:
			case SCENE_DAILYSPIN:
			case SCENE_SELBOOST:
				Settings.System.putInt( context.getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0);
				break;
			case SCENE_GAME:
				Settings.System.putInt( context.getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 1);
				break;
		}
    }
	
	@Override
	public void onResume() {

		super.onResume();

		facebookLoginPlugin.onResume();
		facebooFriendPlugin.onResume();
		facebookPostPlugin.onResume();
		facebookSendRequestsPlugin.onResume();
	}

	@Override
	public void onPause() {

		super.onPause();

		facebookLoginPlugin.onPause();
		facebooFriendPlugin.onPause();
		facebookPostPlugin.onPause();
		facebookSendRequestsPlugin.onPause();
	}

	@Override
	public void onDestroy() {

		super.onDestroy();

		facebookLoginPlugin.onDestory();
		facebooFriendPlugin.onDestory();
		facebookPostPlugin.onDestory();
		facebookSendRequestsPlugin.onDestory();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);

		facebookLoginPlugin.onSaveInstanceState(outState);
		facebooFriendPlugin.onSaveInstanceState(outState);
		facebookPostPlugin.onSaveInstanceState(outState);
		facebookSendRequestsPlugin.onSaveInstanceState(outState);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		facebookLoginPlugin.onActivityResult(requestCode, resultCode, data);
		facebooFriendPlugin.onActivityResult(requestCode, resultCode, data);
		facebookPostPlugin.onActivityResult(requestCode, resultCode, data);
		facebookSendRequestsPlugin.onActivityResult(requestCode, resultCode,
				data);

		ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);

	}

	// //Leaderboard part------------
	// @Override
	// public void onSignInFailed()
	// {
	//
	// }
	//
	//
	// @Override
	// public void onSignInSucceeded()
	// {
	//
	// }
	//
	// public static void gameServicesSignIn() {
	//
	// me.runOnUiThread(new Runnable() {
	// public void run() {
	// me.beginUserInitiatedSignIn();
	// }
	// });
	// }
	//
	// public static void updateTopScoreLeaderboard(String leaderboardID, int
	// score) {
	// if (!me.isSignedIn())
	// return;
	//
	// Games.Leaderboards.submitScore(me.getApiClient(), leaderboardID, score);
	// }
	//
	//
	// public static void showLeaderboards() {
	// if (!me.isSignedIn())
	// return;
	//
	// me.runOnUiThread(new Runnable() {
	// public void run() {
	// me.startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(me.getApiClient()),
	// 5001);
	// }
	// });
	// }
	// //------------Leaderboard part

}

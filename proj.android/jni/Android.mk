LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AimLayer.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/BaseLayer.cpp \
                   ../../Classes/Bubble.cpp \
                   ../../Classes/BubbleGame.cpp \
                   ../../Classes/BubbleLayer.cpp \
                   ../../Classes/Character.cpp \
                   ../../Classes/CMainMenuLayer.cpp \
                   ../../Classes/DailySpin.cpp \
                   ../../Classes/FacebookInterface.cpp \
                   ../../Classes/GoalData.cpp \
                   ../../Classes/LCommon.cpp \
                   ../../Classes/LevelSelect.cpp \
                   ../../Classes/MathUtil.cpp \
                   ../../Classes/PreMapScene.cpp \
                   ../../Classes/SaveUtil.cpp \
                   ../../Classes/SelectBoost.cpp \
                   ../../Classes/SelectChara.cpp \
                   ../../Classes/SplashScene.cpp \
                   ../../Classes/TutoLayer.cpp \
                   ../../Classes/WorldMap.cpp \
                   ../../Classes/effect/CCBlade.cpp \
				   ../../Classes/LKit/BsToggleButton.cpp \
                   ../../Classes/LKit/CCAdView.cpp \
                   ../../Classes/LKit/GrowButton.cpp \
                   ../../Classes/LKit/LCScrollLayer.cpp \
                   ../../Classes/LPurchase/LStoreManager.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

LOCAL_WHOLE_STATIC_LIBRARIES += cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
LOCAL_WHOLE_STATIC_LIBRARIES += chipmunk_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static

include $(BUILD_SHARED_LIBRARY)

$(call import-module,cocos2dx)
$(call import-module,cocos2dx/platform/third_party/android/prebuilt/libcurl)
$(call import-module,CocosDenshion/android)
$(call import-module,extensions)
$(call import-module,external/Box2D)
$(call import-module,external/chipmunk)
	
#include "MathUtil.h"

CCPoint getDistPoint(const CCPoint& pos, float alpha, float dist)
{
	float dy = dist * sin(alpha);
	float dx = dist * cos(alpha);
	return ccp(pos.x + dx, pos.y + dy);
}

bool isAcross(const CCPoint& circlePos, float radius, const CCPoint& pos1, const CCPoint& pos2)
{
	float a0 = asinf(radius / getDistance(circlePos, pos1));
	float a1 = getAlpha(pos1, pos2);
	float a2 = getAlpha(pos1, circlePos);
	return (abs(a1 - a2)) < a0;
}

float getDistance(const CCPoint& pos1, const CCPoint& pos2)
{
	float sq = getDistance2(pos1, pos2);
	return sqrtf(sq);
}

float getDistance2(const CCPoint& pos1, const CCPoint& pos2)
{
	return (pos1.x - pos2.x) * (pos1.x - pos2.x) + (pos1.y - pos2.y) * (pos1.y - pos2.y);
}

float getAlpha(const CCPoint& pos1, const CCPoint& pos2)
{
	float dx = pos2.x - pos1.x;
	float dy = pos2.y - pos1.y;
	return atan2f(dy, dx);
}

float getDistFromLine(const CCPoint& point, const CCPoint& pt1, const CCPoint& pt2)
{
	// 	float a1 = getAlpha(pt1, pt2);
	// 	float a2 = getAlpha(pt1, point);
	// 	float a3 = fabs(a1 - a2);
	// 
	// 	float len = getDistance(pt1, pt2);
	// 
	// 	return fabs(len * sin(a3));
	float dist2 = getDistFromLine2(point, pt1, pt2);
	return sqrtf(dist2);
}

float getDistFromLine2(const CCPoint& point, const CCPoint& pt1, const CCPoint& pt2)
{
	float dy = (pt1.y - pt2.y);
	if (dy == 0)
		return fabs(point.y - pt1.y);

	float dx = (pt1.x - pt2.x);
	if (dx == 0)
		return fabs(point.x - pt1.x);

	float al = dy / dx;
	float bl = pt1.y - al * pt1.x;

	float an = -1 / al;
	float bn = point.y - an * point.x;

	float newx = al * (bn - bl) / (al * al + 1);
	float newy = al * newx + bl;

	return (point.x - newx) * (point.x - newx) + (point.y - newy) * (point.y - newy);
}

bool getAcrossPoint(const CCPoint& ptC, float radius, const CCPoint& pt1, const CCPoint& pt2, CCPoint& crossPoint)
{
	// line equation x = al * y + bl
	double dy = (pt1.y - pt2.y);
	if (dy == 0)
	{
		if (fabs(pt1.y - ptC.y) <= radius)
		{
			crossPoint.y = pt1.y;

			float rt = sqrtf(radius * radius - (pt1.y - ptC.y) * (pt1.y - ptC.y));
			if (pt1.x < ptC.x)
				crossPoint.x = ptC.x - rt;
			else
				crossPoint.x = ptC.x + rt;
			return true;
		}
		return false;
	}
	double al = (pt1.x - pt2.x) / dy;
	double bl = pt1.x - al * pt1.y;

	//circle equation (x - cx)^2 + (y - cy)^2 = r^2
	//A*x + B*y + C = 0;
	double A = al * al + 1;
	double B = 2 * (al * (bl - ptC.x) - ptC.y);
	double C = (bl - ptC.x) * (bl - ptC.x) + ptC.y * ptC.y - radius * radius;

	double rt = B * B - 4 * A * C;
	if (rt < 0) return false;

	CCPoint cross1, cross2;
	cross1.y = (-B - sqrt(rt)) / (2 * A);
	cross1.x = al * cross1.y + bl;

	cross2.y = (-B + sqrt(rt)) / (2 * A);
	cross2.x = al * cross2.y + bl;

	crossPoint = (getDistance2(pt1, cross1) < getDistance2(pt1, cross2))? cross1 : cross2;
	return true;
}

float getAcrossAlpha(const CCPoint& ptCircleCenter, float radius, const CCPoint& pt1, const CCPoint& pt2)
{
	float a1 = getAlpha(pt1, ptCircleCenter);
	float a2 = getAlpha(pt1, pt2);
	float a3 = fabs(a1 - a2);

	float len = getDistance(ptCircleCenter, pt1);

	float dist = fabs(len * sin(a3));
	;
	float a4 = asinf(dist / radius);
	float a5 = M_PI_2 - a3 - a4;

	float alpha = M_PI + a1 + a5;
	if (alpha > M_PI * 2) alpha -= M_PI * 2;
	return alpha;
}

unsigned int getRandRange(unsigned int nStart, unsigned int nEnd)
{
	static int gPrevRand = 0;
    if (gPrevRand == 0)
    {
        time_t curT;
        time(&curT);
        srand(curT);
    }
    
	if (nStart == nEnd) return nStart;

	unsigned int nRand = rand() + gPrevRand;

	gPrevRand = nRand;
	return (nRand % (nEnd - nStart + 1) + nStart);
}

void getLineEquationFromTwoPoints(CCPoint pt1, CCPoint pt2, Line& line)
{
	float dx = pt2.x - pt1.x;
	float dy = pt2.y - pt1.y;

	line.slope = dy / dx;
	line.intercept = pt1.y - line.slope * pt1.x;
}

bool getCrossPointFromTwoLines(Line line1, Line line2, CCPoint& crossPt)
{
	if(abs(line1.slope - line2.slope) <= FLT_EPSILON)
		return false;

	crossPt.x = (line2.intercept - line1.intercept) / (line1.slope - line2.slope);
	crossPt.y = line1.slope * crossPt.x + line1.intercept;

	return true;
}

bool getProjectPointOntoLine(CCPoint linePt1, CCPoint linePt2, CCPoint orgPt, CCPoint& projectPt)
{//if project point is in line returns true otherwise false

	double APx = orgPt.x - linePt1.x;
	double APy = orgPt.y - linePt1.y;
	double ABx = linePt2.x - linePt1.x;
	double ABy = linePt2.y - linePt1.y;

	double magAB2 = ABx * ABx + ABy * ABy; 
	double ABdotAP = ABx * APx + ABy * APy;
	double t = ABdotAP / magAB2;  

	projectPt.x = linePt1.x + ABx * t;
	projectPt.y = linePt1.y + ABy * t;

	if ( t < 0)     
	{
		return false;
		//projectPt.x = linePt1.x;
		//projectPt.y = linePt1.y;
	}   
	else if (t > 1)
	{
		return false;
		//projectPt.x = linePt2.x;
		//projectPt.y = linePt2.y;
	}
	else
	{
		return true;
		//projectPt.x = linePt1.x + ABx * t;
		//projectPt.y = linePt1.y + ABy * t;
	}
}

void rotateAroundBy(CCPoint centerPt, float fAngle, CCPoint oldPt, CCPoint& newPt)
{
	newPt.x = cos(CC_DEGREES_TO_RADIANS(-fAngle)) * (oldPt.x - centerPt.x) - sin(CC_DEGREES_TO_RADIANS(-fAngle)) * (oldPt.y - centerPt.y) + centerPt.x;

	newPt.y = sin(CC_DEGREES_TO_RADIANS(-fAngle)) * (oldPt.x - centerPt.x) + cos(CC_DEGREES_TO_RADIANS(-fAngle)) * (oldPt.y - centerPt.y) + centerPt.y;
}

//
//  LCommon.h
//  IcecreamCandyBlast
//
//  Created by Chen on 7/10/13.
//  Copyright (c) 2013 lion. All rights reserved.
//

#ifndef __LCOMMON_H__
#define __LCOMMON_H__

#include "cocos2d.h"
//#include "vld.h"
#include <map>
#include <vector>
#include <string>

#define TEST_MODE 1

#define SC_DESIGN_WIDTH		768.0f
#define SC_DESIGN_HEIGHT	1024.0f

extern float SC_WIDTH;
extern float SC_HEIGHT;

extern float SC_WIDTH_HALF;
extern float SC_HEIGHT_HALF;
extern float SC_ADHEIGHT;
extern float SC_BANNERHEIGHT;

extern float SC_RATIO;
extern float SC_PADRATIO;
extern float SC_FACTOR;

extern float SC_IF; // invert factor
extern float SC_BG_SCALE; // invert factor
extern float SC_DLG_SCALE; // invert factor

extern int	SC_BUBBLES_DIST;
extern bool IS_LOWMEMORY;

#define kFPS 60

#define MAX_ELEMENT 800
#define MIN_EXPLODE_COUNT 3
#define COLUMN_COUNT	11

enum ITEM_TYPE {
	IT_NONE,
	IT_PUZZLE,
	IT_LIVE,
	IT_MAGIC,
	IT_MAPPIECE,
	IT_BOOSTS
} ;

extern const char* ITEM_IMAGES[];

enum LEVEL_TYPE {
	LVL_NONE,
	LVL_CLEAR_THE_BOARD,
	LVL_FREE_THE_ANIMALS,
	LVL_SAVE_THE_BEE,
	LVL_DEFEAT_THE_MOWS,
};

enum BUBBLE_TYPE {
	BB_NONE,
	BB_NORMAL,
	BB_CANDY,
	BB_BOMB,
	BB_MAGNET,
	BB_ICE,
	BB_STONE,
	BB_ZOMBIE,
	BB_FRIEND,
	BB_MOR,
	BB_BEE,
	BB_NUKE,
	BB_RAINBOW,
	BB_REPELLENT,
	BB_FIREBALL,
	BB_MAX = BB_FIREBALL,
	BB_KIND_COUNT,
} ;

enum CHARA_STATE
{
	CS_LOCKED,
	CS_UNLOCKABLE,
	CS_UNLOCKED,
	CS_MAX_STATE = CS_UNLOCKED
};

enum
{
	WORLD_START = 1,
	WORLD_1 = WORLD_START,
	WORLD_2,
	WORLD_3,
	WORLD_4,
	WORLD_5,
	WORLD_6,
	MAX_WORLD = WORLD_6
};

enum 
{
	CHARA_NONE = -1,
	CHARA_KALI = 0,
	CHARA_PIGGLES, 
	CHARA_ROO, 
	CHARA_JACK, 
	CHARA_PINGU, 
	CHARA_MOMO
};

enum 
{
	TUTO_SMALL_CHARA,
	TUTO_BIG_CHARA,
};

enum 
{
	TUTO_SMALL_HOLE,
	TUTO_BIG_HOLE,
};


typedef struct tagBOOST_COST
{
	int count;
	int payType;
	int magicCost;
} BOOST_COST;


typedef struct tagSPECIAL_BOOST_INFO
{
	int boostId;
	std::string boostDesc;
	
	int item1_count;
	int item1_cost;
	
	float item2_cost;

	int item3_count;
	int item3_free_count;
	int item3_cost;

	bool bPermanent;

} SPECIAL_BOOST_INFO;


typedef struct tagBUBBLE_POINTS
{
	int score;
	float multiplier;

} BUBBLE_POINTS;

struct MemoryStruct {
	char *memory;
	size_t size;
};

typedef std::map<std::string, MemoryStruct*> TFriendPhotoMap;

typedef struct tagFriendInfo
{
    std::string fname;
    std::string fbid;
    
    int nMaxLevel;
    int nMaxScore;
    int nCurScore;
    bool bLocalFriend;
    cocos2d::CCTexture2D* texture;
	//MemoryStruct imgBuff;
    
    void init()
    {
        fname.clear();
        fbid.clear();
        
        nMaxLevel = 0;
        nMaxScore = 0;
        nCurScore = 0;
        bLocalFriend = false;
        texture = 0;
		/*imgBuff.memory = NULL;
		imgBuff.size = 0;*/
    }
} TFriendInfo;

typedef std::vector<TFriendInfo> TFriendList;

typedef std::map<std::string, TFriendInfo*> TFriendMap;

enum
{
    SCENE_MAINMENU = 1000,
    SCENE_SELCHARA,
    SCENE_WORLDMAP,
    SCENE_DAILYSPIN,
    SCENE_SELBOOST,
    SCENE_GAME
};

enum 
{
	BOOST_NONE,
	BOOST_START,
	BOOST_SNIPER_AIM = BOOST_START,
	BOOST_EXTRA_BUBBLE_SWAP,
	BOOST_ZOMBIE_REPELLENT, 
	BOOST_BUBBLE_NUKE,
	BOOST_COLOR_BOMB,
	BOOST_PIN_POPPER,
	BOOST_CAMERA_SCREEN,
	BOOST_UNDO_MOVE,
	BOOST_FORESIGHT,
	BOOST_EXTRA_BUBBLES,
	BOOST_FIREBALL,
	BOOST_RENEW,
	BOOST_DIFFUSION,
	BOOST_COUNT,
	BOOST_SEL = BOOST_UNDO_MOVE,
};


#define BST_MASK_SNIPER_AIM			(1 << BOOST_SNIPER_AIM)
#define BST_MASK_EXTRA_BUBBLE_SWAP	(1 << BOOST_EXTRA_BUBBLE_SWAP)
#define BST_MASK_FORESIGHT			(1 << BOOST_FORESIGHT)
// #define BST_MASK_ZOMBIE_REPELLENT	(1 << BOOST_ZOMBIE_REPELLENT)
// #define BST_MASK_BUBBLE_NUKE		(1 << BOOST_BUBBLE_NUKE)
// #define BST_MASK_COLOR_BOMB 		(1 << BOOST_COLOR_BOMB)
// #define BST_MASK_PIN_POPPER 		(1 << BOOST_PIN_POPPER)
// #define BST_MASK_CAMERA_SCREEN 		(1 << BOOST_CAMERA_SCREEN)
// #define BST_MASK_UNDO_MOVE 			(1 << BOOST_UNDO_MOVE)

#define BST_MASK_STATIC_BOOSTS	0x07

// Game level design
extern const float BUBBLE_SPEED;

extern const float BEE_ROTATION_ANGLE;

extern const int STAR_COND[];
extern const int UNLOCKWORLD_STAR[];

extern const int UNUSED_SCORE_MIN;
extern const int UNUSED_SCORE_MAX;

extern const int DROP_SCORE_MIN;
extern const int DROP_SCORE_PLUS;

extern const int EXPLODE_SCORE;

extern const int LIVE_COST;
extern const int LIVE_BUY_COUNT;

extern const int FREE_SPIN_PERIOD;
extern const int FREE_LIVE_PERIOD;

extern const int UNLOCK_CHARA_STAR[];
extern const int UNLOCK_CHARA_MAGIC[];

extern const int BUY_MAGIC_CNT[];
extern const int BUY_SPIN_CNT[];
extern const int BUY_SPIN_COST[];
extern const int BUY_BOOST_PACK;

extern const int BONUS_MAGIC[];
extern const int BOOST_APPEAR_LEVEL[];
extern std::string BOOST_DESC[];

extern const SPECIAL_BOOST_INFO special_boost_info[];

extern const char animalsEquippedPowerups[];

extern const BUBBLE_POINTS bubble_points_info[];

extern const int STREAKS_PLUS_COUNT;
extern const float STREAKS_PLUS[];

extern const int STREAKS_MINUS_COUNT;
extern const float STREAKS_MINUS[];


extern const int MIN_LEVEL_CONTINUE_6MAGIC;
extern const int MIN_LEVEL_CONTINUE_8MAGIC;

//extern const int PUZZLE_PIECE_PRICE;
extern const int PUZZLE_PIECE_PRICE[];

BOOST_COST getBoostCost(int boostType);
int	getMagicBonus(int nLevel);
std::string getDotDigit(int nDigit);
std::string toString(int n);
int getTimeSec() ;
int getTimeMilis();
int getToday();
int getWorldByLevel(int nLevel);
int getLevelsPerWorld(int nWorld);
const char* getTimeStr(int nTime);
void splitString(const std::string &str, const std::string &separator, std::vector<std::string> &retList);
std::string getChallengeDescShift(const char* desc, int nShift);
bool isTutorialLevel(int nLevel);
bool compScore(TFriendInfo* left, TFriendInfo* right);


#define LEVEL_COUNT_PER_WORLD 16
#define WORLD_COUNT				7
#define MAX_COLORS				7
#define COLOR_MIN				1
#define COLOR_MAX				6
#define COLOR_COUNT				6
#define PIECE_PACK_NUM			6

#define CHARA_COUNT				6
#define BOOST_SEL_COUNT			4
// modified by KUH from 5 ==>20
#define MAX_LIVE				5

#define MAX_SHOOT_COUNT 200
#define MAX_LEVEL				128
#define MAX_ORG_LEVEL			96
#define MAX_LOCAL_FRIENDS		3
#define MAPPIECE_PACK_CNT		4
#define DAILY_CHAL_COUNT		5
#define DAILY_CHAL_STARTLEVEL	9
#define DAILY_CHAL_APPEARLEVEL	14

#define kAdvertiseHeight 85
#define kActionBarHeight 106
#define YES true
#define NO	false
#define MYFBID_DEFAULT	"myfb"

#define RECORD_SEPERATOR "^"
#define VALUE_SEPERATOR "|"


#define PAN_HEIGHT 8505
#define PAN_ORG_H 6058
#define FRIEND_ARR 3
#define MAX_RANK    5
#define LVL_POS_ROWS	8

enum 
{
	TAG_WRLD_PIC_BG,
	TAG_WRLD_PICTURE,
	TAG_WRLD_PICTURE_CLIP,
	TAG_WRLD_NAME,
	TAG_WRLD_SCORE,
	TAG_WRLD_FRNOBG,
	TAG_WRLD_FRNO,
};



#include "LResource.h"

enum GOAL_ACTION
{
	GA_NONE,
	GA_SAVE,
	GA_POP,
	GA_DROP,
	GA_TOUCH,
	GA_DONT,
	GA_COUNT
};

enum 
{
	COL_NONE,
	COL_PURPLE,
	COL_BLUE,
	COL_GREEN,
	COL_YELLOW,
	COL_ORANGE,
	COL_RED,
	COL_ANY,
};

#define LEVEL_PLAYED		"Played level"
#define LEVEL_PLAYED_FIRST_TIME		"Played first time"
#define LEVEL_REPEATED		"Repeated level"
#define REFILL_LIVES		"Refill lives on level"
#define PURCHASE_FMT		"Purchase made %s"
#define PURCHASE_EXTRA_BUBBLES		"Purchase Extra Bubbles in level %d"
#define GOES_TO_PURCHASE_CREDIT_PAGE_AND_THEN_BACKS_OUT_OF_PURCHASE		"Purchase Aborted"
#define USERS_WHO_CONNECT_TO_FACEBOOK		"Connected to Facebook"
#define USERS_WHO_GIFT_LIVES		"Gift lives"
#define USERS_WHO_UNLOCK_FMT		"Unlocked %s"
#define WHO_IS_NOT_MOVING_TO_NEXT_LEVEL		"Not moved to next level on level %d"
#define USERS_WHO_UNLOCK_ACHIEVEMENTS		"Unlocked achievements"
#define USERS_WHO_COMPLETE_DAILY_CHALLENGES		"Completed daily challenges"
#define USERS_WHO_UNLOCK_WORLD_FMT		"Unlocked world %d"
#define WHICH_LEVELS_USERS_USE_A_POWER_UP_ON		"Used a power up on level %d"
#define HOW_MANY_LOSSES_PER_LEVEL		"Losses level %d"
#define HOW_MANY_WINS_PER_LEVEL		"Wins level"
#define HOW_MANY_USERS_COME_TO_SPEND_WHEEL_DAILY		"Come to spend wheel daily"
#define HOW_MANY_USERS_PURCHASE_SPINS_FMT		"Purchase made %d spins"
#define HOW_MANY_USERS_PURCHASE_CREDITS_FMT		"Purchase made %d credits"
#define WHO_COMPLETED_TUTORIAL "Completed tutorials"


typedef struct tagGoalRecord
{
	char action;
	char type;
	char color;
	bool bigger;
	short count;
} TGoalRecord;

typedef struct tagGoalInfo
{
	short level;
	const char* desc;
	bool	isDone;
} TGoalInfo;

enum 
{
	TAT_STARS,
	TAT_ALLSTARS,
	TAT_UNLOCK_CHARA,
	TAT_POP_BUBBLES,
	TAT_DROP_BUBBLES
};
struct TAchievementItem
{
	const char* achieveDesc;
	const char* achieveId;
	int type;
	int count;
	int rewardMagics;
	bool isAchieved;
};
typedef std::vector<TAchievementItem> TAchieveList;

typedef struct tagDailyChallengeInfo
{
	TGoalInfo* pGoalInfo;
	unsigned char rewardType;
	int rewardValue;
} TDailyChallengeInfo;

struct TDailyChallengeState
{
	unsigned char nLastLevel;
	int nLastChangedDay;
};

typedef std::vector<TGoalRecord> TGoalList;
typedef std::vector<TGoalRecord*> TPGoalList;

typedef std::vector<TGoalInfo*> TPGoalInfoList;

typedef std::vector<TDailyChallengeInfo> TDailyChallengeList;

struct TMagicCost
{
	int nMagicCount;
	std::string buyTitle;
	TMagicCost(int nCount, std::string title)
	{
		nMagicCount = nCount;
		buyTitle = title;
	}
};
typedef std::vector<TMagicCost> TMagicCostList;

class TCharaInfo
{
public:
	unsigned char	starCount[MAX_LEVEL];
	char	byPieceCount;

	bool isUnlocked() 
	{
		return byPieceCount >= PIECE_PACK_NUM;
	}
};

struct TReqInfo
{
    std::string senderFBId;
    std::string senderName;
    std::string reqId;
    
    TReqInfo(std::string req_id, std::string fbid, std::string name)
    {
        reqId = req_id;
        senderFBId = fbid;
        senderName = name;
    }
};

typedef std::vector<TReqInfo> TReqList;

struct TMapInfo
{
	unsigned char m_byStar1;
	unsigned char m_byStar2;
	unsigned char m_byStar3;
	unsigned short m_wLimitTime;
};

typedef std::vector<cocos2d::CCPoint> TPOINT_LIST;
typedef std::vector<cocos2d::CCNode*> TNODE_LIST;
typedef std::vector<std::string> TSTRING_LIST;

extern std::string CHARA_NAME[];
extern std::string BOOST_NAME[];
extern std::string LOCAL_FRIEND[];
extern std::string LEVELS_NAME[];

#define WORLDIMG(imgName) (CCString::createWithFormat("Image/world/w%d/%s", LCommon::sharedInstance()->curWorld, imgName)->getCString())

class CSaveUtil;

class LCommon
{
private:
	LCommon();
	~LCommon();

public:
	int curWorld;	//Current world
	int nCurLevel;	//Current level
	int nMaxLevel;	//Max level

	LEVEL_TYPE curLevelType;


	int curLevel();
	int maxLevel();
	int stars(int nLevel);
	LEVEL_TYPE getCurLevelType();
	bool setMaxLevel(int level);
	bool setStarCount(int nLevel, int nCnt);
	void setCurLevel(int level);
	void setCurLevelType(LEVEL_TYPE type);

	int curBoosts;	//Boosts selected in Choose Boosts dialog

	int curChara;	//Current Character
	TCharaInfo charaInfo[CHARA_COUNT];	//Every characters info : every levels star counts, piece count
	bool boostUnlocked[BOOST_COUNT];	//whether boost is unlocked or not

	int magicCount;	//MagicCount user have now
	int liveCount;	//LiveCount user have now
	int spinCount;	//SpinCount user have now
	int payoutCount;
	int boostCount[BOOST_COUNT];	//Every Boost items count user have now
    
    // added by KUH in 2014-09-26
    bool parseMainMenu;
    bool parseSelChar;
    bool parseLevelWorld;

	int totalPopped;
	int totalDropped;
	int totalSaved;

	int freeSpinSecCount;
	int freeLiveSecCount;

	bool isTapToShoot;
	bool isSoundOn;
	bool isNotification;
	bool is3StarsPointHint;

	bool isRemoveAds;
    bool isFBConnected;
	int	lastScore;
	int nGoalAchievemens;
	bool isGameDataLoaded;
    bool isPassLoseLifeTuto;
	bool isPassSpinTuto;

	bool needSeePassLoseLiftTuto;
	bool needSeeSpinTuto;
    std::string myFBID;
    int currentAppVersion;
	int nLastRatedVersion;

	bool isTutoPassed(int nLevel);
	void setTutoPassed(int nLevel);
    bool isGotFreeMagic();
public:
	static LCommon* sharedInstance();
	void saveState();
	void loadState();
	std::string getSavedData();
	void loadFromString(const char* strData);

	void loadIntArray(int* dst, const char* key, int nDefaultVal, int maxLength, CSaveUtil* saveUtil);
	void saveIntArray(int* src, const char* key, int maxLength, CSaveUtil* saveUtil);
    
	void serialGoalData(bool bLoadOrSave, TGoalInfo* goalList, int nCount, const char* strKey);
	void serialLevelGoalData(bool bLoadOrSave);
	void serialDailyChal(bool bLoad);

	void setItemBuy(int type, int index);
	bool isItemBuy(int type, int index);
	void initConstValues();
	bool checkFirstRun();
	bool isBonusLevel();
	int	 getStarsForWorld(int nChara, int nWorld);
    void loadFriendInfo(const char* infoStr);
    cocos2d::CCTexture2D* getImageFromURL(const char* url);
    void addFriendPicture(const char* key, void* buf, int nSize);
	void addLiveCount(int liveCount);
	void decreateSpinCount();
    void serialFreeValues(bool bSave);
	bool getDataFromURL(const char* url, MemoryStruct& buffer);

	bool loadGameDataViaNet();
	void loadFromMetaData(std::string& metadata);

	bool checkFreeTimer();
	void randomizeColor();
	char getShiftColor(char color);

    TFriendMap friendList;
	int	localFriendMaxLevels[MAX_LOCAL_FRIENDS][CHARA_COUNT];
	void clearFriendList();

	TFriendPhotoMap friendPhotoList;

	//TFriendList localFriendList;

	void createTreasureList();
	void createDailyChallengeList();
	static int getOrgTreasureCount();
	static TGoalInfo* getOrgTreasure(int idx);
	TPGoalInfoList	treasureList;
	TDailyChallengeList dailyChallengeList;
	TDailyChallengeState dailyChallengeState;
	unsigned char dailyChalIds[MAX_LEVEL];
	int todayKey;

	bool isViaMoregame;
	char	shiftColor[MAX_LEVEL];
	TMapInfo mapInfo[MAX_LEVEL];
	int     highScore[MAX_LEVEL];
    
    TReqList    liveReqList;
    TReqList    collectList;
	TMagicCostList	magicCostList;

    TReqInfo popCollectAtIndex(int nIndex);
    TReqInfo popLiveReqAtIndex(int nIndex);
	void checkAchievedList(int nType, int nCount, TAchieveList& retList);
	int checkAchievement(int nFinalScore);
	void serialAchievementData(bool bLoad);
    void resetAchievement();
	void notifyCharaUnlocked(int nCharaId);

protected:
	std::string keyForType(int nType, int index);
protected:
	std::map<std::string, bool> buyStateDic;
	int lastFreeSpinSec;
	int lastFreeLiveSec;
};

#endif

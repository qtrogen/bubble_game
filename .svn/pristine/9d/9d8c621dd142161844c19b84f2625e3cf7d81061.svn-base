//
//  BubbleLayer.h
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//
#ifndef __BubbleLayer_H__
#define __BubbleLayer_H__

#include "cocos2d.h"
#include <vector>
#include "cocos-ext.h"
#include "LCommon.h"
#include "Bubble.h"
#include "Box2D/Box2D.h"

USING_NS_CC;
USING_NS_CC_EXT;


#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
//#define ROBOT_SHOOT
#endif

enum 
{
	GS_UPDATE_LOOK,
	GS_LAUNCH_BUBBLE,
	GS_TAKE_BUBBLE,
	GS_SEND_NEXT_BUBBLE,
	GS_REMAIN_BUBBLES,
	GS_UPDATE_SCORE,
	GS_UPDATE_MULTIPLIER,
	GS_END_SUCCESS,
	GS_END_FAIL,
	GS_ANIM_REPELLANT,
	GS_ANIM_NUKE,
	GS_ANIM_RAINBOW,
	GS_ANIM_MORCRASHED,
};

enum
{
	STATE_READY,
	STATE_PLAY,
	STATE_END,
};

#define MASK_ENABLE_SWAP 1

struct MyInfo
{
	int minVal;
	int maxVal;
	int totalVal;

	void init()
	{
		minVal = 100000;
		maxVal = 0;
		totalVal = 0;
	}

	void update(int curVal)
	{
		if (curVal < minVal) minVal = curVal;	
		if (curVal > maxVal) maxVal = curVal;	
		totalVal += curVal;
	}

	std::string toString(int nTotalCount)
	{
		return CCString::createWithFormat("\t\t %d \t\t\t %d \t\t\t %.2f", minVal, maxVal, totalVal * 1.0f / nTotalCount)->getCString();
	}
};

struct MyStructHit
{
	unsigned char color;
	unsigned char popCount;
	unsigned char dropCount;

	void init()
	{
		color = 0;
		popCount = 0;
		dropCount = 0;
	}
};

struct AITestLevelInfo
{
	std::string strTime;
	unsigned int nTotalShoots;
	unsigned int nScore;

	void init()
	{
		strTime = "";
		nTotalShoots = 0;
		nScore = 0;
	}
};

class CBubbleGame;
class CAimLayer;

class CBubbleLayer : public CCLayer {
public:
	CBubbleLayer();
	~CBubbleLayer();

	static CBubbleLayer* create();
	void setGameRoot(CBubbleGame* pBubbleGame) { m_pGameRoot = pBubbleGame; };
	bool loadStageData(int nStage);
	void set_BubbleExistArr();
	void set_OuterEmptyBubblesArr(int nIndex, int nTotalCounts);
	
	void initGame();
	void startGame();
	void continueGame();
	void setStartPos(CCPoint pos);
	void setNextBubblePos(CCPoint nextPos);
	void setBound(CCRect bound);
	CBubble* createNewBubble(bool bNotify = true);

	bool isThisColorInBoard(char nColor);
	int getNewBubbleColorByBoardState();

	void addNewBubbles();

	bool launchBubble();
	void updateBoost();
	bool isPlaying();
	void undoMove();
	bool canUndoMove();

	int currentState() { return m_nState; }
	void setGameState(int nState, int nVal = 0);

	void checkMoveLayer(CCObject* bFromCamera = NULL);
	void afterMoveLayer(CCObject* bFromCamera);
	void startCameraScreen();
    
	void processDiffusion();
	void processBubbleDiffusion(CCNode* node);
	
	unsigned char m_nPrevailColor;


	int	m_nCurShootIndex;
	short m_wStageFlags;
	short m_wStar2;
	short m_wStar3;
	int m_nScoreForUnused;

	int m_nTotalPopped;
	int m_nTotalDropped;
	int m_nTotalStar;

	bool m_bPinPopping;
	CCPoint m_ptPinPos;

	TBackBubbleList backBubbleList;
	stBubbleBack	lastBubbleBack;
	CBubble*		lastBubble;
	int m_nSavedFriends;

	bool m_bLayerMoving;
	int m_nRemainCount;

	float m_fCurrentMultiplier;
	float m_fStreaksPlus;

	int m_nStreaksMinusCount;
	int m_nPrevStreaksMinusCount;

	int nTotalPlaceholderCounts;
	int* BubbleExistArr;
	int* OuterEmptyBubblesArr;

	typedef std::map<int, int> TBubbleStatMap;
	bool	m_bEnableSwapping;
	TBubbleStatMap poppedList;
	TBubbleStatMap droppedList;
	TBubbleStatMap initialList;
	bool stoneTouched;
	bool prevStoneTouched;

	void addHistory(CBubble* bubble, bool bPopped);
	int getTotalVal(char type, char color, bool bPopped);
	int getInitialCount(char type, char color);
	bool touchObstacles[BB_KIND_COUNT];
	bool touchObstaclesBack[BB_KIND_COUNT];
	int getKey(char type, char color);
	std::string getBubbleAnal();
	float getLayerDiff();

	TBubbleList m_BubbleList;	//Bubble List
	float m_fMargin;

#ifdef ROBOT_SHOOT
	// ai logic
	void initAi();
	bool aiShoot(bool bForce = false);

	float findBestAngle();
	void aiLog();
	void analyzeColor(int color);
	void shootBubbleAi(float dt);
	CCPoint getLastPoint(float fAngle);
	int getPointByPos(CCPoint dstPos, CBubble* curBubble);
	void recreateMapData(int nStage, int nPrepareCnt, unsigned char prepareData[MAX_SHOOT_COUNT]);
	void updateReportData();
	std::string keyForBubble(CBubble* bubble);

	MyStructHit colorList[MAX_SHOOT_COUNT];
	int m_nCurShootNo;
	int m_nAiTwice;

	MyInfo infoLeft;
	MyInfo infoPopped;
	MyInfo infoDropped;

	typedef std::map<std::string, int> TMapIntInfo;
	TMapIntInfo mapColorPop;
	TMapIntInfo mapColorDrop;

	typedef std::map<std::string, MyInfo> TMapInfoInfo;
	std::map<std::string, MyInfo> mapInfoColorPop;
	std::map<std::string, MyInfo> mapInfoColorDrop;

#endif // ROBOT_SHOOT

	typedef std::vector<CCPoint>	TPointList;

	void loadGameData();

	int getEdgeIndex(int center, int direction, int column, int maxy);

	void updateTargetMove();
	void updateAngle(CCNode* node);
	void finishedMoving(CCNode* node);
	void finishedMovingForFireball(CCNode* node);
	void processFireballMoving(float dt);

	void setNewBubbleIndex(CBubble* newBubble, CBubble* neighbourBubble);
	void clearBubbleCheck();

	void setBeeGroupBubbles(CBubble* beeBubble);
	void setNeighboursAsBeeGroup(CBubble* bubble);
	void finishedBeeRotation(CCNode* node);
	void updateBubblesInBeeLevel(float dt);

	// bubble logic
	bool getNextPoint(CCPoint pos, float ballSize, CCPoint& ptNext, int& flyOption);
	bool getNextPointForFireball(CCPoint pos, float ballSize, CCPoint& ptNext, int& flyOption);
	bool checkCollision(const CCPoint& fixPos, const CCPoint& flyPos, const CCPoint& dstPos);
	bool processNeighbor(CBubble* src, TBubbleList& candiList, int depth = 0);
	bool processNeighborForBounce(CBubble* src, float fBounce);
	bool processExplodeBubble(TBubbleList& bubbleList, float fDelay, bool bNuke);
	bool processExplodeBubble(CBubble* bubble, float fDelay, bool bNuke);
	bool findBubbles(CBubble* src, bool isByColor, char color, int nCount, TBubbleList& bubbleList);
	bool findBubbles(CBubble* src, bool isByColor, char color, int depth, int& nCount, TBubbleList& bubbleList);

protected:
	TBubbleList getDropBubbleList();
	bool findDropBubbles(CBubble* bubble, TBubbleList& bubbleList);
	void procDropBubbles(TBubbleList& bubbleList, bool bRemove = true);
	void finishedDrop(CCNode*);

	void checkBeeBubbleAround(TBubbleList& dropList);
	CBubble* getBeeBubble();

	bool animateMorFlyToNext();
	void morNormal(CCNode* node);
	void morPrejump(CCNode* node);
	void morJump(CCNode* node);
	void morFlying(CCNode* node);
	void finishedMorJump(CCNode* node, void* data);
	void finishedMorFlying(CCNode* node);

	void checkMorBubbleAround(TBubbleList& dropList);
	int morBubbleCounts();
	CBubble* getMorBubble();


	void finishedExplode(CCNode*);

	void removeBubbleFromList(CBubble* bubble);
	void setBubbleOpacity(CCNode* node, void* data);
	void removeNodeAndCleanup(CCNode* node);

	bool isDropable(CBubble* bubble);

	void animateExplode(CBubble* bubble, float fDelayTime, bool bFireball = false);
	void animateScore(CCPoint pos, float score);
	void animateMultiplier();
	float animateUnused();
	void updateb2PhysicsWorld(float dt);
	void addExtraBubblesToPhysicsWorld(CCNode* sprt);
	void finishedUnusedAnim(CCNode* sprt);

	CCSprite* saveFriend(CBubble* bubble, float fDelayTime);

	void decreaseRemainCount(int nDecCount, bool bRemoveCur = false);

	void endGameSuccess(float dt);
	void endGameFail(float dt);
	void startGamePlay(bool bFirst);
	void afterExplodeProc(bool bNeedDropProc, bool bPrepareNextBubble = true);
	void afterExplodeProcForFireballMoving();
	void addGameOverBubblesToPhysicsWorld();

	void pinPopBubble(CCPoint pos);

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch* touches, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touches, CCEvent* event);
	virtual void ccTouchCancelled(CCTouch* touches, CCEvent* event);

	void onPrev(CCObject* obj);
	virtual void onEnter();
	void initUI();
	void createExplodeAnimeForNormal(CBubble* bubble, float fStartDelayTime);
	void createExplodeAnimeForFireball(CBubble* bubble, float fStartDelayTime);

	void bringBubble(CBubble* bubble);

	void playEffect(const char* sfxFileName);
	void playEffect(const char* sfxFileName, float fVolume);

	void calcStreaksPlusVal(TBubbleList& bubbleList);

	UIPanel*	m_pMainPanel;

	int currentMap;
	int m_nStageCount;

	int m_nFriendCount;

	int	m_nState;
	int m_nScoreIndex;

	// UI Section

	typedef std::vector<char> TColorList;
	TColorList m_pPrepareColorList;	//Color list of remaining bubbles players should shoot
	int m_nTotalBubbleCount;

	CCRect		rcBound;
	CCRect		absBound;
	CCPoint		absPos;
	CCPoint		m_ptNextBubble;
	CCPoint		m_ptEnd;
	double		m_fLaunchAngle;
	bool		m_bLaunchBubbleUp;
	bool		m_bLaunchBubbleDirectionChanged;
	CAimLayer*	m_AimLayer;

	float		m_fRotatedAngleByBee;
	bool		m_bBeeRotating;

	TPointList	m_vecLines;

	typedef std::map<int, CCPoint> TPointMap;
	TPointMap	m_vecMagnets;

	CBubbleGame*	m_pGameRoot;

	CCAnimation*	m_pAnimExplode;

public: // public for games
	CCPoint		m_ptStart;

	CCSprite*	m_pMor;
	//CCPoint		m_ptMorCurPos;
	//CCPoint		m_ptMorNextPos;

	b2World *	b2PhysicsWorld;

};

#endif //__BubbleLayer_H__

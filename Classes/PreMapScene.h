//
//  CPreMapScene.h
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//
#ifndef __PreMapScene_H__
#define __PreMapScene_H__

#include "cocos2d.h"
#include "BaseLayer.h"

USING_NS_CC_EXT;

class CPreMapScene : public CBaseLayer {
public:
	CPreMapScene();
	~CPreMapScene();

	CREATE_SCENE_METHOD(CPreMapScene)
    
	void transitScene(float dt);
protected:
	virtual void onEnterTransitionDidFinish();
    virtual void onEnter();
    virtual void onExit();
protected:
};

#endif //__SplashSecene_H__

//
//  CDailySpin.h
//  IcecreamCandyBlast
//
//  Created by Chen on 7/11/13.
//  Copyright 2013 lion. All rights reserved.
//
#ifndef __CDailySpin_H__
#define __CDailySpin_H__

#include "cocos2d.h"
#include "LPurchase/LStoreManager.h"
#include "cocos-ext.h"
#include "BaseLayer.h"

USING_NS_CC_EXT;
#define LINE_COUNT 3
#define ITEM_KINDS 3
#define SPIN_ITEM_COUNT 3

typedef std::vector<cocos2d::CCSprite*> TSpriteList;

enum 
{
	ST_READY_SPIN,
	ST_ROLLING,
	ST_STOPPING,
};

enum 
{
	SP_BLANK = -1,
	SP_POWERUP = 0,
	SP_LIVE,
	SP_MAGIC,
	SP_PUZZLE_PIG_RABBIT,
	SP_PUZZLE_MONKEY,
	SP_PUZZLE_PENGUIN,
	SP_PUZZLE_PANDA,
	SP_KIND_COUNT
};

extern int DailySpin_PreviousScreen;

class CDailySpin : public CBaseLayer {
public:
	CREATE_SCENE_METHOD(CDailySpin);

	CDailySpin();
	~CDailySpin();

	void setupScroll(UIWidget* spinPane);

	void onPayout(cocos2d::CCObject* sender);
	void onBuySpin(cocos2d::CCObject* sender);
	void onBack(cocos2d::CCObject* sender);
	void onTouchJoyStick(CCObject* sender, TouchEventType eventType);
	void updateSpin(float dt);
	void prepareSpin();
	void showResultPopup(float dt);
	void showNoEnoughSpin();
	void procResult(float dt);

	void createTargetItem();
	void createNextItem();
	void loadSpinData();
	void saveSpinData();
	int	spItemNext[SP_KIND_COUNT];
	int		m_nSpinIndex;
	int		m_nTargetKind;
	int		m_nTargetItem;
	int getRangeForChara(int nCharaIndex);
	int		m_nSpinStopCounter;

	virtual bool onProcBeforeClose(int nButtonTag);
	virtual bool onProcAfterClose(int nTag, int nParam);
	virtual void onEnter();
	virtual void onExit();

	virtual void refreshInfo();

	void refreshSpinCount();
	virtual void onTimer();
protected:
	void startSpin();
	void stopSpin();

protected:
	cocos2d::CCPoint	m_ptJoystickFirstPos;
	CCNode		*m_spinPan;
	UILabelBMFont*	m_labelPayouts;
	UILabelBMFont*	m_labelSpinCount;
	UILabel*		m_labelSpinNext;

	float	m_ptJoystickEndY;
	float	m_fSpeed[LINE_COUNT];
	TSpriteList	m_spItems[LINE_COUNT];
	int		m_pFinalItems[LINE_COUNT];
    std::vector<int>    boostList;
    
	int		m_nSlotState;
	int		m_bSwitchMoving;

	int		m_nSpinItemCount;
	int		m_nRewardPieceChara;
	int		m_nRewardCount;

	int		m_nStoppedIndex;
	bool	m_isWin;
	int		m_nRemainedFreeTime;
	bool	m_bShowNoEnoughPopup;
};

#endif //__CDailySpin_H__
#ifndef __CAIMLAYER_H__
#define __CAIMLAYER_H__

#include <vector>
#include "cocos2d.h"
#include "LCommon.h"

class CAimLayer : public cocos2d::CCLayer
{
public:
	CAimLayer();
	~CAimLayer(void);
	static CAimLayer* create();
//	virtual void draw();
	void setAimDots(TPOINT_LIST* lines);
	void setAimRays(TPOINT_LIST* lines);
	void setDrawInfo(cocos2d::CCPoint ptStart, float fBubbleSize);
	cocos2d::CCSprite* createAimSprite();
	cocos2d::CCSprite* createRayBottomSprite();
	cocos2d::CCSprite* createRayTopSprite();
	void setSniperAim(bool bApply);
protected:
	TPOINT_LIST* m_vecLines;
	float	m_fBubbleSize;
	bool m_bSniperAim;

	std::vector<cocos2d::CCSprite*> m_pAimList;
	std::vector<cocos2d::CCSprite*> m_pRayBottomList;
	std::vector<cocos2d::CCSprite*> m_pRayTopList;
	cocos2d::CCPoint m_ptStart;
};

#endif // __CAIMLAYER_H__


//
//  WorldMap.m
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//

#include "WorldMap.h"
#include "LCommon.h"
#include "CMainMenuLayer.h"
#include "SimpleAudioEngine.h"
#include "AppDelegate.h"
#include "SelectChara.h"
#include "DailySpin.h"

enum 
{
	TAG_WORLD_START = 1,
	TAG_WORLD_HOME = WORLD_START,
	TAG_WORLD_DAYLYSPIN,
	TAG_WORLD_1,
	TAG_WORLD_2,
	TAG_WORLD_3,
	TAG_WORLD_4,
	TAG_WORLD_5,
	TAG_WORLD_6,
};

USING_NS_CC;

CCScene * WorldMap::scene()
{
	CCScene *scn = CCScene::create();
	WorldMap* layer = new WorldMap();
	layer->autorelease();
	scn->addChild(layer);
	return scn;
}

WorldMap::WorldMap()
{
	UIWidget* m_pWidget = dynamic_cast<UIWidget*>(GUIReader::shareReader()->widgetFromJsonFile("Image/worldmap.json"));

	m_pMainLayer->addWidget(m_pWidget);

	UIWidget* imgView = m_pWidget->getChildByName("imageBG");
	imgView->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
	imgView->setScale(SC_BG_SCALE);

	m_pMainPanel = m_pWidget->getChildByName("MainPanel");
	m_pMainPanel->setAnchorPoint(ccp(0.5, 0.5));
	m_pMainPanel->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));

#if(CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
	((UIDragPanel*)m_pMainPanel)->setClippingEnabled(false);
#endif

	((UIDragPanel*)m_pMainPanel)->setBounceEnabled(true);

	UIWidget* btnHome = dynamic_cast<UIWidget*>(m_pMainPanel->getChildByName("btn_home"));
	btnHome->addReleaseEvent(this, coco_releaseselector(WorldMap::onHome));

	UIWidget* btnSpin = dynamic_cast<UIWidget*>(m_pMainPanel->getChildByName("btn_spin"));
	btnSpin->addReleaseEvent(this, coco_releaseselector(WorldMap::onDailySpin));

	for (int i = 0; i < MAX_WORLD; i++)
	{
		UIWidget* button = dynamic_cast<UIWidget*>(m_pMainPanel->getChildByName(CCString::createWithFormat("btn_world%d", i + 1)->getCString()));
		button->addReleaseEvent(this, coco_releaseselector(WorldMap::onSelectWorld));
	}

	createActionBar();
}

WorldMap::~WorldMap()
{
}

void WorldMap::refreshWorldState()
{
	//Liuwei changed!
	//int maxWorld = LCommon::sharedInstance()->maxLevel / LEVEL_COUNT_PER_WORLD;
	int maxWorld = LCommon::sharedInstance()->maxLevel() / LEVEL_COUNT_PER_WORLD;

	for (int i = 0; i < MAX_WORLD; i++)
	{
		UIWidget* button = dynamic_cast<UIWidget*>(m_pMainPanel->getChildByName(CCString::createWithFormat("btn_world%d", i + 1)->getCString()));
		UILabelBMFont* lblStars = dynamic_cast<UILabelBMFont*>(button->getChildByName(CCString::createWithFormat("lblWorld%d", i + 1)->getCString()));
		if (i <= maxWorld)
		{
			button->active();
			
			//Liuwei changed!
			//int nWorldStarCnt = LCommon::sharedInstance()->getStarForWorld(i);
			int nWorldStarCnt = LCommon::sharedInstance()->getStarsForWorld(0, i);
			
			lblStars->setVisible(true);
			lblStars->setText(CCString::createWithFormat("%d/48", nWorldStarCnt)->getCString());
		}
		else
		{
			lblStars->setVisible(false);
			button->disable();
		}
	}
}

void WorldMap::onSelectWorld(cocos2d::CCObject* obj)
{
	UIButton* node = dynamic_cast<UIButton*> (obj);
	LCommon::sharedInstance()->curWorld = node->getTag() - TAG_WORLD_1;

	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CSelCharaScene::scene(), ccWHITE));

	CCLog("selected world : %d", LCommon::sharedInstance()->curWorld);
}
void WorldMap::onHome(cocos2d::CCObject* obj)
{
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CMainMenuLayer::scene(), ccWHITE));
}

void WorldMap::onDailySpin(cocos2d::CCObject* obj)
{
	/*DailySpin_PreviousScreen = 1;
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CDailySpin::scene(), ccWHITE));*/
}

void WorldMap::onMoreApp(cocos2d::CCObject* obj)
{
	AppDelegate::app->showMoreApps();
}

void WorldMap::onEnter()
{
	//Liuwei changed!
	//CCLayer::onEnter();
	CCNode::onEnter();

	refreshWorldState();

	setPosition(ccp(0, SC_ADHEIGHT));
	setAnchorPoint(CCPointZero);
}

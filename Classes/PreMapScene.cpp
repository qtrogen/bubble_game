//
//  CSelCharaScene.m
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//

#include "LCommon.h"
#include "PreMapScene.h"
#include "LevelSelect.h"

USING_NS_CC;

CPreMapScene::CPreMapScene()
{
	UIImageView* imgView = UIImageView::create();
	imgView->loadTexture(WORLDIMG("world_bg.png"));
	imgView->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
	imgView->setScale(SC_BG_SCALE);
	m_pMainLayer->addWidget(imgView);
};

CPreMapScene::~CPreMapScene()
{

}

void CPreMapScene::onEnterTransitionDidFinish()
{
	CBaseLayer::onEnterTransitionDidFinish();
	scheduleOnce(schedule_selector(CPreMapScene::transitScene), 0.2);
}

void CPreMapScene::onEnter()
{
    CBaseLayer::onEnter();
    showIndicator();
}

void CPreMapScene::onExit()
{
    CBaseLayer::onExit();
}

void CPreMapScene::transitScene(float dt)
{
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CLevelSelect::scene(), ccWHITE));
    hideIndicator();
}

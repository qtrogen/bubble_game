
#include "AppDelegate.h"
#include "SimpleAudioEngine.h"
#include "LCommon.h"
#include "LPurchase/LStoreManager.h"
#include "SplashScene.h"
#include "LevelSelect.h"

#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "LJniMediator.h"
#elif(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include "FacebookInterface.h"

#include "platform/android/jni/Java_org_cocos2dx_lib_Cocos2dxHelper.h"
#include "platform/android/jni/JniHelper.h"
#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

/*
 * Class:     com_tumen_LJniMediator
 * Method:    sendToCocos
 * Signature: (Ljava/lang/String;I)V
 */
JNIEXPORT void JNICALL Java_com_tumen_LJniMediator_sendToCocos
  (JNIEnv *env, jobject obj, jint nType, jstring strParam, jint nParam)
{
	/*jboolean isCopy = false;
	const char* chsParam = env->GetStringUTFChars(strParam, &isCopy);
	AppDelegate::app->receivedFromNative(nType, std::string(chsParam), nParam);*/
}

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //CC_TARGET_PLATFORM

USING_NS_CC;
using namespace CocosDenshion;


#define REC_SEPERATOR   "^"
#define VALUE_SEPERATOR   "|"

#define M_SUCCESS	1
#define M_FAIL		2
#define M_ERROR		3

AppDelegate* AppDelegate::app = NULL;

AppDelegate::AppDelegate() {
	app = this;
}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    CCDirector* pDirector = CCDirector::sharedDirector();
    CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();

    pDirector->setOpenGLView(pEGLView);
	
    // turn on display FPS
    pDirector->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);

	LCommon::sharedInstance()->loadState();

//     if (TEST_MODE)
//     {
//	LStoreManager::sharedInstance()->setTestMode();
//    }

#ifdef PRO_VERSION
	if (LCommon::sharedInstance()->checkFirstRun())
	{
	}
#endif

	setScreenValues();

	LCommon::sharedInstance()->initConstValues();

//	pDirector->setContentScaleFactor(SC_FACTOR);

	if (!LCommon::sharedInstance()->isRemoveAds)
	{

#ifndef PRO_VERSION
#endif //PRO_VERSION
	}

	LCommon::sharedInstance()->isSoundOn = YES;

    // create a scene. it's an autorelease object
	CCScene *pScene = CSplashScene::scene();
//	CCScene *pScene = CBubbleGame::scene();

    // run
    pDirector->runWithScene(pScene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    CCDirector::sharedDirector()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();

	SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
	SimpleAudioEngine::sharedEngine()->pauseAllEffects();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    CCDirector::sharedDirector()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();

	SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
	SimpleAudioEngine::sharedEngine()->resumeAllEffects();
}

void AppDelegate::orientationChangedCallback(int nType)
{
	if(nType)
		CCLog("Portrait mode");
	else
		CCLog("Landscape mode");

	CBaseLayer* curScene = dynamic_cast<CBaseLayer*> (CCDirector::sharedDirector()->getRunningScene());
	if (curScene != NULL)
		curScene->orientationChangedCallback();
}

void AppDelegate::receivedFromNative(int nType, const char* strParam, int nParam)
{
    if (nType != MSG_PROFILE_PHOTO)
		CCLog("receivedFromNative %d, %s, %d", nType, strParam, nParam);
	else
		CCLog("receivedFromNative %d, %d", nType, nParam);

	switch ( nType )
	{
        case MSG_AD_HEIGHT:
            SC_BANNERHEIGHT = nParam;
            break;
        case MSG_LOWSPEC_DEVICE:
            IS_LOWMEMORY = (nParam != 0);
            break;
        case MSG_BUY_RESULT:
        {
            std::string sparam = strParam;
            if (nParam == M_SUCCESS)
                LStoreManager::sharedInstance()->successfulPurchase(sparam);
            else
                LStoreManager::sharedInstance()->failedPurchase(sparam);
            break;
        }
        case MSG_RESTORE_RES:
            if (nParam == M_SUCCESS)
            {
                T_PRODUCT_LIST lst;
                lst.push_back(strParam);
                LStoreManager::sharedInstance()->successfulRestore(lst);
            }
            else
                LStoreManager::sharedInstance()->failedRestore(strParam);
            break;
        case MSG_UPDATE_MYINFO:
			didGetMyFBProfile(strParam);
            break;
        case MSG_FB_CONNECTED:
			didFBconnected(nParam != 0);
            break;
        case MSG_PROFILE_PHOTO:
            receivedFriendPicture(strParam, nParam);
            break;
        case MSG_FRIEND_LIST:
            receivedFriendList(strParam);
            break;
		case MSG_PUSH_NOTIFICATION:
			receivedPushNotification();
			break;
        case MSG_FB_LIVEREQ_LIST:
            receivedLiveReqList(strParam);
            break;
        case MSG_FB_COLLECT_LIVE_LIST:
            receivedCollectLiveList(strParam);
            break;
        case MSG_FB_COLLECTLIVE_SUC:
            sendEventToCurrentScene(EVT_COLLECT_LIVE_SUC, strParam, nParam);
            break;
        case MSG_FB_GIVLIVE_SUC:
            sendEventToCurrentScene(EVT_GIVE_LIVE_SUC, strParam, nParam);
            break;
        case MSG_UD_DOWNLOAD_RES:
            receivedGameDataFromNet(strParam);
            break;
        case MSG_UD_UPLOAD_RES:
            break;
		case MSG_RATED_APP:
			receivedRateRes();
			break;
        case MSG_APP_VERSION:
            LCommon::sharedInstance()->currentAppVersion = nParam;
            break;
        case MSG_ACHIEVE_RESET:
            LCommon::sharedInstance()->resetAchievement();
            break;
		case MSG_SCREEN_ORIENTATION_CHANGED:
			orientationChangedCallback(nParam);
			break;
	}
}

void AppDelegate::removeAds()
{
	LCommon::sharedInstance()->isRemoveAds = YES;
	LCommon::sharedInstance()->saveState();
	CCLog("removeAds");
	sendMessageToNative(MSG_REMOVE_ADS, "", 0);
}

bool AppDelegate::sendMessageToNative(int nType, const char* msg, int nParam)
{
	CCLog("send to message %d, %s, %d", nType, msg, nParam);
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	{
		CCLog("send to message2 %d, %s, %d", nType, msg, nParam);
		const char* recvCls = "com/redrhino/bubble/bubble_game";

		JniMethodInfo methodInfo;
		bool isHave = JniHelper::getStaticMethodInfo(methodInfo,
			recvCls,
			"receivedFromCocos2dx",
			"(ILjava/lang/String;I)Z");
		if(isHave){
			CCLog("the receivedFromCocos2dx method exist.");
			jstring StringArg = methodInfo.env->NewStringUTF(msg);
			methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, nType, StringArg, nParam);
		}else{
			CCLog("the receivedFromCocos2dx method not exist.");
		}
	}
	return true;
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return LJniMediator::sharedInstance()->receivedFromCocos2dx(nType, msg, nParam);
#else
	return true;
#endif //CC_TARGET_PLATFORM
}

void AppDelegate::setSceneType(int sceneType)
{
	sendMessageToNative(MSG_SCENE_TYPE, "", sceneType);
}

void AppDelegate::setScreenValues()
{
	if (LCommon::sharedInstance()->isRemoveAds)
	{
		SC_ADHEIGHT = 0;
	}
	else
	{
		SC_ADHEIGHT = SC_BANNERHEIGHT;
	}

#ifdef PRO_VERSION
	SC_ADHEIGHT = 0;
#endif

	CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();
	CCSize viewSize = pEGLView->getFrameSize();

	CCLog("AppDelegate : viewSize : %f : %f", viewSize.width, viewSize.height);

	SC_FACTOR = 1.0f;

	SC_WIDTH = SC_DESIGN_WIDTH;// * SC_IF;
	SC_FACTOR = SC_DESIGN_WIDTH / viewSize.width;
	SC_HEIGHT = (viewSize.height - SC_ADHEIGHT) * SC_FACTOR;// / SC_RATIO;

	SC_IF = 1 / SC_FACTOR;
	SC_RATIO = SC_HEIGHT / SC_DESIGN_HEIGHT;

	// 	SC_FACTOR = 1.0f;
// 	SC_IF = 1 / SC_FACTOR;
// 
// 	SC_WIDTH = 768;// * SC_IF;
// 	SC_RATIO = viewSize.width * SC_FACTOR / 768.0f;
// 	SC_HEIGHT = (viewSize.height - SC_ADHEIGHT) / SC_RATIO;// / SC_RATIO;

	float hs = SC_WIDTH / SC_DESIGN_WIDTH;
	float vs = SC_HEIGHT / SC_DESIGN_HEIGHT;
	SC_BG_SCALE = (hs > vs)? hs : vs;
	//SC_BG_SCALE = vs;

	SC_DLG_SCALE = ((SC_HEIGHT / SC_WIDTH) > 1.5f) ? 1.2 : 1.0f;

	SC_WIDTH_HALF = SC_WIDTH / 2;
	SC_HEIGHT_HALF = SC_HEIGHT / 2;

	CCLog("AppDelegate : SC Size : %f : %f", SC_WIDTH, SC_HEIGHT);

	CCEGLView::sharedOpenGLView()->setDesignResolutionSize(SC_WIDTH, SC_HEIGHT, kResolutionShowAll);
	
	CCRect rect = pEGLView->getViewPortRect();
	CCLog("AppDelegate ViewPort Rect : %f : %f : %f : %f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);

    //CCEGLView::sharedOpenGLView()->setDesignResolutionSize(768, 1365, kResolutionShowAll);

//	CCDirector::sharedDirector()->setContentScaleFactor(2.0f);
}

bool AppDelegate::buyProduct(std::string& productId)
{
	return sendMessageToNative(MSG_BUY_REQUEST, productId.c_str(), 0);
    
}
bool AppDelegate::restoreProducts()
{
	return sendMessageToNative(MSG_RESTORE_REQ, "", 0);
}

void AppDelegate::showLeaderboard()
{
	sendMessageToNative(MSG_SHOW_LDB, "", LCommon::sharedInstance()->lastScore);
}

void AppDelegate::showRevmob()
{
	sendMessageToNative(MSG_SHOW_REVMOB, "", 0);
}

void AppDelegate::showChartboost()
{
	sendMessageToNative(MSG_SHOW_CHARTBOOST, "", 0);
}

void AppDelegate::showAppLovin()
{
	sendMessageToNative(MSG_SHOW_APPLOVIN, "", 0);
}

void AppDelegate::showPlayHaven()
{
	sendMessageToNative(MSG_SHOW_PLAYHAVEN, "", 0);
}

void AppDelegate::showMoreApps()
{
	sendMessageToNative(MSG_SHOW_MOREAPPS, "", 0);
}

void AppDelegate::showFacebook()
{
	sendMessageToNative(MSG_SHOW_FB, "", LCommon::sharedInstance()->lastScore);
}

void AppDelegate::showTwitter()
{
	sendMessageToNative(MSG_SHOW_TW, "", LCommon::sharedInstance()->lastScore);
}

void AppDelegate::showRateApp()
{
	sendMessageToNative(MSG_SHOW_RATE, "", 0);
}

void AppDelegate::connectFacebook()
{
	sendMessageToNative(MSG_CONNECT_FB, "", 1);
}

void AppDelegate::logoutFacebook()
{
	LCommon::sharedInstance()->clearFriendList();
	sendMessageToNative(MSG_CONNECT_FB, "", 0);
}

void AppDelegate::requestFriendList()
{
	sendMessageToNative(MSG_FRIEND_LIST_REQ, "", 0);
}

void AppDelegate::submitScore(int level, int score)
{
	sendMessageToNative(MSG_SUBMIT_SCORE, toString(level).c_str(), score);
}

void AppDelegate::submitAchievement(const char* strid, const char* strDesc)
{
	sendMessageToNative(MSG_SUBMIT_ACHIEVE, strid, 0);

	sendMessageToNative(MSG_NOTIF_ACHIEVE, strDesc, 0);
}

void AppDelegate::submitLeaderboard(const char* strid, int nValue)
{
	sendMessageToNative(MSG_SUBMIT_LEADERBOARD, strid, nValue);
}

void AppDelegate::showIndicator(std::string msg)
{
	sendMessageToNative(MSG_SHOW_INDICATOR, msg.c_str(), 0);
}

void AppDelegate::showIndicator()
{
	showIndicator("Please wait");
}

void AppDelegate::hideIndicator()
{
	sendMessageToNative(MSG_HIDE_INDICATOR, "", 0);
}

void AppDelegate::receivedFriendList(const std::string& friendList)
{
    std::vector<std::string> retList;
    splitString(friendList, REC_SEPERATOR, retList);
    
    if (retList.empty()) return;
    
    TFriendMap& gFriendList = LCommon::sharedInstance()->friendList;
    for (std::vector<std::string>::iterator it = retList.begin(); it != retList.end(); it++)
    {
        std::string record = *it;
        std::vector<std::string> items;
        splitString(record, VALUE_SEPERATOR, items);

        TFriendInfo* finfo = new TFriendInfo();
        finfo->fbid = items.at(0);
        finfo->fname = items.at(1);
        std::string strScore = items.at(2);
        
        int nScore = atoi(strScore.c_str());
        
        finfo->nMaxLevel = (nScore >> 24) & 0xFF;
        finfo->nMaxScore = (nScore & 0xFFFFFF);
        
        TFriendMap::iterator old = gFriendList.find(finfo->fbid);
        if (old == gFriendList.end()) {
            gFriendList.insert(std::make_pair(finfo->fbid, finfo));
        } else {
            TFriendInfo* oldInfo = (*old).second;
            oldInfo->nMaxLevel = finfo->nMaxLevel;
            oldInfo->nMaxScore = finfo->nMaxScore;
        }
    }
    sendEventToCurrentScene(EVT_REFRESH_FRIEND_LIST, "");
}

void AppDelegate::didFBconnected(bool bConnected)
{
	LCommon::sharedInstance()->isFBConnected = bConnected;
	sendEventToCurrentScene(EVT_UPDATE_FBCONNECT_STATE, "", bConnected);
}

void AppDelegate::didGetMyFBProfile(std::string fbid)
{
	std::string oldFBID = LCommon::sharedInstance()->myFBID;
	if (oldFBID == MYFBID_DEFAULT || oldFBID != fbid)
	{
		sendMessageToNative(MSG_UD_DOWNLOAD_REQ, fbid.c_str(), 0);
	}
    else
    {
		sendMessageToNative(MSG_UD_DOWNLOAD_REQ, fbid.c_str(), 1);
        
    }
    
	LCommon::sharedInstance()->myFBID = fbid;
	CCLog("didGetMyFBProfile %s", fbid.c_str());
}

void AppDelegate::receivedFriendPicture(const char* buf, int length)
{
	CCLog("MSG_PROFILE_PHOTO:%d:%d:%d:%d", buf[0], buf[1], buf[2], buf[3]);

    int len = *((int*) buf);
    length -= 4;
    buf += 4;
    const char* key = buf;
    buf += len;
    length -= len;
    
	CCLog("receivedFriendPicture : %d, %s, %d", len, key, length);

	LCommon::sharedInstance()->addFriendPicture(key, (void*) buf, length);
    sendEventToCurrentScene(EVT_REFRESH_FRIEND_LIST, "");
}

void AppDelegate::sendEventToCurrentScene(int nEvent, std::string strParam, int nParam)
{
    CBaseLayer* curScene = dynamic_cast<CBaseLayer*> (CCDirector::sharedDirector()->getRunningScene());
    if (curScene != NULL)
        curScene->onProcEvent(nEvent, nParam, strParam);
    else
        CCLog("Warning sendEventToCurrentScene : Now is transitioning..");
}

void AppDelegate::askLive()
{
	sendMessageToNative(MSG_FB_ASK_LIVE, "", 0);
    
}
void AppDelegate::giveLive(std::string reqid)
{
	sendMessageToNative(MSG_FB_GIVE_LIVE, reqid.c_str(), 0);
}

void AppDelegate::collectLive(std::string reqid)
{
	sendMessageToNative(MSG_FB_COLLECT_LIVE, reqid.c_str(), 0);
}

void AppDelegate::checkIncomingRequests()
{
	sendMessageToNative(MSG_FB_CHECK_REQUESTS, "", 0);
}

void AppDelegate::receivedPushNotification()
{
	sendEventToCurrentScene(EVT_REFRESH_GIFTING_POPUP, "");
}

void AppDelegate::receivedLiveReqList(std::string reqList)
{
    std::vector<std::string> retList;
    splitString(reqList, REC_SEPERATOR, retList);
    
    if (retList.empty()) return;
    
    TReqList& gReqList = LCommon::sharedInstance()->liveReqList;
    gReqList.clear();
    for (std::vector<std::string>::iterator it = retList.begin(); it != retList.end(); it++)
    {
        std::string record = *it;
        std::vector<std::string> items;
        splitString(record, VALUE_SEPERATOR, items);
        
		CCLog("receivedLiveReqList %s, %s, %s", items.at(0).c_str(), items.at(1).c_str(), items.at(2).c_str());

        gReqList.push_back(TReqInfo(items.at(0), items.at(1), items.at(2)));

		//Added by Liuwei!
		MemoryStruct* friendPhoto = new MemoryStruct();
		friendPhoto->memory = NULL;
		friendPhoto->size = 0;

		TFriendPhotoMap::iterator old = LCommon::sharedInstance()->friendPhotoList.find(items.at(1));
		if (old == LCommon::sharedInstance()->friendPhotoList.end())
		{
			LCommon::sharedInstance()->friendPhotoList.insert(std::make_pair(items.at(1), friendPhoto));
			sendMessageToNative(MSG_USER_PHOTO_REQ, items.at(1).c_str(), 0);
		}
		//Added by Liuwei!
    }

    sendEventToCurrentScene(EVT_RECEIVED_LIVEREQ_LIST, "");
}
void AppDelegate::receivedCollectLiveList(std::string reqList)
{
    std::vector<std::string> retList;
    splitString(reqList, REC_SEPERATOR, retList);
    
    if (retList.empty()) return;
    
    TReqList& gReqList = LCommon::sharedInstance()->collectList;
    gReqList.clear();
    for (std::vector<std::string>::iterator it = retList.begin(); it != retList.end(); it++)
    {
        std::string record = *it;
        std::vector<std::string> items;
        splitString(record, VALUE_SEPERATOR, items);
        
        gReqList.push_back(TReqInfo(items.at(0), items.at(1), items.at(2)));

		//Added by Liuwei!
		MemoryStruct* friendPhoto = new MemoryStruct();
		friendPhoto->memory = NULL;
		friendPhoto->size = 0;

		TFriendPhotoMap::iterator old = LCommon::sharedInstance()->friendPhotoList.find(items.at(1));
		if (old == LCommon::sharedInstance()->friendPhotoList.end())
		{
			LCommon::sharedInstance()->friendPhotoList.insert(std::make_pair(items.at(1), friendPhoto));
			sendMessageToNative(MSG_USER_PHOTO_REQ, items.at(1).c_str(), 0);
		}
		//Added by Liuwei!
    }
    
    sendEventToCurrentScene(EVT_RECEIVED_GIVELIVE_LIST, "");
}

void AppDelegate::playSound(const char* sndName, float fVolume)
{
	if (!LCommon::sharedInstance()->isSoundOn) return;
	sendMessageToNative(MSG_SND_PLAY, sndName, fVolume * 100);
    
}
void AppDelegate::stopSound()
{
    sendMessageToNative(MSG_SND_STOP, "", 0);
}

void AppDelegate::uploadGameDataTonet()
{
    std::string str = LCommon::sharedInstance()->getSavedData();

	sendMessageToNative(MSG_UD_UPLOAD_REQ, str.c_str(), 0);
}
void AppDelegate::receivedGameDataFromNet(std::string str)
{
    LCommon::sharedInstance()->loadFromString(str.c_str());
}

void AppDelegate::receivedRateRes()
{
	TMagicCost& cost1 = LCommon::sharedInstance()->magicCostList.at(0);
	LCommon::sharedInstance()->magicCount += cost1.nMagicCount;
	LCommon::sharedInstance()->nLastRatedVersion = LCommon::sharedInstance()->currentAppVersion;
	LCommon::sharedInstance()->saveState();

	sendEventToCurrentScene(EVT_GOT_FREE_MAGIC, "", 0);
}

void AppDelegate::sendFuseEvent(const std::string& strEvent)
{
    sendMessageToNative(MSG_SEND_FUSE_EVENT, strEvent.c_str(), 0);
}

void AppDelegate::sendFuseEventPerLevel(const std::string& strEvent)
{
    sendFuseEventPerLevel(strEvent, LCommon::sharedInstance()->curLevel());
}

void AppDelegate::sendFuseEventPerLevel(const std::string& strEvent, int nLevel)
{
    sendMessageToNative(MSG_SEND_FUSE_EVENT_LEVEL, strEvent.c_str(), nLevel + 1);
}

void AppDelegate::sendFusePurchase(const std::string& strEvent, int nValue)
{
    sendMessageToNative(MSG_SEND_FUSE_PURCAHSE, strEvent.c_str(), nValue);
}

#include "Character.h"
#include "LCommon.h"
#include "Bubble.h"
USING_NS_CC;
USING_NS_CC_EXT;

CCharacter* CCharacter::create(std::string strName)
{
	CCharacter* pRet = new CCharacter();

	CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo(
		CCString::createWithFormat("Image/animation/%s/%s.ExportJson", strName.c_str(), strName.c_str())->getCString());

	if (pRet && pRet->init(strName.c_str()))
	{
		pRet->autorelease();

		return pRet;
	}
	else
	{
		CC_SAFE_DELETE(pRet);
		return NULL;
	}
}
CCharacter::CCharacter()
{
	m_nState = ST_NORMAL;
	m_nLookDirect = LOOK_D;

//	m_pSadSweet = CBubble::createAnimate("Image/animation/sweet", 39);
//	m_pSweetSprite = NULL;
}

CCharacter::~CCharacter(void)
{

}

void CCharacter::setCharaState(int state)
{
	m_nState = state;
//	runAnimation(ANIM_LOOK);
}

void CCharacter::setLookPos(int pos)
{
	if (m_nLookDirect == pos) return;

	m_nLookDirect = pos;
	runAnimation(ANIM_LOOK);
}

void CCharacter::runAnimation(int eType)
{
	std::string strDir;
	strDir += ('A' + m_nLookDirect);
	int nAnimIndex = 15;
	switch (eType)
	{
		case ANIM_LOOK:
			strDir += (m_nState == ST_SAD)? "-sad" : "-watch";
			break;
		case ANIM_TAKEBUBBLE:
			strDir = "TakeBubble";
			break;
		case ANIM_SHOOT:
			strDir += "-shoot";
			break;
		case ANIM_WIN:
			stopSweetAction();
			strDir = "Win";
			break;
		case ANIM_FAIL:
			break;
	}

//	if ((eType == ANIM_LOOK) && (m_nState == ST_SAD))
//		runSweetAction();

	getAnimation()->play(strDir.c_str());
}

void CCharacter::cbEventComplete(CCArmature * amature, MovementEventType evtType, const char * pCh)
{
	if (evtType == COMPLETE)
	{
		runAnimation(ANIM_LOOK);
	}
}

void CCharacter::runSweetAction()
{
//	if (m_pSweetSprite) return;
//
//	CCAnimate* animation = CCAnimate::create(m_pSadSweet);
//
//	m_pSweetSprite = CCSprite::create();
//	m_pSweetSprite->setPosition(ccp(30, 20));
//	addChild(m_pSweetSprite, 10);
//
//	m_pSweetSprite->runAction(CCRepeatForever::create(animation));
}
void CCharacter::stopSweetAction()
{
//	if (m_pSweetSprite)
//		m_pSweetSprite->removeFromParent();
//	m_pSweetSprite = NULL;
}

void CCharacter::onEnter()
{
	CCArmature::onEnter();

	getAnimation()->setMovementEventCallFunc(this, SEL_MovementEventCallFunc(&CCharacter::cbEventComplete));
}


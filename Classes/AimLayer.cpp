#include "AimLayer.h"
#include "LCommon.h"
#include "MathUtil.h"

USING_NS_CC;

#define RAY_HALF_WIDTH 35.0f

CAimLayer::CAimLayer()
	:m_vecLines(NULL), m_bSniperAim(false)
{
	for (int i = 0; i < 50; i++)
	{
		m_pAimList.push_back(createAimSprite());
	}

	for (int i = 0; i < 10; i++)
	{
		m_pRayBottomList.push_back(createRayBottomSprite());
		m_pRayTopList.push_back(createRayTopSprite());
	}
}


CAimLayer::~CAimLayer(void)
{
}

CAimLayer* CAimLayer::create()
{
	CAimLayer *pRet = new CAimLayer();
	if (pRet && pRet->init())
	{
		pRet->autorelease();
		return pRet;
	}
	else
	{
		CC_SAFE_DELETE(pRet);
		return NULL;
	}

}

CCSprite* CAimLayer::createAimSprite()
{
	CCSprite* sprt = CCSprite::create("Image/game/aim.png");
	sprt->setScale(0.7f);
	sprt->setVisible(false);
	addChild(sprt);
	return sprt;
}

CCSprite* CAimLayer::createRayBottomSprite()
{
	CCSprite* sprt = CCSprite::create("Image/game/ray_bottom.png");
	//sprt->setScale(0.7f);
	sprt->setVisible(false);
	addChild(sprt);
	return sprt;
}

CCSprite* CAimLayer::createRayTopSprite()
{
	CCSprite* sprt = CCSprite::create("Image/game/ray_top.png");
	//sprt->setScale(0.7f);
	sprt->setVisible(false);
	addChild(sprt);
	return sprt;
}

void CAimLayer::setAimDots(TPOINT_LIST* lines) 
{ 
	m_vecLines = lines;

	if (m_vecLines->empty()) return;
	CCPoint start = m_ptStart;

	float interval = 60;

	int nIndexDot = 0;
	for (unsigned int i = 0; i < m_vecLines->size(); i++)
	{
		CCPoint& next = m_vecLines->at(i);

		float dist = getDistance(start, next);

		//Dot
		int nCnt = dist / interval;
		float distX = (next.x - start.x) * interval / dist;
		float distY = (next.y - start.y) * interval / dist;

		float dx = start.x + distX;
		float dy = start.y + distY;

		for (int k = 0; k < nCnt; k++)
		{
			if (nIndexDot == m_pAimList.size() && nIndexDot < 200)
				m_pAimList.push_back(createAimSprite());
			CCSprite* sprt = m_pAimList.at(nIndexDot++);
			sprt->setPosition(ccp(dx, dy));
			sprt->setVisible(true);
			dx += distX;
			dy += distY;

			if (nIndexDot < 3)
				sprt->setVisible(false);
		}

		start = next;

		//if (!m_bSniperAim) break;
	}

	if (!m_bSniperAim && nIndexDot > 9) nIndexDot = 9;
	for (unsigned int i = nIndexDot; i < m_pAimList.size(); i++)
	{
		CCSprite* sprt = (CCSprite*) m_pAimList.at(i);
		sprt->setVisible(false);
	}

}


void CAimLayer::setAimRays(TPOINT_LIST* lines) 
{ 
	if(!m_bSniperAim) return;

	m_vecLines = lines;

	if (m_vecLines->empty()) return;
	CCPoint start = m_ptStart;

	CCPoint bottomPt[11];
	CCPoint topPt[11];

	//float sign = angle < M_PI / 2 ? 1.0f : -1.0f;
	float sign = 1.0f;

	//First Pt
	CCPoint& next = m_vecLines->at(0);
	float angle = getAlpha(start, next);

	bottomPt[0] = getDistPoint(start, angle - sign * M_PI / 2, RAY_HALF_WIDTH);
	topPt[0] = getDistPoint(start, angle + sign * M_PI / 2, RAY_HALF_WIDTH);


	for (unsigned int i = 0; i < m_vecLines->size() - 1; i++)
	{
		CCPoint prev, current, next;
		CCPoint pt1, pt2, pt3, pt4;
		Line line1, line2;
		CCPoint crossPt;

		prev = i == 0 ? start : m_vecLines->at(i - 1);
		current = m_vecLines->at(i);
		next = m_vecLines->at(i + 1);

		//Bottom Pt
		angle = getAlpha(prev, current);
		pt1 = getDistPoint(prev, angle - sign * M_PI / 2, RAY_HALF_WIDTH);
		pt2 = getDistPoint(current, angle - sign * M_PI / 2, RAY_HALF_WIDTH);
		
		angle = getAlpha(current, next);
		pt3 = getDistPoint(current, angle - sign * M_PI / 2, RAY_HALF_WIDTH);
		pt4 = getDistPoint(next, angle - sign * M_PI / 2, RAY_HALF_WIDTH);

		getLineEquationFromTwoPoints(pt1, pt2, line1);
		getLineEquationFromTwoPoints(pt3, pt4, line2);
		getCrossPointFromTwoLines(line1, line2, crossPt);
		bottomPt[i + 1] = crossPt;


		//Top Pt
		angle = getAlpha(prev, current);
		pt1 = getDistPoint(prev, angle + sign * M_PI / 2, RAY_HALF_WIDTH);
		pt2 = getDistPoint(current, angle + sign * M_PI / 2, RAY_HALF_WIDTH);

		angle = getAlpha(current, next);
		pt3 = getDistPoint(current, angle + sign * M_PI / 2, RAY_HALF_WIDTH);
		pt4 = getDistPoint(next, angle + sign * M_PI / 2, RAY_HALF_WIDTH);

		getLineEquationFromTwoPoints(pt1, pt2, line1);
		getLineEquationFromTwoPoints(pt3, pt4, line2);
		getCrossPointFromTwoLines(line1, line2, crossPt);
		topPt[i + 1] = crossPt;
	}

	//Last Pt
	start = m_vecLines->size() == 1 ? m_ptStart : m_vecLines->at(m_vecLines->size() - 2);
	next = m_vecLines->at(m_vecLines->size() - 1);
	angle = getAlpha(start, next);
	next = getDistPoint(next, angle, RAY_HALF_WIDTH);

	bottomPt[m_vecLines->size()] = getDistPoint(next, angle - sign * M_PI / 2, RAY_HALF_WIDTH);
	topPt[m_vecLines->size()] = getDistPoint(next, angle + sign * M_PI / 2, RAY_HALF_WIDTH);


	//Draw AimRays
	for (int i = 0; i < m_vecLines->size(); i++)
	{
		//Bottom
		float dxBtm = bottomPt[i].x + (bottomPt[i + 1].x -  bottomPt[i].x) / 2;
		float dyBtm = bottomPt[i].y + (bottomPt[i + 1].y - bottomPt[i].y) / 2;

		float distBtm = getDistance(bottomPt[i], bottomPt[i + 1]);
		angle = getAlpha(bottomPt[i], bottomPt[i + 1]);

		CCSprite* sprtBtm = m_pRayBottomList.at(i);
		sprtBtm->setPosition(ccp(dxBtm, dyBtm));
		sprtBtm->setScaleX(distBtm / sprtBtm->getContentSize().width);
		sprtBtm->setRotation(-angle * 180 / M_PI);
		sprtBtm->setVisible(true);


		//Top
		float dxTop = topPt[i].x + (topPt[i + 1].x -  topPt[i].x) / 2;
		float dyTop = topPt[i].y + (topPt[i + 1].y - topPt[i].y) / 2;

		float distTop = getDistance(topPt[i], topPt[i + 1]);
		angle = getAlpha(topPt[i], topPt[i + 1]);

		CCSprite* sprtTop = m_pRayTopList.at(i);
		sprtTop->setPosition(ccp(dxTop, dyTop));
		sprtTop->setScaleX(distTop / sprtTop->getContentSize().width);
		sprtTop->setRotation(-angle * 180 / M_PI);
		sprtTop->setVisible(true);
	}

	for (int i = m_vecLines->size(); i < m_pRayBottomList.size(); i++)
	{
		CCSprite* sprt = (CCSprite*) m_pRayBottomList.at(i);
		sprt->setVisible(false);
	}

	for (int i = m_vecLines->size(); i < m_pRayTopList.size(); i++)
	{
		CCSprite* sprt = (CCSprite*) m_pRayTopList.at(i);
		sprt->setVisible(false);
	}
}

void CAimLayer::setDrawInfo(cocos2d::CCPoint ptStart, float fBubbleSize)
{
	m_fBubbleSize = fBubbleSize;
	m_ptStart = ptStart;
}

void CAimLayer::setSniperAim(bool bApply)
{
	m_bSniperAim = bApply;
}

// 
// void CAimLayer::draw()
// {
// 	if (m_vecLines == NULL) return;
// 
// 	if (m_vecLines->empty()) return;
// 
// 	glLineWidth( 5.0f );
// 	ccDrawColor4B(255,0,0,255);
// 	CCPoint ptStart = m_ptStart;
// 	for (int i = 0; i < (*m_vecLines).size(); i++)
// 	{
// 		CCPoint ptEnd = (*m_vecLines)[i];
// // 		if (i < (*m_vecLines).size() - 1)
// // 		{
// // 			if (ptEnd.x < SC_WIDTH_HALF) ptEnd.x -= m_fBubbleSize / 2;
// // 			else ptEnd.x += m_fBubbleSize / 2;
// // 		}
// 		ccDrawLine(ptStart, ptEnd);
// 		ptStart = ptEnd;
// 	}
// }

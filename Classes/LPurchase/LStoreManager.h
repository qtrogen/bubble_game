//
//  LStoreManager.h
//  RopeFlay
//
//  Created by Chen on 7/23/13.
//
//

#ifndef __LStoreManager_H__
#define __LStoreManager_H__


#include <string>
#include <vector>

typedef std::vector<std::string> T_PRODUCT_LIST;

class LPurchaseDelegate 
{
public:
	virtual void purchaseSuccessed(std::string& productId) {};
	virtual void purchaseFailed(std::string& productId, std::string& errMsg) {};
	virtual void restoreSuccessed(T_PRODUCT_LIST& productIdList) {};
	virtual void restoreFailed(std::string& errMsg) {};

};

class LStoreManager
{
public:
	LStoreManager();
	~LStoreManager();

	bool isPurchasedProductId(std::string& productId);
	bool buyProduct(std::string& productId);
	bool restore();
	void setTestMode();
	void setDelegate(LPurchaseDelegate* pDelegate);
	static LStoreManager* sharedInstance();

protected:
    bool isTestMode;
	LPurchaseDelegate* m_pDelegate;

public:
	void successfulPurchase(std::string& productId);
	void failedPurchase(std::string errorMessage);
	void successfulRestore(T_PRODUCT_LIST& productIdList);
	void failedRestore(std::string errorMessage);
};

#endif // __LStoreManager_H__

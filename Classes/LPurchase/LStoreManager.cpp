//
//  LStoreManager.m
//  RopeFlay
//
//  Created by Chen on 7/23/13.
//
//

#include "LStoreManager.h"
#include "../AppDelegate.h"
#include "../SaveUtil.h"

LStoreManager* LStoreManager::sharedInstance()
{
    static LStoreManager* instance = NULL;
    if (instance == NULL)
        instance = new LStoreManager();
    return instance;
}

LStoreManager::LStoreManager()
{
    isTestMode = false;
    m_pDelegate = NULL;
}

void LStoreManager::setDelegate(LPurchaseDelegate* delegate)
{
	m_pDelegate = delegate;
}

bool LStoreManager::buyProduct(std::string& productId)
{
    if (isTestMode)
    {
        successfulPurchase(productId);
        return true;
    }
    
    if (m_pDelegate == NULL) return false;
    return AppDelegate::app->buyProduct(productId);
}
bool LStoreManager::restore()
{
    if (isTestMode)
    {
		T_PRODUCT_LIST emptyList;
        successfulRestore(emptyList);
        return true;
    }
    if (m_pDelegate == NULL) return false;
    return AppDelegate::app->restoreProducts();
}

bool LStoreManager::isPurchasedProductId(std::string& productId)
{
    CSaveUtil* defaults = CSaveUtil::sharedInstance();
	return defaults->getBoolForKey(productId.c_str(), false);
}

void LStoreManager::setTestMode()
{
	isTestMode = true;
}

void LStoreManager::successfulPurchase(std::string& productId)
{
	if (m_pDelegate)
    {
        CSaveUtil* defaults = CSaveUtil::sharedInstance();
        defaults->setBoolForKey(productId.c_str(), true);
        defaults->flush();

		m_pDelegate->purchaseSuccessed(productId);
    }
}

void LStoreManager::failedPurchase(std::string errorMessage)
{
	std::string emptyStr;
	if (m_pDelegate)
	{
		m_pDelegate->purchaseFailed(emptyStr, errorMessage);
	}
}

void LStoreManager::successfulRestore(T_PRODUCT_LIST& productIdList)
{
	if (m_pDelegate)
	{
		m_pDelegate->restoreSuccessed(productIdList);
	}
}

void LStoreManager::failedRestore(std::string errorMessage)
{
	if (m_pDelegate)
	{
		m_pDelegate->restoreFailed(errorMessage);
	}
}
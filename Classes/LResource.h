//
//  Resource.h
//  IcecreamCandyBlast
//
//  Created by Chen on 02/06/14.
//  Copyright (c) 2014 lion. All rights reserved.
//

#ifndef __RESOURCE_H__
#define __RESOURCE_H__

#include "cocos2d.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#define FONT_NAME_VAL "Image/fonts/VAL.ttf"
#define FONT_NAME_CANDICE "Image/fonts/Candice.ttf"
#define FONT_NAME_HOBOSTD "Image/fonts/HoboStd.ttf"
#else
#define FONT_NAME_VAL "VAL"
#define FONT_NAME_CANDICE "Candice"
#define FONT_NAME_HOBOSTD "Hobo Std"
#endif

#ifdef PRO_VERSION

#define PD_BUY_MAGIC1        "com.bubble.magic.120"
#define PD_BUY_MAGIC2        "com.bubble.magic.250"
#define PD_BUY_MAGIC3        "com.bubble.magic.520"
#define PD_BUY_MAGIC4        "com.bubble.magic.1400"
#define PD_BUY_MAGIC5        "com.bubble.magic.3000"

#define PD_REMOVEADS        "com.flopstudios.cupcakejump.RemoveAds"

#else // FREE version

#define PD_BUY_MAGIC1        "com.bubble.magic.120"
#define PD_BUY_MAGIC2        "com.bubble.magic.250"
#define PD_BUY_MAGIC3        "com.bubble.magic.520"
#define PD_BUY_MAGIC4        "com.bubble.magic.1400"
#define PD_BUY_MAGIC5        "com.bubble.magic.3000"
#define PD_BUY_MAGIC6        "com.bubble.magic.3000"

#define PD_REMOVEADS        "com.flopstudios.cupcakejump.RemoveAds"

#endif //PRO_VERSION

// sound resource

#define kSND_BG_WORLD_FORMAT		"Sounds/bg_%d.mp3"
#define kSND_BG_MAINTHEME			"Sounds/bg_maintheme.mp3"
#define kSND_BG_MAP					"Sounds/bg_map.mp3"

#define kSND_SFX_ACHIEVE				"Sounds/sfx/achieve_ding.mp3"
#define kSND_SFX_BUBBLE_POP				"Sounds/sfx/bubble_pop.mp3"
#define kSND_SFX_BUBBLE_POP6				"Sounds/sfx/bubble_pop6.mp3"
#define kSND_SFX_READYGO				"Sounds/sfx/ready_go.mp3"
#define kSND_SFX_TAP					"Sounds/sfx/tap.mp3"
#define kSND_SFX_START_ADD				"Sounds/sfx/star_add.mp3"
#define kSND_SFX_FIREWORKS				"Sounds/sfx/fireworks_multiple.mp3"
#define kSND_SFX_BOUNCE                 "Sounds/sfx/bounce.mp3"
#define kSND_SFX_GREAT_JOB              "Sounds/sfx/great_job.mp3"
#define kSND_SFX_SHOOT_BUBBLE           "Sounds/sfx/shoot_bubble.mp3"
#define kSND_SFX_TRY_AGAIN              "Sounds/sfx/try_again.mp3"
#define kSND_SFX_WELL_DONE              "Sounds/sfx/well_done.mp3"
#define kSND_SFX_GAME_AWARD             "Sounds/sfx/game_award.mp3"
#define kSND_SFX_GAME_FAIL             "Sounds/sfx/game_fail.mp3"
#define kSND_SFX_CHEST_OPEN             "Sounds/sfx/chest_open.mp3"
#define kSND_SFX_COUNT_SCORE             "Sounds/sfx/count_score.mp3"
#define kSND_SFX_OUT_BUBBLES             "Sounds/sfx/out_bubbles.mp3"
#define kSND_SFX_DONE_ADDING_UP          "Sounds/sfx/done_adding_up.mp3"
#define kSND_SFX_FRIEND_SAVED            "Sounds/sfx/friend_saved.mp3"
#define kSND_SFX_FIRE_CRACKER            "Sounds/sfx/fire_cracker.mp3"
#define kSND_SFX_AIRPLANE	            "Sounds/sfx/airplane.mp3"
#define kSND_SFX_WIN_BGM	            "Sounds/sfx/win_bgm.mp3"
#define kSND_SFX_WIN_VOICE	            "Sounds/sfx/win_sfx.mp3"

#define kSND_SFX_COLOR_BOMB_S			"Sounds/sfx/color_bomb_s.mp3"
#define kSND_SFX_COLOR_BOMB_U           "Sounds/sfx/color_bomb_u.mp3"
#define kSND_SFX_EXTRA_BUBBLES_S        "Sounds/sfx/extra_bubbles_s.mp3"
#define kSND_SFX_EXTRA_BUBBLES_U        "Sounds/sfx/extra_bubbles_u.mp3"
#define kSND_SFX_EXTRA_SWAP_S           "Sounds/sfx/extra_swap_s.mp3"
#define kSND_SFX_EXTRA_SWAP_U           "Sounds/sfx/extra_swap_u.mp3"
#define kSND_SFX_NUKE_S                 "Sounds/sfx/nuke_s.mp3"
#define kSND_SFX_NUKE_U                 "Sounds/sfx/nuke_u.mp3"
#define kSND_SFX_PIN_S                  "Sounds/sfx/pin_s.mp3"
#define kSND_SFX_PIN_U                  "Sounds/sfx/pin_u.mp3"
#define kSND_SFX_RAINBOW_BUBBLE_S       "Sounds/sfx/rainbow_bubble_s.mp3"
#define kSND_SFX_RAINBOW_BUBBLE_U       "Sounds/sfx/rainbow_bubble_u.mp3"
#define kSND_SFX_REPELLENT_S            "Sounds/sfx/repellent_s.mp3"
#define kSND_SFX_REPELLENT_U            "Sounds/sfx/repellent_u.mp3"
#define kSND_SFX_SNIPER_RIFLE_S         "Sounds/sfx/sniper_rifle_s.mp3"
#define kSND_SFX_SNIPER_RIFLE_U         "Sounds/sfx/count_score.mp3"

#define kSND_SFX_CANDY      "Sounds/sfx/obstacles/candy.mp3"
#define kSND_SFX_DYNAMITE   "Sounds/sfx/obstacles/dynamite.mp3"
#define kSND_SFX_MAGNET     "Sounds/sfx/obstacles/magnet.mp3"
#define kSND_SFX_SNOWFLAKE  "Sounds/sfx/obstacles/snowflake.mp3"
#define kSND_SFX_STONE      "Sounds/sfx/obstacles/stone.mp3"
#define kSND_SFX_ZOMBIE     "Sounds/sfx/obstacles/zombie.mp3"

#define kSND_SFX_FAIL     "Sounds/sfx/bubble_fail.mp3"

#define kSND_SFX_STAR_1     "Sounds/sfx/star_1.mp3"
#define kSND_SFX_STAR_2     "Sounds/sfx/star_2.mp3"
#define kSND_SFX_STAR_3     "Sounds/sfx/star_3.mp3"

#define kSND_SFX_SPIN_START		"Sounds/sfx/spin_start.mp3"
#define kSND_SFX_SPIN_STOP		"Sounds/sfx/spin_stop.mp3"

#endif

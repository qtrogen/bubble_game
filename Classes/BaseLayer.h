//
//  CBaseLayer.h
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//
#ifndef __CBaseLayer_H__
#define __CBaseLayer_H__

#include "cocos2d.h"
#include <vector>
#include "cocos-ext.h"
#include "SimpleAudioEngine.h"
#include "LPurchase/LStoreManager.h"
#include "Bubble.h"

USING_NS_CC_EXT;
USING_NS_CC;

// added by KUH in 2014-09-20
#define GLOW_ADD

enum 
{
	BTN_ID_CLOSE = 0,
	BTN_ID_GIVE_POPUP_CLOSE,
	BTN_ID_COLLECT_POPUP_CLOSE,

	BTN_ID_GLOBAL_START = 100,

	BTN_ID_BUY_MAGIC_1,
	BTN_ID_BUY_MAGIC_2,
	BTN_ID_BUY_MAGIC_3,
	BTN_ID_BUY_MAGIC_4,
	BTN_ID_BUY_MAGIC_5,
	BTN_ID_BUY_MAGIC_6,
	BTN_ID_BUY_MAGIC_7,

	BTN_ID_BUY_SPIN_1,
	BTN_ID_BUY_SPIN_2,
	BTN_ID_BUY_SPIN_3,
	BTN_ID_BUY_SPIN_4,

	BTN_ID_BUY_POWERUP_1,
	BTN_ID_BUY_POWERUP_2,
	BTN_ID_BUY_POWERUP_3,

	BTN_ID_SHOW_BUY_MAGIC,
	BTN_ID_CONTINUE_BUYING_LIVE,
	BTN_ID_ASK_LIVE,
	BTN_ID_OPEN_TREASURE,

	BTN_ID_DAILY_SPIN,
	BTN_ID_BUY_PIECE,

	BTN_ID_SETTING_LEADERBOARD,
	BTN_ID_SETTING_TUTORIAL,
	BTN_ID_SETTING_SOUND,
	BTN_ID_SETTING_NOTIF,
	BTN_ID_SETTING_DONE,

	BTN_ID_START = 200,
};

enum 
{
	POPUP_ID_BUY_SPIN = 10,
	POPUP_ID_BUY_MAGIC,
	POPUP_ID_BUY_LIVE,
	POPUP_ID_START = 100,
};

enum
{
    EVT_START_VALUE = 100,
    EVT_REFRESH_FRIEND_LIST,
	EVT_REFRESH_GIFTING_POPUP,
    EVT_UPDATE_FBCONNECT_STATE,
    EVT_RECEIVED_LIVEREQ_LIST,
    EVT_RECEIVED_GIVELIVE_LIST,
    EVT_COLLECT_LIVE_SUC,
    EVT_GIVE_LIVE_SUC,
	EVT_GOT_FREE_MAGIC,
};

#define CREATE_SCENE_METHOD(classname)      \
    static cocos2d::CCScene* scene()           \
    {                                       \
        cocos2d::CCScene *pRet = new classname();  \
        if (pRet && pRet->init())   \
        {   \
            pRet->autorelease();    \
            return pRet;            \
        }                           \
        else                        \
        {                           \
            CC_SAFE_DELETE(pRet);   \
            return NULL;            \
        }                           \
    }




class CBaseLayer : public CCScene, public LPurchaseDelegate {
public:
	CBaseLayer();
	~CBaseLayer();

	void setSceneID(int sceneID);

	virtual void orientationChangedCallback();

	void showPopup(UIWidget* widget, UIWidget* closeBtn, int nPopupTag = 0);
	void showPopup(UIWidget* widget, const char* closeBtnName, int nPopupTag = 0);
	void showPopup(UIWidget* widget, int nPopupTag = 0, ccColor3B color = ccBLACK, int opacity = 200);
	void showGiftingPopup(UIWidget* widget, const char* closeBtnName, bool bCollect);
    UIWidget* findPopup(int nPopupTag);

	virtual bool onProcBeforeClose(int nButtonTag);
	virtual bool onProcAfterClose(int nButtonTag, int nParam = 0);
	virtual void onPopupClosed(int nPopupTag);
    
    virtual bool onProcEvent(int nEventId, int nParam, std::string strParam);
    
	void addButtonEvent(UIWidget* btn, int nTag, int nActionTag = 0);
	void addButtonEvent(UIWidget* parent, const char* childName, int nTag, int nActionTag = 0);
	void showNoEnoughMagic();
	void showBuyMagic();
	void showBuySpin();
	void showNoEnoughLive();
	void createActionBar();
	void createActionBar1();
	void showSettings();
	void showPuzzleInfo(int nCharaId, int nPuzzleCnt, CCPoint pos);
	void showTreasure();
	void refreshTreasureInfo(UIWidget* popupWidget);

	void showBuyPowerup(int BoostIndex);

	void showIndicator(std::string msg);
	void showIndicator();
	void hideIndicator();
    
	void beforeShowTutorial();
	void showTutorial(const char* strTuto, cocos2d::CCPoint holePos, char byType, bool bClip, bool isTouchConsumed = true);
	void showTutorial1(const char* strTuto, cocos2d::CCPoint holePos, char byType, bool bClip, bool isTouchConsumed = true);
	void showTutorial(const char* strTuto, cocos2d::CCPoint holePos, TBubbleList& bblList, char byType, bool isTouchConsumed = true, bool isCenter = false);
	void showTutorial(const char* strTuto, TBubbleList& posList, char byType, bool isTouchConsumed = true);
	void showTutorial(const char* strTuto, char byType, bool isTouchConsumed = true);

	void FBConnect();

    // added by KUH in 2014-09-20
#ifdef GLOW_ADD
#endif
	virtual void tutorialTapped() {};

	int m_nActionBarHeight;
protected:
	int m_SceneID;
	int m_nSelectedTag;
	int m_nAdditionalParam;
	int m_nCurChara;

	std::deque<UIWidget*>	m_pPopupStack;
	UILabel*		m_lblOutLiveCnt;
	UILayer*		m_pMainLayer;
	UILayer*		m_pPopupLayer;
	UIWidget*		m_pActionBar;
	UIWidget*		m_pSettingPane;

    std::string     m_strSceceName;

	bool			m_bGivePopupShowing;
	bool			m_bCollectPopupShowing;

	virtual void refreshInfo();
	void buyMagic(CCObject* obj);
	void buyLive(CCObject* obj);
	void buySpin(CCObject* obj);

	void onSettingTapShoot(CCObject* obj);
	void onSettingAimShoot(CCObject* obj);
	void updateSettingInfo();

	virtual void refreshSelectChara();
	//void onDailySpin(cocos2d::CCObject* obj);
	//void onBuyPuzzlePiece(cocos2d::CCObject* obj);

	bool closeTopPopup(bool bClean = true);
	void delayProcPopup(float dt);
	void onButtonEvent(cocos2d::CCObject* obj);
	UIWidget* topPopupWidget();

	void onBlankButtonEvent(cocos2d::CCObject* obj);
	void blankTouch(CCObject* sender, TouchEventType eventType);

	void setBGMName(const char* bgmFileName);
	bool isPlayingBGM();
	void playBGM(std::string bgmFileName, float fVolume = 0.1f, bool bLoop = true);
	void playEffect(const char* sfxFileName, bool bLoop = false);
    void playEffect(const char* sfxFileName, float fVolume);
	void stopAllEffects();
	void stopBgm();
    void setSceneName(const char* name) { m_strSceceName = name; };

	// timer proc
	virtual void onTimer();
    
	void onOneSecTimer(float dt);

    std::string bgmName;
    static std::string curBGMName;
	bool m_bNeedCalcTime;

	virtual void onEnter();
	virtual void onExit();

	virtual void purchaseSuccessed(std::string& productId);
	virtual void purchaseFailed(std::string& productId, std::string& errMsg);
};

#endif //__CBaseLayer_H__

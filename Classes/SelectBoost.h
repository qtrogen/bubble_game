//
//  SelBoostSecene.h
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//
#ifndef __SelBoostSecene_H__
#define __SelBoostSecene_H__

#include "cocos2d.h"
#include <vector>
#include "cocos-ext.h"
#include "BaseLayer.h"

USING_NS_CC_EXT;

class CSelBoostScene : public CBaseLayer, public cocos2d::extension::CCTableViewDataSource {
public:
	CSelBoostScene();
	~CSelBoostScene();

	CREATE_SCENE_METHOD(CSelBoostScene);
    
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

protected:
	void onBack(cocos2d::CCObject* obj);
	void onPlay(cocos2d::CCObject* obj);
	void onTreasure(cocos2d::CCObject* obj);
	void onAnimal(cocos2d::CCObject* obj);
	void onChallenge(cocos2d::CCObject* obj);
	void onBoostInfo(cocos2d::CCObject* obj);
	void onGoalCurtain(cocos2d::CCObject* obj);
	void onGoalCurtainMoved();

	void onTickTapped(cocos2d::CCObject* obj);
	void onSelectAnimalPowerup(cocos2d::CCObject* obj);

	void onFBConnect(cocos2d::CCObject* sender);


	/*void showBuyPowerup(int BoostIndex);

	void delayProcPopup(float dt);
	void onBuyBoostItem1(cocos2d::CCObject* obj);
	void onBuyBoostItem2(cocos2d::CCObject* obj);
	void onBuyBoostItem3(cocos2d::CCObject* obj);*/
	
	void refreshBoostInfo(float dt);
	void refreshBoostInfo();

	void refreshChampions(float dt);
	void refreshChampionsDlg();

	void createGoalCurtain();
	void createBoostInfo();

	void createFriendTableView();
	void initLocalFriends();
	void initRankList(int nLevel);

	void addBoost(CCObject* obj);
	void removeBoost(CCObject* obj);
	int  getBoostFromList();

	void createDialog();
	void addImage(const char* imgName, float x, float y);
	void startPlayAnimation();
	void afterMoveHeart(CCNode* node);
	void startGame();

	virtual void onEnter();
	virtual void onTimer();
	void initUI();
    void gotoHomeAfterDelay(float dt);

	virtual bool onProcEvent(int nEventId, int nParam, std::string strParam);

	void delayTest(float dt);

protected:
	CCPoint m_ptPlayHeart;
	UIImageView*	m_pMainPanel;
	UIWidget*	m_pBoostInfo;
	TDailyChallengeList* dailyChalListForCurlevel;

	bool isCurtainVisible;
	bool isCurtainMoving;

	int nSelectedBoostIndex;

	std::vector<int> unlockedBoosts;
	UIButton* selectedBoosts[BOOST_SEL_COUNT];

	UIPanel* fbLoginPanel;
	UIButton* btnFBLogin;
	CCTableView* friendTableView;
	
	TFriendList localFriendList;

	typedef std::vector<TFriendInfo*> TFriendPList;
	TFriendPList rankList;
};

#endif //__SelBoostSecene_H__

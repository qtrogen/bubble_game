#ifndef __CCROTATEBY_H__
#define __CCROTATEBY_H__

#include "cocos2d.h"

USING_NS_CC;

class CCRotateAroundBy : public CCRotateBy
{
public:
	/** creates the action */
	static CCRotateAroundBy* create(float fDuration, float fDeltaAngle, CCPoint rotationPoint);

	//static CCRotateAroundBy* create(float fDuration, float fStartAngle, float fEndAngle, CCPoint rotationPoint);

	/** initializes the action */
	bool initWithDuration(float fDuration, float fDeltaAngle, CCPoint rotationPoint);

	//bool initWithDuration(float fDuration, float fStartAngle, float fEndAngle, CCPoint rotationPoint);


	virtual void startWithTarget(CCNode *pTarget);

	virtual void update(float time);

protected:
	bool	m_bEqualSpeed;

	CCPoint m_rotationPoint;
	CCPoint m_startPoint;

	//float	m_fStartAngle;
	//float	m_fEndAngle;
};

#endif // __CCROTATEBY_H__


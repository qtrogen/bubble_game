#ifndef __CMATHUTIL_H__
#define __CMATHUTIL_H__

#include "cocos2d.h"
USING_NS_CC;

struct Line
{
	float slope; //Slope
	float intercept; //Intercept
};

CCPoint getDistPoint(const CCPoint& pos, float alpha, float dist);
bool isAcross(const CCPoint& circlePos, float radius, const CCPoint& pos1, const CCPoint& pos2);
bool checkCollision(const CCPoint& fixPos, const CCPoint& flyPos, const CCPoint& dstPos);
float getDistance(const CCPoint& pos1, const CCPoint& pos2);
float getDistance2(const CCPoint& pos1, const CCPoint& pos2);
float getAlpha(const CCPoint& pos1, const CCPoint& pos2);
float getDistFromLine(const CCPoint& point, const CCPoint& pt1, const CCPoint& pt2);
float getDistFromLine2(const CCPoint& point, const CCPoint& pt1, const CCPoint& pt2);
float getAcrossAlpha(const CCPoint& ptCircleCenter, float radius, const CCPoint& pt1, const CCPoint& pt2);
bool getAcrossPoint(const CCPoint& ptC, float radius, const CCPoint& pt1, const CCPoint& pt2, CCPoint& crossPoint);
unsigned int getRandRange(unsigned int nStart, unsigned int nEnd);

void getLineEquationFromTwoPoints(CCPoint pt1, CCPoint pt2, Line& line);
bool getCrossPointFromTwoLines(Line line1, Line line2, CCPoint& crossPt);

bool getProjectPointOntoLine(CCPoint linePt1, CCPoint linePt2, CCPoint orgPt, CCPoint& projectPt);

void rotateAroundBy(CCPoint centerPt, float fAngle, CCPoint oldPt, CCPoint& newPt);

#endif // __CMATHUTIL_H__


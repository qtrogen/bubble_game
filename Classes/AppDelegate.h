#ifndef  _APP_DELEGATE_H_
#define  _APP_DELEGATE_H_

#include "cocos2d.h"

/**
brief    The cocos2d Application.

The reason for implement as private inheritance is to hide some interface call by CCDirector.
*/

enum
{
	MSG_REMOVE_ADS	=	200,
	MSG_SUBMIT_ACHIEVE,
	MSG_SUBMIT_SCORE,
	MSG_SUBMIT_LEADERBOARD,
	MSG_SHOW_LDB,
	MSG_NOTIF_ACHIEVE,
	MSG_AD_HEIGHT,
	MSG_APP_VERSION,

	MSG_SHOW_INDICATOR,
	MSG_HIDE_INDICATOR,

	MSG_LOWSPEC_DEVICE,

	MSG_BUY_REQUEST,
	MSG_BUY_RESULT,
	MSG_RESTORE_REQ,
	MSG_RESTORE_RES,

	MSG_SHOW_REVMOB,
	MSG_SHOW_CHARTBOOST,
	MSG_SHOW_MOREAPPS,
	MSG_SHOW_APPLOVIN,
	MSG_SHOW_PLAYHAVEN,

	MSG_SHOW_FB,
	MSG_SHOW_TW,
	MSG_SHOW_RATE,
	MSG_RATED_APP,

	MSG_CONNECT_FB,
	MSG_FB_CONNECTED,
	MSG_FRIEND_LIST_REQ,
	MSG_FRIEND_LIST,
	MSG_UPDATE_MYINFO,
	MSG_USER_PHOTO_REQ,
	MSG_PROFILE_PHOTO,

	MSG_FB_CHECK_REQUESTS,
	MSG_FB_ASK_LIVE,
	MSG_FB_GIVE_LIVE,
	MSG_FB_LIVEREQ_LIST,
	MSG_FB_COLLECT_LIVE_LIST,
	MSG_FB_COLLECT_LIVE,
	MSG_FB_COLLECTLIVE_SUC,
	MSG_FB_GIVLIVE_SUC,

	MSG_PUSH_NOTIFICATION,

	MSG_SND_PLAY,
	MSG_SND_STOP,

	//    MSG_UD_LOGIN_REQ,
	//    MSG_UD_LOGIN_RES,
	MSG_UD_UPLOAD_REQ,
	MSG_UD_UPLOAD_RES,
	MSG_UD_DOWNLOAD_REQ,
	MSG_UD_DOWNLOAD_RES,

	MSG_PUSH_NOTIF,
	MSG_SEND_FUSE_EVENT,
	MSG_SEND_FUSE_EVENT_LEVEL,
	MSG_SEND_FUSE_PURCAHSE,

	MSG_ACHIEVE_RESET,

	MSG_SCENE_TYPE,
	MSG_SCREEN_ORIENTATION_CHANGED,
};

class  AppDelegate : private cocos2d::CCApplication
{
public:
    AppDelegate();
    virtual ~AppDelegate();

    /**
    brief    Implement CCDirector and CCScene init code here.
    return true    Initialize success, app continue.
    return false   Initialize failed, app terminate.
    */
    virtual bool applicationDidFinishLaunching();

    /**
    brief  The function be called when the application enter background
    param  the pointer of the application
    */
    virtual void applicationDidEnterBackground();

    /**
    brief  The function be called when the application enter foreground
    param  the pointer of the application
    */
    virtual void applicationWillEnterForeground();

	void orientationChangedCallback(int nType);
	void setSceneType(int sceneType);

	void removeAds();
    
	void showRevmob();
	void showChartboost();
	void showAppLovin();
	void showPlayHaven();
	void showMoreApps();
	void showLeaderboard();
	void showFacebook();
	void showTwitter();
	void showRateApp();
	void submitScore(int level, int score);
	void submitAchievement(const char* strid, const char* strDesc);
	void setScreenValues();
	void submitLeaderboard(const char* strid, int nValue);

	void connectFacebook();
	void logoutFacebook();
    void requestFriendList();
	void didFBconnected(bool bConnected);
	void didGetMyFBProfile(std::string fbid);
    void checkIncomingRequests();
    void askLive();
    void giveLive(std::string reqid);
    void collectLive(std::string reqid);
    
    void playSound(const char*  sndName, float fVolume);
    void stopSound();

	void showIndicator(std::string msg);
	void showIndicator();
	void hideIndicator();

	void receivedFromNative(int nType, const char* strParam, int nParam);

	void receivedPushNotification();
    void receivedLiveReqList(std::string reqList);
    void receivedCollectLiveList(std::string reqList);
    
    void receivedFriendList(const std::string& friendList);
    void receivedFriendPicture(const char* buf, int length);
	bool buyProduct(std::string& productId);
	bool restoreProducts();
    void uploadGameDataTonet();
	void receivedGameDataFromNet(std::string str);
	void receivedRateRes();
    
    void sendEventToCurrentScene(int nEvent, std::string strParam, int nParam = 0);

	void sendFuseEvent(const std::string& strEvent);
	void sendFuseEventPerLevel(const std::string& strEvent);
	void sendFuseEventPerLevel(const std::string& strEvent, int nLevel);
	void sendFusePurchase(const std::string& strEvent, int nValue);

	static AppDelegate* app;

protected:
	bool sendMessageToNative(int nType, const char* msg, int nParam);
};

#endif // _APP_DELEGATE_H_


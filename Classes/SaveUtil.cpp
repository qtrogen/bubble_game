#include "SaveUtil.h"
#include "LCommon.h"
USING_NS_CC;
USING_NS_CC_EXT;

#define SAVE_FILE_NAME	"ebbsavedata.plist"
CSaveUtil* CSaveUtil::_instance = NULL;

CSaveUtil* CSaveUtil::sharedInstance()
{
	if (_instance == NULL)
    {
		_instance = new CSaveUtil();
    }
	return _instance;
}

CSaveUtil::CSaveUtil()
{
	m_sFilePath = CCFileUtils::sharedFileUtils()->getWritablePath() + "/" + SAVE_FILE_NAME;
	m_pMainDic = NULL;
	loadData();
}

CSaveUtil::~CSaveUtil(void)
{
}

std::string CSaveUtil::getSaveFilePath()
{
	return m_sFilePath;
}

void CSaveUtil::loadData()
{
	CC_SAFE_RELEASE_NULL(m_pMainDic);
	m_pMainDic = CCDictionary::createWithContentsOfFileThreadSafe(m_sFilePath.c_str());
	if (m_pMainDic == NULL)
    {
		m_pMainDic = CCDictionary::create();
        m_pMainDic->retain();
    }
}

std::string CSaveUtil::getSavedData()
{
	CCString* cStr = CCString::createWithContentsOfFile(m_sFilePath.c_str());
	if (cStr == NULL) return "";
	return cStr->getCString();
}

void CSaveUtil::loadFromString(const char* str)
{
	std::string cstrData = str;
	if (cstrData.length() < 10)
    {
        m_pMainDic->removeAllObjects();
        m_pMainDic->writeToFile(m_sFilePath.c_str());
        return;
    }
    saveToFile(cstrData, m_sFilePath);
	loadData();
}

bool    CSaveUtil::getBoolForKey(const char* pKey, bool defaultValue)
{
	if (m_pMainDic == NULL) return defaultValue;
	const CCString* str = m_pMainDic->valueForKey(pKey);
	if (str->length() == 0) return defaultValue;
	return (str->intValue() == 1);
}
int     CSaveUtil::getIntegerForKey(const char* pKey, int defaultValue)
{
	if (m_pMainDic == NULL) return defaultValue;
	const CCString* str = m_pMainDic->valueForKey(pKey);
	if (str->length() == 0) return defaultValue;
	return str->intValue();
}
float    CSaveUtil::getFloatForKey(const char* pKey, float defaultValue)
{
	if (m_pMainDic == NULL) return defaultValue;
	const CCString* str = m_pMainDic->valueForKey(pKey);
	if (str->length() == 0) return defaultValue;
	return str->floatValue();
}
double  CSaveUtil::getDoubleForKey(const char* pKey, double defaultValue)
{
	if (m_pMainDic == NULL) return defaultValue;
	const CCString* str = m_pMainDic->valueForKey(pKey);
	if (str->length() == 0) return defaultValue;
	return str->doubleValue();
}
std::string CSaveUtil::getStringForKey(const char* pKey, const std::string & defaultValue)
{
	if (m_pMainDic == NULL) return defaultValue;
	const CCString* str = m_pMainDic->valueForKey(pKey);
	if (str->length() == 0) return defaultValue;
	return str->getCString();
}

void    CSaveUtil::setBoolForKey(const char* pKey, bool value)
{
	if (m_pMainDic == NULL) return;
	m_pMainDic->setObject(CCString::createWithFormat("%d", value? 1 : 0), pKey);
}
void    CSaveUtil::setIntegerForKey(const char* pKey, int value)
{
	if (m_pMainDic == NULL) return;
	m_pMainDic->setObject(CCString::createWithFormat("%d", value), pKey);
}
void    CSaveUtil::setFloatForKey(const char* pKey, float value)
{
	if (m_pMainDic == NULL) return;
	m_pMainDic->setObject(CCString::createWithFormat("%f", value), pKey);
}
void    CSaveUtil::setStringForKey(const char* pKey, const std::string & value)
{
	if (m_pMainDic == NULL) return;
	m_pMainDic->setObject(CCString::create(value), pKey);
}

void CSaveUtil::getByteArray(const char* pKey, unsigned char* pArray, int nCount, char defaultValue)
{
	std::string strVal = getStringForKey(pKey, "");

	int nLen = nCount;

	if (strVal.length() < nCount)
	{
		nLen = strVal.length();
	}

	int lv;
	for (lv = 0; lv < nLen; lv++)
	{
		pArray[lv] = strVal.at(lv) - '0';
	}

	for (; lv < nCount; lv++) pArray[lv] = defaultValue;
}

void CSaveUtil::setByteArray(const char* pKey, unsigned char* pArray, int nCount)
{
	std::string strVal;
	for (int lv = 0; lv < MAX_LEVEL; lv++)
	{
		strVal += '0' + pArray[lv];
	}

	setStringForKey(pKey, strVal.c_str());
}

void    CSaveUtil::flush()
{
	if (m_pMainDic == NULL) return;
	m_pMainDic->writeToFile(m_sFilePath.c_str());
}

void CSaveUtil::saveToFile(const std::string& strData, const std::string& strPath)
{
	FILE* fp = fopen(strPath.c_str(), "w" );
	if ( fp ) {
		fwrite(strData.data(), 1, strData.length(), fp);
	}

	fclose( fp );
}


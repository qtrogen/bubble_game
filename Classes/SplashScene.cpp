//
//  CSelCharaScene.m
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//

#include "LCommon.h"
#include "AppDelegate.h"
#include "SplashScene.h"
#include "CMainMenuLayer.h"
#include "SelectChara.h"

USING_NS_CC;

CSplashScene::CSplashScene()
{
	CCLayerColor* colLayer = CCLayerColor::create(ccc4(255, 255, 255, 255));
	addChild(colLayer);

	CCSprite* sp = CCSprite::create("Image/home/splash.png");
	sp->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
	addChild(sp);
};

CSplashScene::~CSplashScene()
{

}

void CSplashScene::onEnter()
{
	CBaseLayer::onEnter();
	scheduleOnce(schedule_selector(CSplashScene::loadGameData), 0);
}

void CSplashScene::loadGameData(float dt)
{
	int nStartTime = getTimeSec();
	
	LCommon::sharedInstance()->loadGameDataViaNet();
	CBubble::preloadAnimates();

	int nEndTime = getTimeSec();

	float fDiffMillis = nEndTime - nStartTime;

	float delayTime = fDiffMillis / 1000;
	if (0 < delayTime && delayTime < 2.0f)
		delayTime = 2.0f - delayTime;
	else
		delayTime = 0;

	scheduleOnce(schedule_selector(CSplashScene::transitScene), delayTime);
}
void CSplashScene::transitScene(float dt)
{
	//CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CMainMenuLayer::scene(), ccWHITE));
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CSelCharaScene::scene(), ccWHITE));
}

#ifndef __CBubble_H__
#define __CBubble_H__

#define NEIGHBOR_COUNT	6
#include "cocos2d.h"
#include <vector>

#include "Box2D/Box2D.h"


#define BT_EMPTY 0
#define INDEX_EMPTY	-1
#define INDEX_WALL	-2
#define INDEX_TOP	-3

#define ANIM_PERIOD 0.05f

#define MASK_BUBBLE_SWAP		0x80
#define MASK_BUBBLE_WASTE		0x40
#define MASK_BUBBLE_NOHIT_CANDY 0x20
#define MASK_COLOR				0x1F

#define BBL_SCALE 0.82f
class CBubble;

struct stBubbleBack
{
	bool bEnable;
	cocos2d::CCPoint pos;
	char byType;
	char byColor;
	bool bPopped;
	void*	tag;

	void init()
	{
		bEnable = false;
		byType = 0;
		byColor = 0;
		tag = 0;
	}
};

typedef std::vector<stBubbleBack> TBackBubbleList;

class CBubble : public cocos2d::CCSprite
{
public:
	CBubble(char byType, char byColor);
	~CBubble(void);

	int m_nIndex;
	int m_nUID;
	CBubble* m_pNeighbors[NEIGHBOR_COUNT];
	char m_byColor;
	char m_byType;
	bool m_bChecked;
	bool m_bSwap;
	bool m_bWaste;
	bool m_bNoHitCandy;
	bool m_bIsBeeGroup;

	CBubble* m_pMorMaster;
	CBubble* m_pMorSlave[NEIGHBOR_COUNT];
	bool	m_bHasMor;

	b2Body *m_physicsBody;
	bool m_bGameOverBubble;
	bool m_bTouchedBottomWall;

	char m_byState;

	void initBubble();
	void changeTo(char byType, char byColor);
	void addNeighbor(CBubble* bubble);
	void removeNeighbor(CBubble* bubble);
	bool isNeighbor(CBubble* bubble);
	bool isMorBubbleSeparated();

	void animateExplode(float fDelayTime);
	void prepareDrop();

	void runExplode();
	void runBright();

	void backupTo(stBubbleBack& back);

	static float SIZE;
	static float SIZE_OV;

	static cocos2d::CCAnimation* createAnimate(const char* folder, int nAnimCount, float fPeriod = ANIM_PERIOD);
	static void preloadAnimates();
	static void preloadAnimate(const char* folder, int nAnimCount, float fPeriod = ANIM_PERIOD);
	static std::map<std::string, cocos2d::CCAnimation*> gAnimateCache;
};

typedef std::vector<CBubble*> TBubbleList;

#endif // __CBubble_H__


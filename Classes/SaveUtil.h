#ifndef __SAVE_UTIL_H__
#define __SAVE_UTIL_H__

#include "cocos2d.h"
#include "cocos-ext.h"

class CSaveUtil : public cocos2d::CCObject
{
public:
	CSaveUtil();
	~CSaveUtil(void);

	static CSaveUtil* sharedInstance();
	std::string getSaveFilePath();
	void loadData();
	void loadFromString(const char* str);
	std::string getSavedData();
	static void saveToFile(const std::string& strData, const std::string& strPath);

    /**
    @brief Get bool value by key, if the key doesn't exist, a default value will return.
     You can set the default value, or it is false.
    */
    bool    getBoolForKey(const char* pKey, bool defaultValue);
    /**
    @brief Get integer value by key, if the key doesn't exist, a default value will return.
     You can set the default value, or it is 0.
    */
    int     getIntegerForKey(const char* pKey, int defaultValue);
    /**
    @brief Get float value by key, if the key doesn't exist, a default value will return.
     You can set the default value, or it is 0.0f.
    */
    float    getFloatForKey(const char* pKey, float defaultValue);
    /**
    @brief Get double value by key, if the key doesn't exist, a default value will return.
     You can set the default value, or it is 0.0.
    */
    double  getDoubleForKey(const char* pKey, double defaultValue);
    /**
    @brief Get string value by key, if the key doesn't exist, a default value will return.
    You can set the default value, or it is "".
    */
    std::string getStringForKey(const char* pKey, const std::string & defaultValue);

	void getByteArray(const char* pKey, unsigned char* pArray, int nCount, char defaultValue);

    // set value methods

    /**
    @brief Set bool value by key.
    */
    void    setBoolForKey(const char* pKey, bool value);
    /**
    @brief Set integer value by key.
    */
    void    setIntegerForKey(const char* pKey, int value);
    /**
    @brief Set float value by key.
    */
    void    setFloatForKey(const char* pKey, float value);
    /**
    @brief Set string value by key.
    */
    void    setStringForKey(const char* pKey, const std::string & value);

	void setByteArray(const char* pKey, unsigned char* pArray, int nCount);
    /**
     @brief Save content to xml file
     */
    void    flush();

protected:
	std::string m_sFilePath;
	cocos2d::CCDictionary* m_pMainDic;
	static CSaveUtil* _instance;
};

#endif // __SAVE_UTIL_H__


#ifndef __CCharacter_H__
#define __CCharacter_H__

#include "cocos2d.h"
#include "cocos-ext.h"

typedef enum 
{
	LOOK_A,
	LOOK_B,
	LOOK_C,
	LOOK_D,
	LOOK_E,
	LOOK_F,
	LOOK_G,
	LOOK_H,
} ELOOK_POS;

typedef enum
{
	ANIM_LOOK,
	ANIM_TAKEBUBBLE,
	ANIM_SHOOT,
	ANIM_WIN,
	ANIM_FAIL,
} EANIMATION_TYPE;

typedef enum 
{
	ST_NORMAL,
	ST_SAD
} ECHARA_STATE;

class CCharacter : public cocos2d::extension::CCArmature
{
public:
	CCharacter();
	~CCharacter(void);

	static CCharacter* create(std::string name);
	void setCharaState(int state);
	void setLookPos(int state);
	void runAnimation(int state);

	void cbEventComplete(cocos2d::extension::CCArmature * amature, cocos2d::extension::MovementEventType evtType, const char * pCh);
	void runSweetAction();
	void stopSweetAction();
	virtual void onEnter();
protected:
	cocos2d::CCSprite* m_pSweetSprite;
	cocos2d::CCAnimation* m_pSadSweet;
	int m_nLookDirect;
	int	m_nType;
	int	m_nState;
};

#endif // __CCharacter_H__


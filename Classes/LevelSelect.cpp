//
//  CLevelSelect.m
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//

#include "SimpleAudioEngine.h"
#include "LCommon.h"
#include "AppDelegate.h"
#include "LevelSelect.h"
#include "SelectBoost.h"
#include "SelectChara.h"
#include "MathUtil.h"
#include "DailySpin.h"
#include <algorithm>

enum 
{
	BTN_ID_SETTING = BTN_ID_START,
	BTN_ID_BACK,
	BTN_ID_SPIN,
	BTN_ID_TREASURE,
	BTN_ID_FREEMAGIC,
	BTN_ID_GOTO_SELECTLEVEL,
	BTN_ID_PLAY,

	BTN_ID_LEVEL_START,
    
};

enum
{
    POPUP_ID_GIVE_LIVE = POPUP_ID_START,
    POPUP_ID_COLLECT_LIVE
};

// added by KUH in 2014-09-26 , if TRACK_OFF_DEBUG define, remove cloud over track
//#define TRACK_OFF_DEBUG

class MyDragPanel : public UIScrollView
{
public:
	CCPoint getScrollPos() { return moveChildPoint; };
	void setScrollPos(CCPoint pos) {
		jumpToDestination(pos);
		scrollChildren(0.0f, 0.0f);
	}
    
    void stopScrolling()
    {
        stopAutoScrollChildren();
    }
};

#ifdef LEVEL_PLACEMENT_TEST
class MyMoveButton : public UIButton
{
public:
	MyMoveButton()
	{
		m_bTouchPassedEnabled = false;
	}

	static MyMoveButton* create()
	{
		MyMoveButton* widget = new MyMoveButton();
		if (widget && widget->init())
		{
			widget->autorelease();
			return widget;
		}
		CC_SAFE_DELETE(widget);
		return NULL;
	}
};
#endif //LEVEL_PLACEMENT_TEST

USING_NS_CC;
extern int LEVEL_POS[];

//#define PAN_HEIGHT 8505
//#define PAN_ORG_H 6058
//#define FRIEND_ARR 3
//#define MAX_RANK    5
//#define LVL_POS_ROWS	8

typedef struct tagANIMATION_INFO
{
	std::string srcpath;

	int PosX;
	int PosY;

	float scaleX;
	float scaleY;

} ANIMATION_INFO;


extern int cocosAnimCounts;
extern ANIMATION_INFO cocosAnimInfo[];

extern int particleCounts;
extern ANIMATION_INFO particleInfo[];


CCPoint getLevelPos(int nLevel)
{
	return ccp(LEVEL_POS[nLevel * LVL_POS_ROWS], LEVEL_POS[nLevel * LVL_POS_ROWS + 1]);
}

CLevelSelect::CLevelSelect():
	m_nSelectedLevel(-1), m_isOverview(true)
{
	setSceneID(SCENE_WORLDMAP);

	setBGMName(kSND_BG_MAINTHEME);
    
    TFriendMap& fbfriendList = LCommon::sharedInstance()->friendList;
    
	if (!fbfriendList.empty())
	{
		for (TFriendMap::iterator it = fbfriendList.begin(); it != fbfriendList.end(); it++)
        {
            it->second->texture = NULL;
        }
    }
};

void CLevelSelect::initUI()
{
	m_nSelectedLevel = LCommon::sharedInstance()->curLevel();
	m_isOverview = true;

	m_pWidget = dynamic_cast<UIWidget*>(GUIReader::shareReader()->widgetFromJsonFile("Image/level.json"));
	m_pMainLayer->addWidget(m_pWidget);
	int panHeight = PAN_HEIGHT;

	MyDragPanel* dragPane = (MyDragPanel*) UIDragPanel::create();
	dragPane->setTouchEnable(true);
	dragPane->setSize(CCSizeMake(SC_WIDTH, SC_HEIGHT - m_nActionBarHeight));
	dragPane->setInnerContainerSize(CCSizeMake(SC_WIDTH, panHeight));
	dragPane->setName("dragPane");
	dragPane->setDirection(SCROLLVIEW_DIR_VERTICAL);
	m_pWidget->addChild(dragPane);
    

	// scroll to current map
	CCPoint curPos = getLevelPos(LCommon::sharedInstance()->curLevel());
	//curPos = getLevelPos(26);
	float fPosY = curPos.y - SC_HEIGHT_HALF;
	if (fPosY < 0) fPosY = 0;
	if (fPosY > panHeight - SC_HEIGHT + m_nActionBarHeight) fPosY = panHeight - SC_HEIGHT + m_nActionBarHeight;

	dragPane->setScrollPos(ccp(0, -fPosY));


    // added by KUH in 2014-09-24
#ifdef GLOW_ADD
    if(!LCommon::sharedInstance()->parseSelChar)
    {
        //CCPoint pos = lvPos;
        //pos.y -= 180;
        //pos.x += 60;
        //CCPoint pos = selectedBtn->getWorldPosition();
        
        int curLevel = LCommon::sharedInstance()->curLevel();
        CCPoint pos = getLevelPos(curLevel);
        CCPoint scrPos = dragPane->getScrollPos();
        pos.y += scrPos.y;
        
        showTutorial("Click here to start game at recent level.", pos, TUTO_BIG_CHARA, false, false);
        LCommon::sharedInstance()->parseSelChar = true;
    }
#endif
    
    UIWidget* lvlPane = dynamic_cast<UIWidget*>(GUIReader::shareReader()->widgetFromJsonFile("Image/level/MAP1_1.ExportJson"));
	UIWidget* imgPane = lvlPane->getChildByName("levelPane");
//     for (int i = 0; i < 9; i++)
//     {
//         UIImageView* imgWorld = UIImageView::create();
//         imgWorld->loadTexture(CCString::createWithFormat("Image/level/world_bg_%d.png", i)->getCString());
// 		imgWorld->setAnchorPoint(ccp(0.5f, 0));
//         imgWorld->setPosition(ccp(SC_WIDTH_HALF, 1024 * i));
//         imgPane->addChild(imgWorld);
//     }
    dragPane->addChild(imgPane);
    
// 	UIImageView* imgPane = UIImageView::create();
// 	imgPane->setName("levelPane");
// 	imgPane->loadTexture("Image/level/map.png");
// 	imgPane->setAnchorPoint(CCPointZero);
// 	dragPane->addChild(imgPane);
// 


	//Map animations
	for(int i = 0; i < cocosAnimCounts; i++)
	{
		CCArmature *armature = CCArmature::create(cocosAnimInfo[i].srcpath.c_str());
		armature->getAnimation()->playByIndex(0);
		armature->setPosition(ccp(SC_WIDTH_HALF, cocosAnimInfo[i].PosY));
		armature->setScaleX(cocosAnimInfo[i].scaleX);
		armature->setScaleY(cocosAnimInfo[i].scaleY);
		imgPane->addCCNode(armature);
	}

	//Particle Effect
	for(int i = 0; i < particleCounts; i++)
	{
		CCParticleSystemQuad* emitter = CCParticleSystemQuad::create(particleInfo[i].srcpath.c_str());
		emitter->setPosition(ccp(particleInfo[i].PosX, particleInfo[i].PosY));
		emitter->setScaleX(particleInfo[i].scaleX);
		emitter->setScaleY(particleInfo[i].scaleY);
		//dragPane->getInnerContainer()->addCCNode(emitter);
		imgPane->addCCNode(emitter);
	}
	
	
	int nCount = MAX_LEVEL;
	for (int i = 0; i < nCount; i++)
	{
		UIImageView* btn = UIImageView::create();
		btn->setPosition(getLevelPos(i));
		btn->setName(CCString::createWithFormat("btn_%d", i)->getCString());
		btn->setScale(0.95f);
		bool bEnable = (i <= LCommon::sharedInstance()->maxLevel());

		UILabelBMFont* label = UILabelBMFont::create();

#ifdef TRACK_OFF_DEBUG
        bEnable = true;
#endif

		if (i < LCommon::sharedInstance()->maxLevel())
		{
			int nStarCount = LCommon::sharedInstance()->stars(i);

			registerClick(btn, BTN_ID_LEVEL_START + i);
			btn->loadTexture(CCString::createWithFormat("Image/level/map_enabled_star_%d.png", nStarCount)->getCString());
			btn->setPosition(btn->getPosition() + CCPointMake(0, 10));

			if(nStarCount)			
			{
				label->setFntFile("Image/level/Hobostd110_blue.fnt");
				label->setPosition(ccp(0, -38));
			}else{
				label->setFntFile("Image/level/Hobostd110_orange.fnt");
				label->setPosition(ccp(0, -40));
			}


			//UIImageView* imgEye = UIImageView::create();
			//imgEye->loadTexture("Image/level/map_eye.png");
			//imgEye->setPosition(ccp(-2, 7));
			//imgEye->setName("imgEye");
			//btn->addChild(imgEye);
   
//			// show star 
//			UIPanel* starPane = UIPanel::create();
//			starPane->setName("starPane");
//			btn->addChild(starPane);
//            
//#ifdef TRACK_OFF_DEBUG
//            nStarCount = 3;
//#endif
//			for (int i = 0; i < 3; i++)
//			{
//				UIImageView* imgView = UIImageView::create();
//				float xx = -28 + i * 25;
//				float yy = 7 + (i % 2) * 9 + 35;
//
//				if (i < nStarCount)
//				{
//					imgView->loadTexture("Image/common/star.png");
//					imgView->setScale(0.30f);
//				}
//				else
//				{
//					imgView->loadTexture("Image/common/star_bg.png");
//					imgView->setScale(0.55f);
//					yy += 3;
//				}
//
//				imgView->setPosition(ccp(xx, yy));
//				starPane->addChild(imgView);
//			}
//
//            // modified by KUH in 2014-09-25 from false => true for star
//			starPane->setVisible(true);
//
////			fEndEnablePos = btn->getPosition().y;
		}
		else if (i == LCommon::sharedInstance()->maxLevel())
		{
			registerClick(btn, BTN_ID_LEVEL_START + i);
			btn->loadTexture("Image/level/map_enabled_max.png");
			btn->setPosition(btn->getPosition() + CCPointMake(0, 20));

			label->setFntFile("Image/level/Hobostd110_orange.fnt");
			label->setPosition(ccp(0, -40));

		}else{
			btn->loadTexture("Image/level/map_disabled.png");
			btn->setPosition(btn->getPosition() + CCPointMake(0, 0));

			label->setFntFile("Image/level/Hobostd110_gray.fnt");
			label->setPosition(ccp(0, -21));
		}

		label->setScale(0.5f);
		label->setText(toString(i + 1).c_str());
		btn->addChild(label);

		imgPane->addChild(btn);

#ifdef LEVEL_PLACEMENT_TEST
		float fScale = 0.9f;
		int nLblNum = i + 1;
		int ptStart = i * LVL_POS_ROWS + 2;
		CCString* strImgFriend = CCString::create("Image/friends/def_profile.png");
		for (int k = 0; k < FRIEND_ARR; k++)
		{
			int nnn = i * FRIEND_ARR + k;
			MyMoveButton* btnlbl = MyMoveButton::create();
			if (i % 2 == 0)
				btnlbl->loadTextures("Image/level/balloon_blue.png", "Image/level/balloon_orange.png", "");
			else
				btnlbl->loadTextures("Image/level/balloon_orange.png", "Image/level/balloon_blue.png", "");
			btnlbl->setText("");//toString(nLblNum).c_str());
			btnlbl->setTag(nnn);
			btnlbl->setFontSize(40);
#ifdef LEVEL_PLACEMENT_EDIT
			btnlbl->setTouchEnabled(true);
			btnlbl->addPushDownEvent(this, coco_pushselector(CLevelSelect::onChClicked));
			btnlbl->addMoveEvent(this, coco_moveselector(CLevelSelect::onChTouched));
#endif  // LEVEL_PLACEMENT_EDIT
			CCPoint newPos = ccp(LEVEL_POS[ptStart + k * 2], LEVEL_POS[ptStart + k * 2 + 1]);

			float alpha = getAlpha(newPos, getLevelPos(i));

			btnlbl->setRotation(-alpha * 180 / M_PI);
			btnlbl->setPosition(newPos);
			btnlbl->setScale(fScale);
			pButtons[nnn] = btnlbl;

			UIImageView* imgViewF = UIImageView::create();
			imgViewF->loadTexture(strImgFriend->getCString());
			imgViewF->setScale(0.58f);
			imgViewF->setRotation(alpha * 180 / M_PI);
			btnlbl->addChild(imgViewF);

			imgPane->addChild(btnlbl);

			nLblNum = k + 1;
			fScale = 0.67f;
			strImgFriend = CCString::createWithFormat("Image/friends/friends_0_%d.png", (i + k) % 5);
		}
#endif //LEVEL_PLACEMENT_TEST
	}

	UIWidget* btnSetting = m_pWidget->getChildByName("btnSetting");
	btnSetting->setPosition(ccp(65, SC_HEIGHT - m_nActionBarHeight - 180));
	registerClick(btnSetting, BTN_ID_SETTING);

	UIWidget* btnTreasure = m_pWidget->getChildByName("btnTreasure");
	btnTreasure->setPosition(ccp(SC_WIDTH - 65, 80));
	registerClick(btnTreasure, BTN_ID_TREASURE);

	UIWidget* imgSide = m_pWidget->getChildByName("imgSide");
	float scaleY = (SC_HEIGHT - m_nActionBarHeight) / 950;
	imgSide->setScaleY(scaleY);

	UIWidget* btnSpin = m_pWidget->getChildByName("btnSpin");
	btnSpin->setPosition(ccp(SC_WIDTH - 65, SC_HEIGHT - m_nActionBarHeight - 80));
	btnSpin->setVisible(false);
	//registerClick(btnSpin, BTN_ID_SPIN);

	UIWidget* btnFreeMagic = m_pWidget->getChildByName("btnFreeMagic");

	TMagicCost& cost = LCommon::sharedInstance()->magicCostList[0]; // first free magic
	if (cost.nMagicCount == 0 || LCommon::sharedInstance()->isGotFreeMagic())
	{
		btnFreeMagic->setVisible(false);
	}
	else
	{
		btnFreeMagic->setPosition(ccp(SC_WIDTH - 65, SC_HEIGHT - m_nActionBarHeight - 180));
		registerClick(btnFreeMagic, BTN_ID_FREEMAGIC);
	}

	UIWidget* btnPlay = m_pWidget->getChildByName("btnPlay");
	registerClick(btnPlay, BTN_ID_PLAY);

	UIPanel* selAround = (UIPanel*) m_pWidget->getChildByName("selAround");
	registerClick(selAround, BTN_ID_GOTO_SELECTLEVEL);

    int nStartCloudLevel = 0;
    int nCloudWorld = getWorldByLevel(LCommon::sharedInstance()->maxLevel());
    
    if (nCloudWorld < MAX_WORLD)
    {
        for (int i = 0; i <= nCloudWorld; i++)
        {
            nStartCloudLevel += getLevelsPerWorld(i);
        }
        
        
#ifdef TRACK_OFF_DEBUG
        nStartCloudLevel = 127;
#endif

        CCPoint lvPos = getLevelPos(nStartCloudLevel);
        
        addCloudEffect(lvPos.y + 50, panHeight);
    }

	createActionBar1();

	initPowerups();

	UIWidget* guiPane = m_pWidget->getChildByName("guiPane");
	
	UIButton* btnBack = UIButton::create();
	btnBack->setAnchorPoint(ccp(0.0f, 0.5f));
	btnBack->loadTextures("Image/world/w1/btn_back_n.png", "Image/world/w1/btn_back_p.png", "");
	btnBack->setPosition(ccp(0, SC_HEIGHT - m_nActionBarHeight - 80));
	registerClick(btnBack, BTN_ID_BACK);
	guiPane->addChild(btnBack);

	createRightFriendInfo();

	m_friendLayer = UILayer::create();
	imgPane->addCCNode(m_friendLayer);
	refreshWorldState();


#ifdef LEVEL_PLACEMENT_TEST
#ifdef LEVEL_PLACEMENT_EDIT
	UIButton* btnX;
    
    	btnX = UIButton::create();
    	btnX->setTouchEnable(true);
    	btnX->loadTextures("Image/popup/btn_blue_n.png", "Image/popup/btn_blue_p.png", "");
    	btnX->setFontSize(80);
    	btnX->setText("X");
    	btnX->addPushDownEvent(this, coco_pushselector(CLevelSelect::onBtClicked));
    	btnX->setScale(0.5f);
    	btnX->setPosition(ccp(SC_WIDTH - 110, 540));
    	btnX->setTag(10001);
    	m_pMainLayer->addWidget(btnX);
    
    	btnX = UIButton::create();
    	btnX->setTouchEnable(true);
    	btnX->loadTextures("Image/popup/btn_blue_n.png", "Image/popup/btn_blue_p.png", "");
    	btnX->setFontSize(80);
    	btnX->setText("OUT");
    	btnX->addPushDownEvent(this, coco_pushselector(CLevelSelect::onBtClicked));
    	btnX->setScale(0.5f);
    	btnX->setPosition(ccp(SC_WIDTH - 110, 600));
    	btnX->setTag(10002);
    	m_pMainLayer->addWidget(btnX);
    
    	isX = true;
#endif //LEVEL_PLACEMENT_EDIT
}

void CLevelSelect::onBtClicked(cocos2d::CCObject* obj)
{
	UIButton* btn = (UIButton*) obj;
	int nCurTag = btn->getTag();
	// 	
	// 	for (TFriendList::iterator it = localFriendList.begin(); it != localFriendList.end(); it++) {
	// 		it->nMaxLevel += nCurTag;
	// 		it->nMaxLevel = MAX(0, it->nMaxLevel);
	// 		it->nMaxLevel = MIN(127, it->nMaxLevel);
	// 	}
	// 
	// 	refreshFriendInfoDelay(0);
	if (nCurTag == 10001)
	{
		isX = !isX;

			MyDragPanel* dragPane = (MyDragPanel*) m_pWidget->getChildByName("dragPane");
			dragPane->setTouchEnabled(isX);

		btn->setText(isX? "O" : "X");
	}
	else if (nCurTag == 10002)
	{
		CCLog("Output start");
		for (int i = 0; i < MAX_LEVEL; i++)
		{
			std::string str;

			for (int k = 0; k < 3; k++)
			{
				int nnn = i * 3 + k;
				str += toString(pButtons[nnn]->getPosition().x);
				str += ", ";
				str += toString(pButtons[nnn]->getPosition().y);
				str += ", ";
			}
			CCLog("%s \t\t//%d", str.c_str(), i);
		}
	} else if (nCurTag < 100)
	{
		if (m_nCurSel >= 0)
		{
			UIButton* btnlbl = pButtons[m_nCurSel];
			CCPoint pt = btnlbl->getPosition();
			if (isX) pt.x += nCurTag;
			else pt.y += nCurTag;
			btnlbl->setPosition(pt);
		}
	}
}

void CLevelSelect::onChClicked(cocos2d::CCObject* obj)
{
	UIButton* btn = (UIButton*) obj;
	m_nCurSel = btn->getTag();
}

void CLevelSelect::onChCanceled(cocos2d::CCObject* obj)
{
}

void CLevelSelect::onChTouched(cocos2d::CCObject* obj)
{
	UIButton* btn = (UIButton*) obj;
	MyDragPanel* dragPane = (MyDragPanel*) m_pWidget->getChildByName("dragPane");
	CCPoint mvPos = btn->getTouchMovePos();
	CCPoint newPos = mvPos - dragPane->getScrollPos();

	int nLevel = btn->getTag() / 4;
	float alpha = getAlpha(newPos, getLevelPos(nLevel));

	btn->setRotation(-alpha * 180 / M_PI);
	btn->setPosition(newPos);
	CCLog("ccmoveed %d, %d", (int) btn->getPosition().x, (int) btn->getPosition().y);

#endif // LEVEL_PLACEMENT_TEST
}

void CLevelSelect::initPowerups()
{
	int nCharaId = LCommon::sharedInstance()->curChara;

	for (int i = 1; i <= 5; i++)
	{
		UIButton* pPowerupBtn = (UIButton*)m_pActionBar->getChildByName(CCString::createWithFormat("btn_Powerup_%d", i)->getCString());
		pPowerupBtn->setTag(i);
		pPowerupBtn->addReleaseEvent(this, coco_releaseselector(CLevelSelect::onSelectAnimalPowerup));

		if(nCharaId == i/*Equipped powerup*/ || special_boost_info[i].bPermanent == true/*Permanent powerup*/)
		{
			pPowerupBtn->setColor(ccc3(255, 255, 255));
			pPowerupBtn->setTouchEnable(false);
		}
		else
		{
			pPowerupBtn->setColor(ccc3(100, 100, 100));
			pPowerupBtn->setTouchEnable(true);
		}

	}
}

void CLevelSelect::addCloudEffect(float startY, float endY)
{
	if (endY - startY < 300) return;

	UIWidget* imgBG = m_pWidget->getChildByName("levelPane");

	UIImageView* imgCloud = UIImageView::create();
	imgCloud->loadTexture("Image/common/cloud.png");

	float fStartY = startY;
	float fEnd = endY;

	while (fStartY < fEnd)
	{
		float xx = -75;
		for (int i = 0; i < 6; i++)
		{
			xx += getRandRange(100, 150);
			float yy = fStartY + getRandRange(0, 80) - 40;
			UIWidget* img = imgCloud->clone();
			img->setPosition(ccp(xx, yy));
			float fScale = getRandRange(7, 15) / 20.0f;
			img->setScale(fScale);
			imgBG->addChild(img);

			CCDelayTime* delay = CCDelayTime::create(i * 0.3);
			CCMoveBy* mov = CCMoveBy::create(fScale * 8, ccp(130 * fScale, 0));
			CCSequence* seq = CCSequence::create(delay, mov, mov->reverse(), NULL);
			img->runAction(CCRepeatForever::create(seq));
		}

		fStartY += 110;
	}
}

CLevelSelect::~CLevelSelect()
{
}

void CLevelSelect::refreshWorldState()
{
	UIWidget* imgView = m_pWidget->getChildByName("levelPane");
	UIPanel* selPane = (UIPanel*) m_pWidget->getChildByName("selPane");

	UIWidget* selAround = m_pWidget->getChildByName("selAround");
	selAround->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
	selAround->setScale(SC_BG_SCALE);

	UIWidget* selectedBtn = NULL;
	UIWidget* imgEye;
	UIWidget* panStars;

	if (m_nSelectedLevel >= 0)
	{
		selectedBtn = imgView->getChildByName(CCString::createWithFormat("btn_%d", m_nSelectedLevel)->getCString());
		imgEye = selectedBtn->getChildByName("imgEye");
		panStars = selectedBtn->getChildByName("starPane");

		if (imgEye)
			imgEye->setVisible(m_isOverview);
		if (panStars)
			panStars->setVisible(!m_isOverview);
	}

	if (m_isOverview) // no selected
	{
		imgView->setPosition(CCPointZero);
		imgView->setScale(1.0f);
		selPane->setVisible(false);
		selPane->setTouchEnable(false, true);

		m_friendLayer->setVisible(true);
	}
	else
	{

		// move panel to center
		MyDragPanel* dragPane = (MyDragPanel*) m_pWidget->getChildByName("dragPane");
        dragPane->stopScrolling();
		CCPoint scrollPos = dragPane->getScrollPos();

		selPane->setVisible(true);
		selPane->setTouchEnable(true, true);

		CCPoint dstPoint = selectedBtn->getPosition();
		imgView->setScale(2.0f);

		float scale = 2.0f;
		CCSize sizeContent = CCSize(768, PAN_HEIGHT);

		int nW = SC_WIDTH / scale;
		int nH = (SC_HEIGHT - m_nActionBarHeight) / scale;

		float newX = nW / 2 - dstPoint.x;
		float newY = nH / 2 - dstPoint.y - 80 / scale;

		if (newX > 0) newX = 0;
		if (newX < -(sizeContent.width - nW)) 
			newX = -(sizeContent.width - nW);
		if (newY > 0) newY = 0;
		if (newY < -(sizeContent.height - nH)) 
			newY = -(sizeContent.height - nH);

		newX = newX * scale;
		newY = newY * scale;

		imgView->setPosition(ccp(newX - scrollPos.x, newY - scrollPos.y));

		UIWidget* btnPlay = m_pWidget->getChildByName("btnPlay");
		btnPlay->setPosition(ccp(dstPoint.x * scale + newX, dstPoint.y * scale + newY + 70 * scale));

		m_friendLayer->setVisible(false);
	}
}

void CLevelSelect::onClicked(cocos2d::CCObject* obj)
{
	UIButton* btn = (UIButton*) obj;
	int nTag = btn->getTag();

	if (nTag >= BTN_ID_LEVEL_START)	{
		m_nSelectedLevel = nTag - BTN_ID_LEVEL_START;
#ifdef LEVEL_PLACEMENT_TEST
#ifdef LEVEL_PLACEMENT_EDIT
		for (int i = 0; i < MAX_LEVEL; i++)
		{
			pButtons[i * FRIEND_ARR]->setVisible(m_nSelectedLevel == i);
		}
#endif // LEVEL_PLACEMENT_EDIT
#else
		/*m_isOverview = false;
        initRankList(m_nSelectedLevel);
		refreshWorldState();*/

		m_isOverview = false;
		initRankList(m_nSelectedLevel);

		LCommon::sharedInstance()->setCurLevel(m_nSelectedLevel);
		LCommon::sharedInstance()->setMaxLevel(m_nSelectedLevel);
		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CSelBoostScene::scene(), ccWHITE));

#endif //LEVEL_PLACEMENT_TEST
	} else 
		if (nTag == BTN_ID_GOTO_SELECTLEVEL)	{
		m_isOverview = true;
		refreshWorldState();
	} else if (nTag == BTN_ID_PLAY) {
		LCommon::sharedInstance()->setCurLevel(m_nSelectedLevel);
		LCommon::sharedInstance()->setMaxLevel(m_nSelectedLevel);
		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CSelBoostScene::scene(), ccWHITE));
	} else if (nTag == BTN_ID_BACK) {
		IS_ANIMAL_SELECTED = false;
		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CSelCharaScene::scene(), ccWHITE));
	} else if (nTag == BTN_ID_SETTING) {
		showSettings();
	} else if (nTag == BTN_ID_SPIN) {
		DailySpin_PreviousScreen = SCENE_WORLDMAP;
		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CDailySpin::scene(), ccWHITE));
	} else if (nTag == BTN_ID_FREEMAGIC) {
		AppDelegate::app->showRateApp();
	} else if (nTag == BTN_ID_TREASURE) {
		showTreasure();
	}
}

void CLevelSelect::registerClick(UIWidget* btn, int nTag)
{
	btn->setTouchEnable(true);
	btn->setTag(nTag);
	btn->addReleaseEvent(this, coco_releaseselector(CLevelSelect::onClicked));
}

void CLevelSelect::changedBtnState(UIButton* levelBtn, bool bPressed)
{
	UIPanel* pan = (UIPanel*) levelBtn->getChildByName("levelpan");
	pan->setPosition(ccp(0, bPressed? -7 : 0));
}

void CLevelSelect::onEnter()
{
	CBaseLayer::onEnter();

	//Map animations
	CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("Image/animation/map/w1/W1.ExportJson");
	CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("Image/animation/map/w2/W2.ExportJson");
	CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("Image/animation/map/w3/W3.ExportJson");
	CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("Image/animation/map/w4/W4.ExportJson");
	CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("Image/animation/map/w5/W5.ExportJson");


    initLocalFriends();
    
	initUI();

	onTimer();

    refreshFriendInfoDelay(0);

	//scheduleOnce(schedule_selector(CLevelSelect::delayTest), 2.0f);
	//scheduleOnce(schedule_selector(CLevelSelect::delayTest1), 3.0f);

	setPosition(ccp(0, SC_ADHEIGHT));
}

void CLevelSelect::delayTest(float dt)
{
	//AppDelegate::app->receivedFriendPicture(NULL, 0);
	AppDelegate::app->receivedLiveReqList("329877157212042_100008211465102|100007815340909|Wei Liu");
	//AppDelegate::app->receivedFromNative(MSG_FB_COLLECTLIVE_SUC, "111", 0);
	//AppDelegate::app->receivedFromNative(MSG_FB_COLLECT_LIVE_LIST, "111|222|333^234|35|436^456|346|645", 0);
}

void CLevelSelect::delayTest1(float dt)
{
	refreshFriendInfoDelay(0);
}

void CLevelSelect::onEnterTransitionDidFinish()
{
    CBaseLayer::onEnterTransitionDidFinish();
    //scheduleOnce(schedule_selector(CLevelSelect::sendFriendsRequest), 0.1f);
}

void CLevelSelect::sendFriendsRequest(float dt)
{
    AppDelegate::app->requestFriendList();
    AppDelegate::app->checkIncomingRequests();
}

bool CLevelSelect::onProcEvent(int nEventId, int nParam, std::string strParam)
{
    switch (nEventId) {
        case EVT_REFRESH_FRIEND_LIST:
            scheduleOnce(schedule_selector(CLevelSelect::refreshFriendInfoDelay), 0.01f);
            break;
		case EVT_REFRESH_GIFTING_POPUP:
			scheduleOnce(schedule_selector(CLevelSelect::refreshGiftingPopup), 0.01f);
			break;
        case EVT_RECEIVED_LIVEREQ_LIST:
            scheduleOnce(schedule_selector(CLevelSelect::receivedLiveReqList), 0.01f);
            break;
        case EVT_RECEIVED_GIVELIVE_LIST:
            scheduleOnce(schedule_selector(CLevelSelect::receivedCollectLiveList), 0.01f);
            break;
		case EVT_GIVE_LIVE_SUC:
			giveLive(strParam);
			break;
		case EVT_COLLECT_LIVE_SUC:
			collectLive(strParam);
			break;
		case EVT_GOT_FREE_MAGIC:
			{
				UIWidget* btnFreeMagic = m_pWidget->getChildByName("btnFreeMagic");
				if (btnFreeMagic != NULL)
					btnFreeMagic->setVisible(false);
			}
			break;
    }
    return CBaseLayer::onProcEvent(nEventId, nParam, strParam);
}

void CLevelSelect::refreshFriendInfoDelay(float dt)
{
	TFriendList friendList;

    TFriendMap& fbfriendList = LCommon::sharedInstance()->friendList;

	int myMaxLevel = LCommon::sharedInstance()->maxLevel();
	int nMaxAvatar = 2;
	if (!fbfriendList.empty())
	{
		for (TFriendMap::iterator it = fbfriendList.begin(); it != fbfriendList.end(); it++)
		{
			TFriendInfo* info = it->second;
			if (info->fbid == LCommon::sharedInstance()->myFBID)
			{
				info->fname = "You";
				friendList.insert(friendList.begin(), *info);
			}
			else if (LCommon::sharedInstance()->isFBConnected)
			{
				friendList.push_back(*info);
			}
		}
	}

	for (TFriendList::iterator it = localFriendList.begin(); it != localFriendList.end(); it++) {
// 		if (it->nMaxLevel > myMaxLevel) continue;
// 		nMaxAvatar--;
// 		if (nMaxAvatar < 0) continue;
		friendList.push_back(*it);
	}

    if (friendList.empty()) return;
    
    CCRect rc = CCRectMake(0, 0, 128, 128);

	char frCountPerLevel[MAX_LEVEL];
	char frLastIndexPerLevel[MAX_LEVEL];
	memset(frCountPerLevel, 0, MAX_LEVEL);
	memset(frLastIndexPerLevel, 0, MAX_LEVEL);

	for (TFriendList::iterator it = friendList.begin(); it != friendList.end(); it++) {
		frCountPerLevel[it->nMaxLevel]++;
	}

    for (TFriendList::iterator it = friendList.begin(); it != friendList.end(); it++) {
        TFriendInfo& tinfo = (*it);

		CCLog("refreshFriendInfoDelay fbid: %s", tinfo.fbid.c_str());

		int nLevel = tinfo.nMaxLevel;

		CCPoint levelPos = getLevelPos(nLevel);
		int nPosIndex;
		float fDist;
		int nRID;
		float fScale;
		if (tinfo.fbid == LCommon::sharedInstance()->myFBID)
		{
			nPosIndex = 1;
			fDist = 0;
			fScale = 0.9f;
		} else
		{
			nRID = (frCountPerLevel[nLevel] - frLastIndexPerLevel[nLevel] - 1);
			nPosIndex = nRID % (FRIEND_ARR - 1) + 2;
			fDist = (nRID / (FRIEND_ARR - 1)) * 5;
			fScale = 0.67f;
		}

		CCPoint avatarPos = ccp(LEVEL_POS[nLevel * LVL_POS_ROWS + nPosIndex * 2], LEVEL_POS[nLevel * LVL_POS_ROWS + nPosIndex * 2 + 1]);

		float alpha = getAlpha(avatarPos, levelPos);
		CCPoint diff = ccp(fDist * cos(alpha), fDist * sin(alpha));

		avatarPos = avatarPos + diff;

		CCLog("refreshFriendInfoDelay avatarPos: %f, %f", avatarPos.x, avatarPos.y);

		frLastIndexPerLevel[nLevel]++;

        UIPanel* avatarPan = (UIPanel*) m_friendLayer->getWidgetByName(tinfo.fbid.c_str());
		if (avatarPan == NULL)
        {
			avatarPan = UIPanel::create();
			avatarPan->setSize(CCSize(128, 128));
			avatarPan->setName(tinfo.fbid.c_str());

			UIImageView* imgView = UIImageView::create();
			if (tinfo.bLocalFriend)
				imgView->loadTexture("Image/level/balloon_orange.png");
			else
				imgView->loadTexture("Image/level/balloon_blue.png");
			imgView->setTag(220);
			avatarPan->addChild(imgView);
            
            CCSprite* stencil = CCSprite::create("Image/level/r128.png");
            CCClippingNode* clipNode = CCClippingNode::create(stencil);
            clipNode->setAlphaThreshold(0.05f);
			//clipNode->setInverted(true);
			clipNode->setAnchorPoint(ccp(0.5f, 0.5f));
			clipNode->setScale(0.58f);
			clipNode->setTag(200);
            
            CCSprite* spfriend = CCSprite::create();
            spfriend->setTag(201);
            clipNode->addChild(spfriend);
            
            avatarPan->addCCNode(clipNode);
            
            m_friendLayer->addWidget(avatarPan);
        }

        avatarPan->setPosition(avatarPos);
		avatarPan->setScale(fScale);

		UIImageView* imgView = (UIImageView*)avatarPan->getChildByTag(220);
		imgView->setRotation(-alpha * 180 / M_PI);

        CCNode* clipper = avatarPan->getRenderer()->getChildByTag(200);
        CCAssert(clipper, "clipper is null");
        CCSprite* spfriend = (CCSprite*) clipper->getChildByTag(201);
        CCAssert(spfriend, "spfriend is null");

		
		//MemoryStruct mem;
		//LCommon::sharedInstance()->getDataFromURL("http://192.168.2.117:3000/test.png", mem);
		//CCImage* img = new CCImage();
		//img->initWithImageData(mem.memory, mem.size, CCImage::kFmtPng);
		//
		//CCTexture2D* tex1 = CCTextureCache::sharedTextureCache()->addUIImage(img, "fbid");

		//CCLog("initWithImageData: %d:%c,%c,%c,%c", mem.size, mem.memory[0], mem.memory[1], mem.memory[2], mem.memory[3]);

		//CCTexture2D* tex = CCTextureCache::sharedTextureCache()->textureForKey("fbid");


		//Get texture by CCTextureCache::sharedTextureCache() or imgbuff
        CCTexture2D* tex = CCTextureCache::sharedTextureCache()->textureForKey(tinfo.fbid.c_str());
        if (tex == NULL)
            tex = CCTextureCache::sharedTextureCache()->addImage("Image/friends/def_profile.png");

		TFriendPhotoMap::iterator it1 = LCommon::sharedInstance()->friendPhotoList.find(tinfo.fbid.c_str());
		if (it1 != LCommon::sharedInstance()->friendPhotoList.end())
		{
			if(it1->second->memory)
			{
				CCLog("refreshFriendInfoDelay: %d:%c,%c,%c,%c", it1->second->size, it1->second->memory[0], it1->second->memory[1], it1->second->memory[2], it1->second->memory[3]);

				CCImage* img = new CCImage();
				img->initWithImageData(it1->second->memory, it1->second->size, CCImage::kFmtPng);
				tex->initWithImage(img);
			}
		}


        spfriend->setTexture(tex);
        spfriend->setTextureRect(rc);
    }


	if(m_bGivePopupShowing)
	{
		for (int i = 0; i < LCommon::sharedInstance()->liveReqList.size(); i++)
		{
			TReqInfo& info = LCommon::sharedInstance()->liveReqList.at(i);
			UIImageView* bg = (UIImageView*) lifeGiveScrollView->getChildByName(info.reqId.c_str());
			if(bg)
			{
				CCSprite* spBack = (CCSprite*) bg->getRenderer()->getChildByTag(100);
				CCNode* clipper = spBack->getChildByTag(200);
				CCSprite* spfriend = (CCSprite*) clipper->getChildByTag(201);
				

				//Get texture by CCTextureCache::sharedTextureCache() or imgbuff
				CCTexture2D* tex = CCTextureCache::sharedTextureCache()->textureForKey(info.senderFBId.c_str());
				if (tex == NULL)
					tex = CCTextureCache::sharedTextureCache()->addImage("Image/friends/def_profile.png");

				TFriendPhotoMap::iterator it1 = LCommon::sharedInstance()->friendPhotoList.find(info.senderFBId.c_str());
				if (it1 != LCommon::sharedInstance()->friendPhotoList.end())
				{
					if(it1->second->memory)
					{
						CCLog("refreshFriendInfoDelay: %d:%c,%c,%c,%c", it1->second->size, it1->second->memory[0], it1->second->memory[1], it1->second->memory[2], it1->second->memory[3]);

						CCImage* img = new CCImage();
						img->initWithImageData(it1->second->memory, it1->second->size, CCImage::kFmtPng);
						tex->initWithImage(img);
					}
				}
				

				spfriend->setTexture(tex);
				spfriend->setTextureRect(rc);
			}
		}
	}

	if(m_bCollectPopupShowing)
	{
		for (int i = 0; i < LCommon::sharedInstance()->collectList.size(); i++)
		{
			TReqInfo& info = LCommon::sharedInstance()->collectList.at(i);
			UIImageView* bg = (UIImageView*) lifeCollectScrollView->getChildByName(info.reqId.c_str());
			if(bg)
			{
				CCSprite* spBack = (CCSprite*) bg->getRenderer()->getChildByTag(100);
				CCNode* clipper = spBack->getChildByTag(200);
				CCSprite* spfriend = (CCSprite*) clipper->getChildByTag(201);


				//Get texture by CCTextureCache::sharedTextureCache() or imgbuff
				CCTexture2D* tex = CCTextureCache::sharedTextureCache()->textureForKey(info.senderFBId.c_str());
				if (tex == NULL)
					tex = CCTextureCache::sharedTextureCache()->addImage("Image/friends/def_profile.png");

				TFriendPhotoMap::iterator it1 = LCommon::sharedInstance()->friendPhotoList.find(info.senderFBId.c_str());
				if (it1 != LCommon::sharedInstance()->friendPhotoList.end())
				{
					if(it1->second->memory)
					{
						CCLog("refreshFriendInfoDelay: %d:%c,%c,%c,%c", it1->second->size, it1->second->memory[0], it1->second->memory[1], it1->second->memory[2], it1->second->memory[3]);

						CCImage* img = new CCImage();
						img->initWithImageData(it1->second->memory, it1->second->size, CCImage::kFmtPng);
						tex->initWithImage(img);
					}
				}


				spfriend->setTexture(tex);
				spfriend->setTextureRect(rc);
			}
		}
	}
}

void CLevelSelect::refreshGiftingPopup(float dt)
{
	AppDelegate::app->checkIncomingRequests();
}

void CLevelSelect::receivedLiveReqList(float dt)
{
	if(!m_bGivePopupShowing)
		showReqPopup(false);
}

void CLevelSelect::receivedCollectLiveList(float dt)
{
	if(!m_bCollectPopupShowing)
		showReqPopup(true);
}

void CLevelSelect::showReqPopup(bool bCollect)
{
    TReqList* reqList;

    if (bCollect)
        reqList = &(LCommon::sharedInstance()->collectList);
    else
        reqList = &(LCommon::sharedInstance()->liveReqList);
    
    int nCnt = reqList->size();
    
    if (nCnt == 0) return;
    
	UIWidget* pWidget = dynamic_cast<UIWidget*>(GUIReader::shareReader()->widgetFromJsonFile("Image/reqlive_dlg.json"));

	UIWidget* pBGImage = pWidget->getChildByName("imageBG");
	pBGImage->setScale(SC_DLG_SCALE);
	pBGImage->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
    
    UIImageView* imgTitle = (UIImageView*) pWidget->getChildByName("imgTitle");
    if (bCollect)
        imgTitle->loadTexture("Image/friends/title_collectlives.png");
    
    UILabel* lblCount = (UILabel*) pWidget->getChildByName("lblReqCount");
    lblCount->setText(toString(nCnt).c_str());

	UIScrollView* lifeGiftScrollView;
	if (bCollect)
	{
		lifeCollectScrollView = (UIScrollView*) pWidget->getChildByName("listView");
		lifeGiftScrollView = lifeCollectScrollView;
	}
	else
	{
		lifeGiveScrollView = (UIScrollView*) pWidget->getChildByName("listView");
		lifeGiftScrollView = lifeGiveScrollView;
	}

    CCSize contentSize = lifeGiftScrollView->getInnerContainerSize();
    
    float interval = 150;
    
    contentSize.height = interval * nCnt;
    lifeGiftScrollView->setInnerContainerSize(contentSize);
    float cx = contentSize.width / 2;
    float cy = lifeGiftScrollView->getContentSize().height - interval / 2;
    for (int i = 0; i < nCnt; i++)
    {
        TReqInfo& info = reqList->at(i);
        
        UIImageView* bg = UIImageView::create();
        bg->loadTexture("Image/popup/popup_cell.png");
        bg->setPosition(ccp(cx, cy));
        bg->setName(info.reqId.c_str());
        lifeGiftScrollView->addChild(bg);
        

		CCLabelTTF* lblName = CCLabelTTF::create(info.senderName.c_str(), "Arial", 30, CCSizeMake(210, 50), kCCTextAlignmentCenter);
		lblName->enableStroke(ccc3(157, 57, 1), 2);
		lblName->enableShadow(CCSizeMake(5, -5), 0.2, 0.1);
		lblName->setPosition(ccp(-17, -10));
		bg->addCCNode(lblName);


		UIButton* btn = UIButton::create();
		const char* strNormal = (bCollect)? "Image/friends/btn_collect_n.png" : "Image/friends/btn_give_n.png";
		const char* strPressed = (bCollect)? "Image/friends/btn_collect_p.png" : "Image/friends/btn_give_p.png";
		btn->loadTextures(strNormal, strPressed, "");
		btn->setPosition(ccp(168, 0));
		btn->setTouchEnable(true);
		btn->setTag(i);
		btn->setName("giftbtn");
		if (bCollect)
			btn->addReleaseEvent(this, coco_releaseselector(CLevelSelect::onSelectedCollect));
		else
			btn->addReleaseEvent(this, coco_releaseselector(CLevelSelect::onSelectedGive));
		bg->addChild(btn);


        CCSprite* spBack = CCSprite::create("Image/friends/friend_br_back.png");
        spBack->setPosition(ccp(-180, 0));
		spBack->setTag(100);
        bg->addCCNode(spBack);
        
		CCTexture2D* texture = CCTextureCache::sharedTextureCache()->textureForKey(info.senderFBId.c_str());
        //CCTexture2D* texture = CCTextureCache::sharedTextureCache()->textureForKey("100008211465102");
        if (texture == NULL)
            texture = CCTextureCache::sharedTextureCache()->addImage("Image/friends/def_profile.png");
        CCSprite* spfriend = CCSprite::createWithTexture(texture);

        spfriend->setTag(201);

        CCPoint posCenter = ccp(spBack->getContentSize().width / 2, spBack->getContentSize().height / 2);
        CCSprite* stencil = CCSprite::create("Image/level/r128.png");
        CCClippingNode* clipNode = CCClippingNode::create(stencil);
		clipNode->setAlphaThreshold(0.05f);
        clipNode->setAnchorPoint(ccp(0.5f, 0.5f));
        clipNode->setScale(0.7f);
        clipNode->setTag(200);
        clipNode->addChild(spfriend);
        clipNode->setPosition(posCenter);
        
        spBack->addChild(clipNode);

        CCSprite* spFrame = CCSprite::create("Image/friends/friend_frame.png");
        spFrame->setPosition(posCenter);
        spBack->addChild(spFrame);
        

        cy -= interval;
    }
    
	showGiftingPopup(pWidget, "btnClose", bCollect);
}
//void CLevelSelect::showReqPopup(bool bCollect)
//{
//    TReqList* reqList;
//    if (bCollect)
//        reqList = &(LCommon::sharedInstance()->collectList);
//    else
//        reqList = &(LCommon::sharedInstance()->liveReqList);
//    
//    int nCnt = reqList->size();
//    
//    if (nCnt == 0) return;
//    
//	UIWidget* pWidget = dynamic_cast<UIWidget*>(GUIReader::shareReader()->widgetFromJsonFile("Image/reqlive_dlg.json"));
//
//	UIWidget* pBGImage = pWidget->getChildByName("imageBG");
//	pBGImage->setScale(SC_DLG_SCALE);
//	pBGImage->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
//    
//    UIImageView* imgTitle = (UIImageView*) pWidget->getChildByName("imgTitle");
//    if (bCollect)
//        imgTitle->loadTexture("Image/friends/title_collectlives.png");
//    
//    UILabel* lblCount = (UILabel*) pWidget->getChildByName("lblReqCount");
//    lblCount->setText(toString(nCnt).c_str());
//
//    UIScrollView* listView = (UIScrollView*) pWidget->getChildByName("listView");
//    CCSize contentSize = listView->getInnerContainerSize();
//    
//    float interval = 150;
//    
//    contentSize.height = interval * nCnt;
//    listView->setInnerContainerSize(contentSize);
//    float cx = contentSize.width / 2;
//    float cy = listView->getContentSize().height - interval / 2;
//    for (int i = 0; i < nCnt; i++)
//    {
//        TReqInfo& info = reqList->at(i);
//        
//        UIImageView* bg = UIImageView::create();
//        bg->loadTexture("Image/popup/popup_cell.png");
//        bg->setPosition(ccp(cx, cy));
//        bg->setName(info.senderFBId.c_str());
//        listView->addChild(bg);
//        
//
//		CCLabelTTF* lblName = CCLabelTTF::create(info.senderName.c_str(), "Arial", 30, CCSizeMake(210, 50), kCCTextAlignmentCenter);
//        lblName->enableStroke(ccc3(157, 57, 1), 2);
//        lblName->enableShadow(CCSizeMake(5, -5), 0.2, 0.1);
//        lblName->setPosition(ccp(-17, -10));
//        bg->addCCNode(lblName);
//        
//
//        UIButton* btn = UIButton::create();
//        const char* strNormal = (bCollect)? "Image/friends/btn_collect_n.png" : "Image/friends/btn_give_n.png";
//        const char* strPressed = (bCollect)? "Image/friends/btn_collect_p.png" : "Image/friends/btn_give_p.png";
//		btn->loadTextures(strNormal, strPressed, "");
//        btn->setPosition(ccp(168, 0));
//        btn->setTouchEnable(true);
//        btn->setTag(i);
//        if (bCollect)
//            btn->addReleaseEvent(this, coco_releaseselector(CLevelSelect::onSelectedCollect));
//        else
//            btn->addReleaseEvent(this, coco_releaseselector(CLevelSelect::onSelectedGive));
//        bg->addChild(btn);
//
//
//		UIPanel* avatarPan = UIPanel::create();
//		avatarPan->setSize(CCSize(128, 128));
//		avatarPan->setPosition(ccp(-180, 0));
//		avatarPan->setScale(0.9f);
//        bg->addChild(avatarPan);
//
//		UIImageView* imgView = UIImageView::create();
//		imgView->loadTexture("Image/friends/friend_br_back.png");
//		avatarPan->addChild(imgView);
//        
//		CCRect rc = CCRectMake(0, 0, 128, 128);
//
//        //CCTexture2D* texture = CCTextureCache::sharedTextureCache()->textureForKey(info.senderFBId.c_str());
//        //if (texture == NULL)
//          CCTexture2D* texture = CCTextureCache::sharedTextureCache()->addImage("Image/friends/def_profile.png");
//		
//		CCSprite* spfriend = CCSprite::create();
//		spfriend->setTexture(texture);
//        spfriend->setTextureRect(rc);
//        spfriend->setTag(201);
//
//        CCSprite* stencil = CCSprite::create("Image/level/r128.png");
//        CCClippingNode* clipNode = CCClippingNode::create(stencil);
//        clipNode->setAlphaThreshold(0.05f);
//        clipNode->setAnchorPoint(ccp(0.5f, 0.5f));
//        clipNode->setScale(0.7f);
//        clipNode->setTag(200);
//        clipNode->addChild(spfriend);
//        clipNode->setPosition(ccp(64, 64));
//
//		avatarPan->addCCNode(clipNode);
//
//  //      CCSprite* spFrame = CCSprite::create("Image/friends/friend_frame.png");
//  //      avatarPan->addCCNode(spFrame);
//
//
//        cy -= interval;
//    }
//    
//	showPopup(pWidget, "btnClose");
//}

void CLevelSelect::onSelectedGive(cocos2d::CCObject* obj)
{
	if(LCommon::sharedInstance()->liveCount == 0)
	{
		CCMessageBox("You have no lives.\n Please purchase lives!", "Information");
		return;
	}

    UIWidget* widget = (UIWidget*) obj;
    int reqIndex = widget->getTag();
    TReqInfo info = LCommon::sharedInstance()->popLiveReqAtIndex(reqIndex);
    
    widget->disable();
    widget->setVisible(false);

	TReqList* reqList = &(LCommon::sharedInstance()->liveReqList);
	for (int i = 0; i < reqList->size(); i++)
	{
		TReqInfo& info = reqList->at(i);
		UIWidget* bg = lifeGiveScrollView->getChildByName(info.reqId.c_str());
		UIWidget* btn = bg->getChildByName("giftbtn");
		btn->setTag(i);
		btn->setName("giftbtn");
	}

    std::string strMix = info.reqId + "|" + info.senderFBId;
    AppDelegate::app->giveLive(strMix);

	AppDelegate::app->sendFuseEvent(USERS_WHO_GIFT_LIVES);
}

void CLevelSelect::onSelectedCollect(cocos2d::CCObject* obj)
{
    UIWidget* widget = (UIWidget*) obj;
    int reqIndex = widget->getTag();
    TReqInfo info = LCommon::sharedInstance()->popCollectAtIndex(reqIndex);

    widget->disable();
    widget->setVisible(false);

	TReqList* reqList = &(LCommon::sharedInstance()->collectList);
	for (int i = 0; i < reqList->size(); i++)
	{
		TReqInfo& info = reqList->at(i);
		UIWidget* bg = lifeCollectScrollView->getChildByName(info.reqId.c_str());
		UIWidget* btn = bg->getChildByName("giftbtn");
		btn->setTag(i);
		btn->setName("giftbtn");
	}

    AppDelegate::app->collectLive(info.reqId);
}

void CLevelSelect::giveLive(std::string reqId)
{
	LCommon::sharedInstance()->addLiveCount(-1);
	LCommon::sharedInstance()->saveState();
	refreshInfo();

	//CCMessageBox("The live is successfully given.", "Information");
}

void CLevelSelect::collectLive(std::string reqId)
{
	LCommon::sharedInstance()->addLiveCount(1);
	LCommon::sharedInstance()->saveState();
	refreshInfo();

	CCMessageBox("The live is successfully collected.", "Information");
}

void CLevelSelect::onSelectAnimalPowerup(cocos2d::CCObject* obj)
{
	UIButton* node = dynamic_cast<UIButton*> (obj);
	
	showBuyPowerup(node->getTag());
}

void CLevelSelect::createRightFriendInfo() {
	UIPanel* selPane = (UIPanel*) m_pWidget->getChildByName("selPane");

	tableView = CCTableView::create(this, CCSizeMake(200, SC_HEIGHT - m_nActionBarHeight));
	tableView->setDirection(kCCScrollViewDirectionVertical);
	tableView->setPosition(ccp(SC_WIDTH - 202, 0));
	tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	selPane->addCCNode(tableView);
	tableView->setZOrder(10);
}

CCSize CLevelSelect::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
	return CCSizeMake(256, 220);
}

CCTableViewCell* CLevelSelect::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	bool isLocalFriend = true;
	CCTableViewCell *cell = table->dequeueCell();
	if (!cell) {
		cell = new CCTableViewCell();
		cell->autorelease();

		float cx = 100;
		float cy = 140;
		CCSprite *spBg = CCSprite::create();
		spBg->setTag(TAG_WRLD_PIC_BG);
		spBg->setPosition(ccp(cx, cy));
		cell->addChild(spBg);

        CCSprite* stencil = CCSprite::create("Image/level/r128.png");
        CCClippingNode* clipNode = CCClippingNode::create(stencil);
        clipNode->setAlphaThreshold(0.05f);
        clipNode->setAnchorPoint(ccp(0.5f, 0.5f));
        clipNode->setScale(0.78f);
        clipNode->setTag(TAG_WRLD_PICTURE_CLIP);
        clipNode->setPosition(ccp(cx, cy));
        
		CCSprite* spChara = CCSprite::create();
		spChara->setPosition(ccp(0, 14));
		spChara->setTag(TAG_WRLD_PICTURE);
		clipNode->addChild(spChara);
        
        cell->addChild(clipNode);

		CCLabelTTF *label = CCLabelTTF::create("Name", "Helvetica", 30);
		label->setPosition(ccp(cx, cy - 80));
		label->setTag(TAG_WRLD_NAME);
		cell->addChild(label);

		label = CCLabelTTF::create("", "Helvetica", 30);
		label->setPosition(ccp(cx, cy - 110));
		label->setTag(TAG_WRLD_SCORE);
		cell->addChild(label);

		CCSprite* spNo = CCSprite::create();
		spNo->setPosition(ccp(cx, cy - 45));
		spNo->setTag(TAG_WRLD_FRNOBG);
		cell->addChild(spNo);

		CCLabelBMFont* lblNo = CCLabelBMFont::create("5", "Image/fonts/Hobostd110.fnt");
		lblNo->setPosition(ccp(cx + 2, cy - 43));
		lblNo->setTag(TAG_WRLD_FRNO);
		lblNo->setScale(0.5f);
		cell->addChild(lblNo);
	}

	ccColor3B col = (idx == 0)? ccc3(255, 132, 0) : ccc3(22, 156, 196);

	CCSprite *sprt = (CCSprite*) cell->getChildByTag(TAG_WRLD_PIC_BG);
	CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addImage((idx == 0)? "Image/friends/friend_bg_0.png": "Image/friends/friend_bg_1.png");
	sprt->setTexture(tex);
	sprt->setTextureRect(CCRectMake(0, 0, tex->getContentSize().width, tex->getContentSize().height));

    TFriendInfo* info = rankList.at(idx);

	CCClippingNode* clipNode = (CCClippingNode*) cell->getChildByTag(TAG_WRLD_PICTURE_CLIP);
    
	sprt = (CCSprite*) clipNode->getChildByTag(TAG_WRLD_PICTURE);
    
    tex = CCTextureCache::sharedTextureCache()->textureForKey(info->fbid.c_str());
    if (tex == NULL)
    {
        tex = CCTextureCache::sharedTextureCache()->addImage("Image/friends/def_profile.png");
    }
    
	sprt->setTexture(tex);
	sprt->setTextureRect(CCRectMake(0, 0, tex->getContentSize().width, tex->getContentSize().height));

	sprt = (CCSprite*) cell->getChildByTag(TAG_WRLD_FRNOBG);
	tex = CCTextureCache::sharedTextureCache()->addImage((idx == 0)? "Image/friends/friend_num_0.png": "Image/friends/friend_num_1.png");
	sprt->setTexture(tex);
	sprt->setTextureRect(CCRectMake(0, 0, tex->getContentSize().width, tex->getContentSize().height));

	CCLabelBMFont* lblNo = (CCLabelBMFont*) cell->getChildByTag(TAG_WRLD_FRNO);
	if (idx == 0)
		lblNo->setString("");
	else
 		lblNo->setString(toString(idx + 1).c_str());

	CCLabelTTF* label = (CCLabelTTF*) cell->getChildByTag(TAG_WRLD_NAME);
	label->setString(info->fname.c_str());
	label->setColor(col);

	label = (CCLabelTTF*) cell->getChildByTag(TAG_WRLD_SCORE);
	label->setString(getDotDigit(info->nCurScore).c_str());
	label->setColor(col);

	return cell;
}

unsigned int CLevelSelect::numberOfCellsInTableView(CCTableView *table)
{
	return MIN(MAX_RANK, rankList.size());
}

void CLevelSelect::initLocalFriends()
{
	int curChar = LCommon::sharedInstance()->curChara;
    for (int i = 0; i < MAX_LOCAL_FRIENDS; i++)
    {
        TFriendInfo info;
		info.init();
        info.fbid = CCString::createWithFormat("Image/friends/friends_%d_%d.png", 0, i)->getCString();
        info.texture = CCTextureCache::sharedTextureCache()->addImage(info.fbid.c_str());
        info.texture->retain();
        info.fname = LOCAL_FRIEND[i];
		info.nMaxLevel = LCommon::sharedInstance()->localFriendMaxLevels[i][curChar];
        info.bLocalFriend = true;
        localFriendList.push_back(TFriendInfo(info));
    }

	TFriendMap::iterator it = LCommon::sharedInstance()->friendList.find(LCommon::sharedInstance()->myFBID);
	if (it != LCommon::sharedInstance()->friendList.end())
	{
		it->second->nMaxLevel = LCommon::sharedInstance()->maxLevel();
	}
}

void CLevelSelect::initRankList(int nLevel)
{
    rankList.clear();
    
    int mixList[MAX_LOCAL_FRIENDS];
    
    for (int i = 0; i < MAX_LOCAL_FRIENDS; i++)
        mixList[i] = i;
    
    for (int i = 0; i < MAX_LOCAL_FRIENDS; i++)
    {
        int r1 = getRandRange(1, MAX_LOCAL_FRIENDS - 1);
        int dst = (i + r1) % MAX_LOCAL_FRIENDS;
        int tmp = mixList[i];
        mixList[i] = mixList[dst];
        mixList[dst] = tmp;
    }
    
	int nMaxScore = 55000;
    for (int i = 0; i < MAX_LOCAL_FRIENDS; i++)
    {
        TFriendInfo& info = localFriendList.at(mixList[i]);
		if (nLevel <= info.nMaxLevel)
		{
			info.nCurScore = nMaxScore;
			rankList.push_back(&info);
			nMaxScore -= 1000;
		}
    }
    
    int ixx = 0;
    for (TFriendMap::iterator it = LCommon::sharedInstance()->friendList.begin(); it != LCommon::sharedInstance()->friendList.end(); it++)
    {
        TFriendInfo* frndInfo = it->second;
        
        if (frndInfo->nMaxLevel < nLevel) continue;
        if (LCommon::sharedInstance()->friendList.size() > 1 && frndInfo->fbid == MYFBID_DEFAULT) continue;
        if (frndInfo->fbid == LCommon::sharedInstance()->myFBID)
            frndInfo->nCurScore = LCommon::sharedInstance()->highScore[nLevel];
        else
            frndInfo->nCurScore = 55000 - ixx * 300;
        
        rankList.push_back(frndInfo);
    }

	std::sort(rankList.begin(), rankList.end(), compScore);

    tableView->reloadData();

}

void CLevelSelect::updateRankList(int nLevel)
{
    
}

void CLevelSelect::onTimer()
{
	UIWidget* pLiveWidget = m_pActionBar->getChildByName("panLiveCount");

	if (LCommon::sharedInstance()->liveCount < MAX_LIVE)
	{
		UILabel* lblLiveCount = (UILabel*) pLiveWidget->getChildByName("lblLiveCount");
		pLiveWidget->setVisible(true);

		lblLiveCount->setText(getTimeStr(LCommon::sharedInstance()->freeLiveSecCount));
	}
	else
	{
		pLiveWidget->setVisible(false);
	}
}


int LEVEL_POS[] = {
	280, 46, 186, 53, 330, 105, 357, 39, 	 //0
	260, 163, 245, 247, 184, 196, 184, 129, 	 //1
	395, 256, 388, 341, 316, 285, 337, 204, 	 //2
	535, 287, 531, 372, 485, 229, 566, 222, 	 //3
	638, 377, 549, 410, 618, 307, 690, 322, 	 //4
	617, 489, 612, 574, 689, 521, 684, 449, 	 //5
	490, 533, 484, 621, 487, 457, 424, 485, 	 //6
	334, 540, 331, 628, 402, 581, 323, 464, 	 //7
	197, 553, 197, 637, 246, 492, 176, 481, 	 //8
	95, 593, 163, 631, 109, 522, 42, 527, 	 //9
	100, 693, 179, 653, 107, 759, 37, 747, 	 //10
	196, 736, 234, 658, 166, 807, 233, 802, 	 //11
	313, 768, 306, 684, 312, 842, 380, 728, 	 //12
	433, 815, 428, 899, 502, 775, 442, 744, 	 //13
	539, 860, 632, 925, 628, 831, 569, 801, 	 //14
	555, 962, 472, 911, 459, 983, 593, 1025, 	 //15
	503, 1057, 411, 1044, 517, 1129, 573, 1092, 	 //16
	423, 1137, 393, 1048, 405, 1196, 473, 1191, 	 //17
	314, 1165, 281, 1084, 275, 1231, 342, 1232, 	 //18
	183, 1263, 188, 1354, 174, 1191, 253, 1301, 	 //19
	75, 1312, 157, 1351, 104, 1243, 40, 1242, 	 //20
	78, 1413, 158, 1375, 32, 1479, 102, 1484, 	 //21
	200, 1461, 224, 1386, 159, 1528, 227, 1533, 	 //22
	344, 1480, 345, 1393, 281, 1435, 331, 1553, 	 //23
	475, 1515, 477, 1429, 402, 1555, 461, 1588, 	 //24
	589, 1563, 599, 1473, 529, 1617, 660, 1532, 	 //25
	618, 1676, 696, 1626, 693, 1704, 627, 1749, 	 //26
	511, 1737, 447, 1676, 483, 1809, 560, 1797, 	 //27
	358, 1782, 352, 1689, 339, 1856, 404, 1844, 	 //28
	235, 1790, 261, 1871, 185, 1730, 251, 1718, 	 //29
	161, 1868, 243, 1902, 82, 1842, 131, 1791, 	 //30
	154, 1971, 242, 1971, 74, 2004, 84, 1932, 	 //31
	174, 2069, 246, 2013, 88, 2078, 132, 2136, 	 //32
	274, 2126, 284, 2038, 200, 2166, 257, 2201, 	 //33
	400, 2153, 360, 2225, 438, 2083, 369, 2083, 	 //34
	471, 2241, 381, 2241, 549, 2220, 496, 2172, 	 //35
	410, 2335, 360, 2264, 462, 2395, 495, 2328, 	 //36
	295, 2353, 305, 2268, 288, 2426, 356, 2407, 	 //37
	188, 2380, 217, 2462, 133, 2324, 211, 2308, 	 //38
	125, 2475, 206, 2496, 42, 2468, 85, 2408, 	 //39
	156, 2579, 233, 2534, 78, 2548, 79, 2619, 	 //40
	253, 2636, 291, 2560, 170, 2658, 221, 2708, 	 //41
	373, 2679, 375, 2591, 304, 2729, 367, 2753, 	 //42
	495, 2698, 472, 2784, 453, 2632, 524, 2630, 	 //43
	625, 2730, 549, 2785, 608, 2656, 675, 2676, 	 //44
	638, 2834, 548, 2830, 711, 2864, 702, 2789, 	 //45
	572, 2920, 496, 2877, 615, 2980, 655, 2921, 	 //46
	493, 2986, 437, 2919, 410, 3003, 562, 3033, 	 //47
	420, 3078, 338, 3020, 481, 3117, 482, 3048, 	 //48
	362, 3167, 267, 3151, 326, 3097, 441, 3175, 	 //49
	340, 3265, 261, 3191, 410, 3258, 379, 3318, 	 //50
	282, 3355, 189, 3307, 284, 3401, 348, 3379, 	 //51
	190, 3405, 177, 3305, 242, 3452, 118, 3371, 	 //52
	140, 3500, 220, 3520, 60, 3499, 65, 3428, 	 //53
	195, 3595, 236, 3505, 131, 3618, 74, 3578, 	 //54
	285, 3650, 282, 3565, 200, 3692, 266, 3723, 	 //55
	385, 3677, 388, 3766, 346, 3616, 413, 3608, 	 //56
	500, 3669, 469, 3753, 485, 3594, 550, 3616, 	 //57
	604, 3695, 532, 3757, 681, 3675, 632, 3626, 	 //58
	643, 3792, 539, 3765, 684, 3833, 711, 3765, 	 //59
	575, 3875, 517, 3807, 583, 3945, 648, 3910, 	 //60
	470, 3905, 440, 3829, 448, 3983, 515, 3970, 	 //61
	365, 3930, 378, 4021, 289, 3887, 355, 3862, 	 //62
	283, 4002, 360, 4044, 200, 4002, 234, 3942, 	 //63
	263, 4097, 342, 4052, 186, 4126, 193, 4056, 	 //64
	315, 4185, 352, 4088, 225, 4178, 384, 4152, 	 //65
	405, 4240, 447, 4265, 284, 4230, 444, 4182, 	 //66
	365, 4330, 489, 4268, 330, 4272, 463, 4344, 	 //67
	280, 4385, 360, 4430, 231, 4358, 263, 4298, 	 //68
	210, 4460, 309, 4405, 144, 4455, 139, 4387, 	 //69
	300, 4510, 342, 4424, 200, 4523, 251, 4566, 	 //70
	420, 4530, 419, 4446, 324, 4575, 384, 4590, 	 //71
	530, 4560, 457, 4452, 452, 4609, 527, 4475, 	 //72
	620, 4615, 526, 4658, 635, 4544, 580, 4509, 	 //73
	630, 4720, 530, 4659, 698, 4661, 678, 4596, 	 //74
	550, 4790, 542, 4677, 666, 4791, 699, 4730, 	 //75
	460, 4840, 491, 4729, 518, 4872, 594, 4852, 	 //76
	355, 4875, 424, 4928, 348, 4790, 412, 4772, 	 //77
	267, 4935, 355, 4937, 191, 4894, 255, 4852, 	 //78
	330, 5025, 393, 4984, 267, 5080, 224, 50007, 	 //79
	426, 5084, 333, 5109, 505, 5068, 455, 5014, 	 //80
	475, 5181, 397, 5129, 521, 5243, 553, 5172, 	 //81
	392, 5257, 358, 5176, 408, 5329, 467, 5290, 	 //82
	310, 5325, 318, 5403, 247, 5272, 305, 5239, 	 //83
	218, 5388, 283, 5449, 141, 5368, 188, 5312, 	 //84
	123, 5447, 216, 5466, 54, 5501, 43, 5422, 	 //85
	170, 5541, 234, 5481, 92, 5568, 152, 5615, 	 //86
	275, 5625, 289, 5541, 217, 5680, 281, 5707, 	 //87
	379, 5665, 381, 5753, 357, 5591, 424, 5597, 	 //88
	493, 5671, 472, 5752, 496, 5598, 561, 5629, 	 //89
	605, 5697, 543, 5753, 702, 5680, 651, 5634, 	 //90
	645, 5795, 556, 5763, 709, 5843, 723, 5777, 	 //91
	555, 5875, 536, 5785, 587, 5940, 644, 5892, 	 //92
	444, 5903, 448, 5815, 436, 5978, 502, 5957, 	 //93
	336, 5940, 351, 6027, 271, 5892, 343, 5874, 	 //94
	254, 6005, 339, 6037, 171, 6021, 199, 5949, 	 //95
	268, 6127, 278, 6215, 186, 6099, 198, 6176, 	 //96
	383, 6192, 312, 6256, 376, 6117, 444, 6136, 	 //97
	490, 6222, 422, 6284, 512, 6154, 570, 6182, 	 //98
	560, 6314, 458, 6307, 627, 6355, 634, 6281, 	 //99
	457, 6412, 418, 6335, 470, 6486, 535, 6438, 	 //100
	339, 6447, 324, 6357, 338, 6523, 400, 6498, 	 //101
	208, 6459, 246, 6539, 149, 6402, 218, 6384, 	 //102
	149, 6554, 242, 6563, 65, 6572, 91, 6500, 	 //103
	180, 6671, 245, 6611, 118, 6727, 97, 6654, 	 //104
	285, 6744, 328, 6668, 210, 6785, 270, 6819, 	 //105
	431, 6779, 436, 6691, 364, 6836, 423, 6856, 	 //106
	567, 6805, 519, 6883, 518, 6736, 587, 6730, 	 //107
	664, 6872, 567, 6903, 722, 6819, 658, 6796, 	 //108
	628, 6982, 501, 6940, 690, 7029, 711, 6965, 	 //109
	542, 7056, 480, 6978, 575, 7122, 626, 7072, 	 //110
	448, 7124, 511, 7192, 368, 7103, 420, 7052, 	 //111
	375, 7225, 297, 7174, 396, 7297, 451, 7251, 	 //112
	267, 7315, 328, 7385, 184, 7318, 223, 7251, 	 //113
	183, 7406, 266, 7449, 104, 7436, 118, 7359, 	 //114
	152, 7526, 237, 7507, 68, 7519, 101, 7591, 	 //115
	230, 7636, 261, 7553, 150, 7666, 209, 7708, 	 //116
	360, 7674, 382, 7588, 305, 7737, 372, 7750, 	 //117
	484, 7696, 489, 7610, 507, 7771, 435, 7764, 	 //118
	602, 7751, 563, 7834, 592, 7679, 659, 7696, 	 //119
	649, 7869, 556, 7873, 709, 7918, 688, 7804, 	 //120
	594, 7974, 525, 7918, 624, 8043, 672, 7986, 	 //121
	522, 8074, 578, 8143, 437, 8063, 479, 8004, 	 //122
	409, 8146, 349, 8074, 457, 8209, 484, 8143, 	 //123
	283, 8186, 281, 8099, 302, 8258, 357, 8216, 	 //124
	153, 8224, 62, 8184, 138, 8148, 203, 8168, 	 //125
	138, 8332, 216, 8286, 212, 8358, 77, 8273, 	 //126
	190, 8426, 278, 8446, 157, 8495, 107, 8440, 	 //127
};


int cocosAnimCounts = 5;

ANIMATION_INFO cocosAnimInfo[] = {
	//W1-9
	{"W1", 0, 512, 1.0f, 1.0f, },
	{"W2", 0, 1590, 1.0f, 1.0f, },
	{"W3", 0, 2560, 1.0f, 1.0f, },
	{"W4", 0, 3584, 1.0f, 1.0f, },
	{"W5", 0, 4580, 1.0f, 1.0f, },
};


int particleCounts = 42;

ANIMATION_INFO particleInfo[] = {
	//W1-9
	{"Image/level/particles/w1/River flow 1.plist", 90, 970, 1.0f, 1.0f, },
	{"Image/level/particles/w1/River flow 1b.plist", 100, 960, 1.0f, 1.0f, },

	{"Image/level/particles/w1/River flow 2.plist", 510, 780, 1.0f, 1.0f, },
	{"Image/level/particles/w1/River flow 2b.plist", 520, 770, 1.0f, 1.0f, },

	{"Image/level/particles/w1/Lake flow1.plist", 140, 420, 1.0f, 1.0f, },
	{"Image/level/particles/w1/Lake flow1.plist", 170, 440, 1.0f, 1.0f, },

	{"Image/level/particles/w1/Riverglow.plist", 300, 400, 1.0f, 1.0f, },
	{"Image/level/particles/w1/river blink.plist", 300, 380, 1.0f, 1.0f, },

	{"Image/level/particles/w1/Riverglow2.plist", 180, 380, 1.0f, 1.0f, },

	//W2-7
	//{"Image/level/particles/w2/snow all.plist", 660, 2050, 1.0f, 1.0f, },
	//{"Image/level/particles/w2/snow all2.plist", 380, 2050, 1.0f, 1.0f, },
	{"Image/level/particles/w2/snow all.plist", 380, 2050, 1.0f, 1.0f, },

	{"Image/level/particles/w2/ice glow.plist", 180, 1660, 1.0f, 1.0f, },

	{"Image/level/particles/w2/spikes glow.plist", 340, 1400, 1.0f, 1.0f, },
	{"Image/level/particles/w2/spikes glow2.plist", 660, 1460, 1.0f, 1.0f, },
	{"Image/level/particles/w2/spikes glow3.plist", 500, 1390, 1.0f, 1.0f, },
	{"Image/level/particles/w2/spikes glow4.plist", 110, 1150, 1.0f, 1.0f, },

	//W3-2
	{"Image/level/particles/w3/Sandstorm.plist", -200, 2350, 1.0f, 1.0f, },
	{"Image/level/particles/w3/Sandstorm.plist", -200, 2800, 1.0f, 1.0f, },

	//W4-19
	{"Image/level/particles/w4/River smoke.plist", 200, 3790, 1.0f, 1.0f, },
	{"Image/level/particles/w4/River glow.plist", 300, 3780, 1.0f, 1.0f, },
	{"Image/level/particles/w4/River flow 1.plist", 250, 3740, 1.0f, 1.0f, },
	{"Image/level/particles/w4/River particles 2.plist", 330, 3700, 1.0f, 1.0f, },
	{"Image/level/particles/w4/River particles 3.plist", 260, 3660, 1.0f, 1.0f, },

	{"Image/level/particles/w4/volcano smoke.plist", 650, 3660, 1.0f, 1.0f, },
	{"Image/level/particles/w4/Volcano particles 1.plist", 650, 3660, 1.0f, 1.0f, },

	{"Image/level/particles/w4/Volcano particles 2.plist", 650, 3640, 1.0f, 1.0f, },
	{"Image/level/particles/w4/Volcano glowing.plist", 650, 3640, 1.0f, 1.0f, },

	{"Image/level/particles/w4/Volcano glowing.plist", 620, 3620, 1.0f, 1.0f, },
	{"Image/level/particles/w4/Volcano glowing.plist", 650, 3620, 1.0f, 1.0f, },
	{"Image/level/particles/w4/Volcano glowing.plist", 680, 3620, 1.0f, 1.0f, },

	{"Image/level/particles/w4/River particles 1.plist", 480, 3550, 1.0f, 1.0f, },
	{"Image/level/particles/w4/River flow 2.plist", 300, 3540, 1.0f, 1.0f, },

	{"Image/level/particles/w4/Lava hole particles.plist", 660, 3230, 1.0f, 1.0f, },
	{"Image/level/particles/w4/Lava hole smoke 1.plist", 670, 3180, 1.0f, 1.0f, },
	{"Image/level/particles/w4/Lava hole glowing.plist", 780, 3170, 1.0f, 1.0f, },
	{"Image/level/particles/w4/Lava hole particles.plist", 670, 3140, 1.0f, 1.0f, },
	{"Image/level/particles/w4/Lava hole glow 2.plist", 570, 3170, 1.0f, 1.0f, },

	//W5-10
	//{"Image/level/particles/w5/River candy y.plist", 780, 4980, 1.0f, 1.0f, },
	//{"Image/level/particles/w5/River candy b.plist", 900, 5100, 1.0f, 1.0f, },
	//{"Image/level/particles/w5/River candy r.plist", 960, 5080, 1.0f, 1.0f, },
	{"Image/level/particles/w5/River flow 1.plist", 760, 5010, 1.0f, 1.0f, },
	//{"Image/level/particles/w5/River candy g.plist", 840, 5050, 1.0f, 1.0f, },
	{"Image/level/particles/w5/River flow 1b.plist", 750, 5000, 1.0f, 1.0f, },

	{"Image/level/particles/w5/River flow 2.plist", 300, 4780, 1.0f, 1.0f, },
	{"Image/level/particles/w5/River flow 2b.plist", 290, 4770, 1.0f, 1.0f, },

	{"Image/level/particles/w5/lollipop blink.plist", 570, 4380, 1.0f, 1.0f, },
	{"Image/level/particles/w5/lollipop blink.plist", 740, 4270, 1.0f, 1.0f, },

};

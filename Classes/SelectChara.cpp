//
//  CSelCharaScene.m
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//

#include "SimpleAudioEngine.h"
#include "LCommon.h"
#include "AppDelegate.h"
#include "SelectChara.h"
#include "LevelSelect.h"
#include "SelectBoost.h"
#include "CMainMenuLayer.h"

USING_NS_CC;

enum 
{
	TAG_CELL_LAYER = 2000,
	TAG_PROGRESS,
	TAG_FRIEND,
	TAG_LABEL 
};


CSelCharaScene::CSelCharaScene()
{
	setSceneID(SCENE_SELCHARA);

	m_nCurChara = LCommon::sharedInstance()->curChara;

	setBGMName(kSND_BG_MAINTHEME);

	createInfo();
};

//Checked
void CSelCharaScene::createInfo()
{
	for (int i = 0; i < CHARA_COUNT; i++)
	{
		TSelCharaInfo& charaInfo = m_pCharaInfo[i];
		int nPieceCnt = LCommon::sharedInstance()->charaInfo[i].byPieceCount;
		if (i == 0 || nPieceCnt == PIECE_PACK_NUM)
			charaInfo.state = CS_UNLOCKED;
		else if (nPieceCnt >= PIECE_PACK_NUM / 2)
			charaInfo.state = CS_UNLOCKABLE;
		else
			charaInfo.state = CS_UNLOCKABLE;//CS_LOCKED;

		charaInfo.name = (charaInfo.state == CS_LOCKED)? "???" : CHARA_NAME[i];

		char chState;
		if (charaInfo.state == CS_UNLOCKED)				chState = 'n';
		else if (charaInfo.state == CS_UNLOCKABLE)		chState = 'l';
		else		chState = 'd';

		charaInfo.imgName = CCString::createWithFormat("Image/chara/%s_%c.png", CHARA_NAME[i].c_str(), chState)->getCString();

		int nTotalStars = 0;
		for (int w = 0; w < WORLD_COUNT; w++)
		{
			int stars = LCommon::sharedInstance()->getStarsForWorld(i, w);
			int nLevels = getLevelsPerWorld(w);
			charaInfo.starRate[w] = stars * 100 / (3 * nLevels);

			nTotalStars += stars;
		}

		charaInfo.maxLevel = LCommon::sharedInstance()->maxLevel();
		charaInfo.totalStars = nTotalStars;

	}
}

//Checked
void CSelCharaScene::initUI()
{
	UIWidget* pWidget = dynamic_cast<UIWidget*>(GUIReader::shareReader()->widgetFromJsonFile("Image/selchara.json"));

	m_pMainLayer->addWidget(pWidget);

	m_pMainPanel = pWidget->getChildByName("MainPanel");
	if (SC_HEIGHT / SC_WIDTH > 1.5f)
	{
		m_pMainPanel->setPosition(ccp(m_pMainPanel->getPosition().x - 38, m_pMainPanel->getPosition().y + 30));
		m_pMainPanel->setScale(1.1f);
	}

	UIWidget* imgView = pWidget->getChildByName("imageBG");
	imgView->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
	imgView->setScale(SC_BG_SCALE);

	UIWidget* imgTitle = pWidget->getChildByName("imgTitle");
	imgTitle->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT - m_nActionBarHeight - 90));

	m_pCharaName = dynamic_cast<UILabel*>(m_pMainPanel->getChildByName("lbl_charaName"));
	m_pCharaName->setFontName("Arial");

	// add character pane
	UIPageView* pageView = UIPageView::create();
	float fPanW = SC_WIDTH_HALF * 1.1;
	pageView->setTouchEnabled(true);
    // modified by KUH in 2014-09-20
	pageView->UILayout::setClippingEnabled(false);
	pageView->setPosition(ccp(SC_WIDTH_HALF / 2, SC_HEIGHT_HALF * 0.6));
	pageView->setName("chara_pageview");
	pageView->setSize(CCSizeMake(fPanW, SC_HEIGHT_HALF));
	pageView->addEventListenerPageView(this, pagevieweventselector(CSelCharaScene::pageViewEvent));

	for (int i = 0; i < CHARA_COUNT; i++)
	{
		UIPanel* chLayer = UIPanel::create();
		chLayer->setSize(CCSizeMake(SC_WIDTH, SC_HEIGHT_HALF));

		UIImageView* img = UIImageView::create();
		img->setName("imgview");
		img->loadTexture(m_pCharaInfo[i].imgName.c_str());
		img->setScale(SC_RATIO * 0.75f);

		chLayer->addChild(img);
		img->setPosition(ccp(fPanW / 2, SC_HEIGHT_HALF / 2));
		pageView->addPage(chLayer);
	}

	m_pMainLayer->addWidget(pageView);

	pageView->scrollToPage(m_nCurChara);

	createActionBar();

	UIWidget* pLiveWidget = m_pActionBar->getChildByName("panLiveCount");
	pLiveWidget->setVisible(false);

	// add event
	UIWidget* btnHome = m_pMainPanel->getChildByName("btnHome");
	btnHome->addReleaseEvent(this, coco_releaseselector(CSelCharaScene::onHome));
	btnHome->setVisible(false);

	m_btnDone = m_pMainPanel->getChildByName("btnDone");
	m_btnDone->addReleaseEvent(this, coco_releaseselector(CSelCharaScene::onDone));

	m_btnPuzzle = m_pMainPanel->getChildByName("btnPuzzle");
	m_btnPuzzle->addReleaseEvent(this, coco_releaseselector(CSelCharaScene::onUnlocknow));

	m_btnBuy = m_pMainPanel->getChildByName("btnBuy");
	addButtonEvent(m_btnBuy, BTN_ID_BUY_PIECE);

	UIWidget* btnFBConnect = m_pMainPanel->getChildByName("btnFacebook");
	btnFBConnect->addReleaseEvent(this, coco_releaseselector(CSelCharaScene::onFBConnect));
	btnFBConnect->setAnchorPoint(ccp(1.0f, 0.5f));
	btnFBConnect->setPosition(ccp(SC_WIDTH - 50, btnFBConnect->getPosition().y));

	UIWidget* btnUnlock = m_pMainPanel->getChildByName("btnUnlock");
	btnUnlock->addReleaseEvent(this, coco_releaseselector(CSelCharaScene::onUnlocknow));

	UIWidget* btnInfoLeft = m_pMainPanel->getChildByName("scbtn_left");
	btnInfoLeft->addReleaseEvent(this, coco_releaseselector(CSelCharaScene::onInfoLeft));

	UIWidget* btnInfoRight = m_pMainPanel->getChildByName("scbtn_right");
	btnInfoRight->addReleaseEvent(this, coco_releaseselector(CSelCharaScene::onInfoRight));

	createFriends();
    
    // added by KUH in 2014-09-20
#ifdef GLOW_ADD
    if(!LCommon::sharedInstance()->parseLevelWorld)
    {
        LCommon::sharedInstance()->parseLevelWorld = true;
        CCPoint pos = CCPointZero;
        pos = m_btnDone->getWorldPosition();
        pos.y += 10;
        showTutorial("Click here to continue game.", pos, TUTO_BIG_CHARA, false, false);
    }
    
#endif

	/*CCParticleSystemQuad* emitter = CCParticleSystemQuad::create("Image/level/particles/w2/snow all.plist");
	emitter->setPosition(ccp(320, 200));
	emitter->setScaleX(1.0f);
	emitter->setScaleY(1.0f);
	m_pMainLayer->addChild(emitter);*/

	//showAnimation("W1", 1.0f);
}
void CSelCharaScene::showAnimation(const char* strJsonName, float fScale)
{
	CCArmature *armature = CCArmature::create(strJsonName);
	armature->getAnimation()->playByIndex(0);
	armature->setPosition(ccp(SC_WIDTH_HALF, SC_DESIGN_HEIGHT / 2));
	armature->setScale(fScale);

	m_pMainLayer->setVisible(true);
	m_pMainLayer->addChild(armature);

	//cocostudio::timeline::ActionTimeline *timeline;
}

void CSelCharaScene::refreshSelectChara()
{
	createInfo();

	refreshState();

	UIPageView* pageView = (UIPageView*) m_pMainLayer->getWidgetByName("chara_pageview");
	for (int i = 0; i < CHARA_COUNT; i++)
	{
		UIPanel* chLayer = pageView->getPage(i);
		UIImageView* img = (UIImageView*) chLayer->getChildByName("imgview");
		img->loadTexture(m_pCharaInfo[i].imgName.c_str());
	}
}

//Checked
const char* CSelCharaScene::getCharImgName(int nChara, int nState)
{
	char chState;

	if (nState == CS_UNLOCKED)
		chState = 'n';
	else if (nState == CS_UNLOCKABLE)
		chState = 'l';
	else
		chState = 'd';

	return CCString::createWithFormat("Image/chara/%s_%c.png", CHARA_NAME[nChara].c_str(), chState)->getCString();
}

//Checked
const char* PAN_NAMES[] = {"panLocked", "panUnlock", "panInfo"};
void CSelCharaScene::pageViewEvent(CCObject *pSender, PageViewEventType type)
{
	switch (type)
	{
	case PAGEVIEW_EVENT_TURNING:
		{
			UIPageView* pageView = dynamic_cast<UIPageView*>(pSender);
			m_nCurChara = pageView->getPage();

			refreshState();
		}
		break;

	default:
		break;
	}
}

//Checked
void CSelCharaScene::refreshState()
{
	TSelCharaInfo& charaInfo = m_pCharaInfo[m_nCurChara];

	/*for (int i = 0; i <= CS_MAX_STATE; i++)
	{
		UIWidget* pan = m_pMainPanel->getChildByName(PAN_NAMES[i]);
		pan->setEnabled(i == charaInfo.state);
		pan->setVisible(i == charaInfo.state);
	}*/

	UIImageView* animalPowerup = (UIImageView* )m_pMainPanel->getChildByName("animal_Powerup");
	animalPowerup->loadTexture(CCString::createWithFormat("Image/boosts/%s_Boost.png", CHARA_NAME[m_nCurChara].c_str())->getCString());

	UILabelBMFont* lblPowerupTitle = dynamic_cast<UILabelBMFont*>(m_pMainPanel->getChildByName("powerup_title"));
	lblPowerupTitle->setText(special_boost_info[m_nCurChara].boostTitle.c_str());

	UILabel* lblPowerupExplan;
	lblPowerupExplan = (UILabel*) m_pMainPanel->getChildByName("powerup_explan_1");
	lblPowerupExplan->setText("");
	lblPowerupExplan = (UILabel*) m_pMainPanel->getChildByName("powerup_explan_2");
	lblPowerupExplan->setText(special_boost_info[m_nCurChara].boostDesc.c_str());
	lblPowerupExplan = (UILabel*) m_pMainPanel->getChildByName("powerup_explan_3");
	lblPowerupExplan->setText("");


	m_pCharaName->setText(charaInfo.name.c_str());

	if (charaInfo.state == CS_UNLOCKED)
	{
		//m_btnDone->active();
		m_pFriends->reloadData();

		m_btnDone->setVisible(true);
		m_btnDone->active();

		m_btnPuzzle->setVisible(false);
		m_btnPuzzle->disable();

		m_btnBuy->setVisible(false);
		m_btnBuy->disable();

		/*UIWidget* pan = m_pMainPanel->getChildByName(PAN_NAMES[CS_UNLOCKED]);
		UILabelBMFont* lblLevel = (UILabelBMFont*) pan->getChildByName("lblLevel");
		lblLevel->setText(toString(charaInfo.maxLevel + 1).c_str());
		UILabelBMFont* lblStarCount = (UILabelBMFont*) pan->getChildByName("lblStarCount");
		lblStarCount->setText(toString(charaInfo.totalStars).c_str());*/
	} else if (charaInfo.state == CS_UNLOCKABLE)
	{
		//m_btnDone->disable();

		m_btnDone->setVisible(false);
		m_btnDone->disable();

		m_btnPuzzle->setVisible(true);
		m_btnPuzzle->active();

		m_btnBuy->setVisible(true);
		m_btnBuy->active();

		UILabelBMFont* labelPuzzle = dynamic_cast<UILabelBMFont*>(m_pMainPanel->getChildByName("labelPuzzle"));
		int nCnt = LCommon::sharedInstance()->charaInfo[m_nCurChara].byPieceCount;
		labelPuzzle->setText(CCString::createWithFormat("%d/%d", nCnt, PIECE_PACK_NUM)->getCString());

		UILabelBMFont* labelBuy = dynamic_cast<UILabelBMFont*> (m_pMainPanel->getChildByName("labelBuy"));
		labelBuy->setText(CCString::createWithFormat("%d", PUZZLE_PIECE_PRICE[m_nCurChara])->getCString());

		/*UILabelBMFont* labelNeedMagic = dynamic_cast<UILabelBMFont*>(m_pMainPanel->getChildByName("labelPieces"));
		int nCnt = LCommon::sharedInstance()->charaInfo[m_nCurChara].byPieceCount;
		labelNeedMagic->setText(CCString::createWithFormat("%d/%d", nCnt, PIECE_PACK_NUM)->getCString());*/
	}
	else //LOCKED
	{
		//m_btnDone->disable();

		m_btnDone->setVisible(false);
		m_btnDone->disable();

		m_btnPuzzle->setVisible(true);
		m_btnPuzzle->active();

		m_btnBuy->setVisible(true);
		m_btnBuy->active();
	}
}

CSelCharaScene::~CSelCharaScene()
{
}

//Checked
void CSelCharaScene::onHome(cocos2d::CCObject* obj)
{
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CMainMenuLayer::scene(), ccWHITE));
}

//Checked
void CSelCharaScene::onUnlocknow(cocos2d::CCObject* obj)
{
	int nPuzzleCnt = LCommon::sharedInstance()->charaInfo[m_nCurChara].byPieceCount;
	showPuzzleInfo(m_nCurChara, nPuzzleCnt, ccp(SC_WIDTH_HALF, SC_HEIGHT / 3));
}

//Checked
void CSelCharaScene::onDone(cocos2d::CCObject* obj)
{
	LCommon::sharedInstance()->curChara = m_nCurChara;
    showIndicator();
    scheduleOnce(schedule_selector(CSelCharaScene::moveToMap), 0.1f);
}

void CSelCharaScene::onFBConnect(cocos2d::CCObject* obj)
{
	FBConnect();
}

//Checked
void CSelCharaScene::moveToMap(float dt)
{
	if(IS_ANIMAL_SELECTED)
		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, CSelBoostScene::scene(), ccWHITE));
	else
		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, CLevelSelect::scene(), ccWHITE));
    
	hideIndicator();
}

//Checked
void CSelCharaScene::createFriends()
{
	CCTableView* tableView = CCTableView::create(this, CCSizeMake(528, 140));
	tableView->setPosition(ccp(26, 5));
	tableView->setDirection(kCCScrollViewDirectionHorizontal);
	tableView->setDelegate(this);
	UIWidget* pWidget = m_pMainPanel->getChildByName("panInfo");
	pWidget->addCCNode(tableView);

	m_pFriends = tableView;
}

//Checked
void CSelCharaScene::onInfoLeft(cocos2d::CCObject* obj)
{
	CCPoint curOffset = m_pFriends->getContentOffset();
	curOffset.x += 10;
	if (curOffset.x > 0)
		curOffset.x = 0;
	m_pFriends->setContentOffset(curOffset);
}

//Checked
void CSelCharaScene::onInfoRight(cocos2d::CCObject* obj)
{
	CCPoint curOffset = m_pFriends->getContentOffset();
	curOffset.x -= 10;
	float fMinX = m_pFriends->getViewSize().width - m_pFriends->getContentSize().width;
	if (curOffset.x < fMinX)
		curOffset.x = fMinX;
	m_pFriends->setContentOffset(curOffset);
}

//Checked
void CSelCharaScene::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
	CCLOG("cell touched at index: %i", cell->getIdx());
}

//Checked
CCSize CSelCharaScene::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
	return CCSizeMake(100, 140);
}

//Checked
CCTableViewCell* CSelCharaScene::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCTableViewCell *cell = table->dequeueCell();
	float nProgW = 110;

	if (!cell) {
		cell = new CCTableViewCell();
		cell->autorelease();

		float fcx = 50;
		float fcy = 70;

		CCSprite *sprite = CCSprite::create();
		sprite->setPosition(ccp(fcx, fcy + 5));
		sprite->setTag(TAG_FRIEND);
		cell->addChild(sprite);

		CCPoint progPos = ccp(fcx, fcy - 55);
		CCSprite* progBG = CCSprite::create("Image/common/info_cell.png");
		progBG->setScaleX(0.37f);
		progBG->setScaleY(0.4f);
		progBG->setPosition(progPos);
		cell->addChild(progBG);

		float nProgH = 40;
		float nProgX = -4;

		CCDrawNode* drawNode = CCDrawNode::create();
		CCPoint rectangle[4];
		rectangle[0] = ccp(nProgX - nProgW, -nProgH / 2);
		rectangle[1] = ccp(nProgX, -nProgH / 2);
		rectangle[2] = ccp(nProgX, nProgH / 2);
		rectangle[3] = ccp(nProgX - nProgW, nProgH / 2);

		ccColor4F white = {1, 1, 1, 1};
		drawNode->drawPolygon(rectangle, 4, white, 1, white);

		CCClippingNode* clipNode = CCClippingNode::create(drawNode);
		clipNode->setPosition(ccp(progPos.x + 2, progPos.y + 1));
		clipNode->setTag(TAG_PROGRESS);
		clipNode->setScale(0.6f, 0.7f);

		CCSprite* prog = CCSprite::create("Image/chara/charainfo_prog.png");
		clipNode->addChild(prog);

		cell->addChild(clipNode);

		CCLabelTTF *label = CCLabelTTF::create(toString(idx).c_str(), "Helvetica", 14);
		label->setPosition(progPos);
		label->setColor(ccc3(87, 30, 0));
		label->setTag(TAG_LABEL);
		cell->addChild(label);
	}

	TSelCharaInfo& charaInfo = m_pCharaInfo[m_nCurChara];
	int nMaxWorld = getWorldByLevel(charaInfo.maxLevel);
	bool bUnLocked = idx <= nMaxWorld;
	int nProgress = charaInfo.starRate[idx];

	CCSprite* spFriend = (CCSprite*) cell->getChildByTag(TAG_FRIEND);
	CCTexture2D* tx;
	if (bUnLocked)
		tx = CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/chara/friend_%d.png", idx)->getCString());
	else
		tx = CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/chara/friend_lock_%d.png", idx)->getCString());

	spFriend->setTexture(tx);
	CCRect rc = CCRectZero;
	rc.size = tx->getContentSize();
	spFriend->setTextureRect(rc);

	CCLabelTTF* lblInfo = (CCLabelTTF*) cell->getChildByTag(TAG_LABEL);
	if (bUnLocked)
		lblInfo->setString(CCString::createWithFormat("%d%%", nProgress)->getCString());
	else /*if (nProgress == 100)*/
		lblInfo->setString("LOCKED");

	CCClippingNode* progNode = (CCClippingNode*)cell->getChildByTag(TAG_PROGRESS);
	float dx = -nProgW / 2 + nProgW * nProgress / 100;
	progNode->getStencil()->setPosition(ccp(dx, 0));

	return cell;
}

//Checked
unsigned int CSelCharaScene::numberOfCellsInTableView(CCTableView *table)
{
	return WORLD_COUNT;
}

//Checked
void CSelCharaScene::onEnter()
{
	CBaseLayer::onEnter();
	
	/*CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("Image/w1cocotest/MainScene.json");
	CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("Image/animation/explode/fireworks/fireworks.ExportJson");
	CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("Image/animation/w1/W1.ExportJson");*/

	//CBubble::preloadAnimates();

	initUI();

	refreshState();
}

//
//  CBubbleLayer._
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//

#include "BubbleLayer.h"
#include "SimpleAudioEngine.h"
#include "AppDelegate.h"
#include "MathUtil.h"
#include "BubbleGame.h"
#include "AimLayer.h"
#include "CCRotateAroundBy.h"
#include <algorithm>
#include <ctime>

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include <direct.h>
#include <stdio.h>
#include <stdlib.h>
#endif

#define MAX_DIST 450

#define MIN_HEIGHT (10 * SC_IF)
enum EDGE_TYPE
{
	ET_BLANK = 0,
	ET_WALL = -2,
	ET_TOP = -3,
};

enum FLY_OPTION
{
	FO_NORMAL,
	FO_MAGNET,
};

//         0
//		   __
//		5 /  \ 1
//		4 \__/ 2
//         3
//
//
enum 
{
	zBubbleAnimation,
	zBubbles,
};

#define SCORE_DROP			15
#define SCORE_DROP_FRIEND	150
#define SCORE_POP			10
#define SCORE_POP_FRIEND	100

static const bool istest = true;
static bool bLogDist;

static const int ZOMBIE_DECREASE = 3;

#define PTM_RATIO 32.0

#define CONTINUE_PLAY_EXTRA_BUBBLES	8


struct BubblePr
{
	unsigned char type;
	unsigned char color;
	unsigned char probability;
};

typedef std::vector<BubblePr> BubblePrList;

//typedef std::map<int, BubblePrList> BubbleGroupMap;


CBubbleLayer* CBubbleLayer::create()
{
	CBubbleLayer* layer = new CBubbleLayer();
	layer->autorelease();
	return layer;
}

CBubbleLayer::CBubbleLayer()
	: 
	m_nCurShootIndex(0),
	m_wStageFlags(0),
	m_wStar2(0),
	m_wStar3(0),
	m_nScoreForUnused(0),
	m_nTotalPopped(0),
	m_nTotalDropped(0),
	m_nTotalStar(0),
	m_bPinPopping(false),
	m_ptPinPos(CCPointZero),
	lastBubble(NULL),
	m_nSavedFriends(0),
	m_bLayerMoving(false),
	m_nRemainCount(0),
	m_bEnableSwapping(false),
	stoneTouched(false),
	prevStoneTouched(false),
	m_BubbleList(0),
	m_fMargin(0),
	m_pMainPanel(NULL),
	currentMap(0),
	m_nStageCount(0),
	m_nFriendCount(0),
	m_nState(0),
	m_nScoreIndex(0),
	m_pPrepareColorList(0),
	m_nTotalBubbleCount(0),
	rcBound(CCRectZero),
	absBound(CCRectZero),
	absPos(CCPointZero),
	m_ptNextBubble(CCPointZero),
	m_ptEnd(CCPointZero),
	m_fLaunchAngle(0),
	m_bLaunchBubbleUp(true),
	m_bLaunchBubbleDirectionChanged(false),
	m_AimLayer(NULL),
	m_fRotatedAngleByBee(0),
	m_bBeeRotating(false),
	m_vecLines(0),
	m_pGameRoot(NULL),
	m_pAnimExplode(NULL),
	m_ptStart(CCPointZero),
	m_pMor(NULL),
	//m_ptMorCurPos(CCPointZero),
	//m_ptMorNextPos(CCPointZero),
	m_nStreaksMinusCount(0),
	m_nPrevStreaksMinusCount(0),
	nTotalPlaceholderCounts(0),
	BubbleExistArr(NULL),
	OuterEmptyBubblesArr(NULL)
{
	m_fMargin = 72 * 0.4f;

	absBound = CCRectMake(m_fMargin + CBubble::SIZE / 2, 100, SC_WIDTH - m_fMargin * 2 - CBubble::SIZE, SC_HEIGHT - kActionBarHeight - 100 - CBubble::SIZE);
	absPos = getPosition();
	rcBound = absBound;

#ifndef ROBOT_SHOOT
	setTouchEnabled(true);
	//setTouchPriority(kCCMenuHandlerPriority + 1);
	setTouchMode(kCCTouchesOneByOne);
#endif

	m_pAnimExplode = NULL;

	m_nTotalPopped = 0;
	m_nTotalDropped = 0;
#ifdef ROBOT_SHOOT
	infoLeft.init();
	infoPopped.init();
	infoDropped.init();
#endif
}

CBubbleLayer::~CBubbleLayer()
{
}

void CBubbleLayer::startGame()
{
	m_bPinPopping = false;
	lastBubbleBack.init();

	memset(touchObstacles, 0, BB_KIND_COUNT * sizeof(bool));
	memset(touchObstaclesBack, 0, BB_KIND_COUNT * sizeof(bool));
	
	removeAllChildren();

	m_AimLayer = CAimLayer::create();
	addChild(m_AimLayer);
	m_AimLayer->setDrawInfo(m_ptStart, CBubble::SIZE);

//	loadGameData();

// add top dot line

	float xx = 50;
	while (xx < SC_WIDTH)
	{
		CCSprite* sprt = CCSprite::create("Image/game/dot.png");
		sprt->setPosition(ccp(xx, rcBound.getMaxY() + 30));
		addChild(sprt);
		xx += 30;
	}

	if(!loadStageData(LCommon::sharedInstance()->curLevel()))
		return;

	m_pGameRoot->initGame();
	m_pGameRoot->procGameEvent(GS_UPDATE_SCORE, 0);
	m_pGameRoot->procGameEvent(GS_UPDATE_MULTIPLIER, 0);

	setGameState(STATE_READY);

	startGamePlay(true);

#ifdef ROBOT_SHOOT
	initAi();
#endif
}

void CBubbleLayer::updateBoost()
{
	m_AimLayer->setSniperAim(m_pGameRoot->m_hasSniperAim);
}

void CBubbleLayer::continueGame()
{
	m_nRemainCount = CONTINUE_PLAY_EXTRA_BUBBLES;
	startGamePlay(false);
}

void CBubbleLayer::setStartPos(CCPoint pos)
{
	m_ptStart = pos;
}

void CBubbleLayer::setNextBubblePos(CCPoint nextPos)
{
	m_ptNextBubble = nextPos;
}

void CBubbleLayer::loadGameData()
{
	unsigned long nSize = 0;
	std::string fullPath = CCFileUtils::sharedFileUtils()->fullPathForFilename("map/index.map");
	char* pBuffer = (char*) CCFileUtils::sharedFileUtils()->getFileData(fullPath.c_str(), "rb", &nSize);
	int nHeaderCnt = *((int*) pBuffer);
	std::string headerStr(pBuffer + 4, nHeaderCnt);
	m_nStageCount = *((int*) (pBuffer + 4 + nHeaderCnt));
	CC_SAFE_DELETE_ARRAY(pBuffer);
}

//Checked
bool CBubbleLayer::loadStageData(int nStage)
{
	int nColorShift = LCommon::sharedInstance()->shiftColor[nStage];

	BubblePrList bubbleTypeList;

	std::string levelName = LEVELS_NAME[nStage];
	std::string fullPath;
	if(levelName.find("C") != -1)
	{
		fullPath = CCFileUtils::sharedFileUtils()->fullPathForFilename(CCString::createWithFormat("map/ClearTheBoard/%s.map", levelName.c_str())->getCString());
		 LCommon::sharedInstance()->setCurLevelType(LVL_CLEAR_THE_BOARD);
	}
	else if(levelName.find("M") != -1)
	{
		fullPath = CCFileUtils::sharedFileUtils()->fullPathForFilename(CCString::createWithFormat("map/Defeat Mows/%s.map", levelName.c_str())->getCString());
		LCommon::sharedInstance()->setCurLevelType(LVL_DEFEAT_THE_MOWS);
	}
	else if(levelName.find("A") != -1)
	{
		fullPath = CCFileUtils::sharedFileUtils()->fullPathForFilename(CCString::createWithFormat("map/Free Animals/%s.map", levelName.c_str())->getCString());
		LCommon::sharedInstance()->setCurLevelType(LVL_FREE_THE_ANIMALS);
	}
	else if(levelName.find("B") != -1)
	{
		fullPath = CCFileUtils::sharedFileUtils()->fullPathForFilename(CCString::createWithFormat("map/SaveTheBee/%s.map", levelName.c_str())->getCString());
		LCommon::sharedInstance()->setCurLevelType(LVL_SAVE_THE_BEE);
	}
	else
	{
		LCommon::sharedInstance()->setCurLevelType(LVL_NONE);
		return false;
	}
	
	unsigned long nSize = 0;
	char* pBuffer = (char*) CCFileUtils::sharedFileUtils()->getFileData(fullPath.c_str(), "rb", &nSize);
	int nPos = 0;

	int* pIntBuffer = reinterpret_cast<int*> (pBuffer);

	int nMapDataVersion = pIntBuffer[0]; nPos += 4;
	m_nTotalBubbleCount = pIntBuffer[1] & 0xFFFF; 
	m_wStageFlags = (pIntBuffer[1] >> 16) & 0xFFFF; 
	nPos += 4;
	m_wStar2 = pIntBuffer[2] & 0xFFFF; 
	m_wStar3 = (pIntBuffer[2] >> 16) & 0xFFFF; 
	nPos += 4;

	int nStagePlayMode = pIntBuffer[3]; nPos += 4;

	m_nRemainCount = pIntBuffer[4]; nPos += 4;
	m_nOrgCount = m_nRemainCount;

	//Read BubbleGroupData
	int nSpecialGroupCount = *(int*)(pBuffer + nPos); nPos += 4;
	int nNormalGroupCount = *(int*)(pBuffer + nPos); nPos += 4;

	int nBubbleGroupCounts = *(int*)(pBuffer + nPos); nPos += 4;
	for(int i = 0; i < nBubbleGroupCounts; i++)
	{
		int groupNameLength = *(int*)(pBuffer + nPos); nPos += 4;
		nPos += groupNameLength;

		BubblePrList bubblePrList;
		int nBubblePrCounts = *(int*)(pBuffer + nPos); nPos += 4;
		for(int j = 0; j < nBubblePrCounts; j++)
		{
			BubblePr bubblePr;
			bubblePr.type = *(unsigned char*)(pBuffer + nPos); nPos++;
			bubblePr.color = *(unsigned char*)(pBuffer + nPos); nPos++;
			bubblePr.probability = *(unsigned char*)(pBuffer + nPos); nPos++;
			
			bubblePrList.push_back(bubblePr);
		}

		int nProbability = getRandRange(0, 100);
		int nSum = 0;
		for(int j = 0; j < nBubblePrCounts; j++)
		{
			BubblePr bubblePr = bubblePrList[j];
			nSum += bubblePr.probability;
			if(nProbability <= nSum)
			{
				bubbleTypeList.push_back(bubblePr);
				break;
			}
		}
		if(nProbability > nSum)
		{
			BubblePr bubblePr;
			bubblePr.type = bubblePr.color = 1;
			bubblePr.probability = nProbability - nSum;

			bubbleTypeList.push_back(bubblePr);
		}
	}

//////////////////////////////

	/*int i = 0;
	for (i = 0; i < m_nTotalBubbleCount; i++)
	{
	char byCol = pBuffer[nPos++];

	char byMask = byCol & (~MASK_COLOR);
	char byTmpClr = byCol & MASK_COLOR;
	if (byTmpClr == BB_NONE)
	m_pPrepareColorList.push_back(getRandRange(COLOR_MIN, COLOR_MAX));
	else {
	byTmpClr = ((byTmpClr + nColorShift - COLOR_MIN) % COLOR_COUNT) + COLOR_MIN;
	m_pPrepareColorList.push_back(byTmpClr | byMask);
	}
	}

	for (; i < m_nRemainCount; i++)
	{
	m_pPrepareColorList.push_back(getRandRange(COLOR_MIN, COLOR_MAX));
	}*/


	/*for(int i = 0; i < bubbleTypeList.size(); i++)
	{
		CCLog("%d : %d : %d", i, bubbleTypeList[i].type, bubbleTypeList[i].color);
	}*/
//////////////////////////////

	int nBubbleCnt = *(int*)(pBuffer + nPos); nPos += 4;

	int* tempBuf = new int[nBubbleCnt];
	m_BubbleList.clear();
	m_vecMagnets.clear();

	for (int i = 0; i < nBubbleCnt; i++)
	{
		char byType, byColor;
		
		int isNormalGroup = *(int*)(pBuffer + nPos); nPos += 4;

		int groupIndex = *(int*)(pBuffer + nPos); nPos += 4;
		
		int groupNameLength = *(int*)(pBuffer + nPos); nPos += 4;
		nPos += groupNameLength;

		int mowIndex = *(int*)(pBuffer + nPos); nPos += 4;

		if (groupIndex == -1)
		{
			tempBuf[i] = INDEX_EMPTY;
		}
		else
		{
			if(groupIndex == -2)
			{
				byType = BB_MOR;
				byColor = 0;
			}else{
				int index = isNormalGroup ? nSpecialGroupCount + groupIndex - 1 : groupIndex - 1;

				byType = bubbleTypeList[index].type;
				byColor = bubbleTypeList[index].color;
			}

			tempBuf[i] = m_BubbleList.size();

			/*if (byType == BB_NORMAL || byType == BB_FRIEND || byType == BB_ZOMBIE)
			byColor = ((byColor + nColorShift - COLOR_MIN) % COLOR_COUNT) + COLOR_MIN;*/

			CBubble* pBubble = new CBubble(byType, byColor);
			pBubble->m_nIndex = i;
			m_BubbleList.push_back(pBubble);


			int xx = i % COLUMN_COUNT;
			int yy = i / COLUMN_COUNT;
			float fStartX = m_fMargin + (yy % 2 + 1) * CBubble::SIZE / 2;
			pBubble->setPosition(ccp(fStartX + xx * CBubble::SIZE, rcBound.getMaxY() - yy * CBubble::SIZE_OV));
			addChild(pBubble);

			// build initial map
			char byCol = byColor;
			if (byType != BB_NORMAL && byType != BB_FRIEND && byType != BB_ZOMBIE)
				byCol = 1;
			int key = getKey(byType, byCol);
			TBubbleStatMap::iterator it = initialList.find(key);
			if (it != initialList.end())
				it->second++;
			else {
				int val = 1;
				initialList.insert(std::make_pair(key, val));
			}
		}
	}

	CCLog("Start build neibough!!!");

	// build neibourgh
	int nCnt = m_BubbleList.size();
	int nMaxY = nBubbleCnt / COLUMN_COUNT;

	m_nFriendCount = 0;

	for (int i = 0; i < nCnt; i++)
	{
		CBubble* pBubble = m_BubbleList[i];
		if (pBubble->m_byType == BB_FRIEND)
			m_nFriendCount++;
		else if (pBubble->m_byType == BB_MAGNET)
			m_vecMagnets.insert(std::make_pair(pBubble->m_nUID, pBubble->getPosition()));

		for (int i = 0; i < NEIGHBOR_COUNT; i++)
		{
			int edgeIndex = getEdgeIndex(pBubble->m_nIndex, i, COLUMN_COUNT, nMaxY);

			if (edgeIndex < 0)
				pBubble->m_pNeighbors[i] = NULL;
			else
			{
				int nIndex = tempBuf[edgeIndex];
				pBubble->m_pNeighbors[i] = (nIndex == INDEX_EMPTY)? NULL : m_BubbleList[nIndex];
			}
		}

		pBubble->runBright();
	}


	// build Mor masters and slaves
	for (int i = 0; i < nCnt; i++)
	{
		CBubble* pBubble = m_BubbleList[i];
		if (pBubble->m_byType == BB_MOR && pBubble->m_byColor != 0)
		{
			pBubble->m_byColor = 7;
			pBubble->changeTo(pBubble->m_byType, pBubble->m_byColor);

			for (int i = 0; i < NEIGHBOR_COUNT; i++)
			{
				int edgeIndex = getEdgeIndex(pBubble->m_nIndex, i, COLUMN_COUNT, nMaxY);

				if (edgeIndex < 0)
					pBubble->m_pMorSlave[i] = NULL;
				else
				{
					int nIndex = tempBuf[edgeIndex];
					pBubble->m_pMorSlave[i] = (nIndex == INDEX_EMPTY)? NULL : m_BubbleList[nIndex];

					m_BubbleList[nIndex]->m_pMorMaster = pBubble;
				}
			}
		}
	}

	delete tempBuf;

	//set_BubbleExistArr();

	////Show outerempty 
	//for(int i = 0; i < nTotalPlaceholderCounts; i++)
	//{
	//	if(OuterEmptyBubblesArr[i])
	//	{
	//		int xx = i % COLUMN_COUNT;
	//		int yy = i / COLUMN_COUNT;
	//		float fStartX = m_fMargin + (yy % 2 + 1) * CBubble::SIZE / 2;
	//		CBubble* pBubble = new CBubble(0, 0);
	//		pBubble->setPosition(ccp(fStartX + xx * CBubble::SIZE, rcBound.getMaxY() - yy * CBubble::SIZE_OV));
	//		addChild(pBubble);
	//	}
	//}

	m_bEnableSwapping = (m_wStageFlags & MASK_ENABLE_SWAP) != 0;
	m_nSavedFriends = 0;

	m_wStar2 = LCommon::sharedInstance()->mapInfo[nStage].m_byStar2;
	m_wStar3 = LCommon::sharedInstance()->mapInfo[nStage].m_byStar3;

	return true;
}

void CBubbleLayer::set_BubbleExistArr()
{
	int nMaxIndex = -1;
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		if((*it)->m_nIndex > nMaxIndex)
			nMaxIndex = (*it)->m_nIndex;

	nTotalPlaceholderCounts = (nMaxIndex / COLUMN_COUNT + 2) * COLUMN_COUNT;

	if(BubbleExistArr)
		delete BubbleExistArr;
	if(OuterEmptyBubblesArr)
		delete OuterEmptyBubblesArr;

	BubbleExistArr = new int[nTotalPlaceholderCounts];
	OuterEmptyBubblesArr = new int[nTotalPlaceholderCounts];
	memset(BubbleExistArr, 0, nTotalPlaceholderCounts * sizeof(int));
	memset(OuterEmptyBubblesArr, 0, nTotalPlaceholderCounts * sizeof(int));

	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		BubbleExistArr[(*it)->m_nIndex] = 1;


	OuterEmptyBubblesArr[nTotalPlaceholderCounts - 1] = 1;	
	set_OuterEmptyBubblesArr(nTotalPlaceholderCounts - 1, nTotalPlaceholderCounts);

}

void CBubbleLayer::set_OuterEmptyBubblesArr(int nIndex, int nTotalCounts)
{
	for (int i = 0; i < NEIGHBOR_COUNT; i++)
	{
		int edgeIndex = getEdgeIndex(nIndex, i, COLUMN_COUNT, INT_MAX);

		if (edgeIndex < 0 || edgeIndex >= nTotalCounts || BubbleExistArr[edgeIndex] || OuterEmptyBubblesArr[edgeIndex])
			continue;

		OuterEmptyBubblesArr[edgeIndex] = 1;		
		set_OuterEmptyBubblesArr(edgeIndex, nTotalCounts);
	}
}


void CBubbleLayer::startGamePlay(bool bFirst)
{
	m_bLayerMoving = false;

	if (bFirst) 
	{
		rcBound = absBound;
		setPosition(absPos);

		if (m_pGameRoot->m_CurBubble != NULL)
			m_pGameRoot->m_CurBubble->setVisible(false);
		if (m_pGameRoot->m_NextBubble != NULL)
			m_pGameRoot->m_NextBubble->setVisible(false);
		if (m_pGameRoot->m_CandiBubble != NULL)
			m_pGameRoot->m_CandiBubble->setVisible(false);

		if (m_pGameRoot->m_Foresight1Bubble != NULL)
			m_pGameRoot->m_Foresight1Bubble->setVisible(false);
		if (m_pGameRoot->m_Foresight2Bubble != NULL)
			m_pGameRoot->m_Foresight2Bubble->setVisible(false);
		if (m_pGameRoot->imgForesightFrame1 != NULL)
			m_pGameRoot->imgForesightFrame1->setVisible(false);
		if (m_pGameRoot->imgForesightFrame2 != NULL)
			m_pGameRoot->imgForesightFrame2->setVisible(false);

		checkMoveLayer();

		if(LCommon::sharedInstance()->getCurLevelType() == LVL_DEFEAT_THE_MOWS)
		{
			m_pMor = CCSprite::create("Image/animation/Mor/mor.png");
			//m_ptMorCurPos = ccp(rcBound.getMinX() + 100, rcBound.getMinY() - 1000);
			m_pMor->setPosition(ccp(rcBound.getMinX() + 100, rcBound.getMinY() - 500));
			addChild(m_pMor);
		}

		if(LCommon::sharedInstance()->curLevelType == LVL_DEFEAT_THE_MOWS)
			animateMorFlyToNext();
	}
	else
	{
		setGameState(STATE_PLAY);
	}

	m_pGameRoot->procGameEvent(GS_REMAIN_BUBBLES, m_nRemainCount);
}

int CBubbleLayer::getEdgeIndex(int center, int direction, int column, int maxy)
{
	int x = center % column;
	int y = center / column;

	int nCap = y % 2;

	int nDiff = 0;
	int dy = 0;
	switch(direction)
	{
	case 0: nDiff = nCap - 1;	dy = y - 1; break;
	case 1: nDiff = nCap;		dy = y - 1; break;
	case 2: nDiff = 1;			dy = y; 	break;
	case 3: nDiff = nCap;		dy = y + 1; break;
	case 4: nDiff = nCap - 1;	dy = y + 1; break;
	case 5: nDiff = -1;			dy = y; 	break;
	}

	int dx = x + nDiff;

	if (dy < 0) return INDEX_TOP;
	if (dx < 0 || column - 1 < dx) return INDEX_WALL;
	if (maxy - 1 < dy) return INDEX_EMPTY;

	return dy * column + dx;
}

void CBubbleLayer::onPrev(cocos2d::CCObject* obj)
{
	currentMap = (currentMap + 1) % (MAX_LEVEL + 1);
}

void CBubbleLayer::updateTargetMove()
{
	if (m_pGameRoot->m_CurBubble == NULL) return;
	if (m_nState != STATE_PLAY) return;

	float orgAng = m_fLaunchAngle;

	CCPoint ptStart = m_ptStart;
	CCPoint ptNext;
	bool hasNext;
	m_vecLines.clear();
	m_AimLayer->setDrawInfo(m_ptStart, CBubble::SIZE);

	int nLen = 0;
	int flyOption = FO_NORMAL;
	do 
	{
		hasNext = getNextPoint(ptStart, CBubble::SIZE, ptNext, flyOption);

		m_vecLines.push_back(ptNext);
		ptStart = ptNext;
		if (nLen++ > 10) break;
	} while (hasNext && !m_bLaunchBubbleDirectionChanged);

	m_AimLayer->setVisible(true);
	
	m_AimLayer->setAimDots(&m_vecLines);
	
	m_AimLayer->setAimRays(&m_vecLines);

	m_fLaunchAngle = orgAng;

	int nDirect = m_fLaunchAngle * 180 / M_PI;
	nDirect = (165 - nDirect) / 22;
	if (nDirect < 0) nDirect = 0;
	if (nDirect > 6) nDirect = 6;
	m_pGameRoot->procGameEvent(GS_UPDATE_LOOK, nDirect);
}

//Checked
bool CBubbleLayer::launchBubble()
{
//  	bLogDist = true;
//  	updateTargetMove();
//  	bLogDist = false;

	m_pGameRoot->m_nShootableCheckCount = 0;

	m_AimLayer->setVisible(false);
	if (sin(m_fLaunchAngle) < 0.3f) return false;
	
#ifdef ROBOT_SHOOT
	analyzeColor(m_pGameRoot->m_CurBubble->m_byColor);
#endif

	CBubble* bubble = m_pGameRoot->m_CurBubble;
	m_pGameRoot->m_CurBubble = NULL;

	bringBubble(bubble);

	m_bLaunchBubbleUp = true;
	clearBubbleCheck();
	updateAngle(bubble);

	if (bubble->m_byType == BB_NORMAL)
	{
		decreaseRemainCount(1);
		
		m_pGameRoot->m_nTotalShoots++;
	}

	decreaseRecallCountdown();

    return true;
}

void CBubbleLayer::clearBubbleCheck()
{
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		(*it)->m_bChecked = false;
}

void CBubbleLayer::setBeeGroupBubbles(CBubble* beeBubble)
{
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		(*it)->m_bIsBeeGroup = false;

	beeBubble->m_bIsBeeGroup = true;

	setNeighboursAsBeeGroup(beeBubble);
}

void CBubbleLayer::setNeighboursAsBeeGroup(CBubble* bubble)
{
	for (int i = 0; i < NEIGHBOR_COUNT; i++)
	{
		CBubble* neighbourBubble = bubble->m_pNeighbors[i];

		if (neighbourBubble)
		{
			if(neighbourBubble->m_bIsBeeGroup)
				continue;

			neighbourBubble->m_bIsBeeGroup = true;

			setNeighboursAsBeeGroup(neighbourBubble);
		}
	}
}

void CBubbleLayer::finishedBeeRotation(CCNode* node)
{
	m_bBeeRotating = false;
}

void CBubbleLayer::updateBubblesInBeeLevel(float dt)
{
	if(m_bBeeRotating)
	{
		for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		{
			CBubble* bubble = *it;

			//CCRect viewRect = CCRectMake(rcBound.getMinX() + CBubble::SIZE / 2, rcBound.getMinY() + CBubble::SIZE / 2, rcBound.size.width - CBubble::SIZE, rcBound.size.height - CBubble::SIZE);
			CCRect viewRect = rcBound;

			if(viewRect.containsPoint(bubble->getPosition()) == false)
			{
				processExplodeBubble(bubble, 0, bubble->m_byType == BB_NUKE);
			}		
		}
	}else{
		unschedule(schedule_selector(CBubbleLayer::updateBubblesInBeeLevel));
	}
}

void CBubbleLayer::updateAngle(CCNode* node)
{
	CCPoint ptEnd;

	int flyOption = FO_NORMAL;

	CCPoint curPos = node->getPosition();
	
	bool hasNext;

	CBubble* curBubble = (CBubble*) node;
	if(curBubble->m_byType == BB_FIREBALL)	
		hasNext = getNextPointForFireball(curPos, node->getContentSize().width, ptEnd, flyOption);
	else
		hasNext = getNextPoint(curPos, node->getContentSize().width, ptEnd, flyOption);


	float dist = getDistance(curPos, ptEnd);
	float fMoveTime = fabs(dist) / BUBBLE_SPEED;

	CCMoveTo* moveTo = CCMoveTo::create(fMoveTime, ptEnd);

	CCCallFuncN* callback;
	if(curBubble->m_byType == BB_FIREBALL)	
	{
		m_pGameRoot->m_CurBubbleInBubbleLayer = curBubble;
		m_pGameRoot->m_CurBubblePrevPos = curBubble->getPosition();

		callback = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::finishedMovingForFireball));
		schedule(schedule_selector(CBubbleLayer::processFireballMoving), 0.01f);
	}else{
		if (hasNext)
		{
			callback = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::updateAngle));
			//CCLog("hasNext : true");
		}
		else
		{
			callback = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::finishedMoving));
			//CCLog("hasNext : false");
		}
	}

	CCSequence* seq = CCSequence::create(moveTo, callback, NULL);
	node->runAction(seq);

	if (flyOption == FO_MAGNET)
		playEffect(kSND_SFX_MAGNET);
}

void CBubbleLayer::setNewBubbleIndex(CBubble* newBubble, CBubble* neighbourBubble)
{
	float newX = newBubble->getPositionX();
	float newY = newBubble->getPositionY();

	if(neighbourBubble == NULL)
	{
		int yy = (rcBound.getMaxY() - newY + CBubble::SIZE / 2) / CBubble::SIZE_OV;
		float fStartX = m_fMargin + (yy % 2 + 1) * CBubble::SIZE / 2;
		int xx = (newX + CBubble::SIZE / 2 - fStartX) / CBubble::SIZE;
		if (xx < 0) xx = 0;
		if (xx > COLUMN_COUNT - 1) xx = COLUMN_COUNT - 1;
		
		newBubble->m_nIndex = yy *COLUMN_COUNT + xx;
	}else {
		float neighbourX = neighbourBubble->getPositionX();
		float neighbourY = neighbourBubble->getPositionY();

		int nDirection = -1;
		if(neighbourX < newX)
		{
			if(abs(newY - neighbourY) < CBubble::SIZE_OV / 10 )
				nDirection = 2;
			else if(neighbourY < newY)
				nDirection = 1;
			else
				nDirection = 3;
		}else{
			if(abs(newY - neighbourY) < CBubble::SIZE_OV / 10 )
				nDirection = 5;
			else if(neighbourY < newY)
				nDirection = 0;
			else
				nDirection = 4;
		}

		newBubble->m_nIndex = getEdgeIndex(neighbourBubble->m_nIndex, nDirection, COLUMN_COUNT, INT_MAX);
	}
}

void CBubbleLayer::finishedMovingForFireball(CCNode* node)
{
	unschedule(schedule_selector(CBubbleLayer::processFireballMoving));

	checkMoveLayer();

	m_pGameRoot->procGameEvent(GS_SEND_NEXT_BUBBLE);
}

void CBubbleLayer::processFireballMoving(float dt)
{
	/*CBubble* curBubble = m_pGameRoot->m_CurBubbleInBubbleLayer;
	CCLog("%f : %f", curBubble->getPositionX(), curBubble->getPositionY());*/

	CCPoint linePt1 = m_pGameRoot->m_CurBubbleInBubbleLayer->getPosition();
	CCPoint linePt2 = m_pGameRoot->m_CurBubblePrevPos;

	if(getDistance(linePt1, linePt2) > 1)
	{
		m_fCurrentMultiplier = m_pGameRoot->m_fMultiplier;

		//calcStreaksPlusVal(candiList);
		m_fStreaksPlus = STREAKS_PLUS[0];

		TBubbleList candiList;

		for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		{
			CBubble* bubble = *it;
			CCPoint projectPt;
			if(getProjectPointOntoLine(linePt1, linePt2, bubble->getPosition(), projectPt))
			{
				if(getDistance(bubble->getPosition(), projectPt) < CBubble::SIZE * 0.55)
				{
					//bubble->changeTo(0, 0);
					processExplodeBubble(bubble, 0, m_pGameRoot->m_CurBubbleInBubbleLayer->m_byType == BB_NUKE);

					candiList.push_back(bubble);
				}
			}
		}

		for (TBubbleList::iterator it = candiList.begin(); it != candiList.end(); it++)
		{
			CBubble* bubble = *it;
			removeBubbleFromList(bubble);
		}

		afterExplodeProcForFireballMoving();
	}

	m_pGameRoot->m_CurBubblePrevPos = linePt1;
}

long begintime;
long endtime;
void CBubbleLayer::finishedMoving(CCNode* node)
{
	//CCLog("finishedMoving");
	
	begintime = clock();

	CBubble* curBubble = (CBubble*) node;

	m_nScoreIndex = 0;
	int nPopCount = 0;
	int nDropCount = 0;

	m_bPinPopping = false;
	backBubbleList.clear();
	lastBubbleBack.init();
	lastBubble = curBubble;


	//Rotate Around Bee
	CBubble* beeBubble = NULL;
	CCPoint curBubblePt = curBubble->getPosition();
	CCPoint beeBubblePt = ccp(0, 0);
	bool bHasBee = false;
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
	{
		CBubble* bubble = *it;
		if(bubble->m_byType == BB_BEE)
		{
			beeBubble = bubble;
			beeBubblePt = bubble->getPosition();
			bHasBee = true;
			break;
		}
	}
	if(bHasBee)
	{
		CCPoint newPt = ccp(0, 0);
		rotateAroundBy(beeBubblePt, -m_fRotatedAngleByBee, node->getPosition(), newPt);

		int yy = (rcBound.getMaxY() - newPt.y + CBubble::SIZE / 2) / CBubble::SIZE_OV;
		float fStartX = m_fMargin + (yy % 2 + 1) * CBubble::SIZE / 2;
		int xx = (newPt.x + CBubble::SIZE / 2 - fStartX) / CBubble::SIZE;
		if (xx < 0) xx = 0;
		if (xx > COLUMN_COUNT - 1) xx = COLUMN_COUNT - 1;
		newPt = ccp(fStartX + xx * CBubble::SIZE, rcBound.getMaxY() - yy * CBubble::SIZE_OV);

		rotateAroundBy(beeBubblePt, m_fRotatedAngleByBee, newPt, newPt);
		node->setPosition(newPt);

	}else{
		int yy = (rcBound.getMaxY() - node->getPositionY() + CBubble::SIZE / 2) / CBubble::SIZE_OV;
		float fStartX = m_fMargin + (yy % 2 + 1) * CBubble::SIZE / 2;
		int xx = (node->getPositionX() + CBubble::SIZE / 2 - fStartX) / CBubble::SIZE;
		if (xx < 0) xx = 0;
		if (xx > COLUMN_COUNT - 1) xx = COLUMN_COUNT - 1;
		node->setPosition(ccp(fStartX + xx * CBubble::SIZE, rcBound.getMaxY() - yy * CBubble::SIZE_OV));
	}
	//Rotate Around Bee

	/*endtime = clock();
	CCLog("finishedMoving1 : %f", double(std::difftime(endtime, begintime)));*/

	curBubble->cleanup();

	float fHorz = CBubble::SIZE * 1.2;
	float fVert = CBubble::SIZE_OV * 1.2;

	char itemType = BB_NONE;
	char byCandyColor = BB_NONE;
	bool bHasBombEffect = false;
	TBubbleList candiList;
	int nNeighbourCount = 0;

	bool bTouchStone = false;
	
	// processNeighbor
	CBubble* oneNeighbour = NULL;
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
	{
		CBubble* bubble = *it;
		bubble->m_bChecked = false;
		bubble->setTag(BB_NORMAL);
		if (fabs(bubble->getPositionY() - curBubble->getPositionY()) > fVert) continue;
		if (fabs(bubble->getPositionX() - curBubble->getPositionX()) > fHorz) continue;
		curBubble->addNeighbor(bubble);
		bubble->addNeighbor(curBubble);
		
		oneNeighbour = bubble;

		nNeighbourCount++;
	}

	processNeighborForBounce(curBubble, 0.16f);

	if(bHasBee)
	{
		setBeeGroupBubbles(beeBubble);

		if(!curBubble->m_bIsBeeGroup)
		{
			int yy = (rcBound.getMaxY() - curBubblePt.y + CBubble::SIZE / 2) / CBubble::SIZE_OV;
			float fStartX = m_fMargin + (yy % 2 + 1) * CBubble::SIZE / 2;
			int xx = (curBubblePt.x + CBubble::SIZE / 2 - fStartX) / CBubble::SIZE;
			if (xx < 0) xx = 0;
			if (xx > COLUMN_COUNT - 1) xx = COLUMN_COUNT - 1;
			curBubble->setPosition(ccp(fStartX + xx * CBubble::SIZE, rcBound.getMaxY() - yy * CBubble::SIZE_OV));
		}
	}

	//set new bubble's m_nIndex
	setNewBubbleIndex(curBubble, oneNeighbour);

	/*endtime = clock();
	CCLog("finishedMoving2 : %f", double(std::difftime(endtime, begintime)));*/

	clearBubbleCheck();

	int nCandyCount = 0;

	memcpy(touchObstaclesBack, touchObstacles, BB_KIND_COUNT * sizeof(bool));
	CBubble* blastBomb = NULL;
	for (int i = 0; i < NEIGHBOR_COUNT; i++)
	{
		CBubble* bubble = curBubble->m_pNeighbors[i];
		if (bubble == NULL) continue;
		if (bubble->m_bChecked) continue;

		if (bubble->m_byType == BB_CANDY)
		{
			bubble->m_bChecked = true;
			candiList.push_back(bubble);
			byCandyColor = curBubble->m_byColor;
			playEffect(kSND_SFX_CANDY);
			nCandyCount++;
		}
		else if (bubble->m_byType == BB_BOMB)
		{
			bubble->m_bChecked = true;
			candiList.push_back(bubble);
			blastBomb = bubble;
			bHasBombEffect = true;
			playEffect(kSND_SFX_DYNAMITE);
		} 

		touchObstacles[bubble->m_byType] = true;
	}

	m_BubbleList.push_back(curBubble);
	curBubble->m_bChecked = true;
	candiList.push_back(curBubble);

	//Rotate Around Bee
	if(bHasBee && curBubble->m_bIsBeeGroup)
	{
		//Calculate rotate angle
		float fAngle = 0.0f;
		if(curBubblePt.x < beeBubblePt.x)
		{
			fAngle = BEE_ROTATION_ANGLE;
			if(curBubblePt.y < beeBubblePt.y && m_fLaunchAngle < M_PI_2)
				fAngle = -BEE_ROTATION_ANGLE;
		}else{
			fAngle = -BEE_ROTATION_ANGLE;
			if(curBubblePt.y < beeBubblePt.y && m_fLaunchAngle > M_PI_2)
				fAngle = BEE_ROTATION_ANGLE;
		}


		for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		{
			CBubble* bubble = *it;

			if(bubble->m_byType == BB_BEE || bubble->m_bIsBeeGroup == false)
				continue;

			CCRotateAroundBy* rot1 = CCRotateAroundBy::create(2.0f, fAngle, beeBubblePt);
			CCCallFuncN* callback = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::finishedBeeRotation));
			
			CCSequence* seq = CCSequence::create(rot1, callback, NULL);

			/*m_bBeeRotating = true;

			schedule(schedule_selector(CBubbleLayer::updateBubblesInBeeLevel), 0.1f);*/

			bubble->runAction(seq);
		}

		m_fRotatedAngleByBee += fAngle;
	}
	//Rotate Around Bee

	bool bCheckDrop = false;
	bool haveExplode = true;
	if (curBubble->m_byType == BB_RAINBOW)
	{
		bCheckDrop = true;
		m_pGameRoot->procGameEvent(GS_ANIM_RAINBOW);
		char popColors[MAX_COLORS];
		memset(popColors, 0, MAX_COLORS * sizeof(char));

		// calculate colors
		for (int i = 0; i < NEIGHBOR_COUNT; i++)
		{
			CBubble* bubble = curBubble->m_pNeighbors[i];
			if (bubble == NULL) continue;
			if (bubble->m_byType != BB_NORMAL) continue;
			popColors[bubble->m_byColor]++;
		}

		int nColorCount = 0;
		for (int i = 1; i < MAX_COLORS; i++)
			if (popColors[i] > 0) nColorCount++;

		if (nColorCount > 0)
		{
			int nBubbleCount = 6 / nColorCount;
			//
			for (int i = 1; i < MAX_COLORS; i++)
			{
				if (popColors[i] == 0) continue;
				int nCnt = nBubbleCount;
				clearBubbleCheck();
				findBubbles(curBubble, true, i, nCnt, candiList);
			}

			for (TBubbleList::iterator it = candiList.begin(); it != candiList.end(); it++)
			{
				(*it)->setTag(BB_RAINBOW);
			}
		}
	}
	else if (curBubble->m_byType == BB_NUKE)
	{
		int nCnt = 9;
		clearBubbleCheck();

		if (nNeighbourCount > 0)
		{
			findBubbles(curBubble, false, BB_NONE, nCnt, candiList);
			for (TBubbleList::iterator it = candiList.begin(); it != candiList.end(); it++)
				(*it)->setTag(BB_NUKE);
			bCheckDrop = true;
		}
		m_pGameRoot->procGameEvent(GS_ANIM_NUKE);
	}
	else if (curBubble->m_byType == BB_REPELLENT)
	{
		TBubbleList repCandiList;
		for (int i = 0; i < NEIGHBOR_COUNT; i++)
		{
			CBubble* bubble = curBubble->m_pNeighbors[i];
			if (bubble == NULL) continue;
			if (bubble->m_byType != BB_NORMAL && bubble->m_byType != BB_ZOMBIE) continue;

			clearBubbleCheck();
			processNeighbor(bubble, repCandiList);
		}

		if (!repCandiList.empty())
		{
			bool bHasRepellent = false;
			for (TBubbleList::iterator it = repCandiList.begin(); it != repCandiList.end(); it++)
			{
				CBubble* bubble = (*it);
				if (bubble->m_byType == BB_ZOMBIE)
				{
					char col = bubble->m_byColor;
					bubble->changeTo(BB_NORMAL, col);
					bHasRepellent = true;
				}
			}
			if (bHasRepellent)
				m_pGameRoot->procGameEvent(GS_ANIM_REPELLANT);
		}
	}
	else
	{
		processNeighbor(curBubble, candiList);

		haveExplode = candiList.size() >= MIN_EXPLODE_COUNT;

		// find same color for candy bomb
		if (byCandyColor != BB_NONE)
		{
			for (TBubbleList::iterator it = candiList.begin(); it != candiList.end();)
			{
				if ((*it)->m_byType == BB_ZOMBIE)
					it = candiList.erase(it);
				else
					it++;
			}

			haveExplode = candiList.size() >= MIN_EXPLODE_COUNT;

			int nTotalCount = nCandyCount * 6;
			int nCnt = haveExplode? nTotalCount : (nTotalCount - candiList.size()) + 1;// +1: for candy self

			for (TBubbleList::reverse_iterator it = m_BubbleList.rbegin(); it != m_BubbleList.rend(); it++)
			{
				CBubble* bubble = *it;
				if (bubble->m_bChecked) continue;
				if (bubble->m_byType != BB_NORMAL && bubble->m_byType != BB_FRIEND) continue;

				bubble->m_bChecked = true;
				if (bubble->m_byColor == byCandyColor)
				{
					bubble->setTag(BB_CANDY);
					candiList.push_back(bubble);
					nCnt--;
					if (nCnt <= 0) break;
				}
			}
			m_pGameRoot->procGameEvent(GS_ANIM_RAINBOW);
		}
		//---
		// find for bomb
		if (bHasBombEffect)
		{
			if (byCandyColor == BB_NONE && !haveExplode)
			{
				clearBubbleCheck();
				candiList.clear();
				curBubble->m_bChecked = true;
				candiList.push_back(curBubble);
			}

			float fSameVert = CBubble::SIZE_OV /2 ;
			for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
			{
				CBubble* bubble = *it;
				if (bubble == curBubble) continue;
				if (fabs(bubble->getPositionY() - blastBomb->getPositionY()) > fSameVert) continue;
				bubble->m_bChecked = true;
				if (bubble != blastBomb)
					bubble->setTag(BB_BOMB);
				candiList.push_back(bubble);
			}
		}
		//------
	}

	/*endtime = clock();
	CCLog("finishedMoving3 : %f", double(std::difftime(endtime, begintime)));*/

	curBubble->backupTo(lastBubbleBack);

	if (haveExplode || (byCandyColor != BB_NONE) || bHasBombEffect)
	{
		nPopCount = candiList.size();

		m_fCurrentMultiplier = m_pGameRoot->m_fMultiplier;

		calcStreaksPlusVal(candiList);

		m_nStreaksMinusCount = 0;

		/*endtime = clock();
		CCLog("finishedMoving31 : %f", double(std::difftime(endtime, begintime)));*/

		processExplodeBubble(candiList, 0, curBubble->m_byType == BB_NUKE);

		/*endtime = clock();
		CCLog("finishedMoving32 : %f", double(std::difftime(endtime, begintime)));*/

		bCheckDrop = true;
	} else {
		curBubble->runBright();
		lastBubbleBack.bEnable = true;
	}

	if(!bCheckDrop)
	{
		int nIndex = MIN(m_nStreaksMinusCount, STREAKS_MINUS_COUNT - 1);
		m_pGameRoot->m_fMultiplier = MAX(m_pGameRoot->m_fMultiplier - STREAKS_MINUS[nIndex], 1.0f);

		m_nStreaksMinusCount++;

		animateMultiplier();
	}

	/*endtime = clock();
	CCLog("finishedMoving4 : %f", double(std::difftime(endtime, begintime)));*/

#ifdef ROBOT_SHOOT
	MyStructHit& curHit = colorList[m_nCurShootNo];
	curHit.color = curBubble->m_byColor;
	curHit.popCount = nPopCount;
	curHit.dropCount = nDropCount;
	m_nCurShootNo++;
#endif
	// process after move

	/*endtime = clock();
	CCLog("finishedMoving5 : %f", double(std::difftime(endtime, begintime)));*/

	afterExplodeProc(bCheckDrop);

	/*endtime = clock();
	CCLog("finishedMoving6 : %f", double(std::difftime(endtime, begintime)));*/
}

void CBubbleLayer::calcStreaksPlusVal(TBubbleList& bubbleList)
{
	int nIndex = MIN(MAX(bubbleList.size() - MIN_EXPLODE_COUNT, 0), STREAKS_PLUS_COUNT - 1);

	m_fStreaksPlus = STREAKS_PLUS[nIndex];
}

bool CBubbleLayer::isThisColorInBoard(char nColor)
{
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
	{
		CBubble* bubble = *it;

		char type = bubble->m_byType;
		char color = bubble->m_byColor;

		if (type != BB_NORMAL && type != BB_FRIEND && type != BB_ZOMBIE)
			continue;

		if(color == nColor)
			return true;
	}

	return false;
}

void CBubbleLayer::afterExplodeProc(bool bNeedDropProc, bool bPrepareNextBubble)
{	
	bool haveDrops = false;
	if (bNeedDropProc)
	{
		TBubbleList dropList = getDropBubbleList();

		if (!dropList.empty())
			procDropBubbles(dropList);

		if (m_nFriendCount > 0 && m_nFriendCount == m_nSavedFriends)
			haveDrops = true;

		//if(haveDrops || dropList.size() == m_BubbleList.size())
		if(haveDrops || m_BubbleList.size() == 0)
		{
			schedule(schedule_selector(CBubbleLayer::updateb2PhysicsWorld), 0.03f);

			addGameOverBubblesToPhysicsWorld();
		}
	}

	checkMoveLayer();

	//float delayTime = haveDrops? 2.0f : 0.1f;
	float delayTime = haveDrops? 0.1f : 0.1f;

#ifdef ROBOT_SHOOT
	if(!m_BubbleList.empty() && m_nRemainCount == 0)
	{
		m_nRemainCount = CONTINUE_PLAY_EXTRA_BUBBLES;

		m_pGameRoot->m_NextBubble = createNewBubble(false);
		m_pGameRoot->m_NextBubble->setPosition(m_ptNextBubble);
		m_pGameRoot->m_pAnimLayer->addChild(m_pGameRoot->m_NextBubble);
	}
#endif
	
	if (m_BubbleList.empty())
	{
		setGameState(STATE_END);

		//schedule(schedule_selector(CBubbleLayer::updateb2PhysicsWorld), 0.05f);

#ifdef ROBOT_SHOOT
		unschedule(schedule_selector(CBubbleLayer::shootBubbleAi));
#endif

		scheduleOnce(schedule_selector(CBubbleLayer::endGameSuccess), delayTime);

	} else if (m_nRemainCount == 0)
	{
		setGameState(STATE_END);

		scheduleOnce(schedule_selector(CBubbleLayer::endGameFail), delayTime);

	} else if (bPrepareNextBubble){
		m_pGameRoot->procGameEvent(GS_SEND_NEXT_BUBBLE);
	}
}

void CBubbleLayer::afterExplodeProcForFireballMoving()
{	
	bool haveDrops = false;
		
	TBubbleList dropList = getDropBubbleList();
	/*if (!dropList.empty())
		procDropBubbles(dropList);*/

	if (m_nFriendCount > 0 && m_nFriendCount == m_nSavedFriends)
	{
		//procDropBubbles(m_BubbleList, false);
		haveDrops = true;
		//m_BubbleList.clear();
	}

	if(haveDrops || dropList.size() == m_BubbleList.size())
	{
		schedule(schedule_selector(CBubbleLayer::updateb2PhysicsWorld), 0.03f);

		addGameOverBubblesToPhysicsWorld();
	}
	else
		procDropBubbles(dropList);

	//CBubble* beeBubble = getBeeBubble();
	//if(beeBubble == NULL)
	//	checkMoveLayer();

	//float delayTime = haveDrops? 2.0f : 0.1f;
	float delayTime = haveDrops? 0.1f : 0.1f;

	if (m_BubbleList.empty())
	{
		unschedule(schedule_selector(CBubbleLayer::processFireballMoving));

		setGameState(STATE_END);

		//schedule(schedule_selector(CBubbleLayer::updateb2PhysicsWorld), 0.05f);

		scheduleOnce(schedule_selector(CBubbleLayer::endGameSuccess), delayTime);

	}
}

void CBubbleLayer::addGameOverBubblesToPhysicsWorld()
{
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
	{
		CBubble* bubble = *it;

		stBubbleBack bbk;
		bubble->backupTo(bbk);
		bbk.bPopped = false;

		addHistory(bubble, false);

#ifdef ROBOT_SHOOT
		std::string key = keyForBubble(bubble);
		TMapIntInfo::iterator itcf = mapColorDrop.find(key);
		if (itcf != mapColorDrop.end())
		{
			(*itcf).second ++;
		}
		else
		{
			mapColorDrop.insert(std::make_pair(key, 1));
		}
#endif

		if (bubble != lastBubble)
		{
			backBubbleList.push_back(bbk);
		}

		if (bubble->m_byType == BB_NORMAL || bubble->m_byType == BB_FRIEND)
			bubble->setTexture(CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/bubbles/fruit_%d.png", bubble->m_byColor)->getCString()));

		if(bubble->m_byType == BB_BEE) {			
			m_pGameRoot->saveBee(bubble, getRandRange(0, 1), 0.0f);
		}else {
			// Create ball body and shape
			b2BodyDef ballBodyDef;
			ballBodyDef.type = b2_dynamicBody;
			ballBodyDef.position.Set(bubble->getPositionX() / PTM_RATIO, bubble->getPositionY() / PTM_RATIO);
			ballBodyDef.userData = bubble;
			bubble->m_physicsBody = b2PhysicsWorld->CreateBody(&ballBodyDef);
			bubble->m_bGameOverBubble = true;
			bubble->stopAllActions();

			b2CircleShape circle;
			circle.m_radius = CBubble::SIZE / 2 / PTM_RATIO;

			b2FixtureDef ballShapeDef;
			ballShapeDef.shape = &circle;
			ballShapeDef.density = 0.4f;
			ballShapeDef.friction = 1.0f;
			ballShapeDef.restitution = getRandRange(7, 10) * 0.1f;
			bubble->m_physicsBody->CreateFixture(&ballShapeDef);


			int xImp = getRandRange(0, 1) ? getRandRange(5, 7) * 3 : 0 - (int)getRandRange(5, 7) * 3;
			//int yImp = xImp > 0 ? getRandRange(5, 8) * 5 : getRandRange(9, 12) * 5;
			b2Vec2 force = b2Vec2(xImp, 2);
			//b2Vec2 force = b2Vec2(xImp, 0);

			bubble->m_physicsBody->ApplyLinearImpulse(force, bubble->m_physicsBody->GetPosition());
		}
	}

	m_BubbleList.clear();

	if(LCommon::sharedInstance()->curLevelType == LVL_DEFEAT_THE_MOWS)
		animateMorFlyToNext();

}

//Checked
bool CBubbleLayer::processNeighbor(CBubble* src, TBubbleList& candiList, int depth, bool isFindBestAngle)
{
	int matchCount = 0;
	for (int i = 0; i < NEIGHBOR_COUNT; i++)
	{
		CBubble* neighbor = src->m_pNeighbors[i];
		if (neighbor == NULL) continue;
		if (neighbor->m_bChecked) continue;

		if (neighbor->m_byType == BB_MOR/* && neighbor->m_byColor == 0*/)
		{
			CBubble* masterMor = neighbor->m_pMorMaster;

			if(depth == 0 && src->m_byColor == masterMor->m_byColor)
			{
				candiList.push_back(masterMor);
				masterMor->m_bChecked = true;

				for (int j = 0; j < NEIGHBOR_COUNT; j++)
				{
					CBubble* slaveMor = masterMor->m_pMorSlave[j];					
					candiList.push_back(slaveMor);
					slaveMor->m_bChecked = true;
				}

				for (int j = 0; j < NEIGHBOR_COUNT; j++)
				{
					CBubble* slaveMor = masterMor->m_pMorSlave[j];
					
					char tempColor = slaveMor->m_byColor;
					slaveMor->m_byColor = masterMor->m_byColor;

					processNeighbor(slaveMor, candiList, depth + 1, isFindBestAngle);
					slaveMor->m_byColor = tempColor;
				}
			}

			if(!isFindBestAngle && depth == 0 && masterMor->m_bHasMor && src->m_byColor != masterMor->m_byColor)
			{
				char newColor = masterMor->m_byColor +  + getRandRange(COLOR_MIN, COLOR_MAX - 1);
				newColor = newColor % MAX_COLORS + (newColor >= MAX_COLORS ? 1 : 0);
				masterMor->changeTo(BB_MOR, newColor);
			}

			continue;
		}

		neighbor->m_bChecked = true;

		if (neighbor->m_byType != BB_NORMAL 
			&& neighbor->m_byType != BB_FRIEND
			&& neighbor->m_byType != BB_ZOMBIE) continue;

		if (neighbor->m_byColor != src->m_byColor) continue;

		candiList.push_back(neighbor);

		processNeighbor(neighbor, candiList, depth + 1, isFindBestAngle);
	}

	return true;
}

//Checked
bool CBubbleLayer::processNeighborForBounce(CBubble* src, float fBounce)
{
	for (int i = 0; i < NEIGHBOR_COUNT; i++)
	{
		CBubble* neighbor = src->m_pNeighbors[i];
		if (neighbor == NULL) continue;
		if (neighbor->m_bChecked) continue;
		if (neighbor->m_byType == BB_STONE) continue;

		// add animation for this
		float dx = (neighbor->getPositionX() - src->getPositionX()) * fBounce;
		float dy = (neighbor->getPositionY() - src->getPositionY()) * fBounce;
		if (dx < 2 && dy < 2) continue;

		CCActionInterval* mov1 = CCMoveBy::create(0.1f, ccp(dx, dy));

		neighbor->runAction(CCSequence::create(mov1, mov1->reverse(), NULL));
        playEffect(kSND_SFX_BOUNCE);
		//  

		neighbor->m_bChecked = true;
		processNeighborForBounce(neighbor, fBounce * 0.3f);
	}
	return true;
}

//Checked
bool CBubbleLayer::findBubbles(CBubble* src, bool isByColor, char color, int nCount, TBubbleList& bubbleList)
{
	int nDepth = 1;
	while (nCount > 0)
	{
		int nPrev = nCount;
		if (findBubbles(src, isByColor, color, nDepth, nCount, bubbleList)) break;

		nDepth++;

		if (nDepth > 100) break;
	}

	return false;
}

//Checked
bool CBubbleLayer::findBubbles(CBubble* src, bool isByColor, char color, int depth, int& nCount, TBubbleList& bubbleList)
{
	int nDist = (CBubble::SIZE * depth + 5);
	int nDist2 = nDist * nDist;
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
	{
		CBubble* bubble = *it;
		if (getDistance2(src->getPosition(), bubble->getPosition()) > nDist2) continue;
		if (isByColor && 
			(BB_NORMAL != bubble->m_byType) && 
			(BB_FRIEND != bubble->m_byType)) continue;
		if (BB_NONE != color && color != bubble->m_byColor) continue;
		if (bubble->m_bChecked) continue;
		bubble->m_bChecked = true;

		bubbleList.push_back(bubble);
		nCount--;
		if (nCount <= 0) return true;
	}

	return false;
}

bool CBubbleLayer::processExplodeBubble(TBubbleList& bubbleList, float fDelay, bool bNuke)
{
	float delayTime = fDelay;
    const char* sndPop = bubbleList.size() < 6? kSND_SFX_BUBBLE_POP : kSND_SFX_BUBBLE_POP;
	m_nTotalPopped += bubbleList.size();
    int nDecreaseCount = 0;

	float minPosY = SC_HEIGHT;
	for (TBubbleList::iterator it = bubbleList.begin(); it != bubbleList.end(); it++)
	{
		CBubble* bubble = *it;
		if(bubble->getPositionY() < minPosY)
			minPosY = bubble->getPositionY();
	}

	bool hasZombieEx = false;
	
	do 
	{
		CBubble* bubble = bubbleList.back();
		bubbleList.pop_back();

		stBubbleBack bbk;
		bubble->backupTo(bbk);
		bbk.bPopped = true;

		addHistory(bubble, true);

#ifdef ROBOT_SHOOT
		std::string key = keyForBubble(bubble);
		TMapIntInfo::iterator it = mapColorPop.find(key);
		if (it != mapColorPop.end())
		{
			(*it).second ++;
		}
		else
		{
			mapColorPop.insert(std::make_pair(key, 1));
		}
#endif

//		for (int i = 0; i < NEIGHBOR_COUNT; i++)
//		{
//			CBubble* neighbor = bubble->m_pNeighbors[i];
//			if (NULL == neighbor) continue;
//			neighbor->removeNeighbor(bubble);
//			if (neighbor->m_byType == BB_ICE && bubble->m_byType == BB_NORMAL)
//			{
//				stBubbleBack bbk_ice;
//				neighbor->backupTo(bbk_ice);
//				bbk_ice.bPopped = true;
//				bbk_ice.tag = neighbor;
//				bbk_ice.bEnable = false;
//				backBubbleList.push_back(bbk_ice);
//
//				neighbor->changeTo(BB_NORMAL, bubble->m_byColor);
//				neighbor->m_bChecked = true;
////				createExplodeAnimeForNormal(neighbor, delayTime);
//
//				playEffect(kSND_SFX_SNOWFLAKE);
//			}
//		}

		if (bubble->m_byType == BB_ZOMBIE && 
			bubble->getTag() != BB_BOMB &&
			bubble->getTag() != BB_NUKE) {
			if (!bNuke)
			{
                nDecreaseCount += ZOMBIE_DECREASE;
				hasZombieEx = true;
				bbk.bEnable = false;
			}
		} else if (bubble->m_byType == BB_FRIEND) {
			bubble->prepareDrop();
			bbk.tag = saveFriend(bubble, delayTime);
		} else if (bubble->m_byType == BB_STONE) {
			playEffect(kSND_SFX_STONE);
		} else if (bubble->m_byType == BB_MAGNET) {
			TPointMap::iterator it = m_vecMagnets.find(bubble->m_nUID);
			if (it != m_vecMagnets.end()) m_vecMagnets.erase(it);
		}

		removeBubbleFromList(bubble);

		for (int i = 0; i < NEIGHBOR_COUNT; i++)
		{
			CBubble* neighbor = bubble->m_pNeighbors[i];
			if (NULL == neighbor) continue;
			neighbor->removeNeighbor(bubble);
		}

		delayTime = (bubble->getPositionY() - minPosY) / CBubble::SIZE_OV * 0.08f;

		animateExplode(bubble, delayTime);

		/*if (bubble->getTag() == BB_BOMB)
			delayTime += 0.005f;
		else
			delayTime += 0.02f;*/

		//playEffect(sndPop, 0.011f);

		if (bubble != lastBubble)
		{
			backBubbleList.push_back(bbk);
		}

	} while (!bubbleList.empty());

    decreaseRemainCount(nDecreaseCount, true);

	if (hasZombieEx && !bNuke)
	{
		playEffect(kSND_SFX_ZOMBIE);
	}

	return true;
}

bool CBubbleLayer::processExplodeBubble(CBubble* bubble, float fDelay, bool bNuke)
{
	float delayTime = fDelay;
    const char* sndPop = kSND_SFX_BUBBLE_POP;
	m_nTotalPopped += 1;
    int nDecreaseCount = 0;

	bool hasZombieEx = false;
	

	stBubbleBack bbk;
	bubble->backupTo(bbk);
	bbk.bPopped = true;

	addHistory(bubble, true);

#ifdef ROBOT_SHOOT
	std::string key = keyForBubble(bubble);
	TMapIntInfo::iterator it = mapColorPop.find(key);
	if (it != mapColorPop.end())
	{
		(*it).second ++;
	}
	else
	{
		mapColorPop.insert(std::make_pair(key, 1));
	}
#endif

//		for (int i = 0; i < NEIGHBOR_COUNT; i++)
//		{
//			CBubble* neighbor = bubble->m_pNeighbors[i];
//			if (NULL == neighbor) continue;
//			neighbor->removeNeighbor(bubble);
//			if (neighbor->m_byType == BB_ICE && bubble->m_byType == BB_NORMAL)
//			{
//				stBubbleBack bbk_ice;
//				neighbor->backupTo(bbk_ice);
//				bbk_ice.bPopped = true;
//				bbk_ice.tag = neighbor;
//				bbk_ice.bEnable = false;
//				backBubbleList.push_back(bbk_ice);
//
//				neighbor->changeTo(BB_NORMAL, bubble->m_byColor);
//				neighbor->m_bChecked = true;
////				createExplodeAnimeForNormal(neighbor, delayTime);
//
//				playEffect(kSND_SFX_SNOWFLAKE);
//			}
//		}

	if (bubble->m_byType == BB_ZOMBIE && 
		bubble->getTag() != BB_BOMB &&
		bubble->getTag() != BB_NUKE)
	{
		if (!bNuke)
		{
            nDecreaseCount += ZOMBIE_DECREASE;
			hasZombieEx = true;
			bbk.bEnable = false;
		}
	} 
	else if (bubble->m_byType == BB_FRIEND) {
		bubble->prepareDrop();
		bbk.tag = saveFriend(bubble, delayTime);
	} else if (bubble->m_byType == BB_STONE) {
		playEffect(kSND_SFX_STONE);
	} else if (bubble->m_byType == BB_MAGNET)
	{
		TPointMap::iterator it = m_vecMagnets.find(bubble->m_nUID);
		if (it != m_vecMagnets.end()) m_vecMagnets.erase(it);
	}

	//removeBubbleFromList(bubble);

	delayTime = 0.01f;

	animateExplode(bubble, delayTime, true);

	/*if (bubble->getTag() == BB_BOMB)
		delayTime += 0.005f;
	else
		delayTime += 0.02f;*/

	//playEffect(sndPop, 0.011f);

	if (bubble != lastBubble)
	{
		backBubbleList.push_back(bbk);
	}


    decreaseRemainCount(nDecreaseCount, true);

	if (hasZombieEx && !bNuke)
	{
		playEffect(kSND_SFX_ZOMBIE);
	}

	return true;
}

TBubbleList CBubbleLayer::getDropBubbleList()
{
	clearBubbleCheck();

	TBubbleList dropList;
	unsigned int nFindCount = 0;
	while (nFindCount < m_BubbleList.size())
	{
		CBubble* startBubble = NULL;
		for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		{
			if (! (*it)->m_bChecked)
			{
				startBubble = *it;
				break;
			}
		}
		if (startBubble == NULL) break;

		TBubbleList bubbleList;
		bool bDrop = findDropBubbles(startBubble, bubbleList);
		nFindCount += bubbleList.size();
		if (bDrop)
			dropList.insert(dropList.end(), bubbleList.begin(), bubbleList.end());
	}
	
	checkBeeBubbleAround(dropList);

	checkMorBubbleAround(dropList);

	return dropList;
}

void CBubbleLayer::checkBeeBubbleAround(TBubbleList& dropList)
{
	CBubble* beeBubble = getBeeBubble();
	if(beeBubble)
	{//check if bee bubble is free from around bubbles
		int i = 0;
		for (i = 0; i < NEIGHBOR_COUNT; i++)
		{
			CBubble* neighbor = beeBubble->m_pNeighbors[i];
			if(neighbor != NULL && /*std::*/find(dropList.begin(), dropList.end(), neighbor) == dropList.end())
				break;
		}

		if(i >= NEIGHBOR_COUNT)
		{//bee bubble is free from around bubbles

			dropList.clear();
			dropList.insert(dropList.end(), m_BubbleList.begin(), m_BubbleList.end());
		}
	}
}

CBubble* CBubbleLayer::getBeeBubble()
{
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		if ((*it)->m_byType == BB_BEE)
			return *it;

	return NULL;
}

void CBubbleLayer::checkMorBubbleAround(TBubbleList& dropList)
{
	if(LCommon::sharedInstance()->getCurLevelType() == LVL_DEFEAT_THE_MOWS)
	{
		clearBubbleCheck();

		CBubble* morBubble = getBubbleWithMor();
		if(morBubble) {
			morBubble->m_bChecked = true;
			int i = 0;
			for (i = 0; i < NEIGHBOR_COUNT; i++)
			{
				CBubble* slaveMor = morBubble->m_pMorSlave[i];
				int j = 0;
				for (j = 0; j < NEIGHBOR_COUNT; j++)
				{
					CBubble* neighbor = slaveMor->m_pNeighbors[j];
					if(neighbor != NULL && neighbor->m_byType != BB_MOR && find(dropList.begin(), dropList.end(), neighbor) == dropList.end())
						break;
				}
				if(j < NEIGHBOR_COUNT)
					break;
			}

			if(i >= NEIGHBOR_COUNT)
			{//mor bubble is free from around bubbles

				dropList.insert(dropList.end(), morBubble);
				for (int i = 0; i < NEIGHBOR_COUNT; i++)
					dropList.insert(dropList.end(), morBubble->m_pMorSlave[i]);
			}

		} else if(morTemplatesCount() == 0) {
			dropList.clear();
			dropList.insert(dropList.end(), m_BubbleList.begin(), m_BubbleList.end());
		}
	}
}

int CBubbleLayer::morTemplatesCount()
{
	int counts = 0;
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		if ((*it)->m_byType == BB_MOR && (*it)->m_byColor != 0)
			counts++;

	return counts;
}

CBubble* CBubbleLayer::getBubbleWithMor()
{
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		if ((*it)->m_byType == BB_MOR && (*it)->m_byColor > 0 && (*it)->m_byColor < 7 && (*it)->m_bHasMor && (*it)->m_bChecked == false)
			return *it;

	return NULL;
}

bool CBubbleLayer::animateMorFlyToNext()
{
	CCPoint ptLowestMorPt = ccp(0, INT_MAX);
	CBubble* lowestMorBubble = NULL;

	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
	{
		if ((*it)->m_byType == BB_MOR && (*it)->m_byColor != 0 && (*it)->getPositionY() < ptLowestMorPt.y )
		{
			lowestMorBubble = *it;
			ptLowestMorPt = lowestMorBubble->getPosition();
		}
	}

	CCPoint ptNext;
	bool bRet;
	float fSpeed;

	if(abs(ptLowestMorPt.y - INT_MAX) < FLT_EPSILON)
	{
		ptNext = ccp(rcBound.origin.x + rcBound.size.width / 2 , rcBound.getMaxY() + 300);
		fSpeed = BUBBLE_SPEED / 5;
		bRet = false;
	}else{
		ptNext = ptLowestMorPt;
		fSpeed = BUBBLE_SPEED / 3;
		bRet = true;
	}

	if(lowestMorBubble)
		lowestMorBubble->m_bHasMor = true;

	float dist = getDistance(m_pMor->getPosition(), ptNext);
	float fMoveTime = fabs(dist) / fSpeed;

#ifdef ROBOT_SHOOT
	m_pGameRoot->m_fMoveLayerTime = MAX(m_pGameRoot->m_fMoveLayerTime, fMoveTime);
#endif

	CCSequence* seq;

	if(lowestMorBubble)
	{
		CCCallFuncN* callback1 = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::morPrejump));
		CCDelayTime* delay1 = CCDelayTime::create(0.3f);
		
		CCCallFuncN* callback2 = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::morJump));
		
		CCMoveTo* moveTo = CCMoveTo::create(fMoveTime, ptNext);
		CCScaleTo* scale1 = CCScaleTo::create(fMoveTime / 2, 2.0f);
		CCScaleTo* scale2 = CCScaleTo::create(fMoveTime / 2, 1.0f);
		CCSequence* seq1 = CCSequence::create(scale1, scale2, NULL);
		CCSpawn* spawn = CCSpawn::create(moveTo, seq1, NULL);

		CCCallFuncN* callback3 = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::morPrejump));
		CCDelayTime* delay2 = CCDelayTime::create(0.15f);

		CCCallFuncN* callback4 = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::morNormal));

		CCCallFuncND* callback5 = CCCallFuncND::create(this, callfuncND_selector(CBubbleLayer::finishedMorJump), lowestMorBubble);
		
		seq = CCSequence::create(callback1, delay1, callback2, spawn, callback3, delay2, callback4, callback5, NULL);
	}else{
		CCCallFuncN* callback1 = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::morFlying));

		CCScaleTo* scale = CCScaleTo::create(fMoveTime, 2.5f);
		CCRotateBy* rotate = CCRotateBy::create(fMoveTime, 360);
		CCMoveTo* moveTo = CCMoveTo::create(fMoveTime, ptNext);
		CCSpawn* spawn = CCSpawn::create(scale, rotate, moveTo, NULL);

		CCDelayTime* delay = CCDelayTime::create(0.1f);

		CCCallFuncN* callback2 = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::finishedMorFlying));

		seq = CCSequence::create(callback1, spawn, delay, callback2, NULL);
	}
	
	m_pMor->runAction(seq);

	return bRet;
}

void CBubbleLayer::morNormal(CCNode* node)
{
	CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/animation/Mor/mor.png")->getCString());
	m_pMor->setTexture(tex);
}

void CBubbleLayer::morPrejump(CCNode* node)
{
	CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/animation/Mor/mor_prejump.png")->getCString());
	m_pMor->setTexture(tex);
}

void CBubbleLayer::morJump(CCNode* node)
{
	CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/animation/Mor/mor_jump.png")->getCString());
	m_pMor->setTexture(tex);
}

void CBubbleLayer::morFlying(CCNode* node)
{
	CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/animation/Mor/mor_fly.png")->getCString());
	m_pMor->setTexture(tex);
}

void CBubbleLayer::finishedMorJump(CCNode* node, void* data)
{
	CBubble* lowestMorBubble = (CBubble*) data;
	
	lowestMorBubble->m_byColor = getRandRange(1, 6);
	lowestMorBubble->changeTo(lowestMorBubble->m_byType, lowestMorBubble->m_byColor);

	afterExplodeProc(true, false);
}

void CBubbleLayer::finishedMorFlying(CCNode* node)
{
	m_pGameRoot->procGameEvent(GS_ANIM_MORCRASHED);
}

bool CBubbleLayer::findDropBubbles(CBubble* bubble, TBubbleList& bubbleList)
{
	bool bDrop = true;
	
	if (false == bubble->m_bChecked)
	{
		bDrop = isDropable(bubble);
		bubble->m_bChecked = true;
		bubbleList.push_back(bubble);
	}

	CBubble* neighbor;
	for (int i = 0; i < NEIGHBOR_COUNT; i++)
	{
		neighbor = bubble->m_pNeighbors[i];
		if (neighbor == NULL) continue;
		if (neighbor->m_bChecked) continue;
		neighbor->m_bChecked = true;

		bDrop &= isDropable(neighbor);
		bubbleList.push_back(neighbor);

		bDrop &= findDropBubbles(neighbor, bubbleList);
	}
	return bDrop;
}

bool CBubbleLayer::isDropable(CBubble* bubble)
{
	if ((bubble->getPositionY() >= rcBound.getMaxY() && LCommon::sharedInstance()->getCurLevelType() != LVL_SAVE_THE_BEE) || bubble->m_byType == BB_BEE || bubble->m_byType == BB_MOR) return false;
	//if (bubble->getPositionY() >= rcBound.getMaxY()) return false;

	return true;
}

void CBubbleLayer::procDropBubbles(TBubbleList& bubbleList, bool bRemove)
{
	m_nTotalDropped += bubbleList.size();

	for (TBubbleList::iterator it = bubbleList.begin(); it != bubbleList.end(); it++)
	{
		CBubble* bubble = *it;

		stBubbleBack bbk;
		bubble->backupTo(bbk);
		bbk.bPopped = false;

		addHistory(bubble, false);

#ifdef ROBOT_SHOOT
		std::string key = keyForBubble(bubble);
		TMapIntInfo::iterator itcf = mapColorDrop.find(key);
		if (itcf != mapColorDrop.end())
		{
			(*itcf).second ++;
		}
		else
		{
			mapColorDrop.insert(std::make_pair(key, 1));
		}
#endif
		if (bRemove)
			removeBubbleFromList(bubble);

		float time = 2.0f * (bubble->getPositionY() + getPositionY()) / SC_HEIGHT;
		CCCallFuncN* func = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::finishedDrop));

		float dstX = getRandRange(rcBound.getMinX(), rcBound.getMaxX());

		if (bubble->m_byType == BB_FRIEND)
		{
			bbk.tag = saveFriend(bubble, 0);
			bubble->prepareDrop();
		} else if (bubble->m_byType == BB_MAGNET)
		{
			TPointMap::iterator it = m_vecMagnets.find(bubble->m_nUID);
			if (it != m_vecMagnets.end()) m_vecMagnets.erase(it);
		}

		CCActionInterval* act;
		if (bubble->m_byType == BB_NORMAL || bubble->m_byType == BB_FRIEND)
		{
			bubble->setTexture(CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/bubbles/fruit_%d.png", bubble->m_byColor)->getCString()));
            bubble->stopAllActions();

			CCActionInterval* rot = CCRotateBy::create(time, 540);
			CCActionInterval* fadeout = CCFadeIn::create(time * 0.1f);
			CCActionInterval* delay = CCDelayTime::create(time * 0.3f);
			CCActionInterval* fadein = CCFadeOut::create(time * 0.8f);
			CCActionInterval* fadeseq = CCSequence::create(fadeout, delay, fadein, NULL);
			CCActionInterval* jmp = CCJumpTo::create(time, ccp(dstX, m_ptStart.y + 50), 300, 1);
			act = CCSpawn::create(rot, fadeseq, jmp, NULL);

			CCSequence* seq = CCSequence::createWithTwoActions(act, func);
			bubble->runAction(seq);
		}
		if(bubble->m_byType == BB_BEE) {
			/*CCActionInterval* delay = CCDelayTime::create(0.3f);
			CCActionInterval* jmp = CCJumpTo::create(time, ccp(dstX, m_ptStart.y + 50), 300, 1);
			act = CCSequence::create(delay, jmp, NULL);*/
			m_pGameRoot->saveBee(bubble, getRandRange(0, 1), 0.0f);
		}
		else
		{
			act = CCJumpTo::create(time, ccp(dstX, m_ptStart.y + 50), 300, 1);

			CCSequence* seq = CCSequence::createWithTwoActions(act, func);
			bubble->runAction(seq);
		}

		if(bubble->m_byType == BB_MOR && bubble->m_byColor != 0)
			animateMorFlyToNext();

		if (bubble != lastBubble)
		{
			backBubbleList.push_back(bbk);
		}
	}
}

bool isSameBubble(CBubble* bubble)
{
	return true;
}

//Checked
void CBubbleLayer::removeBubbleFromList(CBubble* bubble)
{
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		if (*it == bubble) 
		{
			m_BubbleList.erase(it);
			break;
		}
}

bool CBubbleLayer::getNextPointForFireball(CCPoint pos, float ballSize, CCPoint& ptNext, int& flyOption)
{
	float xMin = m_fMargin + CBubble::SIZE / 2 - 150;
	float xMax = SC_WIDTH - m_fMargin - CBubble::SIZE / 2 + 150;

	ptNext.x = (cos(m_fLaunchAngle) < 0)? xMin : xMax;
	ptNext.y = pos.y + (ptNext.x - pos.x) * tan(m_fLaunchAngle);

	if (ptNext.y > rcBound.getMaxY() + 150) 
	{
		ptNext.y = rcBound.getMaxY() + 150;
		float rdx = (ptNext.y - pos.y) / tanf(m_fLaunchAngle);
		ptNext.x = pos.x + rdx;
	}

	return false;
}

bool CBubbleLayer::getNextPoint(CCPoint pos, float ballSize, CCPoint& ptNext, int& flyOption)
{
	float xMin = m_fMargin + CBubble::SIZE / 2 - 20;
	float xMax = SC_WIDTH - m_fMargin - CBubble::SIZE / 2 + 20;

	ptNext.x = (cos(m_fLaunchAngle) < 0)? xMin : xMax;
	ptNext.y = pos.y + (ptNext.x - pos.x) * tan(m_fLaunchAngle);

	bool bRet = true;

	if (ptNext.y > rcBound.getMaxY()) 
	{
		ptNext.y = rcBound.getMaxY();
		float rdx = (ptNext.y - pos.y) / tanf(m_fLaunchAngle);
		ptNext.x = pos.x + rdx;

		m_bLaunchBubbleUp = false;
		m_bLaunchBubbleDirectionChanged = true;

		if(LCommon::sharedInstance()->getCurLevelType() != LVL_SAVE_THE_BEE)
			bRet = false;
	}
	else if(ptNext.y < rcBound.getMinY())
	{
		ptNext.y = rcBound.getMinY();
		float rdx = (ptNext.y - pos.y) / tanf(m_fLaunchAngle);
		ptNext.x = pos.x + rdx;

		m_bLaunchBubbleUp = true;
		m_bLaunchBubbleDirectionChanged = true;
	}else{
		m_bLaunchBubbleDirectionChanged = false;
	}

	CBubble* ColBubble = NULL;
	float minDist = SC_HEIGHT * SC_HEIGHT;
	float checkRound = CBubble::SIZE * 0.8;
	int id2 = -1;
	CCPoint ptCross, ptAcrossFinal;
	for (unsigned int i = 0; i < m_BubbleList.size(); i++)
	{
		CBubble* bubble = m_BubbleList[i];
		bool bAcross = getAcrossPoint(bubble->getPosition(), checkRound, pos, ptNext, ptCross);
		float fAlphaNext = getAlpha(pos, ptNext);
		float fAlphaCross = getAlpha(pos, ptCross);

		if (!bAcross) continue;
		if (ptCross.x < xMin || xMax < ptCross.x) continue;
		if (fabs(fAlphaCross - fAlphaNext) > M_PI_2) continue;

		float distFromStart = getDistance2(ptCross, pos);
		if (distFromStart < minDist)
		{
			ColBubble = bubble;
			minDist = distFromStart;
			ptAcrossFinal = ptCross;
			id2 = i;
		}
	}

	if (ColBubble != NULL)
	{
		ptNext = ptAcrossFinal;

		bool bReCalcY = false;;
		if (ptNext.x < xMin) 
		{
			ptNext.x = xMin;
			bReCalcY = true;
		}
		else if (xMax < ptNext.x)
		{
			ptNext.x = xMax;
			bReCalcY = true;
		}

		if (bReCalcY)
			ptNext.y = pos.y + (ptNext.x - pos.x) * tanf(m_fLaunchAngle);

		bRet = false;
	}

	int nFlyOption = FO_NORMAL;
	// for magnet
	if (!m_vecMagnets.empty() && !m_pGameRoot->m_hasSniperAim)
	{
		bool hasMagCurve = false;
		float MAG_MIN_DIST = CBubble::SIZE * CBubble::SIZE / 3;
		float MAG_DIST2 = CBubble::SIZE * CBubble::SIZE * 4;
		float minDist = SC_HEIGHT;
		CCPoint fMagPos;
		for (TPointMap::iterator it = m_vecMagnets.begin(); it != m_vecMagnets.end(); it++)
		{
			CCPoint& magPos = it->second;
			float distN = getDistFromLine2(magPos, pos, ptNext);
			if (distN < MAG_MIN_DIST || MAG_DIST2 < distN) continue;

			float fdist = getDistance(pos, magPos);
			if (fdist > minDist) continue;

			float alpha1 = getAlpha(pos, magPos);
			float alpha2 = getAlpha(pos, ptNext);
			if (fabs(alpha1 - alpha2) > M_PI_2) continue;

			minDist = fdist;
			hasMagCurve = true;
			fMagPos = it->second;
		}

		if (hasMagCurve)
		{
			float alpha1 = getAlpha(pos, fMagPos);
			float alpha2 = getAlpha(pos, ptNext);
			float da = fabs(alpha2 - alpha1);

			float dist2 = getDistance(pos, fMagPos);
			float newD = dist2 * cos(da) - dist2 * sin(da);
			CCPoint ptNew = ccp(pos.x + newD * cos(alpha2), pos.y + newD * sin(alpha2));
			if (ptNew.y - pos.y < 5) ptNew.y = pos.y + 5;

			if (getDistance(pos, ptNext) - getDistance(pos, ptNew) > CBubble::SIZE / 2)
			{
				ptNext = ptNew;
				m_fLaunchAngle = getAlpha(ptNew, fMagPos);
				nFlyOption = FO_MAGNET;
				bRet = true;
			}
		}
	}

	if (bRet && nFlyOption != FO_MAGNET)
	{
		if(m_bLaunchBubbleDirectionChanged)
		{
			m_fLaunchAngle = -m_fLaunchAngle;
		}else{
			int nSign = m_bLaunchBubbleUp ? 1 : -1;
			m_fLaunchAngle = nSign * M_PI - m_fLaunchAngle;
		}
	}

	flyOption = nFlyOption;

	return bRet;
}

void CBubbleLayer::onEnter()
{
	CCLayer::onEnter();

	setPosition(ccp(0, SC_ADHEIGHT));
	setAnchorPoint(CCPointZero);

	//	setScale(SC_RATIO);


	// Create a world
	b2Vec2 gravity = b2Vec2(0.0f, -25.0f);
	b2PhysicsWorld = new b2World(gravity);

	// Create edges around the entire screen
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0,0);

	b2Body *groundBody = b2PhysicsWorld->CreateBody(&groundBodyDef);
	b2EdgeShape groundEdge;
	b2FixtureDef boxShapeDef;
	boxShapeDef.shape = &groundEdge;

	//CCRect wallRect = CCRectMake(absBound.getMinX() / PTM_RATIO, absBound.getMinY() / PTM_RATIO, absBound.size.width / PTM_RATIO, absBound.size.height / PTM_RATIO);
	CCRect wallRect = CCRectMake(0 / PTM_RATIO, 0 / PTM_RATIO, SC_WIDTH / PTM_RATIO, SC_HEIGHT / PTM_RATIO);

	//wall definitions
	groundEdge.Set(b2Vec2(wallRect.getMinX(), wallRect.getMinY()), b2Vec2(wallRect.getMaxX(), wallRect.getMinY()));
	groundBody->CreateFixture(&boxShapeDef);

	groundEdge.Set(b2Vec2(wallRect.getMinX(), wallRect.getMinY()), b2Vec2(wallRect.getMinX(), wallRect.getMaxY()));
	groundBody->CreateFixture(&boxShapeDef);

	groundEdge.Set(b2Vec2(wallRect.getMinX(), wallRect.getMaxY()), b2Vec2(wallRect.getMaxX(), wallRect.getMaxY()));
	groundBody->CreateFixture(&boxShapeDef);

	groundEdge.Set(b2Vec2(wallRect.getMaxX(), wallRect.getMaxY()), b2Vec2(wallRect.getMaxX(), wallRect.getMinY()));
	groundBody->CreateFixture(&boxShapeDef);
}


bool CBubbleLayer::ccTouchBegan(CCTouch* touches, CCEvent* event)
{
	if (m_bLayerMoving) return true;

	CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();
	CCSize viewSize = pEGLView->getFrameSize();
	CCLog("CBubbleLayer screen size : %f : %f", viewSize.width, viewSize.height);

	CCPoint pt2 = touches->getLocation();
	CCLog("CBubbleLayer touch pos1 %f:%f", pt2.x, pt2.y);

	if (pt2.y > absBound.getMaxY() + CBubble::SIZE) return false;
    
	pt2.y += getLayerDiff();

	//if(m_pGameRoot->boostPaneRect.containsPoint(pt2)) return false;

	if (m_bPinPopping)
	{
		pinPopBubble(pt2);
//		m_ptPinPos = pt2;
		return false;
	}

	m_fLaunchAngle = getAlpha(m_ptStart, pt2);
	if (sin(m_fLaunchAngle) < 0.3f) 
	{
		m_AimLayer->setVisible(false);
		return true;
	}

	updateTargetMove();
	
	return true;
}

void CBubbleLayer::ccTouchMoved(CCTouch* touches, CCEvent* event)
{
	if (m_bPinPopping || m_bLayerMoving) return;

	CCPoint pt2 = touches->getLocation();
	if (pt2.y > absBound.getMaxY() + CBubble::SIZE)
    {
		m_AimLayer->setVisible(false);
        return;
    }
	pt2.y += getLayerDiff();

	m_fLaunchAngle = getAlpha(m_ptStart, pt2);

	if (sin(m_fLaunchAngle) < 0.3f) 
	{
		m_AimLayer->setVisible(false);
		return;
	}

	updateTargetMove();
}

void CBubbleLayer::ccTouchEnded(CCTouch* touches, CCEvent* event)
{
	if (m_bLayerMoving) return;

	CCPoint pt2 = touches->getLocation();
	if (pt2.y > absBound.getMaxY() + CBubble::SIZE)
    {
		m_AimLayer->setVisible(false);
        return;
    }

	pt2.y += getLayerDiff();

// 	if (m_bPinPopping)
// 	{
// 		if (getDistance2(pt2, m_ptPinPos) < 30)
// 			pinPopBubble(pt2);
// 		return;
// 	}
	if (LCommon::sharedInstance()->isTapToShoot)
	{
		m_pGameRoot->procGameEvent(GS_LAUNCH_BUBBLE);
	}
}

void CBubbleLayer::ccTouchCancelled(CCTouch* touches, CCEvent* event)
{
	m_AimLayer->setVisible(false);
}


void CBubbleLayer::pinPopBubble(CCPoint pinPos)
{
	CBubble* dstBubble = NULL;
	float dist = CBubble::SIZE * CBubble::SIZE / 4;
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
	{
		CCPoint pos = (*it)->getPosition();
		if (getDistance2(pos, pinPos) < dist) 
		{
			dstBubble = *it;
			m_BubbleList.erase(it);
			break;
		}
	}

	if (NULL == dstBubble) return;

	m_bPinPopping = false;

	backBubbleList.clear();

	TBubbleList bblList;
	bblList.push_back(dstBubble);

	processExplodeBubble(bblList, 0, false);

	afterExplodeProc(true);
}

bool CBubbleLayer::checkCollision(const CCPoint& fixPos, const CCPoint& startPos, const CCPoint& endPos)
{
	float radius = CBubble::SIZE / 2 + 2 * SC_IF;
	float lineAlpha = getAlpha(startPos, endPos);

	float dist1 = getDistFromLine(fixPos, startPos, endPos);
	return dist1 <= radius;
}

// added by KUH in 2014-10-07
static bool bUseFriendColor = false;

CBubble* CBubbleLayer::createNewBubble(bool bNotify)
{
	char nn = 0;
	/*if (m_nCurShootIndex < m_pPrepareColorList.size())
		nn = m_pPrepareColorList.at(m_nCurShootIndex++);
	else
		nn = getRandRange(1, 6);*/
	
	nn = getNewBubbleColorByBoardState();
	//nn = 2;

    ////////////////////////////////////////////// added by KUH in 2014-10-06 ////////////////////////////////////////////
    
    int iFriendBblCnt = 0;
    int iFriendColor = 0;
    
	for (TBubbleList::reverse_iterator it = m_BubbleList.rbegin(); it != m_BubbleList.rend(); it++)
    {
		CBubble* bbl = *it;
        char byType = bbl->m_byType;
        
        if(byType == BB_FRIEND)
        {
            iFriendBblCnt++;
            iFriendColor = bbl->m_byColor;
        }
    }
    
    if (iFriendBblCnt == 1) {
        bUseFriendColor = !bUseFriendColor;
        if(bUseFriendColor)
            nn = iFriendColor;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
	CBubble* bubble = new CBubble(BB_NORMAL, nn);
	bubble->autorelease();

	return bubble;
}

int CBubbleLayer::getNewBubbleColorByBoardState()
{
	set_BubbleExistArr();

	int nColorsCount[COL_ANY] = {0,};
	int nTotalColorsCount = 0;

	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
	{
		CBubble* bubble = *it;
		
		char type = bubble->m_byType;
		char color = bubble->m_byColor;

		if (type != BB_NORMAL && type != BB_FRIEND && type != BB_ZOMBIE)
			continue;

		int nIndex = bubble->m_nIndex;
		int i;
		for (i = 0; i < NEIGHBOR_COUNT; i++)
		{
			int edgeIndex = getEdgeIndex(nIndex, i, COLUMN_COUNT, INT_MAX);

			if (edgeIndex >= 0 && edgeIndex < nTotalPlaceholderCounts && OuterEmptyBubblesArr[edgeIndex])
				break;
		}

		if(i < NEIGHBOR_COUNT)
		{//Outer Bubble
			nColorsCount[color] += 10;
			nTotalColorsCount += 10;
		}
		else{//Inner Bubble
			nColorsCount[color] += 1;
			nTotalColorsCount += 1;
		}
	}

	if(nTotalColorsCount == 0)
		return getRandRange(1, 6);

	int nRandVal = getRandRange(1, nTotalColorsCount);

	int nSum = 0;
	for(int i = COL_PURPLE; i <= COL_RED; i++)
	{
		nSum += nColorsCount[i];
		if(nRandVal <= nSum)
			return i;
	}

	return 0;
}

void CBubbleLayer::addNewBubbles()
{
	TColorList needColors;

	for (TBubbleList::reverse_iterator it = m_BubbleList.rbegin(); it != m_BubbleList.rend(); it++)
	{
		CBubble* bbl = *it;
		if (bbl->m_byType == BB_NORMAL || bbl->m_byType == BB_FRIEND)
		{
			bool bNeedAdd = true;
			if (!needColors.empty())
			{
				for (TColorList::iterator itc = needColors.begin(); itc != needColors.end(); itc++)
				{
					if (bbl->m_byColor == *itc)
					{
						bNeedAdd = false;
						break;
					}
				}
			}

			if(bNeedAdd)
				needColors.push_back(bbl->m_byColor);
		}
	}

	TColorList resColors;
	switch (needColors.size())
	{
	case 1:
		for (int i = 0; i < 4; i++)
			resColors.push_back(needColors.at(0));
		for (int i = 0; i < 4; i++)
			resColors.push_back(getRandRange(COLOR_MIN, COLOR_MAX));
		break;
	case 2:
		for (TColorList::iterator itc = needColors.begin(); itc != needColors.end(); itc++)
			for (int i = 0; i < 3; i++)
				resColors.push_back(*itc);
		for (int i = 0; i < 2; i++)
			resColors.push_back(getRandRange(COLOR_MIN, COLOR_MAX));
		break;
	case 3:
		for (TColorList::iterator itc = needColors.begin(); itc != needColors.end(); itc++)
			for (int i = 0; i < 2; i++)
				resColors.push_back(*itc);
		for (int i = 0; i < 2; i++)
		{
			int randId = getRandRange(0, 2);
			resColors.push_back(needColors.at(randId));
		}
		break;
	case 4:
		for (TColorList::iterator itc = needColors.begin(); itc != needColors.end(); itc++)
			for (int i = 0; i < 2; i++)
				resColors.push_back(*itc);
		break;
	default:
		for (int i = 0; i < 8; i++)
		{
			if (i < needColors.size())
				resColors.push_back(needColors.at(i));
			else
			{
				int randId = getRandRange(0, needColors.size() - 1);
				resColors.push_back(needColors.at(randId));
			}
		}
		break;
	}

	for (int i = 0; i < 8; i++)
		m_pPrepareColorList.push_back(resColors.at(i));
}

//Checked
void CBubbleLayer::createExplodeAnimeForNormal(CBubble* bubble, float fStartDelayTime)
{
	CCAnimation* anime = NULL;
	if (bubble->getTag() == BB_BOMB)
	{
		if (bubble->m_byType == BB_BOMB && !IS_LOWMEMORY)
			anime = CBubble::createAnimate("Image/animation/explode/bomb_center", 21, 0.03f);
		else
			anime = CBubble::createAnimate("Image/animation/explode/bomb_side", 23, 0.03f);
	} else if (bubble->m_byType == BB_ZOMBIE && (bubble->getTag() != BB_NUKE))
		anime = CBubble::createAnimate("Image/animation/explode/zombie", 20);
	else if (bubble->getTag() == BB_RAINBOW) {
		anime = CBubble::createAnimate("Image/animation/explode/rainbow_exp", 35);
		fStartDelayTime += 0.5f;
	}
	else if (bubble->m_byType == BB_NORMAL || bubble->m_byType == BB_FRIEND)
		anime = CBubble::createAnimate(CCString::createWithFormat("Image/animation/bubbles/%d_%d", BB_NORMAL, bubble->m_byColor)->getCString(), 9);
	else return;

	CCSprite* node = CCSprite::create();
	node->setPosition(bubble->getPosition());
	node->setScale(bubble->getScale());
	addChild(node, zBubbleAnimation);

	CCDelayTime* delay1 = CCDelayTime::create(fStartDelayTime);
	CCCallFuncND* callbackND = CCCallFuncND::create(this, callfuncND_selector(CBubbleLayer::setBubbleOpacity), bubble);
	CCDelayTime* delay2 = CCDelayTime::create(0.0f);
	CCAnimate* animation = CCAnimate::create(anime);
	CCCallFuncN* callbackN = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::removeNodeAndCleanup));
	CCSequence* seq = CCSequence::create(delay1, callbackND, delay2, animation, callbackN, NULL);

	node->runAction(seq);

}

void CBubbleLayer::createExplodeAnimeForFireball(CBubble* bubble, float fStartDelayTime)
{
	CCAnimation* anime = NULL;
	anime = CBubble::createAnimate("Image/animation/fireball/burn", 28);

	CCSprite* node = CCSprite::create();
	node->setPosition(bubble->getPosition());
	node->setScale(bubble->getScale());
	addChild(node, zBubbleAnimation);

	CCDelayTime* delay1 = CCDelayTime::create(fStartDelayTime);
	CCCallFuncN* callbackND = CCCallFuncND::create(this, callfuncND_selector(CBubbleLayer::setBubbleOpacity), bubble);
	CCDelayTime* delay2 = CCDelayTime::create(0.0f);
	CCAnimate* animation = CCAnimate::create(anime);
	CCCallFuncN* callbackN = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::removeNodeAndCleanup));
	CCSequence* seq = CCSequence::create(delay1, callbackND, /*delay2, */animation, callbackN, NULL);

	node->runAction(seq);

}

void CBubbleLayer::setBubbleOpacity(CCNode* node, void* data)
{
	CBubble* bubble = (CBubble*) data;
	bubble->setOpacity(0);

	for (int i = 0; i < NEIGHBOR_COUNT; i++)
	{
		CBubble* neighbor = bubble->m_pNeighbors[i];
		if (NULL == neighbor) continue;
		neighbor->removeNeighbor(bubble);

		if (neighbor->m_byType == BB_ICE && bubble->m_byType == BB_NORMAL)
		{
			stBubbleBack bbk_ice;
			neighbor->backupTo(bbk_ice);
			bbk_ice.bPopped = true;
			bbk_ice.tag = neighbor;
			bbk_ice.bEnable = false;
			backBubbleList.push_back(bbk_ice);

			neighbor->changeTo(BB_NORMAL, bubble->m_byColor);
			neighbor->m_bChecked = true;
			//				createExplodeAnimeForNormal(neighbor, delayTime);

			playEffect(kSND_SFX_SNOWFLAKE);
		}
	}
}

void CBubbleLayer::removeNodeAndCleanup(CCNode* node)
{
	node->removeFromParentAndCleanup(true);
}

//Checked
void CBubbleLayer::animateExplode(CBubble* bubble, float fDelayTime, bool bFireball/* = false*/)
{
// 	if (bubble->m_byType != BB_STONE)
// 	{
// 		if (bubble->m_byType == BB_NORMAL)
// 		{
// 			CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/bubbles/explode_%d.png", bubble->m_byColor)->getCString());
// 			bubble->setTexture(tex);
// 		}
	if (bubble->m_byType == BB_NORMAL) {
		//bubble->setOpacity(0);
		CCDelayTime* delay2 = CCDelayTime::create(fDelayTime + 0.25f);
		CCCallFuncN* func = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::finishedExplode));
		CCSequence* seq = CCSequence::create(delay2, func, NULL);
		bubble->runAction(seq);
	}
	else if(bubble->m_byType == BB_MOR) {

		float dstX = getRandRange(rcBound.getMinX(), rcBound.getMaxX());

		float time = 2.0f * (bubble->getPositionY() + getPositionY()) / SC_HEIGHT;

		CCJumpTo* act = CCJumpTo::create(time, ccp(dstX, m_ptStart.y + 350), 300, 1);
		CCCallFuncN* func = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::finishedExplode));

		CCSequence* seq = CCSequence::create(act, func, NULL);
		bubble->runAction(seq);

		if(bubble->m_byType == BB_MOR && bubble->m_byColor != 0)
			animateMorFlyToNext();
	}
	else {
		float fadeTime = 0.5f;
		float showScoreTime = 0.1f;
		CCDelayTime* delay1 = CCDelayTime::create(fDelayTime);
		CCFadeOut* fadeout = CCFadeOut::create(fadeTime);
		CCDelayTime* delay2 = CCDelayTime::create(showScoreTime);
		CCCallFuncN* func = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::finishedExplode));
		CCSequence* seq = CCSequence::create(delay1, fadeout, delay2, func, NULL);
		bubble->runAction(seq);
	}
//	}

	if(bFireball)
		createExplodeAnimeForFireball(bubble, fDelayTime);
	else
		createExplodeAnimeForNormal(bubble, fDelayTime);
}

float CBubbleLayer::animateUnused()
{
	//schedule(schedule_selector(CBubbleLayer::updateb2PhysicsWorld), 0.05f);

	for (int i = 0; i < m_nRemainCount; i++)
	{
		float delayTime = 0.06f * (m_nRemainCount - i);
		CBubble* bubble = NULL;
		if (m_pGameRoot->m_CurBubble != NULL)
		{
			bubble = m_pGameRoot->m_CurBubble;
			m_pGameRoot->m_CurBubble = NULL;
			bringBubble(bubble);

			delayTime = 0.05f;
		} else if (m_pGameRoot->m_NextBubble != NULL)
		{
			bubble = m_pGameRoot->m_NextBubble;
			m_pGameRoot->m_NextBubble = NULL;
			bringBubble(bubble);
			delayTime = 0.1f;
		} else if (m_pGameRoot->m_CandiBubble != NULL)
		{
			bubble = m_pGameRoot->m_CandiBubble;
			m_pGameRoot->m_CandiBubble = NULL;
			bringBubble(bubble);
			delayTime = 0.1f;
		} else if (m_pGameRoot->m_Foresight1Bubble != NULL)
		{
			bubble = m_pGameRoot->m_Foresight1Bubble;
			m_pGameRoot->m_Foresight1Bubble = NULL;
			bringBubble(bubble);
			delayTime = 0.1f;
		} else if (m_pGameRoot->m_Foresight2Bubble != NULL)
		{
			bubble = m_pGameRoot->m_Foresight2Bubble;
			m_pGameRoot->m_Foresight2Bubble = NULL;
			bringBubble(bubble);
			delayTime = 0.1f;
		} else
		{
			bubble = createNewBubble();
			bubble->setPosition(m_ptNextBubble);
			addChild(bubble, zBubbles);
			if (m_nRemainCount > 30) delayTime *= 0.7f;
		}

		//bubble->m_nIndex = i;

		int distX = getRandRange(rcBound.getMinX() + 150, rcBound.getMaxX());
		int h = getRandRange(300, 500);
		int nDiffPos = getRandRange(0, 200);


		/*CCDelayTime* delay = CCDelayTime::create(delayTime);
		
		CCRotateBy* rot = CCRotateBy::create(1.5f, 360);
		CCJumpTo* jmp = CCJumpTo::create(1.5f, ccp(distX, rcBound.getMinY() + nDiffPos), h, 1);
		CCSpawn* sp = CCSpawn::create(rot, jmp, NULL);

		CCCallFuncN* func = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::finishedUnusedAnim));
		CCSequence* seq = CCSequence::create(delay, sp, func, NULL);
		bubble->runAction(seq);*/

		CCDelayTime* delay = CCDelayTime::create(0.05f + delayTime);
		CCCallFuncN* func = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::addExtraBubblesToPhysicsWorld));
		CCSequence* seq = CCSequence::create(delay, func, NULL);
		bubble->runAction(seq);
	}

	return (0.5f + 0.08f * m_nRemainCount + 1.5f + 2.0f);
}

void CBubbleLayer::updateb2PhysicsWorld(float dt)
{
	std::vector<b2Body *> removeb2BodyList;
	removeb2BodyList.clear();

	b2PhysicsWorld->Step(dt, 10, 10);
	for(b2Body *b = b2PhysicsWorld->GetBodyList(); b; b=b->GetNext()) {    
		if (b->GetUserData() != NULL) {

			CBubble *bubble = (CBubble *)b->GetUserData();
			if(this->getChildren()->indexOfObject(bubble) == UINT_MAX)
				continue;

			bubble->setPosition(ccp(b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO));
			bubble->setRotation(-1 * CC_RADIANS_TO_DEGREES(b->GetAngle()));

			if(bubble->getPositionY() < m_ptNextBubble.y)
			{
				//if(!bubble->m_bGameOverBubble || (bubble->m_bGameOverBubble && bubble->m_bTouchedBottomWall) )
				{
					if(bubble->m_bGameOverBubble)
						finishedDrop(bubble);
					else
						finishedUnusedAnim(bubble);

					removeb2BodyList.push_back(b);
				}
				
				/*if(bubble->m_bGameOverBubble && !bubble->m_bTouchedBottomWall)
					bubble->m_bTouchedBottomWall = true;*/

			}
		}        
	}

	for (std::vector<b2Body *>::iterator it = removeb2BodyList.begin(); it != removeb2BodyList.end(); it++)
	{
		b2Body *b = *it;

		b2PhysicsWorld->DestroyBody(b);
		b->SetUserData(NULL);
	}

}

void CBubbleLayer::addExtraBubblesToPhysicsWorld(CCNode* sprt)
{
	CBubble* bubble = (CBubble* )sprt;

	// Create ball body and shape
	b2BodyDef ballBodyDef;
	ballBodyDef.type = b2_dynamicBody;
	ballBodyDef.position.Set(bubble->getPositionX() / PTM_RATIO, bubble->getPositionY() / PTM_RATIO);
	ballBodyDef.userData = bubble;
	bubble->m_physicsBody = b2PhysicsWorld->CreateBody(&ballBodyDef);

	b2CircleShape circle;
	circle.m_radius = CBubble::SIZE / 2 / PTM_RATIO;

	b2FixtureDef ballShapeDef;
	ballShapeDef.shape = &circle;
	ballShapeDef.density = 0.4f;
	ballShapeDef.friction = 1.0f;
	ballShapeDef.restitution = getRandRange(7, 10) * 0.1f;
	bubble->m_physicsBody->CreateFixture(&ballShapeDef);


	int xImp = getRandRange(0, 2) ? getRandRange(5, 7) * 5 : 0 - (int)getRandRange(5, 7) * 5;
	int yImp = xImp > 0 ? getRandRange(5, 8) * 5 : getRandRange(9, 12) * 5;
	b2Vec2 force = b2Vec2(xImp, yImp);
	//b2Vec2 force = b2Vec2(30, 40);

	bubble->m_physicsBody->ApplyLinearImpulse(force, bubble->m_physicsBody->GetPosition());
}


void CBubbleLayer::finishedUnusedAnim(CCNode* sprt)
{
	animateScore(sprt->getPosition(), m_nScoreForUnused);
	sprt->removeFromParent();
}

//Checked
void CBubbleLayer::setGameState(int nState, int nVal)
{
	switch (nState)
	{
	case STATE_READY:
		break;
	case STATE_PLAY:
		break;
	case STATE_END:
		m_AimLayer->setVisible(false);
		break;
	default:
		break;
	}

	m_nState = nState;
}

void CBubbleLayer::endGameSuccess(float dt)
{
	int nMaxTime = animateUnused() * 10;

	// score by saved bubbles
	const int S1 = 20000;
	const int S2 = 30000;
	const int S3 = 50000;

	int nScoreBySaved = 0;
	if (m_nRemainCount >= m_wStar3)
	{
		nScoreBySaved = S3 + (m_nRemainCount - m_wStar3) * 1000;
	} else if (m_nRemainCount >= m_wStar2)
	{
		int step = (S3 - S2) / (m_wStar3 - m_wStar2);
		nScoreBySaved = S3 - (m_wStar3 - m_nRemainCount) * step;
	} else 
	{
		int step = (S2 - S1) / m_wStar2;
		nScoreBySaved = S2 - (m_wStar2 - m_nRemainCount) * step;
	}

	m_nScoreForUnused = (m_nRemainCount == 0)? nScoreBySaved : (nScoreBySaved / m_nRemainCount);

	m_pGameRoot->procGameEvent(GS_END_SUCCESS, nMaxTime);
}

void CBubbleLayer::endGameFail(float dt)
{
	m_pGameRoot->procGameEvent(GS_END_FAIL);
}

void CBubbleLayer::setBound(CCRect bound)
{
	rcBound = bound;
}

void CBubbleLayer::finishedDrop(CCNode* node)
{
	CCPoint pos = node->getPosition();

	CBubble* bubble = (CBubble*) node;
	char byType = bubble->m_byType;
	char byColor = bubble->m_byColor;

	node->removeFromParent();

//	if (bubble->m_byType != BB_NORMAL && bubble->m_byType != BB_FRIEND) return;
//	int nScore = DROP_SCORE_MIN + DROP_SCORE_PLUS * m_nScoreIndex++;

	//int nScore = SCORE_DROP;
	float fScore = bubble_points_info[byColor].score * m_fCurrentMultiplier;

	animateScore(pos, fScore);
}

void CBubbleLayer::finishedExplode(CCNode* node)
{
	CCPoint pos = node->getPosition();
	CBubble* bubble = (CBubble*) node;
	char byType = bubble->m_byType;
	char byColor = bubble->m_byColor;

	node->removeFromParent();

	if (bubble->m_byType == BB_MOR && bubble->m_byColor == 0)
		return;

	//int nScore = byType == BB_FRIEND? SCORE_POP_FRIEND : SCORE_POP;
	float fScore = bubble_points_info[byColor].score * m_fCurrentMultiplier * m_fStreaksPlus;

	m_pGameRoot->m_fMultiplier += bubble_points_info[byColor].multiplier;

//	if (bubble->m_byType != BB_NORMAL) return;

	animateScore(pos, fScore);

	animateMultiplier();
}

//Checked
void CBubbleLayer::animateScore(CCPoint pos, float score)
{
	pos.y -= getLayerDiff();

	m_pGameRoot->animateScore(pos, score);
}

//Checked
void CBubbleLayer::animateMultiplier()
{
	m_pGameRoot->animateMultiplier();
}

//Checked
void CBubbleLayer::decreaseRemainCount(int nDecCount, bool bRemoveCur)
{
	m_nRemainCount -= nDecCount;
	if (m_nRemainCount < 0)
		m_nRemainCount = 0;
	m_pGameRoot->procGameEvent(GS_REMAIN_BUBBLES, m_nRemainCount);
}

void CBubbleLayer::decreaseRecallCountdown()
{
	m_pGameRoot->m_nRecallCountdown = MAX(m_pGameRoot->m_nRecallCountdown - 1, 0);

	m_pGameRoot->procGameEvent(GS_RECALL_COUNTDOWN);
}

CCSprite* CBubbleLayer::saveFriend(CBubble* bubble, float fDelayTime)
{
	CCPoint pos = ccp(bubble->getPositionX(), bubble->getPositionY() - getLayerDiff());
	CCSprite* spFriend = m_pGameRoot->saveFriend(pos, m_nSavedFriends, fDelayTime);

    //AppDelegate::app->playSound(kSND_SFX_FRIEND_SAVED, 0.035f);
	playEffect(kSND_SFX_FRIEND_SAVED, 0.2f);

	m_nSavedFriends++;

	return spFriend;
}

void CBubbleLayer::startCameraScreen()
{
    checkMoveLayer(this);
}

void CBubbleLayer::processDiffusion()
{
	CCLog("void CBubbleLayer::processDiffusion()");

	unsigned char fruitsCount[MAX_COLORS] = {0,};
	int nTotalCounts = 0;

	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
	{
		CBubble* bubble = *it;
		
		char type = bubble->m_byType;
		if (type != BB_NORMAL && type != BB_FRIEND && type != BB_ZOMBIE)
			continue;

		fruitsCount[bubble->m_byColor]++;
		nTotalCounts++;
	}

	unsigned char nPrevailColor = 0;
	unsigned char nPrevailCount = 0;
	for (int i = 0; i < MAX_COLORS; i++)
	{
		if(fruitsCount[i] > nPrevailCount)
		{
			nPrevailCount = fruitsCount[i];
			nPrevailColor = i;
		}
	}

	TBubbleList m_otherList;
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
	{
		CBubble* bubble = *it;

		char type = bubble->m_byType;
		char color = bubble->m_byColor;
		
		if (type != BB_NORMAL && type != BB_FRIEND && type != BB_ZOMBIE)
			continue;

		if(color != nPrevailColor)
			m_otherList.push_back(bubble);
	}


	int nRemoveCounts = m_otherList.size() * (20 + getRandRange(0, 10)) / 100.0f;
	
	for(int i = 0; i < nRemoveCounts; i++)
	{
		int nEraseIndex = getRandRange(0, m_otherList.size() - 1);
		m_otherList.erase(m_otherList.begin() + nEraseIndex);
	}
	
	m_nPrevailColor = nPrevailColor;


	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
	{
		CBubble* bubble = *it;
		
		char type = bubble->m_byType;
		char color = bubble->m_byColor;

		if (type != BB_NORMAL && type != BB_FRIEND && type != BB_ZOMBIE)
			continue;

		if(color == nPrevailColor)
			m_otherList.push_back(bubble);
	}

	for (TBubbleList::iterator it = m_otherList.begin(); it != m_otherList.end(); it++)
	{
		CBubble* bubble = *it;

		CCAnimation* anime = NULL;
		anime = CBubble::createAnimate("Image/animation/explode/diffusion", 28);
		CCAnimate* animation = CCAnimate::create(anime);
		CCCallFuncN* callback1 = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::removeNodeAndCleanup));

		CCSprite* node = CCSprite::create();
		node->setPosition(bubble->getPosition());
		node->setScale(bubble->getScale());
		addChild(node, zBubbleAnimation);
		CCSequence* seq1 = CCSequence::create(animation, callback1, NULL);

		node->runAction(seq1);


		CCFadeOut*	fadeout = CCFadeOut::create(ANIM_PERIOD * 13);
		CCCallFuncN* callback2 = CCCallFuncN::create(this, callfuncN_selector(CBubbleLayer::processBubbleDiffusion));
		CCActionInterval* delay = CCDelayTime::create(ANIM_PERIOD * 2);
		CCFadeIn*	fadein = CCFadeIn::create(ANIM_PERIOD * 13);

		CCSequence* seq2 = CCSequence::create(fadeout, callback2, delay, fadein, NULL);
		bubble->runAction(seq2);
	}
}

void CBubbleLayer::processBubbleDiffusion(CCNode* node)
{
	CBubble* bubble = (CBubble*) node;
	
	bubble->changeTo(bubble->m_byType, m_nPrevailColor);
}

void CBubbleLayer::checkMoveLayer(CCObject* bFromCamera)
{
	if (m_BubbleList.empty() || LCommon::sharedInstance()->curLevelType == LVL_SAVE_THE_BEE)
	{
		afterMoveLayer(false);
		return;
	}

	float minY = m_BubbleList.front()->getPositionY();
	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
	{
		if ((*it)->getPositionY() < minY)
			minY = (*it)->getPositionY();
	}

	float moveY = 0;

	float maxY = rcBound.getMaxY();
	rcBound.size.height = maxY - minY + SC_BUBBLES_DIST;
	rcBound.origin.y = maxY - rcBound.size.height;

	if (rcBound.size.height < absBound.size.height)
	{
		moveY = (absPos.y + absBound.getMaxY()) - (getPositionY() + rcBound.getMaxY());
		rcBound = absBound;
	}
	else
	{
		int curMinY = getPositionY() + rcBound.getMinY();
		int absMinY = absPos.y + absBound.getMinY();
		if ((curMinY + SC_BUBBLES_DIST > absMinY + MAX_DIST)
			|| curMinY < absMinY)
			moveY = absMinY - curMinY;
	}

	if (moveY == 0) 
	{
		afterMoveLayer(bFromCamera);
		return;
	}

	m_bLayerMoving = true;

	m_ptStart.y -= moveY;
	m_ptNextBubble.y -= moveY;

	CCMoveBy* mov = CCMoveBy::create(fabs(moveY / 200), ccp(0, moveY));
	CCCallFuncO* func = CCCallFuncO::create(this, callfuncO_selector(CBubbleLayer::afterMoveLayer), bFromCamera);
	runAction(CCSequence::create(mov, func, NULL));

#ifdef ROBOT_SHOOT
	m_pGameRoot->m_fMoveLayerTime = MAX(m_pGameRoot->m_fMoveLayerTime, fabs(moveY / 200));
#endif

}

void CBubbleLayer::afterMoveLayer(CCObject* bFromCamera)
{
	if (m_pGameRoot->m_CurBubble != NULL)
		m_pGameRoot->m_CurBubble->setVisible(true);
	if (m_pGameRoot->m_NextBubble != NULL)
		m_pGameRoot->m_NextBubble->setVisible(true);
	if (m_pGameRoot->m_CandiBubble != NULL)
		m_pGameRoot->m_CandiBubble->setVisible(true);

	if (m_pGameRoot->m_Foresight1Bubble != NULL)
		m_pGameRoot->m_Foresight1Bubble->setVisible(true);
	if (m_pGameRoot->m_Foresight2Bubble != NULL)
		m_pGameRoot->m_Foresight2Bubble->setVisible(true);
	if (m_pGameRoot->imgForesightFrame1 != NULL)
		m_pGameRoot->imgForesightFrame1->setVisible(true);
	if (m_pGameRoot->imgForesightFrame2 != NULL)
		m_pGameRoot->imgForesightFrame2->setVisible(true);

	m_bLayerMoving = false;

	m_pGameRoot->noticeCanShoot();

#ifdef ROBOT_SHOOT
	schedule(schedule_selector(CBubbleLayer::shootBubbleAi), 0.5f);
#endif // ROBOT_SHOOT

	m_AimLayer->setPosition(absPos);

    if (bFromCamera)
        m_pGameRoot->afterCameraScreen();
}

void CBubbleLayer::playEffect(const char* sfxFileName)
{
    playEffect(sfxFileName, 0.08f);
}

void CBubbleLayer::playEffect(const char* sfxFileName, float fVolume)
{
//#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
//	return;
//#endif
	if (!LCommon::sharedInstance()->isSoundOn) return;

	CocosDenshion::SimpleAudioEngine::sharedEngine()->setEffectsVolume(fVolume);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(sfxFileName, false);
}

//Checked
void CBubbleLayer::bringBubble(CBubble* bubble)
{
	if (bubble == NULL) return;

	CCPoint orgPos = bubble->getPosition();
	bubble->retain();
	bubble->removeFromParent();

	CCPoint newPos = ccp(orgPos.x, orgPos.y + getLayerDiff());
	addChild(bubble, zBubbles);
	bubble->setPosition(newPos);
}

float CBubbleLayer::getLayerDiff()
{
	return (absPos.y - getPositionY());
}

bool CBubbleLayer::isPlaying()
{
	 return (m_nState == STATE_PLAY);
}

bool CBubbleLayer::canUndoMove()
{
	return (lastBubbleBack.bEnable || !backBubbleList.empty());
}

void CBubbleLayer::undoMove()
{
	if (lastBubbleBack.byType == BB_NORMAL)
		m_pGameRoot->rollbackCurBubble(BB_NORMAL, lastBubbleBack.byColor);

	if (lastBubbleBack.bEnable)
	{
		for (int i = 0; i < NEIGHBOR_COUNT; i++)
		{
			if (lastBubble->m_pNeighbors[i] != NULL)
				lastBubble->m_pNeighbors[i]->removeNeighbor(lastBubble);
		}
		for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		{
			if (*it == lastBubble)
			{
				m_BubbleList.erase(it);
				break;
			}
		}

		lastBubble->removeFromParent();
		lastBubbleBack.bEnable = false;
	}

	memcpy(touchObstacles, touchObstaclesBack, BB_KIND_COUNT * sizeof(bool));

	if (backBubbleList.empty()) return;

	int totalScore = 0;
	TBubbleList undoList;
	for (TBackBubbleList::iterator it = backBubbleList.begin(); it != backBubbleList.end(); it++)
	{
		stBubbleBack& bbk = *it;

		if (bbk.byType == BB_ICE && bbk.bPopped && !bbk.bEnable)
		{
			CBubble* org = (CBubble*) bbk.tag;
			org->changeTo(BB_ICE, 1);
		}
		else
		{
			CBubble* bubble = new CBubble(bbk.byType, bbk.byColor);
			bubble->autorelease();
			bubble->setPosition(bbk.pos);

			int key = getKey(bubble->m_byType, bubble->m_byColor);
			if (bbk.bPopped) {
				TBubbleStatMap::iterator it = poppedList.find(key);
				if (it != poppedList.end())
					if (it->second > 0) it->second--;
			} else {
				TBubbleStatMap::iterator it = droppedList.find(key);
				if (it != droppedList.end())
					if (it->second > 0) it->second--;
			}

			addChild(bubble, zBubbles);
			bubble->runBright();

			m_BubbleList.push_back(bubble);
			undoList.push_back(bubble);

			if (bubble->m_byType == BB_ZOMBIE)
			{
				if (!bbk.bEnable) decreaseRemainCount(-ZOMBIE_DECREASE);
			} else if (bubble->m_byType == BB_FRIEND)
			{
				CCSprite* spFriend = (CCSprite*) bbk.tag;
				spFriend->removeFromParent();
				m_nSavedFriends--;
			} else if (bubble->m_byType == BB_MAGNET)
			{
				m_vecMagnets.insert(std::make_pair(bubble->m_nUID, bubble->getPosition()));
			}

			// scoring

			int nScore;
			if (bbk.bPopped)
				nScore = bubble->m_byType == BB_FRIEND? SCORE_POP_FRIEND : SCORE_POP;
			else
				nScore = bubble->m_byType == BB_FRIEND? SCORE_DROP_FRIEND : SCORE_DROP;

			totalScore += nScore;
		}
	}

	float fHorz = CBubble::SIZE * 1.2;
	float fVert = CBubble::SIZE_OV * 1.2;
	for (TBubbleList::iterator it0 = undoList.begin(); it0 != undoList.end(); it0++)
	{
		CBubble* curBubble = *it0;
		for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
		{
			CBubble* bubble = *it;
			bubble->m_bChecked = false;
			if (fabs(bubble->getPositionY() - curBubble->getPositionY()) > fVert) continue;
			if (fabs(bubble->getPositionX() - curBubble->getPositionX()) > fHorz) continue;
			if (curBubble->isNeighbor(bubble)) continue;
			curBubble->addNeighbor(bubble);
			bubble->addNeighbor(curBubble);
		}
	}

	backBubbleList.clear();

	checkMoveLayer();
}

void CBubbleLayer::addHistory(CBubble* bubble, bool bPopped)
{
	int nTag = bubble->getTag();
	if (nTag == BB_RAINBOW || nTag == BB_BOMB || nTag == BB_NUKE) return;

	char type = bubble->m_byType;
	char color = bubble->m_byColor;

	TBubbleStatMap& statMap = bPopped? poppedList : droppedList;

	char byCol = color;
	int key = getKey(type, color);
	TBubbleStatMap::iterator it = statMap.find(key);
	if (it != statMap.end())
		it->second++;
	else {
		int val = 1;
		statMap.insert(std::make_pair(key, val));
	}
}

int CBubbleLayer::getTotalVal(char type, char color, bool bPopped)
{
	TBubbleStatMap& statMap = bPopped? poppedList : droppedList;

	int nCnt = 0;
	if (type == BB_NORMAL && (color == COL_ANY || color == COL_NONE))
	{
		for (int col = COL_PURPLE; col < COL_ANY; col++)
		{
			int key = getKey(type, col);
			TBubbleStatMap::iterator it = statMap.find(key);
			if (it != statMap.end())
				nCnt += it->second;
		}
	}
	else
	{
		int key = getKey(type, color);
		TBubbleStatMap::iterator it = statMap.find(key);
		if (it != statMap.end()) 
			nCnt = it->second;
	}

	return nCnt;
}

int CBubbleLayer::getInitialCount(char type, char color)
{
	int key = getKey(type, color);
	TBubbleStatMap::iterator it = initialList.find(key);
	if (it == initialList.end()) return 0;

	return it->second;
}

//Checked
int CBubbleLayer::getKey(char type, char color)
{
	if (type != BB_NORMAL && type != BB_FRIEND && type != BB_ZOMBIE)
		color = 1;
	return type << 8 | color;
}


extern std::string STR_COLORS[];
extern std::string STR_TYPES[];

std::string CBubbleLayer::getBubbleAnal()
{
	std::string strRes = CCString::createWithFormat("Level %d\n\n", LCommon::sharedInstance()->curLevel() + 1)->getCString();

	strRes += "======  POP  ======\n";
	int nTotal = 0;
	for (TBubbleStatMap::iterator it = poppedList.begin(); it != poppedList.end(); it++)
	{
		int key = it->first;
		int nType = (key & 0x0FF00) >> 8;
		int nColor = key & 0x0FF;

		int count = it->second;
		nTotal += count;
		strRes += CCString::createWithFormat("     (%s, %s) = %d\n", STR_TYPES[nType].c_str(), STR_COLORS[nColor].c_str(), count)->getCString();
	}
	strRes += CCString::createWithFormat("        Total  = %d\n\n", nTotal)->getCString();

	strRes += "======  DROP  ======\n";
	nTotal = 0;
	for (TBubbleStatMap::iterator it = droppedList.begin(); it != droppedList.end(); it++)
	{
		int key = it->first;
		int nType = (key & 0x0FF00) >> 8;
		int nColor = key & 0x0FF;

		int count = it->second;
		nTotal += count;
		strRes += CCString::createWithFormat("     (%s, %s) = %d\n", STR_TYPES[nType].c_str(), STR_COLORS[nColor].c_str(), count)->getCString();
	}

	strRes += CCString::createWithFormat("        Total  = %d\n\n", nTotal)->getCString();
	return strRes;
}








#ifdef ROBOT_SHOOT

void CBubbleLayer::initAi()
{
	m_nCurShootNo = 0;
	for (int i = 0; i < MAX_SHOOT_COUNT; i++)
		colorList[i].init();
}

bool CBubbleLayer::aiShoot(bool bForce)
{
	//CCLog("aiShoot : 1");
	if (m_nState != STATE_PLAY) return false;
	//CCLog("aiShoot : 2");
	if (!m_pGameRoot->m_bRobotShoot) return false;
	//CCLog("aiShoot : 3");

// 	m_nAiTwice++;
// 
// 	if (m_nAiTwice >= 2 || bForce)
// 	{
		CBubble* curBubble = m_pGameRoot->m_CurBubble;
		if (curBubble == NULL) return false;
		
		//CCLog("aiShoot : 4");

		m_nAiTwice = 0;
		m_fLaunchAngle = findBestAngle();
// 		m_AimLayer->setSniperAim(true);
// 		updateTargetMove();
		CCLog("GS_LAUNCH_BUBBLE");
		m_pGameRoot->procGameEvent(GS_LAUNCH_BUBBLE);
		return true;
// 	}
// 
// 	return false;
}

float CBubbleLayer::findBestAngle()
{
	int nInterval = absBound.size.width / 50;
	int ny = m_ptStart.y + 300;
	int nx = absBound.getMinX();

	int nMaxPoint = 0;
	float fAngleForMax = asin(0.35);
	int nMinPoint = 100000;
	float fAngleForMin = asin(0.35);

	int nMaxPointN = 0;
	float fAngleForMaxN = asin(0.35);
	while (nx < absBound.getMaxX())
	{
		float fAngle = getAlpha(m_ptStart, ccp(nx, ny));
		float sinAngle = sin(fAngle);

		if (sinAngle >= 0.3f)
		{
			CCPoint finalPos = getLastPoint(fAngle);
			int nPoint = getPointByPos(finalPos, m_pGameRoot->m_CurBubble);
			if (nPoint > nMaxPoint)
			{
				nMaxPoint = nPoint;
				fAngleForMax = fAngle;
			} else if (nPoint == nMaxPoint)
			{
				if (sinAngle > sin(fAngleForMax))
					fAngleForMax = fAngle;
			} else if (nPoint < nMinPoint)
			{
				nMinPoint = nPoint;
				fAngleForMin = fAngle;
			}

			if (m_pGameRoot->m_NextBubble != NULL)
			{
				int nPointN = getPointByPos(finalPos, m_pGameRoot->m_NextBubble);

				if (nPointN > nMaxPointN)
				{
					nMaxPointN = nPointN;
					fAngleForMaxN = fAngle;
				} else if (nPointN == nMaxPointN)
				{
					if (sinAngle > sin(fAngleForMaxN))
						fAngleForMaxN = fAngle;
				}
			}
		}
		nx += nInterval;
	}

	float fAngleShoot = fAngleForMax;

	if (m_pGameRoot->m_NextBubble != NULL)
	{
		if (m_pGameRoot->m_CurBubble->m_bSwap || ((nMaxPointN > nMaxPoint) && m_bEnableSwapping))
		{
			bool bCurSwap = m_pGameRoot->m_CurBubble->m_bSwap;
			bool bNextSwap = m_pGameRoot->m_NextBubble->m_bSwap;

			bool bCurWaste = m_pGameRoot->m_CurBubble->m_bWaste;
			bool bNextWaste = m_pGameRoot->m_NextBubble->m_bWaste;

			bool bCurNHC = m_pGameRoot->m_CurBubble->m_bNoHitCandy;
			bool bNextNHC = m_pGameRoot->m_NextBubble->m_bNoHitCandy;

			CCPoint curPos = m_pGameRoot->m_CurBubble->getPosition();
			CCPoint nxtPos = m_pGameRoot->m_NextBubble->getPosition();

			CBubble* tmp = m_pGameRoot->m_NextBubble;
			m_pGameRoot->m_NextBubble = m_pGameRoot->m_CurBubble;
			m_pGameRoot->m_CurBubble = tmp;

			m_pGameRoot->m_CurBubble->setPosition(curPos);
			m_pGameRoot->m_NextBubble->setPosition(nxtPos);

			m_pGameRoot->m_CurBubble->m_bSwap = bCurSwap;
			m_pGameRoot->m_NextBubble->m_bSwap = bNextSwap;

			m_pGameRoot->m_CurBubble->m_bWaste = bCurWaste;
			m_pGameRoot->m_NextBubble->m_bWaste = bNextWaste;

			m_pGameRoot->m_CurBubble->m_bNoHitCandy = bCurNHC;
			m_pGameRoot->m_NextBubble->m_bNoHitCandy = bNextNHC;

			fAngleShoot = fAngleForMaxN;
		}
	}

	if (m_pGameRoot->m_CurBubble->m_bWaste)
		fAngleShoot = fAngleForMin;

	return fAngleShoot;
}

CCPoint CBubbleLayer::getLastPoint(float fAngle)
{
	float orgAng = m_fLaunchAngle;
	m_fLaunchAngle = fAngle;

	CCPoint ptStart = m_ptStart;
	CCPoint ptNext;
	bool hasNext;

	int nLen = 0;
	do 
	{
		int flyOption = FO_NORMAL;
		hasNext = getNextPoint(ptStart, CBubble::SIZE, ptNext, flyOption);

		ptStart = ptNext;
		if (nLen++ > 10) break;
	} while (hasNext);
	m_fLaunchAngle = orgAng;

	return ptNext;
}

void CBubbleLayer::aiLog()
{

}

void CBubbleLayer::analyzeColor(int color)
{
//	colorList[m_pGameRoot->m_CurBubble->m_byColor]++;
}

int CBubbleLayer::getPointByPos(CCPoint dstPos, CBubble* pOrgBubble)
{
	int yy = (rcBound.getMaxY() - dstPos.y + CBubble::SIZE / 2) / CBubble::SIZE_OV;
	float fStartX = m_fMargin + (yy % 2 + 1) * CBubble::SIZE / 2;
	int xx = (dstPos.x + CBubble::SIZE / 2 - fStartX) / CBubble::SIZE;
	if (xx < 0) xx = 0;
	if (xx > COLUMN_COUNT - 1) xx = COLUMN_COUNT - 1;

	CCPoint newPos = ccp(fStartX + xx * CBubble::SIZE, rcBound.getMaxY() - yy * CBubble::SIZE_OV);

	// must be in screen
	if (newPos.y - getLayerDiff() > SC_HEIGHT) return 0;

	// find candi list
	float fHorz = CBubble::SIZE * 1.2;
	float fVert = CBubble::SIZE_OV * 1.2;

	char itemType = BB_NONE;
	char byCandyColor = BB_NONE;

	char curColor = pOrgBubble->m_byColor;
	TBubbleList candiList;
	CBubble curBubble(BB_NORMAL, curColor);

	for (TBubbleList::iterator it = m_BubbleList.begin(); it != m_BubbleList.end(); it++)
	{
		CBubble* bubble = *it;
		bubble->m_bChecked = false;
		if (fabs(bubble->getPositionY() - newPos.y) > fVert) continue;
		if (fabs(bubble->getPositionX() - newPos.x) > fHorz) continue;
		curBubble.addNeighbor(bubble);
		if (bubble->m_byType != BB_NORMAL)
		{
			if (bubble->m_byType == BB_CANDY)
			{
				bubble->m_bChecked = true;
				candiList.push_back(bubble);
			}
			else if (bubble->m_byType == BB_BOMB)
			{
				bubble->m_bChecked = true;
				candiList.push_back(bubble);
			}
		}
	}

	curBubble.m_bChecked = true;

	processNeighbor(&curBubble, candiList, 0, true);
	//--------------

	for (TBubbleList::iterator it = candiList.begin(); it != candiList.end(); it++)
	{
		CBubble* bubble = *it;
		if (bubble->m_byType == BB_ZOMBIE) return 0;
	}

	int nTotalPoint = 40 * candiList.size() + 5;
	// processNeighbor
	for (TBubbleList::iterator it = candiList.begin(); it != candiList.end(); it++)
	{
		CBubble* bubble = *it;
		if (bubble->m_byType == BB_ZOMBIE) return 0;
		bubble->m_bChecked = false;

		int matchPoint = 0;

		switch (bubble->m_byType)
		{
		case BB_NORMAL:
			if (bubble->m_byColor == curColor)
				matchPoint = 20;
			else
				matchPoint = 5;
			break;
		case BB_FRIEND:
			if (bubble->m_byColor == curColor)
				matchPoint = 2000;
			break;
		case BB_CANDY:
			if (pOrgBubble->m_bNoHitCandy)
				return 0;
			if ((bubble->m_byColor & (1 << curColor)) != 0)
				matchPoint = 50;
			else 
			{
				return 0;
			}
			break;
		case BB_ICE:
			matchPoint = 12;
			break;
		case BB_STONE:
			matchPoint = 6;
			break;
		case BB_ZOMBIE:
			return 0;
			break;
		case BB_BOMB:
			matchPoint = 1000;
			break;
		default:
			break;
		}

		nTotalPoint += matchPoint;
	}

	return nTotalPoint;
}

void CBubbleLayer::updateReportData()
{
	infoLeft.update(m_nRemainCount);
	infoDropped.update(m_nTotalDropped);
	infoPopped.update(m_nTotalPopped);

	for (TMapIntInfo::iterator it = mapColorPop.begin(); it != mapColorPop.end(); it++)
	{
		std::string key = it->first;
		TMapInfoInfo::iterator infoIt = mapInfoColorPop.find(key);
		if (infoIt != mapInfoColorPop.end())
		{
			infoIt->second.update(it->second);
		}
		else
		{
			MyInfo info;
			info.init();
			info.update(it->second);
			mapInfoColorPop.insert(std::make_pair(key, info));
		}
	}

	for (TMapIntInfo::iterator it = mapColorDrop.begin(); it != mapColorDrop.end(); it++)
	{
		std::string key = it->first;
		int val = it->second;
		TMapInfoInfo::iterator infoIt = mapInfoColorDrop.find(key);
		if (infoIt != mapInfoColorDrop.end())
		{
			infoIt->second.update(val);
		}
		else
		{
			MyInfo info;
			info.init();
			info.update(val);
			mapInfoColorDrop.insert(std::make_pair(key, info));
		}
	}

	m_nTotalDropped = 0;
	m_nTotalPopped = 0;

///////////////////////////////
	AITestLevelInfo testInfo;
	
	time_t theTime = time(NULL);
	struct tm *aTime = localtime(&theTime);

	/*CCLog("year->%d",aTime->tm_year+1900);
	CCLog("month->%d",aTime->tm_mon+1);
	CCLog("date->%d",aTime->tm_mday);
	CCLog("hour->%d",aTime->tm_hour);
	CCLog("minutes->%d",aTime->tm_min);
	CCLog("seconds->%d",aTime->tm_sec);*/

	testInfo.strTime = CCString::createWithFormat("%02d/%02d-%02d:%02d:%02d", aTime->tm_mon+1, aTime->tm_mday, aTime->tm_hour, aTime->tm_min, aTime->tm_sec)->getCString();
	testInfo.nTotalShoots = m_pGameRoot->m_nTotalShoots;
	testInfo.nExtraShoots =  MAX(0, m_pGameRoot->m_nTotalShoots - m_nOrgCount);
	testInfo.nScore = (int)m_pGameRoot->m_fScore;

	m_pGameRoot->m_AITestResults.push_back(testInfo);
}

void CBubbleLayer::recreateMapData(int nStage, int nPrepareCnt, unsigned char prepareData[MAX_SHOOT_COUNT])
{
	unsigned long nSize = 0;
	std::string fullPath = CCFileUtils::sharedFileUtils()->fullPathForFilename(CCString::createWithFormat("map/%d.map", nStage + 1)->getCString());
	char* pBuffer = (char*) CCFileUtils::sharedFileUtils()->getFileData(fullPath.c_str(), "rb", &nSize);
	int nPos = 0;

	int* pIntBuffer = reinterpret_cast<int*> (pBuffer);

	int nMapDataVersion = pIntBuffer[0]; nPos += 4;
	int nPrevPreparedCount = pIntBuffer[1] & 0xFFFF; 
	int wStar1 = (pIntBuffer[1] >> 16) & 0xFFFF; 
	nPos += 4;
	int wStar2 = pIntBuffer[2] & 0xFFFF; 
	int wStar3 = (pIntBuffer[2] >> 16) & 0xFFFF; 
	nPos += 4;
	int nRemainCount = pIntBuffer[3]; nPos += 4;
	int nBubbleCnt = pIntBuffer[4]; nPos += 4;

	if (nPrevPreparedCount > 0)
		nPos += nPrevPreparedCount;

#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)

	std::string parent1 = fullPath.substr(0, fullPath.rfind('/'));
	std::string parent2 = parent1.substr(0, parent1.rfind('/'));

	if (parent1 == parent2)
		parent2 = parent1.substr(0, parent1.rfind('\\'));

	std::string newFolder = parent2 + "/newmap";

	if (_chdir(newFolder.c_str()))
	{
		_mkdir(newFolder.c_str());
	}

	std::string fullPathNew = CCFileUtils::sharedFileUtils()->fullPathForFilename(CCString::createWithFormat("%s/%d.map", newFolder.c_str(), nStage + 1)->getCString());
	FILE* fp = fopen(fullPathNew.c_str(), "wb");
	if (!fp)
	{
		CCMessageBox("FP is null", "new map error");
		return;
	}

	nMapDataVersion = 1;
	fwrite(&nMapDataVersion, sizeof(int), 1, fp);
	fwrite(&nPrepareCnt, sizeof(short), 1, fp);
	fwrite(&wStar1, sizeof(short), 1, fp);
	fwrite(&wStar2, sizeof(short), 1, fp);
	fwrite(&wStar3, sizeof(short), 1, fp);
	fwrite(&nPrepareCnt, sizeof(int), 1, fp);
	fwrite(&nBubbleCnt, sizeof(int), 1, fp);

	fwrite(prepareData, sizeof(char), nPrepareCnt, fp);
	fwrite(pBuffer + nPos, sizeof(char), nSize - nPos, fp);
	fclose(fp);

	std::string fullPathNew2 = CCFileUtils::sharedFileUtils()->fullPathForFilename(CCString::createWithFormat("%s/zzMapInfo_%02d.txt", newFolder.c_str(), nStage + 1)->getCString());
	FILE* fp2 = fopen(fullPathNew2.c_str(), "wb");
	if (!fp2)
	{
		CCMessageBox("FP is null", "new map error");
		return;
	}

	std::string strInfo;
	int nTestCnt = m_pGameRoot->m_nTestRepeatCount;
	strInfo += CCString::createWithFormat("Map %d data\n", nStage + 1)->getCString();
	strInfo += CCString::createWithFormat("Total Repeat Count %d, Pass Count : %d, Fail Count : %d \n", 
		m_pGameRoot->m_nTestRepeatCount, (m_pGameRoot->m_nTestRepeatCount - m_pGameRoot->m_nTestFailCount), m_pGameRoot->m_nTestFailCount)->getCString();
	strInfo += CCString::createWithFormat("          \t\t Min \t\t\t Max \t\t\t Avg\n")->getCString();
	strInfo += "Left      " + infoLeft.toString(nTestCnt) + "\n";
	strInfo += "Popped    " + infoPopped.toString(nTestCnt) + "\n";
	strInfo += "Dropped   " + infoDropped.toString(nTestCnt) + "\n";
	strInfo += "== Color Popped\n";
	for (TMapInfoInfo::iterator it = mapInfoColorPop.begin(); it != mapInfoColorPop.end(); it++)
	{
		std::string key = it->first;
		strInfo += it->first + "  \t\t " + it->second.toString(nTestCnt) + "\n";
	}

	strInfo += "== Color Dropped\n";
	for (TMapInfoInfo::iterator it = mapInfoColorDrop.begin(); it != mapInfoColorDrop.end(); it++)
	{
		std::string key = it->first;
		strInfo += it->first + "  \t\t " + it->second.toString(nTestCnt) + "\n";
	}

	int nStrSize = strInfo.length();
	fwrite(strInfo.c_str(), 1, nStrSize, fp2);
	fclose(fp2);
#endif

	m_nTotalPopped = 0;
	m_nTotalDropped = 0;
#ifdef ROBOT_SHOOT
	infoLeft.init();
	infoPopped.init();
	infoDropped.init();
#endif
}

void CBubbleLayer::shootBubbleAi(float dt)
{
	if (m_pGameRoot->m_CurBubble != NULL)
		unschedule(schedule_selector(CBubbleLayer::shootBubbleAi));

	CCLog("shootBubbleAi");

	aiShoot();
}

std::string colorNames[] = {
	"none",
	"plum",
	"blue",
	"lime",
	"banana",
	"orange",
	"red",
};
std::string CBubbleLayer::keyForBubble(CBubble* bubble)
{
	std::string inf;
	switch (bubble->m_byType)
	{
	case BB_NORMAL:
		inf = "Normal-";
		break;
	case BB_CANDY:
		inf = "Candy";
		break;
	case BB_BOMB:
		inf = "Dynamite";
		break;
	case BB_MAGNET:
		inf = "Magnet";
		break;
	case BB_ICE:
		inf = "Snowflake";
		break;
	case BB_STONE:
		inf = "Stone";
		break;
	case BB_ZOMBIE:
		inf = "Zombie-";
		break;
	case BB_FRIEND:
		inf = "Friend-";
		break;
	default:
		break;
	}

	if (bubble->m_byType == BB_NORMAL || bubble->m_byType == BB_FRIEND || bubble->m_byType == BB_ZOMBIE)
	{
		inf += colorNames[bubble->m_byColor];
	}

	return inf;
}
#endif //ROBOT_SHOOT

#include "GoalData.h"
#include <algorithm>
#include "cocos2d.h"
#include "SaveUtil.h"
#include "AppDelegate.h"
#include "MathUtil.h"

using namespace std;

int getColor(const std::string& str);
int getType(const std::string& str);
int getAction(std::string& str);

string STR_COLORS[] = {"none", "purple", "blue", "green", "yellow", "orange", "red", "any"};
string STR_TYPES[] = {"none", "normal", "candy", "dynamite", "magnet", "ice", "stone", "zombie", "friend", "nuke", "rainbow", "repellent"};
string STR_ACTIONS[] = {"none", "save", "pop", "drop", "touch", "don't"};

CGoalAnalyser::CGoalAnalyser(const char* pInfo)
{
	info = pInfo;

	int index = info.find(',');
	if (index != string::npos)
		info.insert(index, " ");

	std::string sep = " ";
	splitString(info, sep, tokens);

	// remove bubble/bubbles
	for (std::vector<std::string>::iterator it = tokens.begin(); it != tokens.end(); it++)
	{
		if (*it == "bubble" || *it == "bubbles") {
			it = tokens.erase(it);
			if (it == tokens.end()) break;
		}
	}

	nextPos = 0;
}

void CGoalAnalyser::proc()
{
	std::string& first = next();

	int action = getAction(first);
	if (action == GA_DONT) {
		std::string& tnext = next();
		action = getAction(tnext);
		if (action == GA_POP) {
			std::string& t3 = next();
			int count;
			if (t3 == "any") {
				count = 0;
				do 
				{
					std::string t4 = next();
					if (!isMore(t4)) {
						int col = getColor(t4);
						int nType;
						if (col != COL_NONE) {
							nType = BB_NORMAL;
						} else {
							nType = getType(t4);
						}
						TGoalRecord record;
						record.action = GA_POP;
						record.type = nType;
						record.count = count;
						record.color = col;
						record.bigger = false;

						recList.push_back(record);
					}
				} while (hasNext());
			} else if (t3 == "more") {
				if (next() != "than") return;
				std::string strCnt = next();

				std::string strType = next();
				int col = getColor(strType);
				int nType;
				if (col != COL_NONE) {
					nType = BB_NORMAL;
				} else {
					nType = getType(strType);
				}

				TGoalRecord record;
				record.action = GA_POP;
				record.type = nType;
				record.color = col;
				record.count = atoi(strCnt.c_str());
				record.bigger = false;
				recList.push_back(record);
			}
		} else if (action == GA_TOUCH) {
			do 
			{
				std::string t4 = next();
				if (t4 == "any")
					t4 = next();
				if (!isMore(t4)) {
					int col = getColor(t4);
					int nType;
					if (col != COL_NONE) {
						nType = BB_NORMAL;
					} else {
						nType = getType(t4);
					}
					TGoalRecord record;
					record.action = GA_TOUCH;
					record.type = nType;
					record.count = 0;
					record.bigger = false;
					recList.push_back(record);
				}
			} while (hasNext());
		}
	} else {
		bool bHasNext = false;
		do 
		{
			bHasNext = false;
			std::string& t2 = next();
			if (isMore(t2)) 
				continue;
			else if (getAction(t2) != GA_NONE)
			{
				action = getAction(t2);
				t2 = next();
			}

			TGoalRecord newRecord;
			newRecord.action = action;
			newRecord.bigger = true;
			newRecord.type = BB_NORMAL;
			newRecord.color = COL_ANY;
			newRecord.count = 0;

			int count;
			bool bReadNext = true;
			if (isdigit(t2.at(0))) 
				count = atoi(t2.c_str());
			else if (t2 == "all")
				count = -1;
			else if (t2 == "more")
				continue;
			else {
				count = 1;
				bReadNext = false;
			}

			newRecord.count = count;

			recList.push_back(newRecord);

			TGoalRecord& record = recList.back();
			if (hasNext()) { //pop 15
				if (bReadNext) 
					t2 = next();
				//next is color
				int col = getColor(t2);
				if (col != COL_NONE) {
					record.color = col;

					if (hasNext()) {
						std::string& t3 = next();
						if (t3 == "friend") {
							for (TGoalList::iterator it = recList.begin(); it != recList.end(); it++) {
								(*it).type = BB_FRIEND;
							}

							if (hasNext())
								t3 = next();
						} 
						
						if (isMore(t3)) {
							bHasNext = true;
						}
					}
				} else {
					int nType = getType(t2);
					if (nType != BB_NONE) {
						record.type = nType;
					}
				}
			}
		} while (hasNext());
	}
}

bool CGoalAnalyser::isMore(std::string& token)
{
	return (token == "," || token == "and" || token == "or" || token == "&");
}

bool CGoalAnalyser::hasNext()
{
	return nextPos < tokens.size();
}
std::string& CGoalAnalyser::fetchNext()
{
	return tokens.at(nextPos);
}
std::string& CGoalAnalyser::next()
{
	return tokens.at(nextPos++);
}

std::string CGoalAnalyser::toString()
{
	std::string strOut;
    char charr[256];

	for (TGoalList::iterator it = recList.begin(); it != recList.end(); it++) {
		TGoalRecord& record = *it;

		if (record.bigger == false)
			strOut += "Don't ";
		switch (record.action)
		{
		case GA_SAVE:
			strOut += "Save ";
			break;
		case GA_DROP:
			strOut += "Drop ";
			break;
		case GA_POP:
			strOut += "Pop ";
			break;
		case GA_TOUCH:
			strOut += "Touch ";
			break;
		default:
			break;
		}

		sprintf(charr, "%d %s %s,", record.count, STR_COLORS[record.color].c_str(), STR_TYPES[record.type].c_str());
		strOut += charr;
	}
	return strOut;
}

int getColor(const std::string& str) {
	for (int i = 0; i < COL_ANY; i++)
		if (str == STR_COLORS[i])
			return i;
	return COL_NONE;
}

int getType(const std::string& str) {
	for (int i = 0; i <= BB_FRIEND; i++)
		if (str == STR_TYPES[i])
			return i;
	return BB_NONE;
}

int getAction(std::string& str) {
	std::string lowerStr = str;
	std::transform(lowerStr.begin(), lowerStr.end(), lowerStr.begin(), ::tolower);

	for (int i = 0; i < GA_COUNT; i++)
		if (lowerStr == STR_ACTIONS[i])
			return i;
	return GA_NONE;
}

TGoalInfo g_GoalList[] =
{
	{ 9 ,"Save 5 bubbles", false},
	{ 10 ,"Don't pop any green bubbles", false},
	{ 10 ,"Save 5 bubbles", false},
	{ 11 ,"Don't pop any green or yellow bubbles", false},
	{ 11 ,"Save 8 bubbles", false},
	{ 12 ,"Drop 75 bubbles", false},
	{ 13 ,"Save 8 bubbles", false},
	{ 14 ,"Save 4 bubbles", false},
	{ 14 ,"Don't touch any stone bubbles", false},
	{ 15 ,"Don't pop any green bubbles", false},
	{ 15 ,"Save 9 bubbles", false},
	{ 16 ,"Don't pop any blue bubbles", false},
	{ 17 ,"Save 8 bubbles", false},
	{ 18 ,"Save 8 bubbles", false},
	{ 19 ,"Save 4 bubbles", false},
	{ 20 ,"Don't pop any blue or orange bubbles", false},
	{ 20 ,"Save 5 bubbles", false},
	{ 21 ,"Don't pop any blue bubbles", false},
	{ 21 ,"Save 10 bubbles", false},
	{ 22 ,"Don't pop more than 2 dynamite bubbles", false},
	{ 22 ,"Save 11 bubbles", false},
	{ 23 ,"Don't pop any yellow bubbles", false},
	{ 23 ,"Don't touch any stone bubbles", false},
	{ 24 ,"Don't pop any yellow, orange or blue bubbles", false},
	{ 25 ,"Don't pop more than 2 dynamite bubbles", false},
	{ 25 ,"Save 9 bubbles", false},
	{ 26 ,"Don't pop any orange bubbles", false},
	{ 26 ,"Drop 60 bubbles", false},
	{ 27 ,"Save 5 bubbles", false},
	{ 27 ,"Save 8 bubbles", false},
	{ 28 ,"Save 3 bubbles", false},
	{ 29 ,"Don't pop any red bubbles", false},
	{ 29 ,"Don't pop any zombie bubbles", false},
	{ 30 ,"Don't touch any stone bubbles", false},
	{ 31 ,"Pop 65 bubbles", false},
	{ 31 ,"Save 6 bubbles", false},
	{ 32 ,"Drop 78 bubbles", false},
	{ 33 ,"Drop 73 bubbles", false},
	{ 33 ,"Save 10 bubbles", false},
	{ 34 ,"Pop 135 bubbles", false},
	{ 35 ,"Don't touch any stone bubbles", false},
	{ 35 ,"Save 3 bubbles", false},
	{ 36 ,"Drop 75 bubbles", false},
	{ 36 ,"Save 3 bubbles", false},
	{ 37 ,"Save 2 bubbles", false},
	{ 38 ,"Drop 16 red bubbles", false},
	{ 38 ,"Save 4 bubbles", false},
	{ 39 ,"Save 4 bubbles", false},
	{ 40 ,"Save 8 bubbles", false},
	{ 41 ,"Don't touch any stone bubbles", false},
	{ 41 ,"Save 3 bubbles", false},
	{ 42 ,"Drop 85 bubbles", false},
	{ 43 ,"Don't touch any zombie bubbles", false},
	{ 43 ,"Save 5 bubbles", false},
	{ 44 ,"Save 2 bubbles", false},
	{ 45 ,"Drop 135 bubbles", false},
	{ 45 ,"Save 5 bubbles", false},
	{ 46 ,"Save 4 bubbles", false},
	{ 47 ,"Drop 75 bubbles", false},
	{ 47 ,"Save 2 bubbles", false},
	{ 48 ,"Drop yellow friend bubble", false},
	{ 48 ,"Save 6 bubbles", false},
	{ 49 ,"Save 2 bubbles", false},
	{ 50 ,"Save 5 bubbles", false},
	{ 51 ,"Drop 40 bubbles", false},
	{ 51 ,"Save 2 bubbles", false},
	{ 52 ,"Don't touch any stone or zombie bubbles", false},
	{ 52 ,"Save 4 bubbles", false},
	{ 53 ,"Don't touch any zombie bubbles", false},
	{ 53 ,"Save 2 bubbles", false},
	{ 54 ,"Drop 80 bubbles", false},
	{ 54 ,"Save 1 bubble", false},
	{ 55 ,"Don't touch any zombie bubbles", false},
	{ 55 ,"Save 2 bubbles", false},
	{ 56 ,"Don't touch any magnet bubbles", false},
	{ 56 ,"Save 3 bubbles", false},
	{ 57 ,"Drop 155 bubbles", false},
	{ 57 ,"Save 5 bubbles", false},
	{ 58 ,"Save 3 bubbles", false},
	{ 59 ,"Don't touch any zombie bubbles", false},
	{ 59 ,"Save 5 bubbles", false},
	{ 60 ,"Don't touch any stone bubbles", false},
	{ 60 ,"Save 4 bubbles", false},
	{ 61 ,"Don't touch any zombie bubbles", false},
	{ 61 ,"Save 2 bubbles", false},
	{ 62 ,"Drop 15 ice bubbles", false},
	{ 62 ,"Save 3 bubbles", false},
	{ 63 ,"Don't touch any zombie bubbles", false},
	{ 63 ,"Save 3 bubbles", false},
	{ 64 ,"Pop 140 bubbles", false},
	{ 64 ,"Save 3 bubbles", false},
	{ 65 ,"Don't touch any magnet bubbles", false},
	{ 65 ,"Save 4 bubbles", false},
	{ 66 ,"Save 7 bubbles", false},
	{ 67 ,"Pop 200 bubbles", false},
	{ 67 ,"Save 7 bubbles", false},
	{ 68 ,"Don't touch any stone bubbles", false},
	{ 68 ,"Save 5 bubbles", false},
	{ 69 ,"Drop 1 candy bubble", false},
	{ 69 ,"Save 8 bubbles", false},
	{ 70 ,"Don't touch any zombie bubbles", false},
	{ 70 ,"Save 2 bubbles", false},
	{ 71 ,"Don't touch any magnet bubbles", false},
	{ 71 ,"Save 3 bubbles", false},
	{ 72 ,"Don't touch any magnet bubbles", false},
	{ 73 ,"Drop 140 bubbles", false},
	{ 73 ,"Save 5 bubbles", false},
	{ 74 ,"Drop 55 bubbles", false},
	{ 74 ,"Save 3 bubbles", false},
	{ 75 ,"Don't touch any zombie bubbles", false},
	{ 75 ,"Save 4 bubbles", false},
	{ 76 ,"Don't touch any magnet or stone bubbles", false},
	{ 76 ,"Save 2 bubbles", false},
	{ 77 ,"Don't touch any magnet bubbles", false},
	{ 77 ,"Save 2 bubbles", false},
	{ 78 ,"Don't touch any magnet bubbles", false},
	{ 78 ,"Save 11 bubbles", false},
	{ 79 ,"Don't touch any magnet bubbles", false},
	{ 79 ,"Save 6 bubbles", false},
	{ 80 ,"Don't touch any zombie bubbles", false},
	{ 80 ,"Save 11 bubbles", false},
	{ 81 ,"Save 9 bubbles", false},
	{ 82 ,"Don't touch any stone bubbles", false},
	{ 82 ,"Save 2 bubbles", false},
	{ 83 ,"Save 2 bubbles", false},
	{ 84 ,"Don't touch any zombie bubbles", false},
	{ 85 ,"Don't touch any magnet or zombie bubbles", false},
	{ 85 ,"Save 9 bubbles", false},
	{ 86 ,"Don't touch any magnet bubbles", false},
	{ 86 ,"Save 6 bubbles", false},
	{ 87 ,"Save 10 bubbles", false},
	{ 88 ,"Don't touch any magnet bubbles", false},
	{ 88 ,"Save 15 bubbles", false},
	{ 89, "Save 3 bubbles", false},
	{ 90, "Save 4 bubbles", false},
	{ 90, "Don't touch any zombie bubbles", false},
	{ 91, "Save 4 bubbles", false},
	{ 91, "Drop 3 candy bubbles", false},
	{ 92, "Save 8 bubbles  ", false},
	{ 92, "Drop 1 dynamite bubble", false},
	{ 93, "Save 6 bubbles  ", false},
	{ 93, "Don't touch any magnet bubbles", false},
	{ 94, "Save 3 bubbles", false},
	{ 94, "Pop 270 bubbles", false},
	{ 95, "Save 7 bubbles", false},
	{ 95, "Don't touch any magnet bubbles", false},
	{ 96, "Save 7 bubbles", false},
	{ 96, "Pop 200 bubbles", false},
	{ 97, "Save 6 bubbles", false},
	{ 97, "Pop 150 bubbles", false},
	{ 98, "Save 3 bubbles", false},
	{ 98, "Don't touch any zombie bubbles", false},
	{ 99, "Save 3 bubbles", false},
	{ 99, "Don't touch any magnet bubbles", false},
	{ 100, "Save 6 bubbles", false},
	{ 100, "Drop 100 bubbles", false},
	{ 101, "Save 6 bubbles", false},
	{ 101, "Don't touch any magnet bubbles", false},
	{ 102, "Save 6 bubbles", false},
	{ 102, "Don't touch any magnet bubbles", false},
	{ 103, "Save 9 bubbles", false},
	{ 103, "Pop 245 bubbles", false},
	{ 104, "Save 6 bubbles", false},
	{ 104, "Pop 160 bubbles", false},
	{ 105, "Save 4 bubbles", false},
	{ 105, "Don't touch any zombie bubbles", false},
	{ 106, "Save 9 bubbles", false},
	{ 106, "Pop 245 bubbles", false},
	{ 107, "Save 4 bubbles", false},
	{ 107, "Drop 110 bubbles", false},
	{ 108, "Save 7 bubbles", false},
	{ 108, "Pop 30 green bubbles", false},
	{ 109, "Save 4 bubbles", false},
	{ 109, "Pop 175 bubbles", false},
	{ 110, "Save 2 bubbles", false},
	{ 110, "Pop 245 bubbles", false},
	{ 111, "Save 5 bubbles", false},
	{ 111, "Don't touch any magnet bubbles", false},
	{ 112, "Save 8 bubbles", false},
	{ 112, "Pop 180 bubbles", false},
	{ 113, "Save 3 bubbles", false},
	{ 113, "Pop 230 bubbles", false},
	{ 114, "Save 4 bubbles", false},
	{ 114, "Don't touch any stone or zombie bubbles", false},
	{ 115, "Save 8 bubbles", false},
	{ 115, "Don't touch any magnet bubbles", false},
	{ 116, "Save 5 bubbles", false},
	{ 116, "Pop 180 bubbles", false},
	{ 117, "Save 5 bubbles", false},
	{ 117, "Don't touch any stone or magnet bubbles", false},
	{ 118, "Save 6 bubbles", false},
	{ 118, "Don't pop the dynamite bubble", false},
	{ 119, "Save 6 bubbles", false},
	{ 119, "Don't pop more than 5 dynamite bubbles", false},
	{ 120, "Save 7 bubbles", false},
	{ 120, "Don't pop more than 1 dynamite bubble", false},
	{ 121, "Save 4 bubbles", false},
	{ 121, "Don't touch any magnet bubbles", false},
	{ 122, "Save 5 bubbles", false},
	{ 122, "Don't pop more than 4 candy bubbles", false},
	{ 123, "Save 6 bubbles", false},
	{ 123, "Don't pop more than 1 dynamite and  3 candy bubbles", false},
	{ 124, "Save 9 bubbles", false},
	{ 124, "Don't touch any magnet bubbles", false},
	{ 125, "Save 5 bubbles", false},
	{ 125, "Pop 35 purple bubbles", false},
	{ 126, "Save 4 bubbles", false},
	{ 126, "Don't touch any magnet bubbles", false},
	{ 127, "Save 3 bubbles", false},
	{ 127, "Don't touch any magnet bubbles", false},
};

TGoalInfo g_DailyGoalList[] =
{
	{ 9, "Don't touch any stone bubbles", false},
	{ 9, "Don't pop any yellow bubbles", false},
	{ 9, "Pop 85 bubbles", false},
	{ 9, "Save 9 bubbles", false},
	{ 9, "Don't pop any green bubbles", false},
	{ 10, "Drop 10 blue bubbles", false},
	{ 10, "Drop yellow friend bubble", false},
	{ 10, "Drop 19 red bubbles", false},
	{ 10, "Save 6 bubbles", false},
	{ 10, "Drop 65 bubbles", false},
	{ 11, "Drop 20 yellow bubbles", false},
	{ 11, "Drop all blue bubbles", false},
	{ 11, "Drop 20 yellow and 19 green bubbles", false},
	{ 11, "Save 9 bubbles", false},
	{ 11, "Drop 50 bubbles", false},
	{ 12, "Save 5 bubbles", false},
	{ 12, "Pop 140 bubbles", false},
	{ 12, "Save 7 bubbles", false},
	{ 12, "Drop red friend bubble", false},
	{ 12, "Save 8 bubbles", false},
	{ 13, "Drop 4 red bubbles", false},
	{ 13, "Drop 2 green bubbles", false},
	{ 13, "Drop 5 orange bubbles", false},
	{ 13, "Drop 29 bubbles", false},
	{ 13, "Save 8 bubbles", false},
	{ 14, "Drop 1 orange friend bubble", false},
	{ 14, "Drop 25 bubbles", false},
	{ 14, "Drop 30 bubbles", false},
	{ 14, "Save 9 bubbles", false},
	{ 14, "Drop 38 bubbles", false},
	{ 15, "Drop 32 bubbles", false},
	{ 15, "Pop 15 purple bubbles", false},
	{ 15, "Save 10 bubbles", false},
	{ 15, "Drop 45 bubbles", false},
	{ 15, "Save 11 bubbles", false},
	{ 16, "Drop 50 bubbles", false},
	{ 16, "Save 10 bubbles", false},
	{ 16, "Drop 3 yellow bubbles", false},
	{ 16, "Drop 84 bubbles", false},
	{ 16, "Save 14 bubbles", false},
	{ 17, "Drop 25 bubbles", false},
	{ 17, "Pop 8 orange bubbles", false},
	{ 17, "Drop 4 blue bubbles", false},
	{ 17, "Save 10 bubbles", false},
	{ 17, "Drop 35 bubbles", false},
	{ 18, "Pop 64 bubbles", false},
	{ 18, "Don't touch stone bubbles", false},
	{ 18, "Drop 20 bubbles", false},
	{ 18, "Save 9 bubbles", false},
	{ 18, "Drop 35 bubbles", false},
	{ 19, "Drop 3 ice bubbles", false},
	{ 19, "Drop 14 blue bubbles", false},
	{ 19, "Drop 30 bubbles", false},
	{ 19, "Save 7 bubbles", false},
	{ 19, "Pop 110 bubbles", false},
	{ 20, "Pop 29 purple bubbles", false},
	{ 20, "Pop 43 red bubbles", false},
	{ 20, "Drop 60 bubbles", false},
	{ 20, "Drop 85 bubbles", false},
	{ 20, "Save 7 bubbles", false},
	{ 21, "Save 11 bubbles", false},
	{ 21, "Drop 15 bubbles", false},
	{ 21, "Save 12 bubbles", false},
	{ 21, "Pop 42 bubbles", false},
	{ 21, "Drop 25 bubbles", false},
	{ 22, "Drop yellow friend bubble", false},
	{ 22, "Save 12 bubbles", false},
	{ 22, "Pop 47 bubbles", false},
	{ 22, "Save 13 bubbles", false},
	{ 22, "Drop 58 bubbles", false},
	{ 23, "Save 7 bubbles", false},
	{ 23, "Drop yellow friend bubble", false},
	{ 23, "Save 9 bubbles", false},
	{ 23, "Pop 49 bubbles", false},
	{ 23, "Drop 37 bubbles", false},
	{ 24, "Drop 50 bubbles", false},
	{ 24, "Save 6 bubbles", false},
	{ 24, "Pop 89 bubbles", false},
	{ 24, "Drop 60 bubbles", false},
	{ 24, "Save 8 bubbles", false},
	{ 25, "Drop 75 bubbles", false},
	{ 25, "Drop blue friend bubble", false},
	{ 25, "Pop 80 bubbles", false},
	{ 25, "Drop 80 bubbles", false},
	{ 25, "Save 10 bubbles", false},
	{ 26, "Pop 100 bubbles", false},
	{ 26, "Drop 78 bubbles", false},
	{ 26, "Save 4 bubbles", false},
	{ 26, "Pop 130 bubbles", false},
	{ 26, "Save 5 bubbles", false},
	{ 27, "Pop 90 bubbles", false},
	{ 27, "Drop green and blue friend bubbles", false},
	{ 27, "Save 9 bubbles", false},
	{ 27, "Drop 65 bubbles", false},
	{ 27, "Drop 6 ice bubbles", false},
	{ 28, "Drop 55 bubbles", false},
	{ 28, "Drop 12 zombie bubbles", false},
	{ 28, "Drop 68 bubbles", false},
	{ 28, "Pop 120 bubbles", false},
	{ 28, "Save 5 bubbles", false},
	{ 29, "Pop 120 bubbles", false},
	{ 29, "Drop 75 bubbles", false},
	{ 29, "Drop 1 yellow friend bubble", false},
	{ 29, "Drop 90 bubbles", false},
	{ 29, "Save 8 bubbles", false},
	{ 30, "Pop 30 bubbles", false},
	{ 30, "Save 8 bubbles", false},
	{ 30, "Drop 50 bubbles", false},
	{ 30, "Save 10 bubbles", false},
	{ 30, "Drop 65 bubbles", false},
	{ 31, "Drop 20 stone bubbles", false},
	{ 31, "Pop 75 bubbles", false},
	{ 31, "Drop yellow and orange friend bubbles", false},
	{ 31, "Drop 100 bubbles", false},
	{ 31, "Save 8 bubbles", false},
	{ 32, "Save 7 bubbles", false},
	{ 32, "Pop 102 bubbles", false},
	{ 32, "Save 10 bubbles", false},
	{ 32, "Drop 90 bubbles", false},
	{ 32, "Drop green, orange and red friend bubbles", false},
	{ 33, "Pop 78 bubbles", false},
	{ 33, "Drop green and blue friend bubbles", false},
	{ 33, "Drop 4 ice bubbles", false},
	{ 33, "Drop all zombie bubbles", false},
	{ 33, "Save 12 bubbles", false},
	{ 34, "Drop 95 bubbles", false},
	{ 34, "Drop 2 red friend bubbles", false},
	{ 34, "Pop 154 bubbles", false},
	{ 34, "Drop 110 bubbles", false},
	{ 34, "Save 4 bubbles", false},
	{ 35, "Drop 70 bubbles", false},
	{ 35, "Pop 95 bubbles", false},
	{ 35, "Drop 8 ice bubbles", false},
	{ 35, "Drop 78 bubbles", false},
	{ 35, "Save 5 bubbles", false},
	{ 36, "Drop yellow friend bubble", false},
	{ 36, "Drop 85 bubbles", false},
	{ 36, "Drop 19 ice bubbles", false},
	{ 36, "Drop 1 blue and 2 orange friend bubbles", false},
	{ 36, "Pop 85 bubbles", false},
	{ 37, "Drop 85 bubbles", false},
	{ 37, "Drop 10 ice bubbles", false},
	{ 37, "Pop 132 bubbles", false},
	{ 37, "Drop 100 bubbles", false},
	{ 37, "Save 4 bubbles", false},
	{ 38, "Drop 115 bubbles", false},
	{ 38, "Save 5 bubbles", false},
	{ 38, "Pop 150 bubbles", false},
	{ 38, "Drop 175 bubbles", false},
	{ 38, "Save 6 bubbles", false},
	{ 39, "Save 6 bubbles", false},
	{ 39, "Drop 5 orange friend bubbles", false},
	{ 39, "Pop 200 bubbles", false},
	{ 39, "Drop 130 bubbles", false},
	{ 39, "Save 9 bubbles", false},
	{ 40, "Drop 50 bubbles", false},
	{ 40, "Pop 135 bubbles", false},
	{ 40, "Drop 58 bubbles", false},
	{ 40, "Save 10 bubbles", false},
	{ 40, "Drop green and yellow friend bubbles", false},
	{ 41, "Drop 85 bubbles", false},
	{ 41, "Pop 171 bubbles", false},
	{ 41, "Drop 95 bubbles", false},
	{ 41, "Drop 1 candy bubble", false},
	{ 41, "Save 5 bubbles", false},
	{ 42, "Pop 170 bubbles", false},
	{ 42, "Drop yellow friend", false},
	{ 42, "Don't touch any stone bubbles", false},
	{ 42, "Save 3 bubbles", false},
	{ 42, "Drop 102 bubbles", false},
	{ 43, "Pop 112 bubbles", false},
	{ 43, "Save 7 bubbles", false},
	{ 43, "Drop 115 bubbles", false},
	{ 43, "Save 9 bubbles", false},
	{ 43, "Drop 130 bubbles", false},
	{ 44, "Drop 82 bubbles", false},
	{ 44, "Drop 10 ice bubbles", false},
	{ 44, "Save 4 bubbles", false},
	{ 44, "Drop 98 bubbles", false},
	{ 44, "Pop 140 bubbles", false},
	{ 45, "Drop 15 ice bubbles", false},
	{ 45, "Drop 1 candy bubble", false},
	{ 45, "Drop 150 bubbles", false},
	{ 45, "Pop 158 bubbles", false},
	{ 45, "Save 9 bubbles", false},
	{ 46, "Drop 100 bubbles", false},
	{ 46, "Pop 205 bubbles", false},
	{ 46, "Drop 115 bubbles", false},
	{ 46, "Drop 1 candy bubble", false},
	{ 46, "Save 7 bubbles", false},
	{ 47, "Pop 160 bubbles", false},
	{ 47, "Drop 85 bubbles", false},
	{ 47, "Don't pop more than 9 red bubbles", false},
	{ 47, "Drop 16 green bubbles", false},
	{ 47, "Save 5 bubbles", false},
	{ 48, "Save 9 bubbles", false},
	{ 48, "Drop 2 candy bubbles", false},
	{ 48, "Pop 190 bubbles", false},
	{ 48, "Drop 110 bubbles", false},
	{ 48, "Save 13 bubbles", false},
	{ 49, "Drop 92 bubbles", false},
	{ 49, "Don't touch any magnet bubbles", false},
	{ 49, "Drop 100 bubbles", false},
	{ 49, "Pop 110 bubbles", false},
	{ 49, "Save 5 bubbles", false},
	{ 50, "Drop 95 bubbles", false},
	{ 50, "Pop 130 bubbles", false},
	{ 50, "Save 7 bubbles", false},
	{ 50, "Save 9 bubbles", false},
	{ 50, "Drop 100 bubbles", false},
	{ 51, "Pop 100 bubbles", false},
	{ 51, "Don't pop any blue bubbles", false},
	{ 51, "Don't touch any magnet bubbles", false},
	{ 51, "Drop 70 bubbles", false},
	{ 51, "Don't touch any stone or magnet bubbles", false},
	{ 52, "Drop 70 bubbles", false},
	{ 52, "Drop yellow friend bubble", false},
	{ 52, "Pop 115 bubbles", false},
	{ 52, "Drop 80 bubbles", false},
	{ 52, "Save 8 bubbles", false},
	{ 53, "Drop 88 bubbles", false},
	{ 53, "Save 3 bubbles", false},
	{ 53, "Drop 120 bubbles", false},
	{ 53, "Save 5 bubbles", false},
	{ 53, "Pop 155 bubbles", false},
	{ 54, "Pop 120 bubbles", false},
	{ 54, "Save 3 bubbles", false},
	{ 54, "Drop 90 bubbles", false},
	{ 54, "Don't touch any zombie bubbles", false},
	{ 54, "Save 5 bubbles", false},
	{ 55, "Drop 105 bubbles", false},
	{ 55, "Pop 115 bubbles", false},
	{ 55, "Drop 12 ice bubbles", false},
	{ 55, "Drop 120 bubbles", false},
	{ 55, "Save 5 bubbles", false},
	{ 56, "Drop 108 bubbles", false},
	{ 56, "Save 5 bubbles", false},
	{ 56, "Pop 105 bubbles", false},
	{ 56, "Drop 120 bubbles", false},
	{ 56, "Save 7 bubbles", false},
	{ 57, "Don't touch any magnet bubbles", false},
	{ 57, "Save 8 bubbles", false},
	{ 57, "Pop 180 bubbles", false},
	{ 57, "Don't touch any magnet or stone bubbles", false},
	{ 57, "Save 11 bubbles", false},
	{ 58, "Save 5 bubbles", false},
	{ 58, "Drop 100 bubbles", false},
	{ 58, "Pop 157 bubbles", false},
	{ 58, "Drop 120 bubbles", false},
	{ 58, "Save 7 bubbles", false},
	{ 59, "Drop 115 bubbles", false},
	{ 59, "Drop 125 bubbles", false},
	{ 59, "Pop 125 bubbles", false},
	{ 59, "Drop 135 bubbles", false},
	{ 59, "Drop 1 dynamite bubble", false},
	{ 60, "Pop 160 bubbles", false},
	{ 60, "Drop 80 bubbles", false},
	{ 60, "Drop 93 bubbles", false},
	{ 60, "Save 7 bubbles", false},
	{ 60, "Don't touch any ice or magnet bubbles", false},
	{ 61, "Drop 110 bubbles", false},
	{ 61, "Don't touch any magnet bubbles", false},
	{ 61, "Pop 125 bubbles", false},
	{ 61, "Drop 125 bubbles", false},
	{ 61, "Save 5 bubbles", false},
	{ 62, "Drop 110 bubbles", false},
	{ 62, "Pop 150 bubbles", false},
	{ 62, "Drop 110 bubbles", false},
	{ 62, "Save 5 bubbles", false},
	{ 62, "Don't pop more than 2 dynamite bubbles", false},
	{ 63, "Drop 150 bubbles", false},
	{ 63, "Drop 168 bubbles", false},
	{ 63, "Pop 145 bubbles", false},
	{ 63, "Drop 14 yellow and 16 ice bubbles", false},
	{ 63, "Save 6 bubbles", false},
	{ 64, "Save 5 bubbles", false},
	{ 64, "Drop 85 bubbles", false},
	{ 64, "Don't touch any stone bubbles", false},
	{ 64, "Drop 100 bubbles", false},
	{ 64, "Save 7 bubbles", false},
	{ 65, "Drop 75 bubbles", false},
	{ 65, "Pop 145 bubbles", false},
	{ 65, "Save 6 bubbles", false},
	{ 65, "Drop 90 bubbles", false},
	{ 65, "Save 9 bubbles", false},
	{ 66, "Drop 110 bubbles", false},
	{ 66, "Save 10 bubbles", false},
	{ 66, "Drop 122 bubbles", false},
	{ 66, "Pop 158 bubbles", false},
	{ 66, "Save 12 bubbles", false},
	{ 67, "Drop 88 bubbles", false},
	{ 67, "Save 9 bubbles", false},
	{ 67, "Pop 235 bubbles", false},
	{ 67, "Drop 100 bubbles", false},
	{ 67, "Save 11 bubbles", false},
	{ 68, "Pop 145 bubbles", false},
	{ 68, "Don't touch any magnet or stone bubbles", false},
	{ 68, "Drop 105 bubbles", false},
	{ 68, "Drop 115 bubbles", false},
	{ 68, "Save 8 bubbles", false},
	{ 69, "Drop 100 bubbles", false},
	{ 69, "Drop 120 bubbles", false},
	{ 69, "Save 10 bubbles", false},
	{ 69, "Save 13 bubbles", false},
	{ 69, "Pop 132 bubbles", false},
	{ 70, "Drop 95 bubbles", false},
	{ 70, "Drop 108 bubbles", false},
	{ 70, "Save 5 bubbles", false},
	{ 70, "Pop 124 bubbles", false},
	{ 70, "Drop 118 bubbles", false},
	{ 71, "Pop 155 bubbles", false},
	{ 71, "Drop 110 bubbles", false},
	{ 71, "Drop 125 bubbles", false},
	{ 71, "Don't pop any green bubble", false},
	{ 71, "Save 6 bubbles", false},
	{ 72, "Drop 60 bubbles", false},
	{ 72, "Save 1 bubble", false},
	{ 72, "Drop 83 bubbles", false},
	{ 72, "Save 3 bubbles", false},
	{ 72, "Pop 185 bubbles", false},
	{ 73, "Don't touch any zombie bubbles", false},
	{ 73, "Don't touch any magnet bubbles", false},
	{ 73, "Drop 167 bubbles", false},
	{ 73, "Pop 178 bubbles", false},
	{ 73, "Save 8 bubbles", false},
	{ 74, "Don't touch any stone or ice bubbles", false},
	{ 74, "Save 6 bubbles", false},
	{ 74, "Drop 78 bubbles", false},
	{ 74, "Pop 44 yellow bubbles", false},
	{ 74, "Pop 190 bubbles", false},
	{ 75, "Drop 130 bubbles", false},
	{ 75, "Pop 45 green bubbles and 14 blue bubbles", false},
	{ 75, "Pop 150 bubbles", false},
	{ 75, "Save 7 bubbles", false},
	{ 75, "Drop 150 bubbles", false},
	{ 76, "Drop 15 blue and 10 purple bubbles", false},
	{ 76, "Drop 95 bubbles", false},
	{ 76, "Pop 202 bubbles", false},
	{ 76, "Drop 110 bubbles", false},
	{ 76, "Save 4 bubbles", false},
	{ 77, "Drop 70 bubbles", false},
	{ 77, "Pop 50 blue bubbles", false},
	{ 77, "Pop 190 bubbles", false},
	{ 77, "Save 4 bubbles", false},
	{ 77, "Drop 85 bubbles", false},
	{ 78, "Pop 130 bubbles", false},
	{ 78, "Drop 100 bubbles", false},
	{ 78, "Drop 28 ice bubbles", false},
	{ 78, "Drop 125 bubbles", false},
	{ 78, "Save 20 bubbles", false},
	{ 79, "Drop 80 bubbles", false},
	{ 79, "Save 9 bubbles", false},
	{ 79, "Pop 190 bubbles", false},
	{ 79, "Save 12 bubbles", false},
	{ 79, "Drop 95 bubbles", false},
	{ 80, "Don't touch any magnet", false},
	{ 80, "Drop 80 bubbles", false},
	{ 80, "Drop 7 ice bubbles", false},
	{ 80, "Pop 160 bubbles", false},
	{ 80, "Drop 92 bubbles", false},
	{ 81, "Drop 95 bubbles", false},
	{ 81, "Pop 150 bubbles", false},
	{ 81, "Drop 108 bubbles", false},
	{ 81, "Drop 15 ice bubbles", false},
	{ 81, "Pop 28 blue bubbles and 45 red bubbles", false},
	{ 82, "Save 5 bubbles", false},
	{ 82, "Drop 130 bubbles", false},
	{ 82, "Drop 145 bubbles", false},
	{ 82, "Pop 138 bubbles", false},
	{ 82, "Drop 2 candy bubbles", false},
	{ 83, "Drop 105 bubbles", false},
	{ 83, "Pop 30 blue and 35 yellow bubbles", false},
	{ 83, "Drop 120 bubbles", false},
	{ 83, "Save 4 bubbles", false},
	{ 83, "Pop 135 Bubbles", false},
	{ 84, "Drop 120 bubbles", false},
	{ 84, "Pop 140 bubbles", false},
	{ 84, "Save 3 bubbles", false},
	{ 84, "Drop 145 bubbles", false},
	{ 84, "Pop 155 bubbles", false},
	{ 85, "Pop 190 bubbles", false},
	{ 85, "Drop 100 bubbles", false},
	{ 85, "Drop 20 red bubbles", false},
	{ 85, "Drop 125 bubbles", false},
	{ 85, "Pop 15 blue and drop 20 blue bubbles", false},
	{ 86, "Drop 90 bubbles", false},
	{ 86, "Pop 36 red bubbles", false},
	{ 86, "Pop 154 bubbles", false},
	{ 86, "Drop 100 bubbles", false},
	{ 86, "Save 12 bubbles", false},
	{ 87, "Don't touch any magnet bubbles", false},
	{ 87, "Drop 115 bubbles", false},
	{ 87, "Pop 190 bubbles", false},
	{ 87, "Drop 125 bubbles", false},
	{ 87, "Save 15 bubbles", false},
	{ 88, "Pop 175 bubbles", false},
	{ 88, "Drop 125 bubbles", false},
	{ 88, "Drop 2 candy and 15 ice bubbles", false},
	{ 88, "Save 20 bubbles", false},
	{ 88, "Drop 145 bubbles", false},
	{ 89, "Pop 190 bubbles", false},
	{ 89, "Don't touch any zombie bubbles", false},
	{ 89, "Drop 135 bubbles", false},
	{ 89, "Save 7 bubbles", false},
	{ 89, "Drop 150 bubbles.", false},
	{ 90, "Drop 145 bubbles", false},
	{ 90, "Pop 200 bubbles", false},
	{ 90, "Drop 170 bubbles", false},
	{ 90, "Pop 230 bubbles", false},
	{ 90, "Save 8 bubbles", false},
	{ 91, "Drop 150 bubbles", false},
	{ 91, "Save 7 bubbles", false},
	{ 91, "Drop 175 bubbles", false},
	{ 91, "Pop 185 bubbles", false},
	{ 91, "Save 11 bubbles", false},
	{ 92, "Pop 265 bubbles", false},
	{ 92, "Drop 130 bubbles", false},
	{ 92, "Drop 1 candy bubble", false},
	{ 92, "Pop 285 bubbles", false},
	{ 92, "Drop 140 bubbles", false},
	{ 93, "Don't touch any zombie bubbles", false},
	{ 93, "Pop 200 bubbles", false},
	{ 93, "Drop 155 bubbles", false},
	{ 93, "Save 11 bubbles", false},
	{ 93, "Pop 218 bubbles", false},
	{ 94, "Drop 160 bubbles", false},
	{ 94, "Don't touch any magnet bubbles", false},
	{ 94, "Pop 300 bubbles", false},
	{ 94, "Save 6 bubbles  ", false},
	{ 94, "Drop 180 bubbles", false},
	{ 95, "Pop 240 bubbles", false},
	{ 95, "Drop 200 bubbles", false},
	{ 95, "Save 13 bubbles", false},
	{ 95, "Drop 225 bubbles", false},
	{ 95, "Pop 260 bubbles", false},
	{ 96, "Drop 70 bubbles", false},
	{ 96, "Drop 10 purple bubbles", false},
	{ 96, "Save 13 bubbles", false},
	{ 96, "Drop 85 bubbles", false},
	{ 96, "Pop 215 bubbles", false},
	{ 97, "Drop red friend bubble", false},
	{ 97, "Drop 140 bubbles", false},
	{ 97, "Pop 175 bubbles", false},
	{ 97, "Don't touch any stone bubbles", false},
	{ 97, "Save 11 bubbles", false},
	{ 98, "Drop 135 bubbles", false},
	{ 98, "Pop 200 bubbles", false},
	{ 98, "Save 6 bubbles", false},
	{ 98, "Drop 154 bubbles", false},
	{ 98, "Pop 230 bubbles", false},
	{ 99, "Save 6 bubbles", false},
	{ 99, "Pop 110 bubbles", false},
	{ 99, "Pop 38 orange bubbles", false},
	{ 99, "Drop 14 snowflake bubbles", false},
	{ 99, "Drop 137 bubbles", false},
	{ 100, "Pop 150 bubbles", false},
	{ 100, "Pop 40 blue bubbles", false},
	{ 100, "Save 10 bubbles", false},
	{ 100, "Drop 25 blue bubbles", false},
	{ 100, "Drop 128 bubbles", false},
	{ 101, "Pop 190 bubbles", false},
	{ 101, "Save 10 bubbles", false},
	{ 101, "Drop 25 blue bubbles", false},
	{ 101, "Pop 215 bubbles", false},
	{ 101, "Drop 130 bubbles", false},
	{ 102, "Drop 110 bubbles", false},
	{ 102, "Pop 200 bubbles", false},
	{ 102, "Save 12 bubbles", false},
	{ 102, "Drop 13 red and 16 purple bubbles", false},
	{ 102, "Drop 125 bubbles", false},
	{ 103, "Drop 150 bubbles", false},
	{ 103, "Drop 20 red bubbles", false},
	{ 103, "Pop 260 bubbles", false},
	{ 103, "Drop 170 bubbles", false},
	{ 103, "Save 17 bubbles", false},
	{ 104, "Pop 50 blue bubbles", false},
	{ 104, "Drop 25 blue bubbles", false},
	{ 104, "Save 12 bubbles", false},
	{ 104, "Pop 180 bubbles", false},
	{ 104, "Drop 150 bubbles", false},
	{ 105, "Drop 125 bubbles", false},
	{ 105, "Save 8 bubbles", false},
	{ 105, "Drop 17 green and 17 blue bubbles", false},
	{ 105, "Pop 255 bubbles", false},
	{ 105, "Drop 145 bubbles", false},
	{ 106, "Pop 65 blue bubbles", false},
	{ 106, "Drop 215 bubbles", false},
	{ 106, "Save 16 bubbles", false},
	{ 106, "Pop 270 bubbles", false},
	{ 106, "Drop 240 bubbles", false},
	{ 107, "Pop 205 bubbles", false},
	{ 107, "Save 8 bubbles", false},
	{ 107, "Don't pop any yellow bubbles", false},
	{ 107, "Drop 60 snowflake bubbles", false},
	{ 107, "Drop 140 bubbles", false},
	{ 108, "Pop 35 orange bubbles", false},
	{ 108, "Drop 120 bubbles", false},
	{ 108, "Pop 140 bubbles", false},
	{ 108, "Drop 135 bubbles", false},
	{ 108, "Save 12 bubbles", false},
	{ 109, "Pop 40 purple bubbles", false},
	{ 109, "Drop 160 bubbles", false},
	{ 109, "Save 8 bubbles", false},
	{ 109, "Drop 180 bubbles", false},
	{ 109, "Pop 195 bubbles", false},
	{ 110, "Drop 15 yellow and 12 snowflake bubbles", false},
	{ 110, "Pop 90 yellow bubbles", false},
	{ 110, "Save 5 bubbles", false},
	{ 110, "Pop 265 bubbles", false},
	{ 110, "Drop 120 bubbles", false},
	{ 111, "Drop 100 bubbles", false},
	{ 111, "Pop 215 bubbles", false},
	{ 111, "Don't pop any purple bubbles", false},
	{ 111, "Save 9 bubbles", false},
	{ 111, "Pop 230 bubbles", false},
	{ 112, "Pop 45 yellow bubbles", false},
	{ 112, "Drop 150 bubbles", false},
	{ 112, "Don't touch any stone bubble", false},
	{ 112, "Save 14 bubbles", false},
	{ 112, "Drop 165 bubbles", false},
	{ 113, "Pop 41 green bubbles", false},
	{ 113, "Save 6 bubbles", false},
	{ 113, "Drop 90 bubbles", false},
	{ 113, "Pop 245 bubbles", false},
	{ 113, "Drop 100 bubbles", false},
	{ 114, "Pop 44 red bubbles", false},
	{ 114, "Drop 20 orange bubbles", false},
	{ 114, "Save 8 bubbles", false},
	{ 114, "Pop 210 bubbles", false},
	{ 114, "Drop 145 bubbles", false},
	{ 115, "Pop 72 green bubbles", false},
	{ 115, "Save 15 bubbles", false},
	{ 115, "Drop 15 yellow bubbles", false},
	{ 115, "Pop 245 bubbles", false},
	{ 115, "Drop 115 bubbles", false},
	{ 116, "Pop 39 blue and 34 purple bubbles", false},
	{ 116, "Drop 115 bubbles", false},
	{ 116, "Save 10 bubbles", false},
	{ 116, "Pop 205 bubbles", false},
	{ 116, "Drop 132 bubbles", false},
	{ 117, "Pop 45 orange bubbles", false},
	{ 117, "Pop 50 blue bubbles", false},
	{ 117, "Drop 105 bubbles", false},
	{ 117, "Save 9 bubbles", false},
	{ 117, "Pop 210 bubbles", false},
	{ 118, "Drop 115 bubbles", false},
	{ 118, "Pop 50 red bubbles", false},
	{ 118, "Save 11 bubbles", false},
	{ 118, "Pop 180 bubbles", false},
	{ 118, "Drop 130 bubbles", false},
	{ 119, "Pop 55 yellow and 24 green bubbles", false},
	{ 119, "Don't touch any magnet or zombie bubbles", false},
	{ 119, "Pop 165 bubbles", false},
	{ 119, "Save 10 bubbles", false},
	{ 119, "Drop 140 bubbles", false},
	{ 120, "Pop 55 green bubbles", false},
	{ 120, "Drop 25 orange bubbles", false},
	{ 120, "Save 12 bubbles", false},
	{ 120, "Pop 185 bubbles", false},
	{ 120, "Drop 115 bubbles", false},
	{ 121, "Drop 15 snowflake bubbles", false},
	{ 121, "Pop 55 green bubbles", false},
	{ 121, "Drop 135 bubbles", false},
	{ 121, "Pop 160 bubbles", false},
	{ 121, "Save 8 bubbles", false},
	{ 122, "Pop 50 green bubbles", false},
	{ 122, "Don't touch any magnet bubbles", false},
	{ 122, "Pop 225 bubbles", false},
	{ 122, "Save 9 bubbles", false},
	{ 122, "Drop 25 yellow and 15 orange bubbles", false},
	{ 123, "Pop 45 blue bubbles", false},
	{ 123, "Drop 20 green bubbles", false},
	{ 123, "Save 10 bubbles", false},
	{ 123, "Pop 170 bubbles", false},
	{ 123, "Drop 160 bubbles", false},
	{ 124, "Pop 200 bubbles", false},
	{ 124, "Drop 17 green bubbles", false},
	{ 124, "Pop 45 green bubbles", false},
	{ 124, "Pop 215 bubbles", false},
	{ 124, "Drop 165 bubbles", false},
	{ 125, "Drop 10 green and 15 blue bubbles", false},
	{ 125, "Pop 180 bubbles", false},
	{ 125, "Save 9 bubbles", false},
	{ 125, "Pop 195 bubbles", false},
	{ 125, "Drop 125 bubbles", false},
	{ 126, "Pop 215 bubbles", false},
	{ 126, "Pop 75 green bubbles", false},
	{ 126, "Save 8 bubbles", false},
	{ 126, "Drop 155 bubbles", false},
	{ 126, "Pop 225 bubbles", false},
	{ 127, "Pop 300 bubbles", false},
	{ 127, "Save 7 bubbles", false},
	{ 127, "Pop 320 bubbles", false},
	{ 127, "Save 13 bubbles", false},
	{ 127, "Pop 70 green bubbles", false},
};


int LCommon::getOrgTreasureCount()
{
	return sizeof(g_GoalList) / sizeof(TGoalInfo);
}

TGoalInfo* LCommon::getOrgTreasure(int idx)
{
	if (idx < 0 || getOrgTreasureCount() <= idx) return NULL;

	return &g_GoalList[idx];
}

void LCommon::createTreasureList()
{
	treasureList.clear();

	int nLevel = LCommon::sharedInstance()->curLevel();
	int nSize = sizeof(g_GoalList) / sizeof(TGoalInfo);
	for (int i = 0; i < nSize; i++) 
	{
        if (isTutorialLevel(g_GoalList[i].level)) continue;
		if (g_GoalList[i].level == nLevel)
			treasureList.push_back(&g_GoalList[i]);
	}
}

void LCommon::createDailyChallengeList()
{
	dailyChallengeList.clear();

	if (maxLevel() < DAILY_CHAL_APPEARLEVEL) return;

	int nMaxLevel = maxLevel();

	int nToday = getToday();

	int nNextLevel = dailyChallengeState.nLastLevel;
    
	int nNextIndex = dailyChalIds[nNextLevel];

	if (nToday > dailyChallengeState.nLastChangedDay)
	{
		int nLvlCount = (nMaxLevel - DAILY_CHAL_STARTLEVEL + 1);
		nNextLevel += getRandRange(1, nLvlCount / 2);
        while (nNextLevel <= nMaxLevel) {
            if (!isTutorialLevel(nNextLevel)) break;
        }
		if (nNextLevel > nMaxLevel)
			nNextLevel = DAILY_CHAL_STARTLEVEL + (nNextLevel - nMaxLevel);

		nNextIndex = dailyChalIds[nNextLevel];
		nNextIndex++;
		if (nNextIndex == DAILY_CHAL_COUNT)
		{
			int nStart = (nNextLevel - DAILY_CHAL_STARTLEVEL) * DAILY_CHAL_COUNT;
			//reaches end. initialize all
			for (int i = 0; i < DAILY_CHAL_COUNT; i++)
			{
				g_DailyGoalList[nStart + i].isDone = false;
			}
			nNextIndex = 0;
		}
	}

	int nTotalIndex = (nNextLevel - DAILY_CHAL_STARTLEVEL) * DAILY_CHAL_COUNT + nNextIndex;

	int nCurLevel = curLevel();
	TPGoalInfoList dailyList;
	int nSize = sizeof(g_DailyGoalList) / sizeof(TGoalInfo);
	for (int i = 0; i < nSize; i++) 
	{
		if (g_DailyGoalList[i].level != nCurLevel) continue;
		if (i == nTotalIndex) continue;

		TGoalInfo* goalInfo = &g_DailyGoalList[i];
		if (goalInfo->isDone)
			dailyList.push_back(goalInfo);
	}

	dailyChallengeState.nLastLevel = nNextLevel;
	dailyChallengeState.nLastChangedDay = nToday;
	dailyList.push_back(&g_DailyGoalList[nTotalIndex]);

	for (int i = dailyList.size() - 1; i >= 0; i--)
	{
		TGoalInfo* goalInfo = dailyList.at(i);

		TDailyChallengeInfo info;
		info.pGoalInfo = goalInfo;

		info.rewardType = IT_MAGIC;
		info.rewardValue = 3;
		dailyChallengeList.push_back(info);
	}

	saveState();
}

void LCommon::serialGoalData(bool bLoadOrSave, TGoalInfo* goalList, int nCount, const char* strKey)
{
	CSaveUtil* defaults = CSaveUtil::sharedInstance();

	int nCurLevel = -1;
	int nSuffix = 0;

	if (bLoadOrSave)
	{
		std::string strGoal = defaults->getStringForKey(strKey, "");
		for (int i = 0; i < nCount; i++)
			goalList[i].isDone = (i < strGoal.length() && strGoal.at(i) == '1');
	}
	else
	{
		char* doneChar = new char[nCount + 1];
		for (int i = 0; i < nCount; i++)
		{
			doneChar[i] = goalList[i].isDone? '1' : '0';
		}

		doneChar[nCount] = 0;

		defaults->setStringForKey(strKey, doneChar);
		delete doneChar;
	}
}

void LCommon::serialLevelGoalData(bool bLoadOrSave)
{
	serialGoalData(bLoadOrSave, g_GoalList, sizeof(g_GoalList) / sizeof(TGoalInfo), "levelgoal");
}
void LCommon::serialDailyChal(bool bLoadOrSave)
{
	CSaveUtil* defaults = CSaveUtil::sharedInstance();

	int nToday = getToday();
	if (bLoadOrSave)
	{
		dailyChallengeState.nLastLevel = defaults->getIntegerForKey("dch_lastlv", DAILY_CHAL_STARTLEVEL);
		dailyChallengeState.nLastChangedDay = defaults->getIntegerForKey("dch_compday", nToday);
		defaults->getByteArray("dch_lastids", dailyChalIds, MAX_LEVEL, 0);
	}
	else
	{
		defaults->setIntegerForKey("dch_lastlv", dailyChallengeState.nLastLevel);
		defaults->setIntegerForKey("dch_compday", dailyChallengeState.nLastChangedDay);
		defaults->setByteArray("dch_lastids", dailyChalIds, MAX_LEVEL);
	}

	serialGoalData(bLoadOrSave, g_DailyGoalList, sizeof(g_DailyGoalList) / sizeof(TGoalInfo), "daily_chal");
}

TAchievementItem g_AchieveList[] =
{
	{ "Completing all levels in a world with 3 stars - King of This World", "com.gamesfun.fruitbubbleblast.achievements.3stars", TAT_STARS, 3, 1, false},
	{ "Completing all levels in a world with 2 stars - Chief Bubble Blaster", "com.gamesfun.fruitbubbleblast.achievements.2stars", TAT_STARS, 2, 1, false},
	{ "Completing all levels in a world with 1 star - Bubble Blaster", "com.gamesfun.fruitbubbleblast.achievements.1stars", TAT_STARS, 1, 1, false},
	{ "Complete all Worlds and Levels with at least 3 stars - King of The Freeing World!", "com.gamesfun.fruitbubbleblast.achievements.all.3stars", TAT_ALLSTARS, 3, 1, false},
	{ "Complete all worlds and levels with at least 2 stars - Two is better than One", "com.gamesfun.fruitbubbleblast.achievements.all.2stars", TAT_ALLSTARS, 2, 1, false},
	{ "Complete all worlds and levels with at least 1 star - Friend Freeing Ace", "com.gamesfun.fruitbubbleblast.achievements.all.1stars", TAT_ALLSTARS, 1, 1, false},
	{ "Unlock Panda -  Momo the Panda", "com.gamesfun.fruitbubbleblast.achievements.unlockpanda", TAT_UNLOCK_CHARA, CHARA_MOMO, 1, false},
	{ "Unlock Rabbit -  Roo the Rabbit", "com.gamesfun.fruitbubbleblast.achievements.unlockrabbit", TAT_UNLOCK_CHARA, CHARA_ROO, 1, false},
	{ "Unlock Penguin - Pingu the Penguin", "com.gamesfun.fruitbubbleblast.achievements.unlockpenguin", TAT_UNLOCK_CHARA, CHARA_PINGU, 1, false},
	{ "Unlock Monkey - Jack the Monkey", "com.gamesfun.fruitbubbleblast.achievements.unlockmonkey", TAT_UNLOCK_CHARA, CHARA_JACK, 1, false},
	{ "Unlock Pig - Piggles the Pig", "com.gamesfun.fruitbubbleblast.achievements.unlockpig", TAT_UNLOCK_CHARA, CHARA_PIGGLES, 1, false},
	{ "Pop 100 bubbles - Bubble Popping Star", "com.gamesfun.fruitbubbleblast.achievements.100bubbles", TAT_POP_BUBBLES, 100, 1, false},
	{ "Pop 500 bubbles - Bubble Popping Nut", "com.gamesfun.fruitbubbleblast.achievements.500bubbles", TAT_POP_BUBBLES, 500, 1, false},
	{ "Pop 1000 bubbles - Bubble Popping Buff", "com.gamesfun.fruitbubbleblast.achievements.1kbubbles", TAT_POP_BUBBLES, 1000, 1, false},
	{ "Pop 3000 bubbles - Bubble Popping Fanatic", "com.gamesfun.fruitbubbleblast.achievements.3kbubbles", TAT_POP_BUBBLES, 3000, 1, false},
	{ "Pop 5000 bubbles - Bubble Popping Chief", "com.gamesfun.fruitbubbleblast.achievements.5kbubbles", TAT_POP_BUBBLES, 7000, 1, false},
	{ "Pop 7500 bubbles - Bubble Popping Awesomeness", "com.gamesfun.fruitbubbleblast.achievements.7.5kbubbles", TAT_POP_BUBBLES, 7500, 1, false},
	{ "Pop 10,000 bubbles - Bubble Popping Genius", "com.gamesfun.fruitbubbleblast.achievements.10kbubbles", TAT_POP_BUBBLES, 10000, 1, false},
	{ "Pop 20,000 bubbles - The King of Pops", "com.gamesfun.fruitbubbleblast.achievements.20kbubbles", TAT_POP_BUBBLES, 20000, 1, false},
	{ "Drop 25 bubbles - The Great Bubble Dropper", "com.gamesfun.fruitbubbleblast.achievements.drop25bubbles", TAT_DROP_BUBBLES, 25, 1, false},
	{ "Drop 50 bubbles - The Super Bubble Dropper", "com.gamesfun.fruitbubbleblast.achievements.drop50bubbles", TAT_DROP_BUBBLES, 50, 1, false},
	{ "Drop 100 bubbles - The Amazing Bubble Dropper", "com.gamesfun.fruitbubbleblast.achievements.drop100bubbles", TAT_DROP_BUBBLES, 100, 1, false},
	{ "Drop 250 bubbles - The Master Bubble Dropper", "com.gamesfun.fruitbubbleblast.achievements.drop250bubbles", TAT_DROP_BUBBLES, 250, 1, false},
	{ "Drop 500 bubbles - The Expert Bubble Dropper", "com.gamesfun.fruitbubbleblast.achievements.drop500bubbles", TAT_DROP_BUBBLES, 500, 1, false},
	{ "Drop 1000 bubbles - The Supreme Bubble Dropper", "com.gamesfun.fruitbubbleblast.achievements.drop1kbubbles", TAT_DROP_BUBBLES, 1000, 1, false},
	{ "Drop 3000 bubbles - The Boss Bubble Dropper", "com.gamesfun.fruitbubbleblast.achievements.drop3kbubbles", TAT_DROP_BUBBLES, 3000, 1, false},
	{ "Drop 5000 bubbles - The King of Drops", "com.gamesfun.fruitbubbleblast.achievements.drop5kbubbles", TAT_DROP_BUBBLES, 5000, 1, false},
};

#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#define LEADERBOARD_HIGHSCORE "com.gamesfun.fruitbubbleblast.leaderboard.highscore"
#define LEADERBOARD_MOSTSTARS "com.gamesfun.fruitbubbleblast.leaderboard.moststars"
#define LEADERBOARD_MOST_SAVED "com.gamesfun.fruitbubbleblast.leaderboard.mostbubblessaved"
#define LEADERBOARD_MOST_POP "com.gamesfun.fruitbubbleblast.leaderboard.mostbubblespopped"
#define LEADERBOARD_MOST_DROP "com.gamesfun.fruitbubbleblast.leaderboard.mostbubblesdropped"
#elif(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#define LEADERBOARD_HIGHSCORE "CgkI57jr69MeEAIQAQ"
#define LEADERBOARD_MOSTSTARS "CgkI57jr69MeEAIQAg"
#define LEADERBOARD_MOST_SAVED "CgkI57jr69MeEAIQAw"
#define LEADERBOARD_MOST_POP "CgkI57jr69MeEAIQBA"
#define LEADERBOARD_MOST_DROP "CgkI57jr69MeEAIQBQ"
#else
#define LEADERBOARD_HIGHSCORE ""
#define LEADERBOARD_MOSTSTARS ""
#define LEADERBOARD_MOST_SAVED ""
#define LEADERBOARD_MOST_POP ""
#define LEADERBOARD_MOST_DROP ""
#endif

void LCommon::checkAchievedList(int nType, int nCount, TAchieveList& retList)
{
	int nSize = sizeof(g_AchieveList) / sizeof(TAchievementItem);

	for (int i = 0; i < nSize; i++)
	{
		TAchievementItem& it = g_AchieveList[i];
		if (it.type != nType) continue;;
		if (it.isAchieved) continue;

		if (nCount >= it.count)
		{
			it.isAchieved = true;
			retList.push_back(it);
		}
	}
}

int LCommon::checkAchievement(int nFinalScore)
{
	TAchieveList achieveList;
	checkAchievedList(TAT_DROP_BUBBLES, totalDropped, achieveList);
	checkAchievedList(TAT_POP_BUBBLES, totalPopped, achieveList);

	int nMinStar = 3;
	int nTotalMinStar = 3;
	int nLastLevel = LEVEL_COUNT_PER_WORLD - 1;
	for (int i = 0; i < MAX_LEVEL; i++)
	{
		int nStar = stars(i);
		if (nStar < nMinStar) nMinStar = nStar;
		if (nStar < nTotalMinStar) nTotalMinStar = nStar;

		int nWorld = getWorldByLevel(i);
		if (i == nLastLevel)
		{
			checkAchievedList(TAT_STARS, nMinStar, achieveList);
			nLastLevel += getLevelsPerWorld(nWorld);
			nMinStar = 3;
		}
	}

	checkAchievedList(TAT_ALLSTARS, nTotalMinStar, achieveList);

	int nMagicCount = 0;
	for (TAchieveList::iterator it = achieveList.begin(); it != achieveList.end(); it++)
	{
		nMagicCount += it->rewardMagics;
		AppDelegate::app->submitAchievement(it->achieveId, it->achieveDesc);
		AppDelegate::app->sendFuseEventPerLevel(USERS_WHO_UNLOCK_ACHIEVEMENTS);

	}
    
    int nHighScore = 0;
    for (int lv = 0; lv < MAX_LEVEL; lv++)
        if (nHighScore < highScore[lv])
            nHighScore = highScore[lv];
    
    int nTotalStars = 0;
    for (int ch = 0; ch < CHARA_COUNT; ch++)
        for (int lv = 0; lv < MAX_LEVEL; lv++)
            nTotalStars += charaInfo[ch].starCount[lv];
    
    AppDelegate::app->submitLeaderboard(LEADERBOARD_HIGHSCORE, nHighScore);
    AppDelegate::app->submitLeaderboard(LEADERBOARD_MOSTSTARS, nTotalStars);
    AppDelegate::app->submitLeaderboard(LEADERBOARD_MOST_SAVED, totalSaved);
    AppDelegate::app->submitLeaderboard(LEADERBOARD_MOST_POP, totalPopped);
    AppDelegate::app->submitLeaderboard(LEADERBOARD_MOST_DROP, totalDropped);

	return nMagicCount;
}

void LCommon::notifyCharaUnlocked(int nCharaId)
{
	TAchieveList achieveList;
	checkAchievedList(TAT_UNLOCK_CHARA, nCharaId, achieveList);

	for (TAchieveList::iterator it = achieveList.begin(); it != achieveList.end(); it++)
	{
		AppDelegate::app->submitAchievement(it->achieveId, it->achieveDesc);
		AppDelegate::app->sendFuseEventPerLevel(USERS_WHO_UNLOCK_ACHIEVEMENTS);
	}
}

void LCommon::serialAchievementData(bool bLoad)
{
	const char* strKey = "achvdata";

	CSaveUtil* defaults = CSaveUtil::sharedInstance();

	int nSize = sizeof(g_AchieveList) / sizeof(TAchievementItem);

	if (bLoad)
	{
		std::string strVal = defaults->getStringForKey(strKey, "");
		for (int i = 0; i < nSize; i++)
			g_AchieveList[i].isAchieved = (i < strVal.length() && strVal.at(i) == '1');
	}
	else
	{
		char* doneChar = new char[nSize + 1];
		for (int i = 0; i < nSize; i++)
		{
			doneChar[i] = g_AchieveList[i].isAchieved? '1' : '0';
		}

		doneChar[nSize] = 0;

		defaults->setStringForKey(strKey, doneChar);
		delete doneChar;
	}

}

void LCommon::resetAchievement()
{
	int nSize = sizeof(g_AchieveList) / sizeof(TAchievementItem);
	for (int i = 0; i < nSize; i++)
		g_AchieveList[i].isAchieved = false;
    saveState();
}

std::string getChallengeDescShift(const char* desc, int nShift)
{
	std::string info = desc;

	std::vector<std::string> tokens;
	std::string res;
	for (int i = 0; i < info.length(); i++)
	{
		if (desc[i] == ' ' || desc[i] == ',')
		{
			tokens.push_back(res);
			res.clear();
			std::string tok;
			tok += desc[i];
			tokens.push_back(tok);
		}
		else
			res += desc[i];
	}
	if (!res.empty())
		tokens.push_back(res);

	std::string strOut;
	for (std::vector<std::string>::iterator it = tokens.begin(); it != tokens.end(); it++)
	{
		for (int i = COLOR_MIN; i <= COLOR_MAX; i++)
		{
			if (*it == STR_COLORS[i])
			{
				int newCol = (i + nShift - COLOR_MIN) % COLOR_COUNT + COLOR_MIN;
				it->assign(STR_COLORS[newCol]);
				break;
			}
		}

		strOut += *it;
	}

	return strOut;
}

void testGoalTargeting()
{

// 	CGoalAnalyser goalAnal("Pop 15 blue and drop 20 blue bubbles");
// 	goalAnal.proc();
// 	std::string out = goalAnal.toString();
// 
// 	cocos2d::CCLog("%s", out.c_str());
// 
	int nCnt = sizeof(g_GoalList) / sizeof(TGoalInfo);

	for (int i = 0; i < nCnt; i++)
	{
		TGoalInfo& goalInfo = g_GoalList[i];
		CGoalAnalyser goalAnal(goalInfo.desc);
		goalAnal.proc();
		std::string out = goalAnal.toString();

		cocos2d::CCLog("(%d) lv(%d) : %s", i, goalInfo.level, out.c_str());
	}
}


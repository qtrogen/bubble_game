/*
 * cocos2d+ext for iPhone
 *
 * Copyright (c) 2011 - Ngo Duc Hiep
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifndef __CCBlade_H__
#define __CCBlade_H__

#include "cocos2d.h"
#include <vector>

#define USE_LAGRANGE        1
#define USE_STL_LIST        0
#define USE_UPDATE_FOR_POP  1

inline float fangle(cocos2d::CCPoint vect);
inline float lagrange1(cocos2d::CCPoint p1, cocos2d::CCPoint p2, float x);

inline void CCPointSet(cocos2d::CCPoint *v, float x, float y);
inline void f1(cocos2d::CCPoint p1, cocos2d::CCPoint p2, float d, cocos2d::CCPoint *o1, cocos2d::CCPoint *o2);

typedef std::vector<cocos2d::CCPoint> TPathList;

class CCBlade : public cocos2d::CCNodeRGBA {
public:
	CCBlade();
	CCBlade(int limit, float y);
	~CCBlade();

	static CCBlade* bladeWithMaximumPoint(int limit, float y);
	void push(cocos2d::CCPoint v);
	void pop(int n);
	void clear();
	void reset();
	void dim(bool bdim);
	void finish();
	void setBaseY(float y);
	void disappear();
	void initAppear();

protected:
	void draw();
	void update(float dt);
	void set_width(float newWidth);
	void shift();
	void populateVertices();

public:
	unsigned int pointLimit;
	cocos2d::CCTexture2D *texture;
	float width;
	bool autoDim;
	TPathList path;

protected:
	int count;
	cocos2d::CCPoint *vertices;
	cocos2d::CCPoint *coordinates;
    unsigned char *colorPointer;

	bool breset;
    bool _finish;
    bool _willPop;
    bool disappearing;
    
    float timeSinceLastPop;
    float popTimeInterval;
    float orgY;
};

#endif //__CCBlade_H__

/*
 * cocos2d+ext for iPhone
 *
 * Copyright (c) 2011 - Ngo Duc Hiep
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include "CCBlade.h"

USING_NS_CC;

#define POP_TIME_INTERVAL 0.2f


inline float fangle(CCPoint vect){
	if (vect.x == 0.0 && vect.y == 0.0) {
		return 0;
	}
	
	if (vect.x == 0.0) {
		return vect.y > 0 ? M_PI/2 : -M_PI/2;
	}
	
	if (vect.y == 0.0 && vect.x < 0) {
		return (float)-M_PI;
	}
	
	float angle = atan(vect.y / vect.x);
    
	return vect.x < 0 ? angle + M_PI : angle;
}

inline void f1(CCPoint p1, CCPoint p2, float d, CCPoint *o1, CCPoint *o2){
	float l = ccpDistance(p1, p2);
	float angle = fangle(ccpSub(p2, p1));
	*o1 = ccpRotateByAngle(ccp(p1.x + l,p1.y + d), p1, angle);
	*o2 = ccpRotateByAngle(ccp(p1.x + l,p1.y - d), p1, angle);
}

inline float lagrange1(CCPoint p1, CCPoint p2, float x){
	return (x-p1.x)/(p2.x - p1.x)*p2.y + (x-p2.x)/(p1.x - p2.x)*p1.y ;
}

inline void CCPointSet(CCPoint *v, float x, float y){
	v->x = x;
	v->y = y;
}

CCBlade* CCBlade::bladeWithMaximumPoint(int limit, float y) {
    CCBlade* blade = new CCBlade(limit, y);
	blade->autorelease();
	return blade;
}

CCBlade::CCBlade(int limit, float y)
{
    orgY = y;
    pointLimit = limit;
	width = 20;
	
    vertices = new CCPoint[2*limit+5];
    coordinates = new CCPoint[2*limit+5];
    colorPointer =  new GLubyte[(limit * 2 + 5) * 4];
    
    setColor(ccWHITE);
    
    for (int i = 0; i < (limit * 2 + 5) * 4; i++)
        colorPointer[i] = 255;

    CCPointSet(coordinates+0, 0.00, 0.5);
    breset = false;
    
#if USE_UPDATE_FOR_POP
    popTimeInterval = POP_TIME_INTERVAL;
    
    timeSinceLastPop = 0;
    scheduleUpdateWithPriority(0);
#endif
    
	setShaderProgram(CCShaderCache::sharedShaderCache()->programForKey(kCCShader_PositionTextureColor));
}

CCBlade::~CCBlade()
{
	delete vertices;
	delete coordinates;
	delete colorPointer;
}

void CCBlade::populateVertices()
{
    vertices[0] = path.at(0);
    CCPoint pre = vertices[0];
    
    unsigned int i = 0;
    CCPoint it = path.at(1);
	float dd = width / path.size();
	while (i < path.size() - 2){
        float ww = width - i * dd;
		f1(pre, it, ww , vertices+2*i+1, vertices+2*i+2);

		//lci
        int idx = i * 8;
        colorPointer[idx+0] = 255;
        colorPointer[idx+1] = 255;
        colorPointer[idx+2] = 255;
        colorPointer[idx+4] = 255;
        colorPointer[idx+5] = 255;
        colorPointer[idx+6] = 255;

        GLubyte op;
        
        if (ww < width / 5)
            op = 0;
        else
            op = ww * ww * 200 / (width * width);

        colorPointer[idx+3] = op;
        colorPointer[idx+7] = op;

        CCPointSet(coordinates+2*i+1, .5, 1.0);
		CCPointSet(coordinates+2*i+2, .5, 0.0);
		
		i++;
		pre = it;
		
		it = path.at(i+1);
	}
    
    colorPointer[i * 8 + 3] = 0;
    colorPointer[i * 8 + 7] = 0;
    i++;
    colorPointer[i * 8 + 3] = 0;
    colorPointer[i * 8 + 7] = 0;
    
    CCPointSet(coordinates+1, 0.25, 1.0);
	CCPointSet(coordinates+2, 0.25, 0.0);
	
	vertices[2*path.size()-3] = it;
	CCPointSet(coordinates+2*path.size()-3, 0.75, 0.5);
}

void CCBlade::shift()
{
	int index = 2 * pointLimit - 1;
	for (int i = index; i > 3; i -= 2) {
		vertices[i] = vertices[i-2];
		vertices[i-1] = vertices[i-3];
	}
}

void CCBlade::set_width(float newWidth)
{
    width = newWidth ;//* CC_CONTENT_SCALE_FACTOR();
}

void CCBlade::setBaseY(float y)
{
    orgY = y;
}


#define DISTANCE_TO_INTERPOLATE 10
#define MIN_DISTANCE_TO_INTERPOLATE 1

void CCBlade::push(CCPoint v)
{
    _willPop = false;
    
	if (breset) {
		return;
	}
    
#if USE_LAGRANGE
    
    v.y -= orgY;
    if (path.size() == 0) {
		path.insert(path.begin(), v);
        return;
    }
    
    CCPoint first = path.at(0);
    float dist = ccpDistance(v, first);
    if (dist < MIN_DISTANCE_TO_INTERPOLATE)
    {
        return;
    }
//    else
//        if (path.size() > 1)
//    {
//        CCPoint second = [[path.at(1] CCPointValue];
//        CCPoint prevVector = ccp(first.y - second.y, first.x - second.x);
//        CCPoint newVector = ccp(v.y - first.y, v.x - first.x);
//        
//        float prevAngle = fangle(prevVector);
//        float newAngle = fangle(newVector);
//        
//        if (fabs(newAngle - prevAngle) > M_PI / 3)
//        {
//            return;
//        }
//        
//    }
    if (dist < DISTANCE_TO_INTERPOLATE) {
		path.insert(path.begin(), v);
        if (path.size() > pointLimit) {
            path.pop_back();
        }
    }else{
        int num = dist / DISTANCE_TO_INTERPOLATE;
        CCPoint iv = ccpMult(ccpSub(v, first), (float)1./(num + 1));
		for (int i = 1; i <= num + 1; i++) {
			path.insert(path.begin(), ccpAdd(first, ccpMult(iv, i)));
		}
		while (path.size() > pointLimit) {
			path.pop_back();
		}
    }
#else // !USE_LAGRANGE
	path.push_front(v);
	if (path.size() > pointLimit) {
		path.pop_back();
	}
#endif // !USE_LAGRANGE
    
	
	populateVertices();
}

void CCBlade::pop(int n)
{
    while (path.size() > 0 && n > 0) {
        path.pop_back();
        n--;
    }
    
    if (path.size() > 2) {
        populateVertices();
    }
}

void CCBlade::clear()
{
    path.clear();
    popTimeInterval = POP_TIME_INTERVAL;
    disappearing = false;
	breset = false;
    removeFromParentAndCleanup(true);
}

void CCBlade::reset()
{
	breset = true;
}

void CCBlade::dim(bool bdim)
{
	breset = bdim;
}

void CCBlade::initAppear()
{
    popTimeInterval = POP_TIME_INTERVAL;
    _finish = false;
}

void CCBlade::disappear()
{
    popTimeInterval = POP_TIME_INTERVAL / 20;

    disappearing = true;
}

void CCBlade::update(float dt)
{
    dt *= 5;
    if (_finish)
        dt *= 5;
    timeSinceLastPop += dt;
    
    float precision = 1/60.0f;
    float roundedTimeSinceLastPop = precision * ceil(timeSinceLastPop/precision); // helps because fps flucuate around 1./60.
    
    int numberOfPops = (int)  (roundedTimeSinceLastPop/popTimeInterval) ;
    timeSinceLastPop = timeSinceLastPop - numberOfPops * popTimeInterval;
    
    for (int npop = 0; npop < numberOfPops; npop++) {
        
        if ((breset && path.size() > 0) || (autoDim && _willPop)) {
            pop(1);
            if (path.size() < 3) {
                clear();
                if (_finish) {
                    return; // if we continue  will have been deallocated
                }
            }
        }
        
    }
}

void CCBlade::draw()
{
    
#if !USE_UPDATE_FOR_POP
    if ((breset && path.size() > 0) || (autoDim && _willPop)) {
        pop(1);
        if (path.size() < 3) {
            clear();
            if (_finish) {
                return; // if we continue  will have been deallocated
            }
        }
    }
#endif
    
    if(path.empty())
        return;
    
    if (path.size() < 3) {
        return;
    }
    
    _willPop = true;
    CC_NODE_DRAW_SETUP();
//	ccGLEnableVertexAttribs(kCCVertexAttribFlag_Position |  kCCVertexAttribFlag_TexCoords);
    
    //lci
    ccGLEnableVertexAttribs(kCCVertexAttribFlag_PosColorTex );

    ccGLBindTexture2D( texture->getName() );
//    ccGLBlendFunc(CC_BLEND_SRC, CC_BLEND_DST);
//    ccGLBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    ccGLBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(kCCVertexAttrib_TexCoords, 2, GL_FLOAT, GL_FALSE, 0, coordinates);
    
    //lci
    glVertexAttribPointer(kCCVertexAttrib_Color, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, colorPointer);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 2*path.size()-2);
	
    CC_INCREMENT_GL_DRAWS(1);
}

void CCBlade::finish()
{
    _finish = true;
}

//
//  WorldMap.h
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//
#ifndef __WorldMap_H__
#define __WorldMap_H__

#include "cocos2d.h"
#include <vector>
#include "cocos-ext.h"
#include "BaseLayer.h"

USING_NS_CC_EXT;

class WorldMap : public CBaseLayer {
public:
	WorldMap();
	~WorldMap();

	static cocos2d::CCScene* scene();

protected:
	void onHome(cocos2d::CCObject* obj);
	void onDailySpin(cocos2d::CCObject* obj);
	void onSelectWorld(cocos2d::CCObject* obj);

	void onMoreApp(cocos2d::CCObject* obj);
	void refreshWorldState();
	virtual void onEnter();

	int currentMap;
protected:
	UIWidget*	m_pMainPanel;
};

#endif //__WorldMap_H__

//
//  DailySpin.cpp
//  IcecreamCandyBlast
//
//  Created by Chen on 12/01/13.
//  Copyright 2013 lion. All rights reserved.
//

#include "cocos-ext.h"
#include "SimpleAudioEngine.h"
#include "LCommon.h"

#include "DailySpin.h"
#include "LevelSelect.h"
#include "SelectChara.h"
#include "SelectBoost.h"
#include "MathUtil.h"
#include "SaveUtil.h"
#include "AppDelegate.h"

USING_NS_CC;

enum 
{
	BTN_ID_BUY_SPIN = BTN_ID_START,
	BTN_ID_FIND_PIECE,
};

int DailySpin_PreviousScreen = 0;

#define ITEM_INTERVAL 140

struct TSpinItem
{
	int spinID;
	std::string imgFileName;

	TSpinItem(int nId, const char* fileName)
	{
		spinID = nId;
		imgFileName = fileName;
	}
};

CDailySpin::CDailySpin()
{
	setSceneID(SCENE_DAILYSPIN);

	loadSpinData();
	UIWidget* pWidget = dynamic_cast<UIWidget*>(GUIReader::shareReader()->widgetFromJsonFile("Image/daily_spin.json"));

	m_pMainLayer->addWidget(pWidget);

	UIWidget* imgView = pWidget->getChildByName("imageBG");
	imgView->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
	imgView->setScale(SC_BG_SCALE);

	UIWidget* mainPanel = pWidget->getChildByName("MainPanel");
	mainPanel->setAnchorPoint(ccp(0.5, 0.5));
	mainPanel->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
	mainPanel->setScale(SC_DLG_SCALE);

	UIWidget* spinPan = mainPanel->getChildByName("spinPane");

	setupScroll(spinPan);

	UIWidget* btnBack = pWidget->getChildByName("btnBack");
	btnBack->setPosition(ccp(0, SC_HEIGHT - m_nActionBarHeight - 80));
	btnBack->addReleaseEvent(this, coco_releaseselector(CDailySpin::onBack));

	UIWidget* btnBuySpin = pWidget->getChildByName("btnBuySpin");
	btnBuySpin->addReleaseEvent(this, coco_releaseselector(CDailySpin::onBuySpin));

	UIWidget* btnPayouts = pWidget->getChildByName("btnPayouts");
	btnPayouts->addReleaseEvent(this, coco_releaseselector(CDailySpin::onPayout));

	m_labelPayouts = dynamic_cast<UILabelBMFont*> (pWidget->getChildByName("labelPayout"));
	m_labelPayouts->setText(toString(LCommon::sharedInstance()->payoutCount).c_str());

	m_labelSpinCount = dynamic_cast<UILabelBMFont*> (pWidget->getChildByName("labelSpinCount"));
	refreshSpinCount();

	UIWidget* imgSpinBall = pWidget->getChildByName("imgSpinBall");
	imgSpinBall->setTouchEnabled(true);
	imgSpinBall->addTouchEventListener(this, toucheventselector(CDailySpin::onTouchJoyStick));

	m_labelSpinNext = NULL;
	m_bShowNoEnoughPopup = false;

	m_nSlotState = ST_READY_SPIN;
	m_bSwitchMoving = false;

	createActionBar();
	prepareSpin();

	setBGMName(kSND_BG_MAINTHEME);
}

void CDailySpin::onEnter()
{
	CBaseLayer::onEnter();

	AppDelegate::app->sendFuseEvent(HOW_MANY_USERS_COME_TO_SPEND_WHEEL_DAILY);
	
	if (!LCommon::sharedInstance()->isPassSpinTuto)
	{
		UIWidget* imgSpinBall = (UIWidget*) m_pMainLayer->getWidgetByName("imgSpinBall");
		CCPoint pos = imgSpinBall->getWorldPosition();
		pos.y += 37;
		pos.x += 2;
		showTutorial("Drag down the spin.", pos, TUTO_SMALL_CHARA, false);
		LCommon::sharedInstance()->isPassSpinTuto = true;
		LCommon::sharedInstance()->saveState();
	}

	stopBgm();
}

void CDailySpin::onExit()
{
	CBaseLayer::onExit();
}

void CDailySpin::setupScroll(UIWidget* spinPane)
{
	CCClippingNode* clipper = CCClippingNode::create();
	clipper->setContentSize(  CCSizeMake(423, 276) );
	clipper->setPosition( CCPointZero );
	spinPane->addCCNode(clipper);

	CCSprite* spBack = CCSprite::create("Image/spin/spin_dlg_back.png");
	spBack->setPosition(ccp(211, 138));
	clipper->setStencil(spBack);
	clipper->setAlphaThreshold(0.2f);

	m_spinPan = clipper;
}

CDailySpin::~CDailySpin()
{

}

void CDailySpin::onTouchJoyStick(CCObject* sender, TouchEventType eventType)
{
	UIWidget* widg = dynamic_cast<UIWidget*> (sender);

	if (LCommon::sharedInstance()->spinCount <= 0) 
	{
		showNoEnoughSpin();
		return;
	}

	switch (eventType)
	{
	case TOUCH_EVENT_BEGAN:
		if (m_bSwitchMoving || m_nSlotState != ST_READY_SPIN) break;

		m_bSwitchMoving = true;

		m_ptJoystickFirstPos = widg->getPosition();
		m_ptJoystickEndY = -m_ptJoystickFirstPos.y - 10;
		for (int i = 0; i < LINE_COUNT; i++)
			m_fSpeed[i] = 0;

		break;
	case TOUCH_EVENT_MOVED:
		{
			if (!m_bSwitchMoving || m_nSlotState == ST_STOPPING) break;
			float newPosY = m_ptJoystickFirstPos.y + widg->getTouchMovePos().y - widg->getTouchStartPos().y;

			if (newPosY > m_ptJoystickFirstPos.y)
				newPosY = m_ptJoystickFirstPos.y;
			if (newPosY < m_ptJoystickEndY)
				newPosY = m_ptJoystickEndY;
			float newPosX = m_ptJoystickFirstPos.x;
			widg->setPosition(ccp(newPosX, newPosY));

			float distY = (m_ptJoystickFirstPos.y - newPosY);

			if (distY > 90 && m_nSlotState == ST_READY_SPIN)
				startSpin();
		}
		break;
	case TOUCH_EVENT_ENDED:
	case TOUCH_EVENT_CANCELED:
		{
			if (!m_bSwitchMoving) break;
			m_bSwitchMoving = false;

			float distY = (m_ptJoystickFirstPos.y - widg->getPosition().y);
			float time = 0.15f * distY / (m_ptJoystickFirstPos.y - m_ptJoystickEndY);
			CCMoveTo* movOrg = CCMoveTo::create(time, m_ptJoystickFirstPos);
			widg->runAction(movOrg);
			if (m_nSlotState == ST_ROLLING)
				stopSpin();
			else
				m_nSlotState = ST_READY_SPIN;

			playEffect(kSND_SFX_SPIN_START, 0.4f);
		}
		break;
	}
}

void CDailySpin::startSpin()
{
	if (m_nSlotState != ST_READY_SPIN) return;

	m_nSlotState = ST_ROLLING;
	createTargetItem();
    
	m_isWin = false;

	for (int i = 0; i < LINE_COUNT; i++)
    {
		m_fSpeed[i] = 40 + 10 * (i % 2) - 5 * i;
        m_pFinalItems[i] = -2;
    }

	m_nSpinStopCounter = 7;

	schedule(schedule_selector(CDailySpin::updateSpin), 0.01f);
}

void CDailySpin::stopSpin()
{
	m_nStoppedIndex = 0;
	m_nSlotState = ST_STOPPING;
}

void CDailySpin::updateSpin(float dt)
{
	float centerY = 140;
	for (int i = 0; i < 3; i++)
	{
		bool bNeedStop = false;
		if (m_nSlotState == ST_STOPPING) 
		{
			if (m_fSpeed[i] == 0) continue;

			if (m_fSpeed[i] < 10)
			{
				float minDiff = SC_HEIGHT;
				for (TSpriteList::iterator it = m_spItems[i].begin(); it != m_spItems[i].end(); it++)
				{
					float curY = (*it)->getPositionY();
					float diff = curY - centerY;
					if ((diff >= 0) && (diff < minDiff)) 
					{
						minDiff = diff;
						m_pFinalItems[i] = (*it)->getTag();
					}
				}

				bool bCanStop = false;
				if (m_nTargetItem != IT_NONE && m_pFinalItems[i] == m_nTargetItem) 
					bCanStop = true;
				if (m_nTargetItem == IT_NONE)
				{
					if (i == 2)
					{
						if (m_pFinalItems[0] == m_pFinalItems[1] && m_pFinalItems[0] == m_pFinalItems[2])
							bCanStop = false;
						else
						{
							if (m_nSpinStopCounter > 0) m_nSpinStopCounter--;
							if (m_nSpinStopCounter == 0) bCanStop = true;
						}
					}
					else
						bCanStop = true;
				}

				if (minDiff < 15 && bCanStop)
				{
					m_fSpeed[i] = minDiff;
					bNeedStop = true;
					m_nStoppedIndex++;
				}
			} else if (m_nStoppedIndex == i)
			{
				m_fSpeed[i] *= 0.98f;
			}
		}

		for (TSpriteList::iterator it = m_spItems[i].begin(); it != m_spItems[i].end(); it++)
		{
			CCPoint newPos = (*it)->getPosition();
			newPos.y -= m_fSpeed[i];
			if (newPos.y < -ITEM_INTERVAL / 2)
				newPos.y += ITEM_INTERVAL * (m_nSpinItemCount);
			(*it)->setPosition(newPos);
		}

		if (bNeedStop)
		{
			m_fSpeed[i] = 0;

			playEffect(kSND_SFX_SPIN_STOP, 0.4f);
		}
	}

	if (m_nSlotState == ST_STOPPING)
	{
		for (int i = 0; i < 3; i++)
		{
			if (m_fSpeed[i] > 0) return;
		}

		m_nSlotState = ST_READY_SPIN;
		unschedule(schedule_selector(CDailySpin::updateSpin));
		scheduleOnce(schedule_selector(CDailySpin::procResult), 0);
	}
}

void CDailySpin::prepareSpin()
{
	m_nSpinItemCount = 0;
	std::vector<TSpinItem> spinList;
	for (int i = 0; i < SPIN_ITEM_COUNT; i++)
	{
		spinList.push_back(TSpinItem(IT_PUZZLE + i, ITEM_IMAGES[IT_PUZZLE + i]));
		m_nSpinItemCount++;
	}
	for (int i = 1; i < BOOST_COUNT; i++)
	{
		if (LCommon::sharedInstance()->boostUnlocked[i])
		{
            boostList.push_back(i);

			const char* fileName = CCString::createWithFormat("Image/boosts/boost_%d.png", i)->getCString();
			spinList.push_back(TSpinItem(IT_BOOSTS + i, fileName));
			m_nSpinItemCount++;
		}
	}

	for (int i = 0; i < LINE_COUNT; i++)
	{
		int x = 76 + 136 * i;
		int nIdDiff = m_nSpinItemCount * i / 3;
		for (int j = 0; j < m_nSpinItemCount; j++)
		{
			int y = j * ITEM_INTERVAL;
			int nId = (j + nIdDiff) % m_nSpinItemCount;

			TSpinItem& spinItem = spinList.at(nId);

			CCSprite* sp = CCSprite::create(spinItem.imgFileName.c_str());
			sp->setPosition(ccp(x, y));
			if (spinItem.spinID >= IT_BOOSTS) sp->setScale(0.75f);
			else			sp->setScale(1.7f);
			sp->setTag(spinItem.spinID);
			m_spinPan->addChild(sp);

			m_spItems[i].push_back(sp);
		}
	}
}

void CDailySpin::procResult(float dt)
{
	bool isWin = true;
	for (int i = 1; i < LINE_COUNT; i++)
		if (m_pFinalItems[0] != m_pFinalItems[i]) 
		{
			isWin = false;
			break;
		}

	m_isWin = isWin;

	if (isWin)
	{
		int nWinItem = m_pFinalItems[0];
		switch (nWinItem)
		{
		case IT_PUZZLE:
			{
				m_nRewardCount = 1;

				if (LCommon::sharedInstance()->charaInfo[m_nRewardPieceChara].byPieceCount < PIECE_PACK_NUM)
					LCommon::sharedInstance()->charaInfo[m_nRewardPieceChara].byPieceCount += m_nRewardCount;
				if (LCommon::sharedInstance()->charaInfo[m_nRewardPieceChara].isUnlocked())
				{
					LCommon::sharedInstance()->notifyCharaUnlocked(m_nRewardPieceChara);
					AppDelegate::app->sendFuseEventPerLevel(
						CCString::createWithFormat(PURCHASE_FMT, CHARA_NAME[m_nRewardPieceChara].c_str())->getCString(), 0);
				}
			}
			break;
		case IT_LIVE:
			m_nRewardCount = 1;
			if (LCommon::sharedInstance()->liveCount < MAX_LIVE)
			{
				// live gives 7 at first but cannt bigger than 5
				LCommon::sharedInstance()->addLiveCount(m_nRewardCount);
			}
			break;
		case IT_MAGIC:
			m_nRewardCount = 3;
			LCommon::sharedInstance()->payoutCount += m_nRewardCount;
			m_labelPayouts->setText(toString(LCommon::sharedInstance()->payoutCount).c_str());
			break;
		default:
			{
				int nBoostId = nWinItem - IT_BOOSTS;
				m_nRewardCount = 1;
				LCommon::sharedInstance()->boostCount[nBoostId] += m_nRewardCount;
			}
			break;
		}

		createNextItem();
		saveSpinData();
	}
	CCAssert(LCommon::sharedInstance()->spinCount > 0, "Spin count must bigger than zero");
	LCommon::sharedInstance()->decreateSpinCount();
	LCommon::sharedInstance()->saveState();
	refreshSpinCount();
	refreshInfo();

	if (!isWin && LCommon::sharedInstance()->spinCount > 0) return;

	scheduleOnce(schedule_selector(CDailySpin::showResultPopup), 0.0f);
}

void CDailySpin::showResultPopup(float dt)
{
	int nItemId = m_pFinalItems[0];

	UIImageView* imgView = UIImageView::create();
	if (m_isWin && nItemId == IT_PUZZLE)
		imgView->loadTexture("Image/spin/piece_win.png");
	else
		imgView->loadTexture("Image/spin/popup_bg.png");

	imgView->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));

	UIButton* closeBtn = UIButton::create();
	closeBtn->loadTextures("Image/spin/btn_close_n.png", "Image/spin/btn_close_p.png", "");
	closeBtn->setPosition(ccp(199, 151));
	imgView->addChild(closeBtn);

	if (m_isWin)
	{
		if (nItemId != IT_PUZZLE)
		{
			UIImageView* winView = UIImageView::create();
			winView->loadTexture("Image/spin/cont_win.png");
			imgView->addChild(winView);

			float fScale;
			const char* fileName;
			if (nItemId >= IT_BOOSTS) {
				fileName = CCString::createWithFormat("Image/boosts/boost_%d.png", nItemId - IT_BOOSTS)->getCString();
				fScale = 0.75f;
			} else {
				fileName = ITEM_IMAGES[nItemId];
				fScale = 1.7f;
			}

			UIImageView* itemView = UIImageView::create();
			itemView->loadTexture(fileName);
			itemView->setPosition(ccp(-135, -72));
			itemView->setScale(fScale);
			imgView->addChild(itemView);
		}

		UILabelBMFont* itemCount = UILabelBMFont::create();
		itemCount->setFntFile("Image/fonts/Hobostd84.fnt");
		itemCount->setText(toString(m_nRewardCount).c_str());
		itemCount->setPosition(ccp(100, -80));
		itemCount->setScale(1.5f);
		imgView->addChild(itemCount);

		if (nItemId == IT_PUZZLE)
		{
			closeBtn->setPosition(ccp(219, 229));

			itemCount->setPosition(ccp(85, 65));
			itemCount->setScale(1.0f);
			itemCount->setText(toString(m_nRewardCount).c_str());

			UILabel* lblPieceName = UILabel::create();
			lblPieceName->setFontSize(40);
			lblPieceName->setPosition(ccp(75, -73));
			lblPieceName->setColor(ccc3(255, 251, 194));
			lblPieceName->setText(CCString::createWithFormat("%s Piece!", CHARA_NAME[m_nRewardPieceChara].c_str())->getCString());
			imgView->addChild(lblPieceName);

			int nCnt = LCommon::sharedInstance()->charaInfo[m_nRewardPieceChara].byPieceCount;
			UIImageView* itemPuzzle = UIImageView::create();
			itemPuzzle->loadTexture(CCString::createWithFormat("Image/puzzle/%s_%d.png", CHARA_NAME[m_nRewardPieceChara].c_str(), nCnt - 1)->getCString());
			itemPuzzle->setScale(0.6f);
			itemPuzzle->setPosition(ccp(-130, -117));
			imgView->addChild(itemPuzzle);

			UILabelBMFont* itemPieces = UILabelBMFont::create();
			itemPieces->setFntFile("Image/fonts/Hobostd84.fnt");
			itemPieces->setText(CCString::createWithFormat("%d/%d", nCnt, PIECE_PACK_NUM)->getCString());
			itemPieces->setPosition(ccp(75, -155));
			itemPieces->setScale(1.0f);
			imgView->addChild(itemPieces);

			UIButton* btn = UIButton::create();
			btn->loadTextures("Image/spin/spin_n.png", "Image/spin/spin_p.png", "");
			btn->setPosition(ccp(158, -154));
			addButtonEvent(btn, BTN_ID_FIND_PIECE);
			imgView->addChild(btn);

			if (nCnt == PIECE_PACK_NUM)
				playEffect(kSND_SFX_CHEST_OPEN);
		}
	}
	else
	{
		UIImageView* loseView = UIImageView::create();
		loseView->loadTexture("Image/spin/lose_title.png");
		loseView->setPosition(ccp(-5, 75));
		imgView->addChild(loseView);

		UIImageView* loseFree = UIImageView::create();
		loseFree->loadTexture("Image/spin/cont_lose.png");
		loseFree->setPosition(ccp(-5, -10));
		imgView->addChild(loseFree);

		UILabel* lblNextFree = UILabel::create();
		lblNextFree->setFontSize(40);
		lblNextFree->setPosition(ccp(90, -4));
		lblNextFree->setColor(ccc3(255, 251, 194));
		m_labelSpinNext = lblNextFree;
		imgView->addChild(lblNextFree);

		UIButton* btnBuy = UIButton::create();
		btnBuy->loadTextures("Image/spin/btn_buy_n.png", "Image/spin/btn_buy_p.png", "");
		btnBuy->setPosition(ccp(0, -100));
		btnBuy->setScale(0.7f);
		addButtonEvent(btnBuy, BTN_ID_BUY_SPIN);

		onTimer();
		
		imgView->addChild(btnBuy);
	}

	showPopup(imgView, closeBtn);
}

void CDailySpin::showNoEnoughSpin()
{
	if (m_bShowNoEnoughPopup) return;
	m_isWin = false;
	m_bShowNoEnoughPopup = true;

	UIImageView* imgView = UIImageView::create();
	imgView->loadTexture("Image/spin/popup_bg.png");
	imgView->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));

	UIButton* closeBtn = UIButton::create();
	closeBtn->loadTextures("Image/spin/btn_close_n.png", "Image/spin/btn_close_p.png", "");
	closeBtn->setPosition(ccp(199, 151));
	imgView->addChild(closeBtn);

	UIImageView* loseFree = UIImageView::create();
	loseFree->loadTexture("Image/spin/cont_lose.png");
	loseFree->setPosition(ccp(-5, -10));
	imgView->addChild(loseFree);

	UILabel* lblNextFree = UILabel::create();
	lblNextFree->setFontSize(40);
	lblNextFree->setPosition(ccp(90, -4));
	lblNextFree->setColor(ccc3(255, 251, 194));
	m_labelSpinNext = lblNextFree;
	imgView->addChild(lblNextFree);

	UIButton* btnBuy = UIButton::create();
	btnBuy->loadTextures("Image/spin/btn_buy_n.png", "Image/spin/btn_buy_p.png", "");
	btnBuy->setPosition(ccp(0, -100));
	btnBuy->setScale(0.7f);
	addButtonEvent(btnBuy, BTN_ID_BUY_SPIN);

	onTimer();

	imgView->addChild(btnBuy);

	showPopup(imgView, closeBtn);
}

bool CDailySpin::onProcBeforeClose(int nButtonTag)
{
	m_labelSpinNext = NULL;
	m_bShowNoEnoughPopup = false;
	if (nButtonTag == BTN_ID_FIND_PIECE)
	{
		int nPieceCnt = LCommon::sharedInstance()->charaInfo[m_nRewardPieceChara].byPieceCount;
		showPuzzleInfo(m_nRewardPieceChara, nPieceCnt, ccp(SC_WIDTH_HALF, SC_HEIGHT / 2));
		return false;
	}

	return true;
}

bool CDailySpin::onProcAfterClose(int nTag, int nParam)
{
	if (CBaseLayer::onProcAfterClose(nTag, nParam)) return true;

	switch (nTag)
	{
	case BTN_ID_BUY_SPIN:
		onBuySpin(NULL);
		break;
	default:
		return false;
	}

	return true;
}

void CDailySpin::onPayout(CCObject* sender)
{
	LCommon::sharedInstance()->magicCount += LCommon::sharedInstance()->payoutCount;
	LCommon::sharedInstance()->payoutCount = 0;
	m_labelPayouts->setText(toString(LCommon::sharedInstance()->payoutCount).c_str());
	refreshInfo();
}

void CDailySpin::onBack(CCObject* sender)
{
	if(DailySpin_PreviousScreen == SCENE_SELCHARA)
		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CSelCharaScene::scene(), ccWHITE));
	else if(DailySpin_PreviousScreen == SCENE_WORLDMAP)
		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CLevelSelect::scene(), ccWHITE));
	else if(DailySpin_PreviousScreen == SCENE_SELBOOST)
		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CSelBoostScene::scene(), ccWHITE));
}

void CDailySpin::onBuySpin(CCObject* sender)
{
	showBuySpin();
    
// 	for (int i = 0; i < 1000; i++)
// 	{
// 		createTargetItem();
// 		int nNext = m_nTargetKind == SP_BLANK? -1 : spItemNext[m_nTargetKind];
// 
// 		if (m_nRewardPieceChara != CHARA_NONE)
// 		{
// 			LCommon::sharedInstance()->charaInfo[m_nRewardPieceChara].byPieceCount++;
// 			CCLog("%04d:\t\t%d\t\t%d, %s", m_nSpinIndex, m_nTargetKind, nNext, CHARA_NAME[m_nRewardPieceChara].c_str());
// 		}
// 		else
// 			CCLog("%04d:\t\t%d\t\t%d", m_nSpinIndex, m_nTargetKind, nNext);
// 
// 		createNextItem();
// 	}
}

void CDailySpin::refreshSpinCount()
{
	std::string str = toString(LCommon::sharedInstance()->spinCount);
	m_labelSpinCount->setText(str.c_str());
}

void CDailySpin::onTimer()
{
	if (m_labelSpinNext)
		m_labelSpinNext->setText(getTimeStr(LCommon::sharedInstance()->freeSpinSecCount));
}

void CDailySpin::refreshInfo()
{
	CBaseLayer::refreshInfo();
	refreshSpinCount();
}

void CDailySpin::createTargetItem()
{
	m_nTargetKind = SP_BLANK;
	m_nRewardPieceChara = CHARA_NONE;
	for (int i = 0; i < SP_KIND_COUNT; i++)
	{
		if (spItemNext[i] >= 0 && m_nSpinIndex >= spItemNext[i])
		{
            if (i == SP_POWERUP && boostList.empty()) continue;
			m_nTargetKind = i;
			break;
		}
	}

	m_nTargetItem = IT_NONE;

	switch (m_nTargetKind)
	{
	case SP_POWERUP:
		{
			int nVal;
            
            nVal = getRandRange(1, boostList.size());
			if (nVal != BOOST_NONE)
            {
				m_nTargetItem = IT_BOOSTS + boostList.at(nVal - 1);
            }
		}
		break;
	case SP_LIVE:
		m_nTargetItem = IT_LIVE;
		break;
	case SP_MAGIC:
		m_nTargetItem = IT_MAGIC;
		break;
	case SP_PUZZLE_PIG_RABBIT:
		{
            m_nTargetItem = IT_PUZZLE;
			bool isPig = getRandRange(0, 100) < 50;

			if (isPig && !LCommon::sharedInstance()->charaInfo[CHARA_PIGGLES].isUnlocked())
				m_nRewardPieceChara = CHARA_PIGGLES;
			else if (!LCommon::sharedInstance()->charaInfo[CHARA_ROO].isUnlocked())
				m_nRewardPieceChara = CHARA_ROO;
			else
				m_nRewardPieceChara = CHARA_NONE;
		}
		break;
	case SP_PUZZLE_MONKEY:
		m_nTargetItem = IT_PUZZLE;
		m_nRewardPieceChara = CHARA_JACK;
		break;
	case SP_PUZZLE_PENGUIN:
		m_nTargetItem = IT_PUZZLE;
		m_nRewardPieceChara = CHARA_PINGU;
		break;
	case SP_PUZZLE_PANDA:
		m_nTargetItem = IT_PUZZLE;
		m_nRewardPieceChara = CHARA_MOMO;
		break;
	}

    m_nSpinIndex++;
}

void CDailySpin::createNextItem()
{
	int nPrevRange = 1;
	int nNewRange = 1;
	bool bNeedEnd = false;
	switch (m_nTargetKind)
	{
	case SP_LIVE:
		nNewRange = nPrevRange = 10;
		break;
	case SP_POWERUP:
		nNewRange = nPrevRange = 30;
		break;
	case SP_MAGIC:
		nNewRange = nPrevRange = 50;
		break;
	case SP_PUZZLE_PIG_RABBIT:
		{
			nNewRange = nPrevRange = 20;
			if (LCommon::sharedInstance()->charaInfo[CHARA_PIGGLES].isUnlocked()
				&& LCommon::sharedInstance()->charaInfo[CHARA_ROO].isUnlocked())
				bNeedEnd = true;
		}
		break;
	case SP_PUZZLE_MONKEY:
		nNewRange = nPrevRange = 60;
		if (LCommon::sharedInstance()->charaInfo[CHARA_JACK].isUnlocked()) bNeedEnd = true;
		break;
	case SP_PUZZLE_PENGUIN:
		nNewRange = nPrevRange = 75;
		if (LCommon::sharedInstance()->charaInfo[CHARA_PINGU].isUnlocked()) bNeedEnd = true;
		break;
	case SP_PUZZLE_PANDA:
		nNewRange = nPrevRange = 100;
		if (LCommon::sharedInstance()->charaInfo[CHARA_MOMO].isUnlocked()) bNeedEnd = true;
		break;
	}

	if (bNeedEnd)
	{
		spItemNext[m_nTargetKind] = -1;
		m_nTargetKind = SP_BLANK;
	} else if (m_nTargetKind != SP_BLANK)
	{
		int nextStart = (spItemNext[m_nTargetKind] / nPrevRange + 1) * nPrevRange;
		int nNextDiff = getRandRange(nNewRange / 5, nNewRange - 1);
		spItemNext[m_nTargetKind] = nextStart + nNextDiff;
	}
}

int CDailySpin::getRangeForChara(int nCharaIdx)
{
	int nRange = 0;
	if (nCharaIdx <= CHARA_PIGGLES)
		nRange = 20;
	else if (nCharaIdx <= CHARA_ROO)
		nRange = 60;
	else if (nCharaIdx <= CHARA_JACK)
		nRange = 75;
	else //(nCharaIdx <= CHARA_MOMO)
		nRange = 100;
	return nRange;
}

void CDailySpin::loadSpinData()
{
	CSaveUtil* defaults = CSaveUtil::sharedInstance();
	for (int i = 0; i < SP_KIND_COUNT; i++)
	{
		int nRange;
		switch (i)
		{
		case SP_LIVE:
			nRange = 10;
			break;
		case SP_POWERUP:
			nRange = 30;
			break;
		case SP_MAGIC:
			nRange = 50;
			break;
		case SP_PUZZLE_PIG_RABBIT:
			nRange = 20;
			break;
		case SP_PUZZLE_MONKEY:
			nRange = 60;
			break;
		case SP_PUZZLE_PENGUIN:
			nRange = 75;
			break;
		case SP_PUZZLE_PANDA:
			nRange = 100;
			break;
		}

		int nVal = getRandRange(i, nRange);
		spItemNext[i] = defaults->getIntegerForKey(CCString::createWithFormat("spItemNext_%d", i)->getCString(), nVal);
	}
	m_nSpinIndex = defaults->getIntegerForKey("m_nSpinIndex", 0);
}
void CDailySpin::saveSpinData()
{
	CSaveUtil* defaults = CSaveUtil::sharedInstance();
	for (int i = 0; i < SP_KIND_COUNT; i++)
	{
		defaults->setIntegerForKey(CCString::createWithFormat("spItemNext_%d", i)->getCString(), spItemNext[i]);
	}
	defaults->setIntegerForKey("m_nSpinIndex", m_nSpinIndex);
	defaults->flush();
}

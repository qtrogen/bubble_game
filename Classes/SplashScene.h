//
//  CSplashScene.h
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//
#ifndef __SplashSecene_H__
#define __SplashSecene_H__

#include "cocos2d.h"
#include "BaseLayer.h"

USING_NS_CC_EXT;

class CSplashScene : public CBaseLayer {
public:
	CSplashScene();
	~CSplashScene();

	CREATE_SCENE_METHOD(CSplashScene)
    
	void loadGameData(float dt);
	void transitScene(float dt);
protected:
	virtual void onEnter();
protected:
};

#endif //__SplashSecene_H__

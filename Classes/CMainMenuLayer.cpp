//
//  CMainMenuLayer.m
//  IcecreamCandyBlast
//
//  Created by Chen on 7/11/13.
//  Copyright 2013 lion. All rights reserved.
//

#include "cocos-ext.h"
#include "LCommon.h"
#include "AppDelegate.h"

#include "CMainMenuLayer.h"
#include "SelectChara.h"
#include "BubbleGame.h"

#include "LevelSelect.h"
#include "SelectBoost.h"
#include "DailySpin.h"

USING_NS_CC;

CMainMenuLayer::CMainMenuLayer()
{
	setSceneID(SCENE_MAINMENU);

	UIWidget* pWidget = dynamic_cast<UIWidget*>(GUIReader::shareReader()->widgetFromJsonFile("Image/mainmenu.json"));

	m_pMainLayer->addWidget(pWidget);

	UIWidget* imgView = pWidget->getChildByName("imageBG");
	imgView->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
	imgView->setScale(SC_BG_SCALE);

	UIWidget* mainPanel = pWidget->getChildByName("MainPanel");
	mainPanel->setAnchorPoint(ccp(0.5, 0.5));
	mainPanel->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));

	UIWidget* btnStart = pWidget->getChildByName("startBtn");
	btnStart->addReleaseEvent(this, coco_releaseselector(CMainMenuLayer::onPlay));

	btnFBConnect = (UIButton*) pWidget->getChildByName("fb_connect");
	btnFBConnect->addReleaseEvent(this, coco_releaseselector(CMainMenuLayer::onFBConnect));
	btnFBConnect->loadTextures("Image/home/fb_logout_n.png", "Image/home/fb_logout_p.png", "");
	btnFBConnect->loadTextures("Image/home/fb_connect_n.png", "Image/home/fb_connect_p.png", "");

	UIWidget* btnMore = pWidget->getChildByName("moreGame");
	btnMore->addReleaseEvent(this, coco_releaseselector(CMainMenuLayer::onMore));
    
    // added  by KUH in 20140920
#ifdef GLOW_ADD
    if(!LCommon::sharedInstance()->parseMainMenu)
    {
        
        CCPoint pos = CCPointZero;
        pos = btnStart->getWorldPosition();
		showTutorial("Click Start button to start game.", pos, TUTO_BIG_CHARA, false, false);
        LCommon::sharedInstance()->parseMainMenu = true;
    }

   #endif
    
//	CBubble::preloadAnimates();

	setBGMName(kSND_BG_MAINTHEME);

}

CMainMenuLayer::~CMainMenuLayer()
{

}

void CMainMenuLayer::onEnter()
{
	CBaseLayer::onEnter();
    refreshFBConnect();
}

void CMainMenuLayer::refreshFBConnect()
{
    bool isConnected = LCommon::sharedInstance()->isFBConnected;
	const char* btn_n = isConnected ? "Image/home/fb_logout_n.png" : "Image/home/fb_connect_n.png";
	const char* btn_p = isConnected ? "Image/home/fb_logout_p.png" : "Image/home/fb_connect_p.png";
    btnFBConnect->loadTextures(btn_n, btn_p, "");

	//CCLog("refreshFBConnect %s, %s", btn_n, btn_p);
}

bool CMainMenuLayer::onProcEvent(int nEventId, int nParam, std::string strParam)
{
    if (nEventId == EVT_UPDATE_FBCONNECT_STATE)
    {
        refreshFBConnect();
        //return true;
    }
    return CBaseLayer::onProcEvent(nEventId, nParam, strParam);
}

void CMainMenuLayer::onRestorePurchase(CCObject* sender)
{
    LStoreManager::sharedInstance()->restore();
}

void CMainMenuLayer::onPlay(CCObject* sender)
{
	if (!LCommon::sharedInstance()->isGameDataLoaded) 
	{
		CCMessageBox("Cannot load game data!\n Please check Internet connection.", "Warning");
		return;
	}

	LCommon::sharedInstance()->isViaMoregame = false;

	AppDelegate::app->setScreenValues();
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CSelCharaScene::scene(), ccWHITE));
}

void CMainMenuLayer::onMore(CCObject* sender)
{
	AppDelegate::app->showMoreApps();
    
//    LCommon::sharedInstance()->resetAchievement();
    
//    playEffect(kSND_SFX_DONE_ADDING_UP, 1.0f);

// 	if (!LCommon::sharedInstance()->isGameDataLoaded) return;
// 
// 	LCommon::sharedInstance()->isViaMoregame = true;
// 	AppDelegate::app->setScreenValues();
// 	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CSelCharaScene::scene(), ccWHITE));

//	CCDictionary* pDic = CCDictionary::createWithContentsOfFile("aitestinfo.txt");
//
//	const CCString* strStage = pDic->valueForKey("TestStage");
//	int nStage = (strStage == NULL)? 1 : strStage->intValue();
//	if (nStage < 1) nStage = 1;
//
//	LCommon::sharedInstance()->curLevel = nStage - 1;
//	LCommon::sharedInstance()->curWorld = LCommon::sharedInstance()->curLevel / LEVEL_COUNT_PER_WORLD;
//
//	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CSelBoostScene::scene(), ccWHITE));
// 	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CBubbleGame::scene(), ccWHITE));
}

void CMainMenuLayer::onFBConnect(cocos2d::CCObject* sender)
{
    FBConnect();
}

void CMainMenuLayer::onGameCenter(CCObject* sender)
{
    AppDelegate::app->showLeaderboard();
}

void CMainMenuLayer::restoreSuccessed(T_PRODUCT_LIST& productIdList)
{
    AppDelegate::app->removeAds();
}
void CMainMenuLayer::restoreFailed(std::string& errMsg)
{
    
}
void CMainMenuLayer::onTimer()
{
//    btnFBConnect->setVisible(!LCommon::sharedInstance()->isFBConnected);
}


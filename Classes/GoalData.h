#ifndef __GOAL_DATA_H__

#include <vector>
#include <string>
#include "LCommon.h"

class CGoalAnalyser
{
public:
	CGoalAnalyser(const char* pInfo);

	TGoalList recList;
	void proc();

	std::string toString();

protected:
	std::vector<std::string> tokens;

	bool hasNext();
	std::string& fetchNext();
	std::string& next();
	bool isMore(std::string& token);
	void preproc();

	std::string info;
	int nextPos;
};

void testGoalTargeting();
#endif // !__GOAL_DATA_H__

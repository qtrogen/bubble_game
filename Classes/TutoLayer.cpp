#include "TutoLayer.h"
#include "LCommon.h"

USING_NS_CC;
const int nOpacity = 200;

CTutoLayer::CTutoLayer()
	:m_pSelectorTarget(NULL), m_pCallbackFunc(NULL)
{
	setTouchMode(kCCTouchesOneByOne);
}


CTutoLayer::~CTutoLayer(void)
{
}

CTutoLayer* CTutoLayer::create(bool isTouchConsumed)
{
	CTutoLayer *pRet = new CTutoLayer();
	if (pRet && pRet->init())
	{
		pRet->m_isTouchConsumed = true;
//		pRet->m_isTouchConsumed = isTouchConsumed;
		pRet->autorelease();
		return pRet;
	}
	else
	{
		CC_SAFE_DELETE(pRet);
		return NULL;
	}
}

void CTutoLayer::setCallback(CCObject* pSelectorTarget, cocos2d::SEL_CallFunc func)
{
	m_pSelectorTarget = pSelectorTarget;
	m_pCallbackFunc = func;
}

void CTutoLayer::showTutorial(const char* strTuto, CCPoint pos, char byType, bool bClip, char byHoleType/* = TUTO_SMALL_HOLE*/)
{
	CCPoint handPos;

	addHoleBack(pos, handPos, byHoleType);

	showTutorialCommon(strTuto, handPos, byType);
}

void CTutoLayer::showTutorial(const char* strTuto, TBubbleList& bubbleList, char byType, bool bShowHand)
{
	CCLayerColor* layer = CCLayerColor::create();
//	layer->setContentSize(CCSizeMake(SC_WIDTH, SC_HEIGHT));
	layer->setColor(ccBLACK);
	layer->setOpacity(nOpacity);
	addChild(layer);

	CCPoint handPos;
	addBubbleList(layer, bubbleList, handPos);

	if (!bShowHand)
		handPos.x = -100;
	showTutorialCommon(strTuto, handPos, byType);
}

// added by KUH in 2014-09-20
#ifdef GLOW_ADD
#endif

void CTutoLayer::addHoleBack(cocos2d::CCPoint holePos, CCPoint& handPos, char byHoleType/* = TUTO_SMALL_HOLE*/)
{
	CCClippingNode *clipper = CCClippingNode::create();
	clipper->setAnchorPoint(CCPointZero);
	clipper->setPosition(CCPointZero);
	clipper->setAlphaThreshold(1.0f);
	clipper->setInverted(true);
	addChild(clipper);

	CCLayerColor* layer = CCLayerColor::create();
	layer->setContentSize(CCSizeMake(SC_WIDTH, SC_HEIGHT));
	layer->setColor(ccBLACK);
	layer->setOpacity(nOpacity);
	clipper->addChild(layer);

	CCSprite *stencil;
	if (byHoleType == TUTO_SMALL_HOLE)
		stencil = CCSprite::create("Image/game/tuto_point.png");
	else
		stencil = CCSprite::create("Image/game/tuto_point1.png");
	stencil->setPosition( holePos );
	clipper->setStencil(stencil);

	CCSprite *bright;
	if (byHoleType == TUTO_SMALL_HOLE)
		bright = CCSprite::create("Image/game/tuto_point.png");
	else
		bright = CCSprite::create("Image/game/tuto_point1.png");
	bright->setOpacity(nOpacity);
	bright->setPosition( holePos );
	addChild(bright);

	handPos.x = holePos.x - 100;
	handPos.y = holePos.y;
}

void CTutoLayer::addBubbleList(CCLayer* layer, TBubbleList& bubbleList, CCPoint& handPos)
{
	CCPoint lt = ccp(SC_WIDTH, 0);
	CCPoint rb = ccp(0, SC_HEIGHT);

	for (TBubbleList::iterator it = bubbleList.begin(); it != bubbleList.end(); it++)
	{
		CCSprite* spBack = CCSprite::create("Image/game/tuto_glow.png");
		CCPoint wrldPos = (*it)->convertToWorldSpaceAR(CCPointZero);
		spBack->setPosition(wrldPos);
		layer->addChild(spBack);

		lt.x = MIN(lt.x, wrldPos.x);
		lt.y = MAX(lt.y, wrldPos.y);

		rb.x = MAX(rb.x, wrldPos.x);
		rb.y = MIN(rb.y, wrldPos.y);
	}

	for (TBubbleList::iterator it = bubbleList.begin(); it != bubbleList.end(); it++)
	{
		CCPoint wrldPos = (*it)->convertToWorldSpaceAR(CCPointZero);

		CBubble* bubble = new CBubble((*it)->m_byType, (*it)->m_byColor);
		bubble->autorelease();
		bubble->setPosition(wrldPos);
		layer->addChild(bubble);
	}

	handPos.x = lt.x - 100;
	handPos.y = lt.y;
}

void CTutoLayer::showTutorial(const char* strTuto, char byType)
{
	CCLayerColor* layer = CCLayerColor::create();
	//	layer->setContentSize(CCSizeMake(SC_WIDTH, SC_HEIGHT));
	layer->setColor(ccBLACK);
	layer->setOpacity(nOpacity);
	addChild(layer);

	showTutorialCommon(strTuto, CCPointZero, byType);
}

void CTutoLayer::showTutorial(const char* strTuto, cocos2d::CCPoint holePos, TBubbleList& bubbleList, char byType, bool bCenter)
{
	CCPoint handPos;
	addHoleBack(holePos, handPos);

	CCPoint handPos2;
	addBubbleList(this, bubbleList, handPos2);

	showTutorialCommon(strTuto, handPos, byType, bCenter);
}

void CTutoLayer::showTutorialCommon(const char* strTuto, CCPoint handPos, char byType, bool isCenter)
{
	setTouchEnabled(true);

	CCPoint tutoBgPos = ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF);
	if (!isCenter)
	{
		if (handPos.y <= 10)
			tutoBgPos.y = SC_HEIGHT_HALF;
		if (handPos.y < SC_HEIGHT_HALF)
			tutoBgPos.y += SC_HEIGHT_HALF / 3;
		else
			tutoBgPos.y -= SC_HEIGHT_HALF / 3;
	}

	CCRect labelRect;
	const char* tutoBgName;
	float fCharaScale;
	if (byType == TUTO_BIG_CHARA)
	{
		tutoBgName = "Image/game/tuto_box0.png";
		labelRect.size = CCSizeMake(342, 105);
		labelRect.origin = ccp(-64, -85);
		fCharaScale = 1.2f;
	}
	else
	{
		tutoBgName = "Image/game/tuto_box1.png";
		labelRect.size = CCSizeMake(281, 140);
		labelRect.origin = ccp(-56, -54);
		fCharaScale = 1.2f;
	}

	CCSprite* sprtChara = CCSprite::create(tutoBgName);
	sprtChara->setPosition(tutoBgPos);
	sprtChara->setScale(fCharaScale);
	addChild(sprtChara);

	labelRect.origin.x += sprtChara->getContentSize().width / 2;
	labelRect.origin.y += sprtChara->getContentSize().height / 2;

	float fontSize = 22.0f;
	int nLen = strlen(strTuto);
	if (nLen > 80)
		fontSize = 20.0f;

	CCLabelTTF* label = CCLabelTTF::create(strTuto, FONT_NAME_HOBOSTD, fontSize, labelRect.size, kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);

	label->setPosition(ccp(labelRect.getMidX(), labelRect.getMidY()));
	label->setFontFillColor(ccBLACK);
	label->setColor(ccBLACK);
	sprtChara->addChild(label);

	if (handPos.x > 100 && handPos.y > 10)
	{
		CCSprite* sprtHand = CCSprite::create("Image/game/tuto_hand.png");
		sprtHand->setPosition(handPos);
		addChild(sprtHand);

		CCMoveBy* moveBy = CCMoveBy::create(1.0f, ccp(10, 0));
		sprtHand->runAction(CCRepeatForever::create(CCSequence::createWithTwoActions(moveBy, moveBy->reverse())));
	}

	//	addButtonEvent(pan, BTN_ID_CLOSE);
}

void CTutoLayer::hideTutorial()
{
	removeAllChildren();

	setTouchEnabled(false);

	setVisible(false);

	delayCallback(0);
//	scheduleOnce(schedule_selector(CTutoLayer::delayCallback), 0.3f);
}

void CTutoLayer::delayCallback(float dt)
{
	if (m_pSelectorTarget != NULL && m_pCallbackFunc != NULL)
		(m_pSelectorTarget->*m_pCallbackFunc)();
}

bool CTutoLayer::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	hideTutorial();
	return m_isTouchConsumed;
}
void CTutoLayer::ccTouchMoved(CCTouch* touches, CCEvent* event)
{

}
void CTutoLayer::ccTouchEnded(CCTouch* touches, CCEvent* event)
{
}

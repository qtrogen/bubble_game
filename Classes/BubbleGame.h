//
//  BubbleGame.h
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//
#ifndef __BubbleGame_H__
#define __BubbleGame_H__

#include "cocos2d.h"
#include <vector>
#include "Bubble.h"
#include "AimLayer.h"
#include "cocos-ext.h"
#include "BubbleLayer.h"
#include "BaseLayer.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCharacter;

class CBubbleGame : public CBaseLayer {
public:
	CBubbleGame();
	~CBubbleGame();

	CREATE_SCENE_METHOD(CBubbleGame);

	void orientationChangedCallback();

	void procGameEvent(int nEvent, int nVal = 0, float fVal = 0.0f);
	CCSprite* saveFriend(CCPoint pos, int nIndex, float fDelayTime);
	void saveBee(CBubble* beeBubble, int nIndex, float fDelayTime);
	void animeSavedFriend(CCNode* sprt, void* score);
	void animateScore(CCPoint pos, float score);

	void animateMultiplier();

	CBubble*	m_CurBubble;
	CBubble*	m_NextBubble;
	CBubble*	m_CandiBubble;

	CBubble*	m_CurBubbleInBubbleLayer;
	CCPoint		m_CurBubblePrevPos;

	CBubble*	m_Foresight1Bubble;
	CBubble*	m_Foresight2Bubble;
	CCSprite*	imgForesightFrame1;
	CCSprite*	imgForesightFrame2;

	void initGame();
	void rollbackCurBubble(int newType, int newCol);
	bool	m_hasCandiNode;
	bool	m_hasForesight;
	bool	m_hasSniperAim;

	int m_nTutoIndex;
	int m_nTutoParam;
	int m_nTutoBoost;
	int m_nTutoBoostBack;

	bool noticeTutoEvent(int evtId, int nParam = 0);
	virtual void tutorialTapped();
	void createCandyPos();
	void createForesightPos();
	void noticeCanShoot();
	void noticeStartLevel();
	int m_nShootableCheckCount;

	void afterCameraScreen();
	bool m_isTutoPassed;

protected:
	void setGameState(int nState);
	void processFor3Stars(float dt);
	void runTakeAnim();
	void gameEnd(float dt);
    void startStartAnimation();

	void calcGameResult(bool isWin, int nLevel, int nScore);
	int	 procChallenge();
	bool checkGoalAchieved(const char* goalDesc);
	void noticeGoalAchieved(int nType, int nCount, const char* desc, float fDelay);

	void startGame();
	void startReadyGo(float dt);
	void afterReadyGo(CCNode* sprite, void* data);

	void animeLeft10Bubbles(float dt);
	void showOutLive();
	bool onOutLivePlay();
	void updateOutliveTimer(float dt);
	void updateScore();
	void updateMultiplier();
	virtual bool onProcAfterClose(int nTag, int nParam);
	virtual void onPopupClosed(int nPopupTag);
    virtual bool onProcBeforeClose(int nButtonTag);

	// ui actions
	void onHome();
    void gotoHomeAfterDelay(float dt);
	void onRetry();
	void onNext();

	void onPauseGame(CCObject* obj);
	void showExitLevel(bool bExit);
	void onBeforeExitGame();

	//---------
	virtual void onEnter();
	virtual void onExit();
	virtual void onTimer();

	void initUI();
	void preload();

	void prepareNextBubble(float dt);
	void sendNextBubble(float dt);
	void setCurrentBubble(CCNode* node);
	void setCurrentBubbleBySwitch(CCNode* node);
	void setNextBubble(CCNode* node);
	void setCandiBubble(CCNode* node);
	void setForesight1Bubble(CCNode* node);
	void switchBubble(CCObject* obj);
	void switchExtraBubble(CCObject* obj);
	void animSwitchBubble(bool isForNextBubble);
	void launchAimBubble(CCObject* obj);
	void launchBubble();

	// animation effects
	void cbAnimationComplete(CCArmature * amature, MovementEventType evtType, const char * pCh);
	void addStarEffect(UIWidget* star, int index);
	void showWinPopup();
	void showFireworks();
	void afterFireworks(float dt);
    void playStarSfx();
    void playChallengeSfx();
    void playJobSfx(float dt);
	void showAnimation(const char* strJsonName, float fScale = 1.0f);

	bool m_bNukeAnimation;

	// boost related
	void initPowerups();
	void onSelectAnimalPowerup(cocos2d::CCObject* obj);
	void initBoostButtons();
	void initRecalls();
	void onBoost(CCObject* obj);
	void showBuyBoost(int boostId);
	void useBoost(int bosotId);
	void refreshBoostButtons();
	void undoMove();
	void onAchievedNewLevel(int nLevel);

	void setRemainBubbles(int remainCnt);

	void startCameraScreen();
	void startPinPopper();
	void remainBubblesRenew();
	void processDiffusion();
	void animateScore(float dt);
	void startBeeAnimation(float dt);
	void playFireCracker(float dt);
	void starTappingTutorial(float dt);
	void onStarClick(CCObject* obj);

	void callbackEndBeeAnimation(CCNode* node);

	void addAnimationObj(UIWidget* node);

	int curLevel;

	UIWidget*	m_pWinPane;
	CCSprite*	m_pImgHoleCover;
	CCSprite*	m_pFireworksPane;

public:
	float m_fScore;
	float m_fPrevScore;

	int m_nAnimScore;
	int m_nScoreCounter;
	int m_nScoreMultip;
	UILabelBMFont*	m_lblWinScore;

	float m_fMultiplier;
	float m_fPrevMultiplier;

	int		m_nTotalShoots;

	bool		m_bNeverShoot;

	CCPoint		m_ptStart;
	CCPoint		m_ptNextBubble;
	CCPoint		m_ptCandiBubble;
	CCPoint		m_ptForesight1Bubble;
	CCPoint		m_ptForesight2Bubble;

	CBubbleLayer*	m_pBubbleLayer;
	UIPanel*	m_pMainPanel;
	CCLayer*		m_pAnimLayer;

	CCSprite*	m_spLeg;
	CCPoint		m_ptLegPos;
	CCharacter*	m_pCharacter;
	CCLabelBMFont*	m_remainedBubbles;
	UILabelBMFont*	m_lblScore;
	UILabelBMFont*	m_lblMultiplier;
	UILabelBMFont*	m_lblStars;
	int m_nGameState;
	bool		m_bGameSuccess;

	// For game result popup
	int m_nStarCount;
	int m_nMagicCount;
	int m_nLiveCount;
	
	bool m_bRecallAvailable;
	int m_nRecallCountdown;

	int m_nTimerValue;
	UILabel* m_lblTimer;

	int m_nGameTimeValue;
	bool bDisplayedRemainAnime;

	/*CCRect		boostPaneRect;
	CCRect		extraBubbleRect;*/

	// for AI
#ifdef ROBOT_SHOOT
public:
	UIButton* m_aiBtn;
	int		m_nTestStageCount;
	int		m_nTestRepeatCount;
	int		m_nTestFailCount;
	int		m_nTestMaxStage;

	UILabel*	m_lblRepeatCount;
	UILabel*	m_lblFailCount;
	UILabel*	m_lblCurStage;

	float m_fMoveLayerTime;

#define MAX_TEST_STAGE_COUNT 1000

	std::vector<AITestLevelInfo> m_AITestResults;

	MyStructHit		colorsList[MAX_TEST_STAGE_COUNT][MAX_SHOOT_COUNT];
	
	bool m_bRobotShoot;
	
	void onAiButton(CCObject* obj);
	void aiStart();
	void aiStop();
	bool aiAnalyze();
	void updateTestInfo();
	void aiRetry(float dt);
	void aiPlayStart(float dt);
#endif //ROBOT_SHOOT
};

#endif //__BubbleGame_H__

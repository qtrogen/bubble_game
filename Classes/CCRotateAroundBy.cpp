#include "CCRotateAroundBy.h"

USING_NS_CC;

#define RAY_HALF_WIDTH 35.0f

//http://indiedevstories.com/2012/08/14/custom-cocos2d-action-rotating-sprite-around-arbitrary-point/

CCRotateAroundBy* CCRotateAroundBy::create(float fDuration, float fDeltaAngle, CCPoint rotationPoint)
{
	CCRotateAroundBy *pRotateBy = new CCRotateAroundBy();
	pRotateBy->initWithDuration(fDuration, fDeltaAngle, rotationPoint);
	pRotateBy->autorelease();

	return pRotateBy;
}

//CCRotateAroundBy* CCRotateAroundBy::create(float fDuration, float fStartAngle, float fEndAngle, CCPoint rotationPoint)
//{
//	CCRotateAroundBy *pRotateBy = new CCRotateAroundBy();
//	pRotateBy->initWithDuration(fDuration, fStartAngle, fEndAngle, rotationPoint);
//	pRotateBy->autorelease();
//
//	return pRotateBy;
//}

bool CCRotateAroundBy::initWithDuration(float fDuration, float fDeltaAngle, CCPoint rotationPoint)
{
	if (CCRotateBy::initWithDuration(fDuration, fDeltaAngle))
	{
		this->m_rotationPoint = rotationPoint;

		m_bEqualSpeed = true;

		return true;
	}

	return false;
}

//bool CCRotateAroundBy::initWithDuration(float fDuration, float fStartAngle, float fEndAngle, CCPoint rotationPoint)
//{
//	if (CCRotateBy::initWithDuration(fDuration, fStartAngle))
//	{
//		this->m_rotationPoint = rotationPoint;
//
//		this->m_fStartAngle = fStartAngle;
//		this->m_fEndAngle = fEndAngle;
//
//		m_bEqualSpeed = false;
//
//		return true;
//	}
//
//	return false;
//}


void CCRotateAroundBy::startWithTarget(CCNode *pTarget)
{
	CCRotateBy::startWithTarget(pTarget);

	this->m_startPoint = pTarget->getPosition();
}

void CCRotateAroundBy::update(float time)
{
	CCLog("%f", time);

	float angleX, angleY;

	/*angleX = m_fAngleX * time;
	angleY = m_fAngleY * time;*/

	angleX = (2 * time - time * time) * m_fAngleX;
	angleY = (2 * time - time * time) * m_fAngleY;

	/*angleX = (pow(time - 1, 3) + 1) * m_fAngleX;
	angleY = (pow(time - 1, 3) + 1) * m_fAngleY;*/


	float x = cos(CC_DEGREES_TO_RADIANS(-angleX)) * (m_startPoint.x - m_rotationPoint.x) - sin(CC_DEGREES_TO_RADIANS(-angleX)) * (m_startPoint.y - m_rotationPoint.y) + m_rotationPoint.x;
	float y = sin(CC_DEGREES_TO_RADIANS(-angleY)) * (m_startPoint.x - m_rotationPoint.x) + cos(CC_DEGREES_TO_RADIANS(-angleY)) * (m_startPoint.y - m_rotationPoint.y) + m_rotationPoint.y;

	m_pTarget->setPosition(x, y);
	
	m_pTarget->setRotationX(m_fStartAngleX + angleX);
	m_pTarget->setRotationY(m_fStartAngleY + angleY);
}
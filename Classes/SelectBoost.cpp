//
//  CSelBoostScene.m
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//

#include "SimpleAudioEngine.h"
#include "LCommon.h"
#include "AppDelegate.h"
#include "SelectChara.h"
#include "SelectBoost.h"
#include "LevelSelect.h"
#include "BubbleGame.h"
#include "PreMapScene.h"
#include "MathUtil.h"
#include <algorithm>

enum
{
	TAG_TBL_BOOST_INFO = 10,
	//TAG_TBL_DAILY_CHAL,
	TAG_TBL_CHAMPIONS,
	TAG_TBL_FRIEND_LIST,

	TAG_BST_ICON = 100,
	TAG_BST_TITLE,
	TAG_BST_DESC1,
	TAG_BST_DESC2,

	TAG_CHL_DESC,
	TAG_CHL_REWARD_TYPE,
	TAG_CHL_REWARD_VAL,
	TAG_CHL_LVL_NUM,
	TAG_CHL_DONE,

	TAG_BTN_FBCONNECT,
};

enum 
{
	BTN_ID_BACK = BTN_ID_START,
	BTN_ID_DAILY_CHAL_DONE,
};

USING_NS_CC;

std::string sfxIds[] = {
	kSND_SFX_SNIPER_RIFLE_S,    
	kSND_SFX_EXTRA_BUBBLES_S,   
	kSND_SFX_PIN_S,             
	kSND_SFX_COLOR_BOMB_S,
	kSND_SFX_EXTRA_SWAP_S,      
	kSND_SFX_NUKE_S,            
	kSND_SFX_REPELLENT_S,       
	kSND_SFX_RAINBOW_BUBBLE_S,  
};


extern TGoalInfo g_GoalList[];


CSelBoostScene::CSelBoostScene()
{
	setSceneID(SCENE_SELBOOST);

	setBGMName(kSND_BG_MAINTHEME);

	LCommon::sharedInstance()->createDailyChallengeList();

	dailyChalListForCurlevel = &LCommon::sharedInstance()->dailyChallengeList;

	for (int i = BOOST_START; i < BOOST_COUNT; i++)
	{
		if (LCommon::sharedInstance()->boostUnlocked[i])
			unlockedBoosts.push_back(i);
	}

	nSelectedBoostIndex = -1;
};

CSelBoostScene::~CSelBoostScene()
{
}

//Checked
void CSelBoostScene::initUI()
{
	UIImageView* imgView = UIImageView::create();
	imgView->loadTexture(WORLDIMG("world_bg.png"));
	imgView->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
	imgView->setScale(SC_BG_SCALE);
	m_pMainLayer->addWidget(imgView);

	m_pMainPanel = UIImageView::create();
	m_pMainPanel->setScale(SC_DLG_SCALE);
	m_pMainPanel->loadTexture(WORLDIMG("dlg_bg.png"));
	m_pMainPanel->setAnchorPoint(ccp(0.5, 1));
	m_pMainPanel->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT - m_nActionBarHeight - 85));
	m_pMainLayer->addWidget(m_pMainPanel);
	
	createDialog();

	// add buttons
	UIButton* btnBack = UIButton::create();
	btnBack->setAnchorPoint(ccp(0.0f, 0.5f));
	btnBack->setTouchEnabled(true);
	btnBack->loadTextures(WORLDIMG("btn_back_n.png"), WORLDIMG("btn_back_p.png"), "");
	btnBack->setPosition(ccp(0, SC_HEIGHT - 150));
	btnBack->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onBack));
	m_pMainLayer->addWidget(btnBack);

	UIButton* btnTreasure = UIButton::create();
	btnTreasure->setTouchEnabled(true);
	btnTreasure->setAnchorPoint(ccp(1.0f, 0.5f));
	btnTreasure->loadTextures("Image/boosts/btn_treasure_n.png", "Image/boosts/btn_treasure_p.png", "");
	btnTreasure->setPosition(ccp(SC_WIDTH, 80));
	btnTreasure->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onTreasure));
	m_pMainLayer->addWidget(btnTreasure);

	std::string strAnimal = CCString::createWithFormat("Image/chara/%s_n.png", CHARA_NAME[LCommon::sharedInstance()->curChara].c_str())->getCString();
	UIButton* btnAnimal = UIButton::create();
	btnAnimal->setTouchEnabled(true);
	btnAnimal->setAnchorPoint(ccp(0.0f, 0.5f));
	btnAnimal->loadTextures(strAnimal.c_str(), strAnimal.c_str(), "");
	btnAnimal->setScale(0.35);
	btnAnimal->setPosition(ccp(10, 100));
	btnAnimal->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onAnimal));
	m_pMainLayer->addWidget(btnAnimal);

	UIButton* btnChallenge = UIButton::create();
	btnChallenge->setTouchEnabled(true);
	btnChallenge->setAnchorPoint(ccp(1.0f, 0.5f));
	btnChallenge->loadTextures("Image/boosts/challenge_n.png", "Image/boosts/challenge_p.png", "");
	btnChallenge->setPosition(ccp(SC_WIDTH - 10, SC_HEIGHT - m_nActionBarHeight - 70));
	btnChallenge->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onChallenge));
	m_pMainLayer->addWidget(btnChallenge);

	createBoostInfo();

	createActionBar();

	//createGoalCurtain();

	createFriendTableView();
}

//Checked
void CSelBoostScene::createDialog()
{
	int nCurLevel = LCommon::sharedInstance()->curLevel();
	UIImageView* imgView = UIImageView::create();
	imgView->loadTexture(WORLDIMG("boost_bg.png"));
	m_pMainPanel->addChild(imgView);
	imgView->setAnchorPoint(ccp(0.5, 1));
	imgView->setPosition(ccp(0, 0));


	UIImageView* levelNameImgView = UIImageView::create();

	std::string levelNameStr = LEVELS_NAME[nCurLevel];
	if(levelNameStr.find("C") != -1)
		levelNameImgView->loadTexture("Image/level/lbl_clearboard.png");
	else if(levelNameStr.find("M") != -1)
		levelNameImgView->loadTexture("Image/level/lbl_defeatmows.png");
	else if(levelNameStr.find("A") != -1)
		levelNameImgView->loadTexture("Image/level/lbl_freeanimals.png");
	else if(levelNameStr.find("B") != -1)
		levelNameImgView->loadTexture("Image/level/lbl_savebee.png");

	m_pMainPanel->addChild(levelNameImgView);
	levelNameImgView->setPosition(ccp(0, -158));


	UILabelAtlas* lblLevel = UILabelAtlas::create();
	char firstChar = '0';
	lblLevel->setProperty(
		CCString::createWithFormat("%d", nCurLevel + 1)->getCString(),
		WORLDIMG("level_num.png"),
		41, 64, &firstChar);
	int xx = 46;
	if (nCurLevel >= 99) xx = 78;
	else if (nCurLevel >= 9) xx = 66;
	lblLevel->setPosition(ccp(xx, -48));
	m_pMainPanel->addChild(lblLevel);

	int xpos_star[] = {-172, 3, 180};
	for (int i = 0; i < LCommon::sharedInstance()->stars(nCurLevel); i++)
	{
		UIImageView* imgStar = UIImageView::create();
		imgStar->loadTexture("Image/common/star.png");
		imgStar->setRotation(0.0f);
		imgStar->setScale(1.0f);
		imgStar->setPosition(ccp(xpos_star[i], -305));
		m_pMainPanel->addChild(imgStar);
	}
    
	//char* animal[] = {"Kali", "Piggles", "Roo", "Jack", "Pingu", "Momo"};
	int xpos_boost[] = {-1000, -228, -113, 3, 115, 227};

	int nCharaId = LCommon::sharedInstance()->curChara;

	for (int i = 1; i <= 5; i++)
	{
		UIImageView* imgView_Animal = UIImageView::create();
		imgView_Animal->loadTexture(CCString::createWithFormat("Image/boosts/%s.png", CHARA_NAME[i].c_str())->getCString());
		imgView_Animal->setAnchorPoint(ccp(0.5, 0));
		imgView_Animal->setPosition(ccp(xpos_boost[i], -490));
		m_pMainPanel->addChild(imgView_Animal);


		UIButton* btnBoost = UIButton::create();
		btnBoost->loadTextures("Image/boosts/boost_item_bg_n.png", "Image/boosts/boost_item_bg_p.png", "");
		btnBoost->setTouchEnabled(i != nCharaId);
		btnBoost->setAnchorPoint(ccp(0.5, 1));
		btnBoost->setPosition(ccp(xpos_boost[i], -480));
		btnBoost->setTag(i);
		m_pMainPanel->addChild(btnBoost);
		if(i != nCharaId)
			btnBoost->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onSelectAnimalPowerup));


		UIImageView* imgView_Boost = UIImageView::create();
		imgView_Boost->loadTexture(CCString::createWithFormat("Image/boosts/%s_Boost.png", CHARA_NAME[i].c_str())->getCString());
		imgView_Boost->setAnchorPoint(ccp(0.5, 1));
		imgView_Boost->setPosition(ccp(xpos_boost[i], -480));
		m_pMainPanel->addChild(imgView_Boost);


		UIButton* btn_BoostTick = UIButton::create();
		btn_BoostTick->loadTextures("Image/boosts/boost_tick.png", "Image/boosts/boost_tick.png", "");
		btn_BoostTick->setAnchorPoint(ccp(0.5, 0.5));
		btn_BoostTick->setPosition(ccp(xpos_boost[i] + 40, -480 - 95));
		btn_BoostTick->setTag(i);
		btn_BoostTick->setTouchEnabled(i != nCharaId);
		btn_BoostTick->setVisible(i == nCharaId);
		btn_BoostTick->setName(CCString::createWithFormat("BoostTickBtn_%d", i)->getCString());
		m_pMainPanel->addChild(btn_BoostTick);
		if(i != nCharaId)
			btn_BoostTick->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onTickTapped));


		UIButton* btn_BoostFrame = UIButton::create();
		btn_BoostFrame->loadTextures("Image/boosts/boost_labelframe.png", "Image/boosts/boost_labelframe.png", "");
		btn_BoostFrame->setTouchEnabled(i != nCharaId);
		btn_BoostFrame->setVisible(i != nCharaId);
		btn_BoostFrame->setAnchorPoint(ccp(0.5, 0.5));
		btn_BoostFrame->setPosition(ccp(xpos_boost[i] + 40, -480 - 95));
		btn_BoostFrame->setTag(i);
		btn_BoostFrame->setName(CCString::createWithFormat("BoostLabelBtn_%d", i)->getCString());
		m_pMainPanel->addChild(btn_BoostFrame);
		if(i != nCharaId)
			btn_BoostFrame->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onSelectAnimalPowerup));


		const char* strLabel = (LCommon::sharedInstance()->boostCount[special_boost_info[i].boostId] == 0) ? 
			"+" : CCString::createWithFormat("%d", LCommon::sharedInstance()->boostCount[special_boost_info[i].boostId])->getCString();

		UILabelBMFont* label_BoostCount = UILabelBMFont::create();
		label_BoostCount->setFntFile("Image/fonts/Myriadpro111_brown.fnt");
		label_BoostCount->setText(strLabel);
		label_BoostCount->setScale(0.35);
		label_BoostCount->setAnchorPoint(ccp(0.5, 0.5));
		label_BoostCount->setPosition(ccp(xpos_boost[i] + 41, -480 - 94));
		label_BoostCount->setName(CCString::createWithFormat("BoostLabel_%d", i)->getCString());
		label_BoostCount->setVisible(i != nCharaId);
		m_pMainPanel->addChild(label_BoostCount);
	}

	UIButton* btnPlay = UIButton::create();
	btnPlay->loadTextures("Image/boosts/btn_play_n.png", "Image/boosts/btn_play_p.png", "");
	btnPlay->setTouchEnabled(true);
	btnPlay->setPosition(ccp(2, -680));
	btnPlay->setName("btnplay");
	btnPlay->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onPlay));
	m_pMainPanel->addChild(btnPlay);

	m_ptPlayHeart = btnPlay->getWorldPosition();
	m_ptPlayHeart.x += 76 * SC_DLG_SCALE;
}

void CSelBoostScene::onTickTapped(cocos2d::CCObject* obj)
{
	UIButton* node = dynamic_cast<UIButton*> (obj);
	nSelectedBoostIndex = node->getTag();

	refreshBoostInfo();
}

void CSelBoostScene::onSelectAnimalPowerup(cocos2d::CCObject* obj)
{
	UIButton* node = dynamic_cast<UIButton*> (obj);
	nSelectedBoostIndex = node->getTag();

	if(LCommon::sharedInstance()->boostCount[special_boost_info[node->getTag()].boostId] > 0)
	{
		refreshBoostInfo();
	}
	else
	{
		showBuyPowerup(node->getTag());
	}
}

//void CSelBoostScene::showBuyPowerup(int nBoostIndex)
//{
//	UIWidget* magicWidget = (GUIReader::shareReader()->widgetFromJsonFile("Image/buy_boost.json"));
//
//	UIWidget* pBGImage = magicWidget->getChildByName("imageBG");
//	pBGImage->setScale(SC_DLG_SCALE);
//	pBGImage->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
//
//	//Boost Icon, Title, Description
//	UIImageView* pBoostIcon = (UIImageView*)pBGImage->getChildByName("ImageView_boost");
//	pBoostIcon->loadTexture(CCString::createWithFormat("Image/boosts/boost_%d.png", special_boost_info[nBoostIndex].boostId)->getCString());
//
//	UIImageView* pBoostName = (UIImageView*)pBGImage->getChildByName("ImageView_BoostName");
//	pBoostName->loadTexture(CCString::createWithFormat("Image/boosts/boost_effect_desc_%d.png", special_boost_info[nBoostIndex].boostId)->getCString());
//
//	UILabelBMFont* lblBoostDesc = (UILabelBMFont*) pBGImage->getChildByName("lbl_BoostDesc");
//	lblBoostDesc->setText(special_boost_info[nBoostIndex].boostDesc.c_str());
//
//	//Item1
//	UIImageView* pItem1Icon = (UIImageView*)pBGImage->getChildByName("ImageView_boost_1");
//	pItem1Icon->loadTexture(CCString::createWithFormat("Image/boosts/boost_%d.png", special_boost_info[nBoostIndex].boostId)->getCString());
//
//	UILabelBMFont* lblItem1Count = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Count_1");
//	lblItem1Count->setText(CCString::createWithFormat("x %d", special_boost_info[nBoostIndex].item1_count)->getCString());
//
//	UILabelBMFont* lblItem1Price = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Price_1");
//	lblItem1Price->setText(CCString::createWithFormat("x %d", special_boost_info[nBoostIndex].item1_cost)->getCString());
//
//	UIButton* btnBuy1 = (UIButton*) pBGImage->getChildByName("btnBuy1");
//	btnBuy1->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onBuyBoostItem1));
//
//
//	//Item2
//	UIImageView* pItem2Icon = (UIImageView*)pBGImage->getChildByName("ImageView_boost_2");
//	pItem2Icon->loadTexture(CCString::createWithFormat("Image/boosts/boost_%d.png", special_boost_info[nBoostIndex].boostId)->getCString());
//
//	UILabelBMFont* lblItem2Count = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Count_2");
//	lblItem2Count->setText(CCString::createWithFormat("PERMANENT")->getCString());
//
//	UILabelBMFont* lblItem2Price = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Price_2");
//	lblItem2Price->setText(CCString::createWithFormat("$%.2f", special_boost_info[nBoostIndex].item2_cost)->getCString());
//
//	UIButton* btnBuy2 = (UIButton*) pBGImage->getChildByName("btnBuy2");
//	btnBuy2->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onBuyBoostItem2));
//
//
//	//Item3
//	UIImageView* pItem3Icon = (UIImageView*)pBGImage->getChildByName("ImageView_boost_3");
//	pItem3Icon->loadTexture(CCString::createWithFormat("Image/boosts/boost_%d.png", special_boost_info[nBoostIndex].boostId)->getCString());
//
//	UILabelBMFont* lblItem3Count = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Count_3");
//	lblItem3Count->setText(CCString::createWithFormat("x %d", special_boost_info[nBoostIndex].item3_count)->getCString());
//
//	UILabelBMFont* lblItem3AdditionCount = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Count_3_Addition");
//	lblItem3AdditionCount->setText(CCString::createWithFormat("+%d FREE", special_boost_info[nBoostIndex].item3_free_count)->getCString());
//
//	UILabelBMFont* lblItem3Price = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Price_3");
//	lblItem3Price->setText(CCString::createWithFormat("x %d", special_boost_info[nBoostIndex].item3_cost)->getCString());
//
//	UIButton* btnBuy3 = (UIButton*) pBGImage->getChildByName("btnBuy3");
//	btnBuy3->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onBuyBoostItem3));
//
//	//Close Button
//	UIButton* closeBtn = (UIButton*) magicWidget->getChildByName("btnClose");
//
//	showPopup(magicWidget, closeBtn);
//}
//
//void CSelBoostScene::delayProcPopup(float dt)
//{
//	closeTopPopup();
//}
//
//void CSelBoostScene::onBuyBoostItem1(cocos2d::CCObject* obj)
//{
//	if(special_boost_info[nSelectedBoostIndex].item1_cost > LCommon::sharedInstance()->magicCount)
//	{
//		showNoEnoughMagic();
//	}else{
//		LCommon::sharedInstance()->magicCount -= special_boost_info[nSelectedBoostIndex].item1_cost;
//		LCommon::sharedInstance()->boostCount[special_boost_info[nSelectedBoostIndex].boostId] += special_boost_info[nSelectedBoostIndex].item1_count;
//		
//		refreshInfo();
//		LCommon::sharedInstance()->saveState();
//
//		refreshBoostInfo();
//		
//		scheduleOnce(schedule_selector(CSelBoostScene::delayProcPopup), 0);
//	}
//}
//
//void CSelBoostScene::onBuyBoostItem2(cocos2d::CCObject* obj)
//{
//	
//}
//
//void CSelBoostScene::onBuyBoostItem3(cocos2d::CCObject* obj)
//{
//	if(special_boost_info[nSelectedBoostIndex].item3_cost > LCommon::sharedInstance()->magicCount)
//	{
//		showNoEnoughMagic();
//	}else{
//		LCommon::sharedInstance()->magicCount -= special_boost_info[nSelectedBoostIndex].item3_cost;
//		LCommon::sharedInstance()->boostCount[special_boost_info[nSelectedBoostIndex].boostId] += special_boost_info[nSelectedBoostIndex].item3_count + special_boost_info[nSelectedBoostIndex].item3_free_count;
//		
//		refreshInfo();
//		LCommon::sharedInstance()->saveState();
//
//		refreshBoostInfo();
//
//		scheduleOnce(schedule_selector(CSelBoostScene::delayProcPopup), 0);
//	}
//}

void CSelBoostScene::refreshBoostInfo(float dt)
{
	refreshBoostInfo();
}

void CSelBoostScene::refreshBoostInfo()
{
	UIButton* btn_BoostTick = (UIButton*)m_pMainPanel->getChildByName(CCString::createWithFormat("BoostTickBtn_%d", nSelectedBoostIndex)->getCString());
	UIButton* btn_BoostFrame = (UIButton*)m_pMainPanel->getChildByName(CCString::createWithFormat("BoostLabelBtn_%d", nSelectedBoostIndex)->getCString());
	UILabelBMFont* label_BoostCount = (UILabelBMFont*)m_pMainPanel->getChildByName(CCString::createWithFormat("BoostLabel_%d", nSelectedBoostIndex)->getCString());

	if(btn_BoostTick->isVisible())
	{
		btn_BoostTick->setVisible(false);
		btn_BoostTick->setEnabled(false);

		btn_BoostFrame->setVisible(true);
		btn_BoostFrame->setEnabled(true);

		label_BoostCount->setVisible(true);
		label_BoostCount->setEnabled(true);
	}
	else
	{
		int nBoostCount = LCommon::sharedInstance()->boostCount[special_boost_info[nSelectedBoostIndex].boostId];

		btn_BoostTick->setVisible(nBoostCount > 0);
		btn_BoostTick->setEnabled(nBoostCount > 0);

		btn_BoostFrame->setVisible(nBoostCount == 0);
		btn_BoostFrame->setEnabled(nBoostCount == 0);

		const char* strLabel = (nBoostCount == 0) ? "+" : CCString::createWithFormat("%d", nBoostCount)->getCString();
		label_BoostCount->setText(strLabel);
		label_BoostCount->setVisible(nBoostCount == 0);
		label_BoostCount->setEnabled(nBoostCount == 0);
	}
}

void CSelBoostScene::refreshChampions(float dt)
{
	refreshChampionsDlg();
}

void CSelBoostScene::refreshChampionsDlg()
{
	bool isConnected = LCommon::sharedInstance()->isFBConnected;

	fbLoginPanel->setVisible(!isConnected);
	btnFBLogin->setTouchEnable(!isConnected);
	friendTableView->setVisible(isConnected);

	UIWidget* challengePopup = topPopupWidget();
	UIWidget* widget = challengePopup->getChildByName("POPUP_CHALLENGE");
	if(widget)
	{
		CCTableView* tableView = (CCTableView* )widget->getRenderer()->getChildByTag(TAG_TBL_CHAMPIONS);
		if(isConnected)
		{
			//tableView->setContentSize(CCSizeMake(550, 480));
			tableView->setViewSize(CCSizeMake(550, 480));
			tableView->setPosition(ccp(-270, -250));
		}
		else
		{
			//tableView->setContentSize(CCSizeMake(550, 150));
			tableView->setViewSize(CCSizeMake(550, 150));
			tableView->setPosition(ccp(-270, 60));
		}
		tableView->reloadData();

		UIButton* btnFBConnect = (UIButton* )widget->getChildByTag(TAG_BTN_FBCONNECT);
		btnFBConnect->setTouchEnabled(!isConnected);
		btnFBConnect->setVisible(!isConnected);
	}

	/*if(challengePopup)
		challengePopup->setUpdateEnabled(true);*/
}

void CSelBoostScene::addImage(const char* imgName, float x, float y)
{
	UIImageView* imgView = UIImageView::create();
	imgView->loadTexture(WORLDIMG(imgName));
	m_pMainPanel->addChild(imgView);
	imgView->setPosition(ccp(x, y));
}

void CSelBoostScene::onBack(cocos2d::CCObject* obj)
{
    showIndicator();
    scheduleOnce(schedule_selector(CSelBoostScene::gotoHomeAfterDelay), 0.01f);
}

void CSelBoostScene::gotoHomeAfterDelay(float dt)
{
    CCTextureCache::sharedTextureCache()->removeAllTextures();
    
    if (IS_LOWMEMORY)
        CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(0, CPreMapScene::scene(), ccWHITE));
    else
    {
    	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CLevelSelect::scene(), ccWHITE));
        hideIndicator();
    }
}

void CSelBoostScene::onBoostInfo(cocos2d::CCObject* obj)
{
	showPopup(m_pBoostInfo, "btnClose");
}

//Checked
void CSelBoostScene::onPlay(cocos2d::CCObject* obj)
{
	LCommon::sharedInstance()->curBoosts = 0;

	int boostList = getBoostFromList();
	
	/*int nReqCoins = 0;
	int nReqMagic = 0;

	for (int i = 0; i < BOOST_SEL; i++)
	{
		int boostid = i + 1;

		if ((boostList & (1 << boostid)) == 0) continue;
		if (LCommon::sharedInstance()->boostCount[boostid] > 0) continue;
		BOOST_COST cost = getBoostCost(boostid);
		if (cost.payType == IT_NONE) continue;

		if (cost.payType == IT_MAGIC)
			nReqMagic += cost.magicCost;
	}*/

	bool bOK = true;
	/*if (bOK && nReqMagic > LCommon::sharedInstance()->magicCount)
	{
		showNoEnoughMagic();
		bOK = false;
	}*/

	if (bOK && LCommon::sharedInstance()->liveCount <= 0)
	{
		showNoEnoughLive();
		bOK = false;
	}

	if (bOK)
	{
		LCommon::sharedInstance()->curBoosts = boostList;

		/*for (int i = 0; i < BOOST_SEL; i++)
		{
			int boostid = i + 1;

			if ((boostList & (1 << boostid)) == 0) continue;
			if (LCommon::sharedInstance()->boostCount[boostid] > 0) continue;

			AppDelegate::app->sendFuseEventPerLevel(CCString::createWithFormat(PURCHASE_FMT, BOOST_NAME[boostid].c_str())->getCString(), 0);

			BOOST_COST cost = getBoostCost(boostid);

			LCommon::sharedInstance()->boostCount[boostid] += cost.count;
			LCommon::sharedInstance()->magicCount -= cost.magicCost;
		}*/

		startPlayAnimation();
	}
}

//Checked
void CSelBoostScene::startPlayAnimation()
{
	UIPanel* coverLayer = UIPanel::create();
	coverLayer->setTouchEnabled(true);
	coverLayer->setSize(CCSizeMake(SC_WIDTH, SC_HEIGHT));
	m_pPopupLayer->addWidget(coverLayer);
	
	UIWidget* actionHeart = m_pActionBar->getChildByName("icon_live");
	CCPoint startPos = actionHeart->getWorldPosition();

	CCSprite* glow = CCSprite::create("Image/common/glow.png");
	glow->setPosition(startPos);
	m_pMainLayer->addChild(glow, 19);

	CCSprite* heart = CCSprite::create("Image/common/item_live.png");
	heart->setPosition(startPos);
	heart->setScale(0.6f);
	m_pMainLayer->addChild(heart, 21);

	ccBezierConfig bezier2;
	bezier2.controlPoint_1 = ccp(100, (m_ptPlayHeart.y + startPos.y) / 2);
	bezier2.controlPoint_2 = ccp(-SC_WIDTH_HALF / 2, (m_ptPlayHeart.y + startPos.y) / 2);
	bezier2.endPosition = m_ptPlayHeart;

	// for glow
	CCActionInterval* fade1 = CCFadeIn::create(2.0f);
	glow->runAction(CCSequence::createWithTwoActions(fade1, fade1->reverse()));

	CCActionInterval* scale1 = CCScaleTo::create(0.1f, 0.8f);

	float fMoveTime = 1;
	CCActionInterval* bezierTo1 = CCBezierTo::create(fMoveTime, bezier2);
	CCActionInterval* scale2 = CCScaleTo::create(fMoveTime, 1.2f);
	CCActionInterval* rot = CCRotateBy::create(fMoveTime, -360);
	CCSpawn* spMove = CCSpawn::create(bezierTo1, scale2, rot, NULL);

	CCCallFuncN* func = CCCallFuncN::create(this, callfuncN_selector(CSelBoostScene::afterMoveHeart));

	heart->runAction(CCSequence::create(scale1, spMove, func, NULL));
}

//Checked
void CSelBoostScene::afterMoveHeart(CCNode* node)
{
	
	CCSprite* stars = CCSprite::create("Image/common/stars.png");
	stars->setPosition(m_ptPlayHeart);
	stars->setScale(0.8f);
	m_pMainLayer->addChild(stars, 20);

	float fOutTime = 0.5f;
	CCActionInterval* scaleS = CCScaleTo::create(fOutTime, 1.2f);
	CCActionInterval* fadeS = CCFadeIn::create(fOutTime * 0.5);

	CCSequence* seq1 = CCSequence::create(fadeS, fadeS->reverse(), NULL);

	CCActionInterval* rotS = CCRotateBy::create(fOutTime, -90);

	CCSpawn* spOut1 = CCSpawn::create(scaleS, seq1, rotS, NULL);
	stars->runAction(spOut1);

	CCActionInterval* scale3 = CCScaleTo::create(fOutTime, 2.0f);
	CCActionInterval* fade2 = CCFadeOut::create(fOutTime);
	CCSpawn* spOut = CCSpawn::create(scale3, fade2, NULL);

	CCCallFunc* funcLoad = CCCallFunc::create(this, callfunc_selector(CBaseLayer::showIndicator));
    CCDelayTime*    delay = CCDelayTime::create(0.1f);
	CCCallFunc* func = CCCallFunc::create(this, callfunc_selector(CSelBoostScene::startGame));

	node->runAction(CCSequence::create(spOut, funcLoad, delay, func, NULL));
}

//Checked
void CSelBoostScene::startGame()
{
    CCTextureCache::sharedTextureCache()->removeUnusedTextures();
    CCScene* gmscene = CBubbleGame::scene();
    hideIndicator();
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, gmscene, ccWHITE));
}

void CSelBoostScene::onTreasure(cocos2d::CCObject* obj)
{
	showTreasure();
}

void CSelBoostScene::onAnimal(cocos2d::CCObject* obj)
{
	IS_ANIMAL_SELECTED = true;
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CSelCharaScene::scene(), ccWHITE));
}

void CSelBoostScene::onChallenge(cocos2d::CCObject* obj)
{
	int nCurLevel = LCommon::sharedInstance()->curLevel();

	UIImageView* pBGImage = UIImageView::create();
	pBGImage->loadTexture(WORLDIMG("dlg_bg.png"));
	pBGImage->setScale(SC_DLG_SCALE);
	pBGImage->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
	pBGImage->setName("POPUP_CHALLENGE");

	CCSprite *spBg = CCSprite::create(WORLDIMG("challenge_bg.png"));
	spBg->setPosition(ccp(0, 0));
	pBGImage->addCCNode(spBg);
	
	UILabelAtlas* lblLevel = UILabelAtlas::create();
	char firstChar = '0';
	lblLevel->setProperty(
		CCString::createWithFormat("%d", nCurLevel + 1)->getCString(),
		WORLDIMG("level_num.png"),
		41, 64, &firstChar);
	int xx = 46;
	if (nCurLevel >= 99) xx = 78;
	else if (nCurLevel >= 9) xx = 66;
	lblLevel->setPosition(ccp(xx, 335));
	pBGImage->addChild(lblLevel);

	UIButton* btnClose = UIButton::create();
	btnClose->loadTextures("Image/popup/btn_close_n.png", "Image/popup/btn_close_p.png", "");
	btnClose->setPosition(ccp(280, 360));
	pBGImage->addChild(btnClose);

	CCTableView* tableView;
	if(LCommon::sharedInstance()->isFBConnected)
	{
		tableView = CCTableView::create(this, CCSizeMake(550, 480));
		tableView->setPosition(ccp(-270, -250));
	}
	else
	{
		tableView = CCTableView::create(this, CCSizeMake(550, 150));
		tableView->setPosition(ccp(-270, 60));
	}

	tableView->setDirection(kCCScrollViewDirectionVertical);
	tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	pBGImage->addCCNode(tableView);
	tableView->setTag(TAG_TBL_CHAMPIONS);
	tableView->reloadData();

	UIButton* btnFBConnect = UIButton::create();
	btnFBConnect->loadTextures("Image/common/fb_connect_n.png", "Image/common/fb_connect_p.png", "");
	btnFBConnect->setPosition(ccp(0, -100));
	btnFBConnect->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onFBConnect));
	pBGImage->addChild(btnFBConnect);
	btnFBConnect->setTouchEnabled(!LCommon::sharedInstance()->isFBConnected);
	btnFBConnect->setVisible(!LCommon::sharedInstance()->isFBConnected);
	btnFBConnect->setTag(TAG_BTN_FBCONNECT);

	showPopup(pBGImage, btnClose);
}
//void CSelBoostScene::onChallenge(cocos2d::CCObject* obj)
//{
//	UIImageView* pBGImage = UIImageView::create();
//	pBGImage->loadTexture("Image/boosts/challenge_bg.png");
//	pBGImage->setScale(SC_DLG_SCALE);
//	pBGImage->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
//
//	UIButton* btnDone = UIButton::create();
//	btnDone->loadTextures("Image/boosts/btn_done_n.png", "Image/boosts/btn_done_p.png", "");
//	btnDone->setPosition(ccp(0, -302));
//	addButtonEvent(btnDone, BTN_ID_DAILY_CHAL_DONE);
//	pBGImage->addChild(btnDone);
//
//	UIButton* btnClose = UIButton::create();
//	btnClose->loadTextures("Image/popup/btn_close_n.png", "Image/popup/btn_close_p.png", "");
//	btnClose->setPosition(ccp(248, 388));
//	pBGImage->addChild(btnClose);
//
//	CCTableView* tableView = CCTableView::create(this, CCSizeMake(514, 484));
//	tableView->setPosition(ccp(-256, -193));
//	tableView->setDirection(kCCScrollViewDirectionVertical);
//	tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
//	pBGImage->addCCNode(tableView);
//	tableView->setTag(TAG_TBL_DAILY_CHAL);
//	tableView->reloadData();
//
//	UIImageView* headermark = UIImageView::create();
//	headermark->loadTexture("Image/boosts/spliter2.png");
//	headermark->setPosition(ccp(0, 260));
//	pBGImage->addChild(headermark);
//
//	showPopup(pBGImage, btnClose);
//}

//Checked
void CSelBoostScene::addBoost(CCObject* obj)
{
	UIButton* node = (UIButton*) obj;
	int boostid = node->getTag();

	for (int i = 0; i < BOOST_SEL_COUNT; i++)
		if (selectedBoosts[i]->isActive() &&  selectedBoosts[i]->getTag() == boostid) return;

	for (int i = 0; i < BOOST_SEL_COUNT; i++)
	{
		if (selectedBoosts[i]->isActive()) continue;
		selectedBoosts[i]->setTag(boostid);

		const char* boostImg = CCString::createWithFormat("Image/boosts/boost_%d.png", boostid)->getCString();
		selectedBoosts[i]->setNormalTexture(boostImg);
		selectedBoosts[i]->setPressedTexture(boostImg);

		selectedBoosts[i]->active();

		break;
	}

	playEffect(sfxIds[boostid - 1].c_str());
}

//Checked
void CSelBoostScene::removeBoost(CCObject* obj)
{
	UIButton* boostBtn = (UIButton*) obj;
	boostBtn->disable();
}

//Checked
int CSelBoostScene::getBoostFromList()
{
	/*int boosts = 0;
	for (int i = 0; i < BOOST_SEL_COUNT; i++)
	{
		if (selectedBoosts[i]->isActive())
			boosts |= 1 << selectedBoosts[i]->getTag();
	}*/

	int boosts = 0;
	for (int i = 1; i <= 5; i++)
	{
		UIButton* btn_BoostTick = (UIButton*)m_pMainPanel->getChildByName(CCString::createWithFormat("BoostTickBtn_%d", i)->getCString());

		if (btn_BoostTick->isVisible())
			boosts |= 1 << special_boost_info[i].boostId;
	}

	return boosts;
}

void CSelBoostScene::onEnter()
{
	CBaseLayer::onEnter();

	initLocalFriends();
	initRankList(LCommon::sharedInstance()->nCurLevel);

	initUI();
	onTimer();

	if (LCommon::sharedInstance()->liveCount < MAX_LIVE && !LCommon::sharedInstance()->isPassLoseLifeTuto)
	{
		UIWidget* btnplay = m_pMainPanel->getChildByName("btnplay");
		CCPoint pos = btnplay->getWorldPosition();
		pos.x += 76 * SC_DLG_SCALE;
		pos.y += 8;

		showTutorial("You lost a life, but don't worry! You will always get more lives, they'll replenish automatically every 30 minutes.", pos, TUTO_SMALL_CHARA, true);
		LCommon::sharedInstance()->isPassLoseLifeTuto = true;
		LCommon::sharedInstance()->saveState();
	}

	setPosition(ccp(0, SC_ADHEIGHT));

	//scheduleOnce(schedule_selector(CSelBoostScene::delayTest), 5.0f);
}

void CSelBoostScene::delayTest(float dt)
{
	AppDelegate::app->didFBconnected(true);
}

void CSelBoostScene::onTimer()
{
	UIWidget* pLiveWidget = m_pActionBar->getChildByName("panLiveCount");
	if (LCommon::sharedInstance()->liveCount < MAX_LIVE)
	{
		UILabel* lblLiveCount = (UILabel*) pLiveWidget->getChildByName("lblLiveCount");
		pLiveWidget->setVisible(true);

		lblLiveCount->setText(getTimeStr(LCommon::sharedInstance()->freeLiveSecCount));
	}
	else
	{
		pLiveWidget->setVisible(false);
	}
}

//Checked
void CSelBoostScene::createBoostInfo()
{
	UIImageView* imgBg = UIImageView::create();
	imgBg->loadTexture("Image/popup/popup_bg2.png");
	imgBg->setScale(SC_DLG_SCALE);
	imgBg->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));

	UIButton* btnClose = UIButton::create();
	btnClose->loadTextures("Image/popup/btn_close_n.png", "Image/popup/btn_close_p.png", "");
	btnClose->setPosition(ccp(240, 437));
	btnClose->setName("btnClose");
	imgBg->addChild(btnClose);

	UIImageView* imgTitle = UIImageView::create();
	imgTitle->loadTexture("Image/boosts/bi_title.png");
	imgTitle->setPosition(ccp(0, 411));
	imgBg->addChild(imgTitle);

	CCLabelBMFont* lblTitle1 = CCLabelBMFont::create("Purchase boosts to improve your", "Image/fonts/Myriadpro111_brown.fnt");
	lblTitle1->setScale(0.6f);
	lblTitle1->setPosition(ccp(0, 367));
	imgBg->addCCNode(lblTitle1);

	CCLabelBMFont* lblTitle2 = CCLabelBMFont::create("abilities this level.", "Image/fonts/Myriadpro111_brown.fnt");
	lblTitle2->setScale(0.6f);
	lblTitle2->setPosition(ccp(0, 329));
	imgBg->addCCNode(lblTitle2);

	CCTableView* tableView = CCTableView::create(this, CCSizeMake(506, 680));
	tableView->setPosition(ccp(-256, -378));
	tableView->setDirection(kCCScrollViewDirectionVertical);
	tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	imgBg->addCCNode(tableView);
	tableView->setTag(TAG_TBL_BOOST_INFO);
	tableView->reloadData();

	UIImageView* imgHeader = UIImageView::create();
	imgHeader->loadTexture("Image/popup/header_split.png");
	imgHeader->setPosition(ccp(0, 274));
	imgBg->addChild(imgHeader);

	m_pBoostInfo = imgBg;
	m_pBoostInfo->retain();
}

//Checked
unsigned int CSelBoostScene::numberOfCellsInTableView(CCTableView *table)
{
	if (table->getTag() == TAG_TBL_BOOST_INFO)
		//return unlockedBoosts.size();
		return 0;
	if (table->getTag() == TAG_TBL_CHAMPIONS)
		//return dailyChalListForCurlevel->size();
		return LCommon::sharedInstance()->isFBConnected ? MIN(MAX_RANK, rankList.size()) : 1;
	if (table->getTag() == TAG_TBL_FRIEND_LIST)
		return MIN(MAX_RANK, rankList.size());
	return 0;
}

//Checked
CCSize CSelBoostScene::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
	if (table->getTag() == TAG_TBL_BOOST_INFO)
		return CCSizeMake(506, 140);
	if (table->getTag() == TAG_TBL_CHAMPIONS)
		return CCSizeMake(540, 140);
	if (table->getTag() == TAG_TBL_FRIEND_LIST)
		return CCSizeMake(120, 160);
	return CCSizeZero;
}

//Checked TAG_TBL_BOOST_INFO part
CCTableViewCell* CSelBoostScene::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	if (table->getTag() == TAG_TBL_BOOST_INFO)
	{
		CCTableViewCell *cell = table->dequeueCell();
		int nBoostId = unlockedBoosts.at(idx);

		if (!cell) {
			cell = new CCTableViewCell();
			cell->autorelease();

			CCSprite *sprite = CCSprite::create();
			sprite->setPosition(ccp(74, 74));
			sprite->setScale(0.7f);
			sprite->setTag(TAG_BST_ICON);
			cell->addChild(sprite);

			CCSprite *spTitle = CCSprite::create();
			spTitle->setPosition(ccp(144, 129));
			spTitle->setAnchorPoint(ccp(0, 1));
			spTitle->setTag(TAG_BST_TITLE);
			cell->addChild(spTitle);

			CCLabelBMFont* lblTitle1 = CCLabelBMFont::create("Purchase boosts to improve your", "Image/fonts/Myriadpro111_brown.fnt");
			lblTitle1->setScale(0.6f);
			lblTitle1->setAnchorPoint(ccp(0, 1));
			lblTitle1->setPosition(ccp(146, 94));
			lblTitle1->setTag(TAG_BST_DESC1);
			cell->addChild(lblTitle1);

			CCLabelBMFont* lblTitle2 = CCLabelBMFont::create("abilities this level.", "Image/fonts/Myriadpro111_brown.fnt");
			lblTitle2->setScale(0.6f);
			lblTitle2->setAnchorPoint(ccp(0, 1));
			lblTitle2->setPosition(ccp(146, 61));
			lblTitle2->setTag(TAG_BST_DESC2);
			cell->addChild(lblTitle2);

			CCSprite *spSplit= CCSprite::create("Image/boosts/bi_split.png");
			spSplit->setPosition(ccp(256, 9));
			cell->addChild(spSplit);
		}

		CCRect rc = CCRectZero;

		CCTexture2D* txIcon = CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/boosts/boost_%d.png", nBoostId)->getCString());
		CCSprite* sprt1 = (CCSprite*) cell->getChildByTag(TAG_BST_ICON);
		sprt1->setTexture(txIcon);
		rc.size = txIcon->getContentSize();
		sprt1->setTextureRect(rc);

		CCTexture2D* txTitle = CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/boosts/boost_desc_%d.png", nBoostId)->getCString());
		CCSprite* sprt2 = (CCSprite*) cell->getChildByTag(TAG_BST_TITLE);
		sprt2->setTexture(txTitle);
		rc.size = txTitle->getContentSize();
		sprt2->setTextureRect(rc);

		CCLabelBMFont* lblDesc1 = (CCLabelBMFont*) cell->getChildByTag(TAG_BST_DESC1);
		lblDesc1->setString(BOOST_DESC[nBoostId * 2].c_str());
		CCLabelBMFont* lblDesc2 = (CCLabelBMFont*) cell->getChildByTag(TAG_BST_DESC2);
		lblDesc2->setString(BOOST_DESC[nBoostId * 2 + 1].c_str());

		return cell;
	} else if (table->getTag() == TAG_TBL_CHAMPIONS)
	{
		CCTableViewCell *cell = table->dequeueCell();

		if (!cell) {
			cell = new CCTableViewCell();
			cell->autorelease();

			CCSprite* spBg = CCSprite::create("Image/popup/challenge_cell.png");
			spBg->setPosition(ccp(270, 60));
			cell->addChild(spBg);

			float cx = 80;
			float cy = 60;

			CCSprite* spBack = CCSprite::create("Image/friends/friend_br_back.png");
			spBack->setPosition(ccp(cx, cy));
			cell->addChild(spBack);

			CCTexture2D* texture = CCTextureCache::sharedTextureCache()->addImage("Image/friends/def_profile.png");
			CCSprite* spfriend = CCSprite::createWithTexture(texture);
			spfriend->setTag(TAG_WRLD_PICTURE);

			//CCPoint posCenter = ccp(spBack->getContentSize().width / 2, spBack->getContentSize().height / 2);
			CCSprite* stencil = CCSprite::create("Image/level/r128.png");
			CCClippingNode* clipNode = CCClippingNode::create(stencil);
			clipNode->setAlphaThreshold(0.05f);
			clipNode->setAnchorPoint(ccp(0.5f, 0.5f));
			clipNode->setScale(0.7f);
			clipNode->setTag(200);
			clipNode->addChild(spfriend);
			//clipNode->setPosition(posCenter);
			clipNode->setPosition(ccp(cx, cy));
			clipNode->setTag(TAG_WRLD_PICTURE_CLIP);

			//spBack->addChild(clipNode);
			cell->addChild(clipNode);

			CCSprite* spFrame = CCSprite::create("Image/friends/friend_frame.png");
			//spFrame->setPosition(posCenter);
			spFrame->setPosition(ccp(cx, cy));
			//spBack->addChild(spFrame);
			cell->addChild(spFrame);


			CCLabelTTF* lblName = CCLabelTTF::create("You", "Arial", 40, CCSizeMake(210, 50), kCCTextAlignmentLeft);
			lblName->enableStroke(ccc3(157, 57, 1), 3);
			lblName->enableShadow(CCSizeMake(3, -3), 0.2, 0.1);
			lblName->setPosition(ccp(cx + 170, cy + 5));
			lblName->setTag(TAG_WRLD_NAME);
			cell->addChild(lblName);

			CCLabelTTF* lblScore = CCLabelTTF::create("1,332,121", "Arial", 30, CCSizeMake(210, 50), kCCTextAlignmentRight);
			lblScore->enableStroke(ccc3(157, 57, 1), 2);
			lblScore->setPosition(ccp(cx + 240, cy - 5));
			lblScore->setTag(TAG_WRLD_SCORE);
			cell->addChild(lblScore);

			CCSprite* spAnimal = CCSprite::create("Image/boosts/Piggles.png");
			spAnimal->setPosition(ccp(cx + 400, cy + 5));
			cell->addChild(spAnimal);

		}

		TFriendInfo* info = NULL;

		if(LCommon::sharedInstance()->isFBConnected)
		{
			info = rankList.at(idx);
		}else {
			for (TFriendPList::iterator it = rankList.begin(); it != rankList.end(); it++)
			{
				if((*it)->fname == "You")
				{
					info = *it;
					break;
				}
			}
		}

		if(info != NULL)
		{
			CCClippingNode* clipNode = (CCClippingNode*) cell->getChildByTag(TAG_WRLD_PICTURE_CLIP);

			CCSprite *sprt = (CCSprite*) clipNode->getChildByTag(TAG_WRLD_PICTURE);

			CCTexture2D* tex = CCTextureCache::sharedTextureCache()->textureForKey(info->fbid.c_str());
			if (tex == NULL)
			{
				tex = CCTextureCache::sharedTextureCache()->addImage("Image/friends/def_profile.png");
			}
			sprt->setTexture(tex);
			sprt->setTextureRect(CCRectMake(0, 0, tex->getContentSize().width, tex->getContentSize().height));


			CCLabelTTF* label = (CCLabelTTF*) cell->getChildByTag(TAG_WRLD_NAME);
			label->setString(info->fname.c_str());
			//label->setColor(col);

			label = (CCLabelTTF*) cell->getChildByTag(TAG_WRLD_SCORE);
			label->setString(getDotDigit(info->nCurScore).c_str());
			//label->setColor(col);
		}
		
		return cell;

	}/* else if (table->getTag() == TAG_TBL_DAILY_CHAL)
	{
		CCTableViewCell *cell = table->dequeueCell();

		if (!cell) {
			cell = new CCTableViewCell();
			cell->autorelease();

			CCSprite* spBg = CCSprite::create("Image/boosts/challenge_item_bg.png");
			spBg->setPosition(ccp(250, 60));
			cell->addChild(spBg);

			CCLabelTTF* desc = CCLabelTTF::create("desc", "Arial", 30, CCSizeMake(330, 75), kCCTextAlignmentLeft);
			desc->setTag(TAG_CHL_DESC);
			desc->setPosition(ccp(300, 65));
			cell->addChild(desc);

			float cx = 8;
			float cy = -5;
			CCLabelBMFont* lblLv = CCLabelBMFont::create("Lvl", "Image/fonts/Myriadpro111_brown.fnt");
			lblLv->setPosition(ccp(cx + 64, cy + 100));
			lblLv->setScale(0.6f);
			cell->addChild(lblLv);

			CCLabelBMFont* lblLvNum = CCLabelBMFont::create("99", "Image/fonts/Myriadpro111_brown.fnt");
			lblLvNum->setPosition(ccp(cx + 67, cy + 55));
			lblLvNum->setScale(1.0f);
			lblLvNum->setTag(TAG_CHL_LVL_NUM);
			cell->addChild(lblLvNum);

// 			CCSprite* spRewardType = CCSprite::create();
// 			spRewardType->setPosition(ccp(70, 85));
// 			spRewardType->setScale(0.8f);
// 			spRewardType->setTag(TAG_CHL_REWARD_TYPE);
// 			cell->addChild(spRewardType);
// 
// 			CCLabelBMFont* rewardCount = CCLabelBMFont::create("", "Image/fonts/Myriadpro111_brown.fnt");
// 			rewardCount->setPosition(ccp(72, 38));
// 			rewardCount->setScale(0.5f);
// 			rewardCount->setTag(TAG_CHL_REWARD_VAL);
// 			cell->addChild(rewardCount);

			CCSprite* spDone = CCSprite::create("Image/boosts/mark_done.png");
			spDone->setTag(TAG_CHL_DONE);
			spDone->setPosition(ccp(397, 20));
			cell->addChild(spDone);
		}

		TDailyChallengeInfo& info = dailyChalListForCurlevel->at(idx);

		CCLabelTTF* desc = (CCLabelTTF*) cell->getChildByTag(TAG_CHL_DESC);
		int nShiftColor = LCommon::sharedInstance()->shiftColor[info.pGoalInfo->level];
		std::string strNewDesc = getChallengeDescShift(info.pGoalInfo->desc, nShiftColor);

		desc->setString(strNewDesc.c_str());

// 		CCSprite* spRewardType = (CCSprite*) cell->getChildByTag(TAG_CHL_REWARD_TYPE);
// 		const char* imgName = "";
// 		if (info.rewardType == IT_LIVE)
// 			imgName = "Image/common/item_live.png";
// 		else if (info.rewardType == IT_MAGIC)
// 			imgName = "Image/common/item_magic.png";
// 		CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addImage(imgName);
// 		spRewardType->setTexture(tex);
// 		spRewardType->setTextureRect(CCRectMake(0, 0, tex->getContentSize().width, tex->getContentSize().height));
// 
// 		CCLabelBMFont* rewardCount = (CCLabelBMFont*) cell->getChildByTag(TAG_CHL_REWARD_VAL);
// 		rewardCount->setString(CCString::createWithFormat("+%d", info.rewardValue)->getCString());

		CCLabelBMFont* lvlNum = (CCLabelBMFont*) cell->getChildByTag(TAG_CHL_LVL_NUM);
		lvlNum->setString(toString(info.pGoalInfo->level + 1).c_str());

		CCSprite* spDone = (CCSprite*) cell->getChildByTag(TAG_CHL_DONE);
		spDone->setVisible(info.pGoalInfo->isDone);

		return cell;
	}*/else if (table->getTag() == TAG_TBL_FRIEND_LIST)
	{
		bool isLocalFriend = true;
		CCTableViewCell *cell = table->dequeueCell();
		if (!cell) {
			cell = new CCTableViewCell();
			cell->autorelease();

			float cx = 70;
			float cy = 130;
			CCSprite *spBg = CCSprite::create();
			spBg->setTag(TAG_WRLD_PIC_BG);
			spBg->setPosition(ccp(cx, cy));
			cell->addChild(spBg);

			CCSprite* stencil = CCSprite::create("Image/level/r128.png");
			CCClippingNode* clipNode = CCClippingNode::create(stencil);
			clipNode->setAlphaThreshold(0.05f);
			clipNode->setAnchorPoint(ccp(0.5f, 0.5f));
			clipNode->setScale(0.78f);
			clipNode->setTag(TAG_WRLD_PICTURE_CLIP);
			clipNode->setPosition(ccp(cx, cy));

			CCSprite* spChara = CCSprite::create();
			spChara->setPosition(ccp(0, 14));
			spChara->setTag(TAG_WRLD_PICTURE);
			clipNode->addChild(spChara);

			cell->addChild(clipNode);

			CCLabelTTF *label = CCLabelTTF::create("Name", "Helvetica", 30);
			label->setPosition(ccp(cx, cy - 80));
			label->setTag(TAG_WRLD_NAME);
			cell->addChild(label);

			label = CCLabelTTF::create("", "Helvetica", 30);
			label->setPosition(ccp(cx, cy - 110));
			label->setTag(TAG_WRLD_SCORE);
			cell->addChild(label);

			CCSprite* spNo = CCSprite::create();
			spNo->setPosition(ccp(cx, cy - 45));
			spNo->setTag(TAG_WRLD_FRNOBG);
			cell->addChild(spNo);

			CCLabelBMFont* lblNo = CCLabelBMFont::create("5", "Image/fonts/Hobostd110.fnt");
			lblNo->setPosition(ccp(cx + 2, cy - 43));
			lblNo->setTag(TAG_WRLD_FRNO);
			lblNo->setScale(0.5f);
			cell->addChild(lblNo);
		}

		ccColor3B col = (idx == 0)? ccc3(255, 132, 0) : ccc3(22, 156, 196);

		CCSprite *sprt = (CCSprite*) cell->getChildByTag(TAG_WRLD_PIC_BG);
		CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addImage((idx == 0)? "Image/friends/friend_bg_0.png": "Image/friends/friend_bg_1.png");
		sprt->setTexture(tex);
		sprt->setTextureRect(CCRectMake(0, 0, tex->getContentSize().width, tex->getContentSize().height));

		TFriendInfo* info = rankList.at(idx);

		CCClippingNode* clipNode = (CCClippingNode*) cell->getChildByTag(TAG_WRLD_PICTURE_CLIP);

		sprt = (CCSprite*) clipNode->getChildByTag(TAG_WRLD_PICTURE);

		tex = CCTextureCache::sharedTextureCache()->textureForKey(info->fbid.c_str());
		if (tex == NULL)
		{
			tex = CCTextureCache::sharedTextureCache()->addImage("Image/friends/def_profile.png");
		}

		sprt->setTexture(tex);
		sprt->setTextureRect(CCRectMake(0, 0, tex->getContentSize().width, tex->getContentSize().height));

		sprt = (CCSprite*) cell->getChildByTag(TAG_WRLD_FRNOBG);
		tex = CCTextureCache::sharedTextureCache()->addImage((idx == 0)? "Image/friends/friend_num_0.png": "Image/friends/friend_num_1.png");
		sprt->setTexture(tex);
		sprt->setTextureRect(CCRectMake(0, 0, tex->getContentSize().width, tex->getContentSize().height));

		CCLabelBMFont* lblNo = (CCLabelBMFont*) cell->getChildByTag(TAG_WRLD_FRNO);
		if (idx == 0)
			lblNo->setString("");
		else
			lblNo->setString(toString(idx + 1).c_str());

		CCLabelTTF* label = (CCLabelTTF*) cell->getChildByTag(TAG_WRLD_NAME);
		label->setString(info->fname.c_str());
		label->setColor(col);

		label = (CCLabelTTF*) cell->getChildByTag(TAG_WRLD_SCORE);
		label->setString(getDotDigit(info->nCurScore).c_str());
		label->setColor(col);

		cell->setScale(0.8f);

		return cell;
	}

	return NULL;
}

void CSelBoostScene::onGoalCurtain(cocos2d::CCObject* obj)
{
	if (isCurtainMoving) return;
	isCurtainMoving = true;

	int nMoveY = isCurtainVisible? -226 : 226;
	CCMoveBy* mov1 = CCMoveBy::create(0.3f, ccp(0, nMoveY));
	CCCallFunc* func = CCCallFunc::create(this, callfunc_selector(CSelBoostScene::onGoalCurtainMoved));
	CCSequence* seq = CCSequence::create(mov1, func, NULL);

	UIWidget* curtain = m_pMainLayer->getWidgetByName("goalCurtain");
	curtain->runAction(seq);
}

void CSelBoostScene::onGoalCurtainMoved()
{
	isCurtainMoving = false;
	isCurtainVisible = !isCurtainVisible;
}

void CSelBoostScene::createGoalCurtain()
{
	int nCurLevel = LCommon::sharedInstance()->curLevel();
	int nSize = LCommon::getOrgTreasureCount();

	UIImageView* imgView = UIImageView::create();
	imgView->loadTexture("Image/boosts/curtain.png");
	imgView->setName("goalCurtain");
	imgView->setPosition(ccp(SC_WIDTH_HALF, -70));
	imgView->setTouchEnable(true);
	imgView->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onGoalCurtain));
	m_pMainLayer->addWidget(imgView);

	int showPos = 0;
	for (int i = 0; i < 2; i++) {
		TGoalInfo* goalInfo = NULL;

		for (int j = showPos; j < nSize; j++)
		{
			if (g_GoalList[j].level < nCurLevel || g_GoalList[j].isDone) continue;
			showPos = j + 1;
			goalInfo = &g_GoalList[j];
			break;
		}

		if (goalInfo == NULL) break;

		CCSprite* goalBack = CCSprite::create("Image/boosts/goal_ct_bg.png");
		goalBack->setPosition(ccp(30, 20 - 110 * i));
		goalBack->setAnchorPoint(ccp(0.5f, 0.5f));

		float cx = 290;
		float cy = 45;

		CCSprite* spLevel = CCSprite::create("Image/common/level_bg.png");
		spLevel->setPosition(ccp(cx - 290, cy - 6));
		spLevel->setScale(0.9f);
		goalBack->addChild(spLevel);

		CCLabelBMFont* lblLv = CCLabelBMFont::create("Lvl", "Image/fonts/Myriadpro111_brown.fnt");
		lblLv->setPosition(ccp(64, 100));
		lblLv->setScale(0.6f);
		spLevel->addChild(lblLv);

		CCLabelBMFont* lblLvNum = CCLabelBMFont::create(toString(goalInfo->level + 1).c_str(), "Image/fonts/Myriadpro111_brown.fnt");
		lblLvNum->setPosition(ccp(64, 55));
		lblLvNum->setScale(1.0f);
		spLevel->addChild(lblLvNum);

		int nShiftColor = LCommon::sharedInstance()->shiftColor[goalInfo->level];
		std::string strNewDesc = getChallengeDescShift(goalInfo->desc, nShiftColor);
		CCLabelTTF* lblDesc = CCLabelTTF::create(strNewDesc.c_str(), "Arial", 27, CCSizeMake(350, 70), kCCTextAlignmentLeft, kCCVerticalTextAlignmentCenter);
		lblDesc->setPosition(ccp(cx - 50, cy));
		lblDesc->setFontFillColor(ccWHITE);
		goalBack->addChild(lblDesc);

		CCSprite* item = CCSprite::create(ITEM_IMAGES[IT_MAPPIECE]);
		item->setScale(1.0f);
		item->setPosition(ccp(cx + 200, cy));
		goalBack->addChild(item);

		CCLabelTTF* lblCount = CCLabelTTF::create("x1", "Arial", 30);
		lblCount->setPosition(ccp(cx + 260, cy));
		lblCount->setFontFillColor(ccWHITE);
		goalBack->addChild(lblCount);

		imgView->addCCNode(goalBack);
	}

	isCurtainVisible = false;
	isCurtainMoving = false;
}

void CSelBoostScene::createFriendTableView()
{
	UIImageView* imgView = UIImageView::create();
	imgView->loadTexture("Image/boosts/bottom_sidebar.png");
	imgView->setAnchorPoint(ccp(0.5f, 0.0f));
	imgView->setPosition(ccp(SC_WIDTH_HALF, 0));
	m_pMainLayer->addWidget(imgView);

	fbLoginPanel = UIPanel::create();
	fbLoginPanel->setTouchEnabled(true);
	fbLoginPanel->setAnchorPoint(ccp(0.5f, 0.0f));
	fbLoginPanel->setSize(CCSizeMake(imgView->getSize().width, imgView->getSize().height));
	imgView->addChild(fbLoginPanel);

	int animalIndex[6] = {0, 2, 3, 4, 1, 5};
	CCSprite* animal = CCSprite::create(CCString::createWithFormat("Image/friends/friends_%d_0.png", animalIndex[LCommon::sharedInstance()->curChara])->getCString());
	animal->setAnchorPoint(ccp(0.0f, 1.0f));
	animal->setPosition(ccp(5, fbLoginPanel->getSize().height));
	animal->setScale(0.8f);
	fbLoginPanel->addCCNode(animal);

	CCSprite* lblFunWithFriends = CCSprite::create("Image/common/lbl_fun_with_friends.png");
	lblFunWithFriends->setAnchorPoint(ccp(1.0f, 1.0f));
	lblFunWithFriends->setPosition(ccp(fbLoginPanel->getSize().width - 20, fbLoginPanel->getSize().height - 20));
	fbLoginPanel->addCCNode(lblFunWithFriends);

	btnFBLogin = UIButton::create();
	btnFBLogin->setAnchorPoint(ccp(0.5f, 0.0f));
	btnFBLogin->setTouchEnabled(true);
	btnFBLogin->loadTextures("Image/common/fb_login_n.png", "Image/common/fb_login_p.png", "");
	btnFBLogin->setPosition(ccp(fbLoginPanel->getSize().width / 2, 10));
	btnFBLogin->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onFBConnect));
	fbLoginPanel->addChild(btnFBLogin);


	friendTableView = CCTableView::create(this, CCSizeMake(360, 160));
	friendTableView->setDirection(kCCScrollViewDirectionHorizontal);
	friendTableView->setPosition(ccp(-180, 0));
	friendTableView->setTag(TAG_TBL_FRIEND_LIST);
	imgView->addCCNode(friendTableView);
	friendTableView->setVisible(false);

	friendTableView->reloadData();


	bool isConnected = LCommon::sharedInstance()->isFBConnected;
	fbLoginPanel->setVisible(!isConnected);
	btnFBLogin->setTouchEnable(!isConnected);
	friendTableView->setVisible(isConnected);
}

void CSelBoostScene::onFBConnect(cocos2d::CCObject* sender)
{
	FBConnect();
}

void CSelBoostScene::initRankList(int nLevel)
{
	rankList.clear();

	int mixList[MAX_LOCAL_FRIENDS];

	for (int i = 0; i < MAX_LOCAL_FRIENDS; i++)
		mixList[i] = i;

	for (int i = 0; i < MAX_LOCAL_FRIENDS; i++)
	{
		int r1 = getRandRange(1, MAX_LOCAL_FRIENDS - 1);
		int dst = (i + r1) % MAX_LOCAL_FRIENDS;
		int tmp = mixList[i];
		mixList[i] = mixList[dst];
		mixList[dst] = tmp;
	}

	int nMaxScore = 55000;
	for (int i = 0; i < MAX_LOCAL_FRIENDS; i++)
	{
		TFriendInfo& info = localFriendList.at(mixList[i]);
		if (nLevel <= info.nMaxLevel)
		{
			info.nCurScore = nMaxScore;
			rankList.push_back(&info);
			nMaxScore -= 1000;
		}
	}

	int ixx = 0;
	for (TFriendMap::iterator it = LCommon::sharedInstance()->friendList.begin(); it != LCommon::sharedInstance()->friendList.end(); it++)
	{
		TFriendInfo* frndInfo = it->second;

		if (frndInfo->nMaxLevel < nLevel) continue;
		if (LCommon::sharedInstance()->friendList.size() > 1 && frndInfo->fbid == MYFBID_DEFAULT) continue;
		if (frndInfo->fbid == LCommon::sharedInstance()->myFBID)
			frndInfo->nCurScore = LCommon::sharedInstance()->highScore[nLevel];
		else
			frndInfo->nCurScore = 55000 - ixx * 300;

		rankList.push_back(frndInfo);
	}

	std::sort(rankList.begin(), rankList.end(), compScore);
}

void CSelBoostScene::initLocalFriends()
{
	int curChar = LCommon::sharedInstance()->curChara;
	for (int i = 0; i < MAX_LOCAL_FRIENDS; i++)
	{
		TFriendInfo info;
		info.fbid = CCString::createWithFormat("Image/friends/friends_%d_%d.png", 0, i)->getCString();
		info.texture = CCTextureCache::sharedTextureCache()->addImage(info.fbid.c_str());
		info.texture->retain();
		info.fname = LOCAL_FRIEND[i];
		info.nMaxLevel = LCommon::sharedInstance()->localFriendMaxLevels[i][curChar];
		info.bLocalFriend = true;
		localFriendList.push_back(TFriendInfo(info));
	}

	TFriendMap::iterator it = LCommon::sharedInstance()->friendList.find(LCommon::sharedInstance()->myFBID);
	if (it != LCommon::sharedInstance()->friendList.end())
	{
		it->second->nMaxLevel = LCommon::sharedInstance()->maxLevel();
	}
}

bool CSelBoostScene::onProcEvent(int nEventId, int nParam, std::string strParam)
{
	switch (nEventId) {
	case BTN_ID_BUY_POWERUP_1:
	case BTN_ID_BUY_POWERUP_3:
		scheduleOnce(schedule_selector(CSelBoostScene::refreshBoostInfo), 0.01f);
		break;
	case EVT_UPDATE_FBCONNECT_STATE:
		scheduleOnce(schedule_selector(CSelBoostScene::refreshChampions), 0.01f);
		break;
	}

	return CBaseLayer::onProcEvent(nEventId, nParam, strParam);
}
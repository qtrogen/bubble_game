//
//  LevelSelect.h
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//
#ifndef __LevelSelect_H__
#define __LevelSelect_H__

#include "cocos2d.h"
#include <vector>
#include "cocos-ext.h"
#include "BaseLayer.h"

USING_NS_CC_EXT;

//#define LEVEL_PLACEMENT_TEST
//#define LEVEL_PLACEMENT_EDIT

class CLevelSelect : public CBaseLayer, public cocos2d::extension::CCTableViewDataSource {
public:
	CLevelSelect();
	~CLevelSelect();

    CREATE_SCENE_METHOD(CLevelSelect);

protected:
    void refreshFriendInfoDelay(float dt);
	void refreshGiftingPopup(float dt);
    void receivedLiveReqList(float dt);
    void receivedCollectLiveList(float dt);
    
    void showReqPopup(bool bLiveReq);
    void onSelectedGive(cocos2d::CCObject* obj);
    void onSelectedCollect(cocos2d::CCObject* obj);
	void giveLive(std::string reqId);
    void collectLive(std::string reqId);

	void onSelectAnimalPowerup(cocos2d::CCObject* obj);

	void onMoreApp(cocos2d::CCObject* obj);
	void refreshWorldState();
	void initUI();
	void createRightFriendInfo();
    
	void initPowerups();

    virtual void onEnterTransitionDidFinish();

    void sendFriendsRequest(float dt);

	void onClicked(cocos2d::CCObject* obj);
	void registerClick(UIWidget* btn, int nTag);

	void addCloudEffect(float startY, float endY);

	virtual void onEnter();
	virtual void onTimer();
    virtual bool onProcEvent(int nEventId, int nParam, std::string strParam);
    
	void changedBtnState(UIButton* levelBtn, bool bPressed);

	virtual CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	virtual CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);
    
    void initLocalFriends();
    void initRankList(int nLevel);
    void updateRankList(int nLevel);
	
	void delayTest(float dt);
	void delayTest1(float dt);

protected:
	UIWidget* m_pWidget;
    UILayer*    m_friendLayer;
	int			m_nSelectedLevel;
	bool		m_isOverview;
    
    CCTableView* tableView;
    
	UIScrollView* lifeGiveScrollView;
	UIScrollView* lifeCollectScrollView;

    TFriendList localFriendList;
    
	char frCountPerLevel[MAX_LEVEL];
	char frLastIndexPerLevel[MAX_LEVEL];

    typedef std::vector<TFriendInfo*> TFriendPList;
    TFriendPList rankList;

#ifdef LEVEL_PLACEMENT_TEST
	//tsts
	int m_nCurSel;
	bool isX;
	UIButton*	pButtons[MAX_LEVEL * 4];
	void onChClicked(cocos2d::CCObject* obj);
	void onChTouched(cocos2d::CCObject* obj);
	void onChCanceled(cocos2d::CCObject* obj);
	void onBtClicked(cocos2d::CCObject* obj);
#endif //LEVEL_PLACEMENT_TEST
};

#endif //__LevelSelect_H__

//
//  CMainMenuLayer.h
//  IcecreamCandyBlast
//
//  Created by Chen on 7/11/13.
//  Copyright 2013 lion. All rights reserved.
//
#ifndef __CMainMenuLayer_H__
#define __CMainMenuLayer_H__

#include "cocos2d.h"
#include "LPurchase/LStoreManager.h"
#include "cocos-ext.h"
#include "BaseLayer.h"

USING_NS_CC_EXT;

class CMainMenuLayer : public CBaseLayer {
public:
    CREATE_SCENE_METHOD(CMainMenuLayer);
    
	CMainMenuLayer();
	~CMainMenuLayer();

	virtual void restoreSuccessed(T_PRODUCT_LIST& productIdList);
	virtual void restoreFailed(std::string& errMsg);
    virtual bool onProcEvent(int nEventId, int nParam, std::string strParam);

	virtual void onEnter();
	virtual void onTimer();

	void onPlay(cocos2d::CCObject* sender);
	void onMore(cocos2d::CCObject* sender);
	void onFBConnect(cocos2d::CCObject* sender);
	void onRestorePurchase(cocos2d::CCObject* sender);
	void onGameCenter(cocos2d::CCObject* sender);
    
protected:
    void refreshFBConnect();
    UIButton* btnFBConnect;
};

#endif //__CMainMenuLayer_H__
#include "Bubble.h"
#include "LCommon.h"

float CBubble::SIZE = 62;
float CBubble::SIZE_OV = SIZE * 0.86f;

std::map<std::string, cocos2d::CCAnimation*> CBubble::gAnimateCache;

USING_NS_CC;

#define BRIGHT_TAG 333

//Checked
CBubble::CBubble(char byType, char byColor)
	: m_byType(byType)
{
	static int lastUID = 1;
	if (++lastUID > 0x0FFFFF)
		lastUID = 1;
	m_nUID = lastUID;

	if (byType == BB_NORMAL)
	{
		m_byColor = byColor & MASK_COLOR;
		m_bSwap = (byColor & MASK_BUBBLE_SWAP) != 0;
		m_bWaste = (byColor & MASK_BUBBLE_WASTE) != 0;
		m_bNoHitCandy = (byColor & MASK_BUBBLE_NOHIT_CANDY) != 0;
	}
	else
	{
		m_byColor = byColor;
		m_bSwap = false;
		m_bWaste = false;
		m_bNoHitCandy = false;
	}

	m_bIsBeeGroup = false;

	m_bGameOverBubble = false;
	m_bTouchedBottomWall = false;

	unsigned char byCol = (m_byType == BB_CANDY)? 1 : m_byColor;
	if (byType == BB_FRIEND)
		byType = BB_NORMAL;
	bool bSuc = initWithFile(CCString::createWithFormat("Image/bubbles/bubble_%d_%d.png", byType, byCol)->getCString());
	setScale(BBL_SCALE);
	CC_ASSERT(bSuc);
	initBubble();
}

CBubble::~CBubble(void)
{
}

//Checked
void CBubble::backupTo(stBubbleBack& back)
{
	back.bEnable = true;
	back.pos = getPosition();
	back.byType = m_byType;
	back.byColor = m_byColor;
}

//Checked
void CBubble::initBubble()
{
	for (int i = 0; i < NEIGHBOR_COUNT; i++)
		m_pNeighbors[i] = NULL;

	m_pMorMaster = NULL;

	for (int i = 0; i < NEIGHBOR_COUNT; i++)
		m_pMorSlave[i] = NULL;

	m_bHasMor = false;

	if (m_byType == BB_FRIEND)
	{
		CCSprite* sp = CCSprite::create();
		sp->setPosition(ccp(64, 64));
		addChild(sp);
        
        if (IS_LOWMEMORY)
        {
            CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/bubbles/friend_w%d.png", LCommon::sharedInstance()->curWorld + 1)->getCString());
            sp->setTexture(tex);
            sp->setTextureRect(CCRectMake(0, 0, tex->getContentSize().width, tex->getContentSize().height));
        }
        else
        {
            CCAnimation* anime = createAnimate(CCString::createWithFormat("Image/animation/bubbles/friend_%d", LCommon::sharedInstance()->curWorld + 1)->getCString(), 40);
            CCAnimate* animate = CCAnimate::create(anime);
            //		CCDelayTime* delay2 = CCDelayTime::create(0.2f);
            sp->runAction(CCRepeatForever::create(animate));
        }
	}

//	runBright();
// 	CCLabelTTF* label = CCLabelTTF::create(stringWithFormat("%d", m_nUID).c_str(), "Arial", 50);
// 	label->setColor(ccBLACK);
// 	label->setPosition(ccp(getContentSize().width / 2, getContentSize().height / 2));
// 	addChild(label);
}

void CBubble::prepareDrop()
{
	if (m_byType == BB_FRIEND)
		removeAllChildren();
}

void CBubble::changeTo(char byType, char byColor)
{
	m_byType = byType;
	m_byColor = byColor;

	if (byType == BB_FRIEND)
		byType = BB_NORMAL;

	if(byType == BB_FIREBALL) {
		CCAnimation* anime = CBubble::createAnimate("Image/animation/fireball/normal", 12, 0.07);
		CCAnimate* animation = CCAnimate::create(anime);
		CCRepeatForever* action = CCRepeatForever::create(animation);
		runAction(action);		
	}else{
		CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/bubbles/bubble_%d_%d.png", byType, byColor)->getCString());
		setTexture(tex);
	}

	//stopAllActions();

//	runBright();
}

void CBubble::animateExplode(float fDelayTime)
{
	if (m_byType == BB_NORMAL)
	{
		CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("Image/bubbles/explode_%d", m_byType)->getCString());
		setTexture(tex);

		CCFadeIn* fade = CCFadeIn::create(0.5f);
		CCCallFunc* func = CCCallFunc::create(this, callfunc_selector(CBubble::removeFromParent));
		CCSequence* seq = CCSequence::createWithTwoActions(fade, func);
		runAction(seq);
	}
	else
	{
		removeFromParent();
	}
}

void CBubble::addNeighbor(CBubble* bubble)
{
	for (int i = 0; i < NEIGHBOR_COUNT; i++)
		if (m_pNeighbors[i] == NULL)
		{
			m_pNeighbors[i] = bubble;
			break;
		}
}

bool CBubble::isNeighbor(CBubble* bubble)
{
	if (bubble == NULL) return false;
	for (int i = 0; i < NEIGHBOR_COUNT; i++)
		if (m_pNeighbors[i] == bubble)
			return true;
	return false;
}

bool CBubble::isMorBubbleSeparated()
{
	for (int i = 0; i < NEIGHBOR_COUNT; i++)
		for (int j = 0; j < NEIGHBOR_COUNT; j++)
			if(m_pMorSlave[i]->m_pNeighbors[j])
				return false;

	return true;
}

void CBubble::removeNeighbor(CBubble* bubble)
{
	for (int i = 0; i < NEIGHBOR_COUNT; i++)
		if (m_pNeighbors[i] == bubble)
		{
			m_pNeighbors[i] = NULL;
			break;
		}
}

//Checked
void CBubble::runBright()
{
	if (m_byType == BB_ICE || m_byType == BB_STONE || m_byType == BB_NUKE || m_byType == BB_RAINBOW) return;

	int nCnt = (m_byType == BB_CANDY)? 110 : 40;
	unsigned char byColor = (m_byType == BB_CANDY)? 1 : m_byColor;
	const char* animFolder;
	if (m_byType == BB_NORMAL || m_byType == BB_FRIEND)
	{
		animFolder = "Image/animation/explode/normal";
		nCnt = 20;
	}
	else if (IS_LOWMEMORY) return;
	else
		animFolder = CCString::createWithFormat("Image/animation/bubbles/%d_%d", m_byType, byColor)->getCString();
	CCAnimation* animation = createAnimate(animFolder, nCnt);

	float delayTime = 0.0f;
	if (m_byType == BB_NORMAL)
	{
		delayTime = 3.0f + 0.5f * m_byColor;
	} else if (m_byType == BB_STONE)
	{
		delayTime = 10.0f;
	}

	CCDelayTime* delay2 = CCDelayTime::create(delayTime);
	CCAnimate* anime = CCAnimate::create(animation);
	CCSequence* seq = CCSequence::create(anime, delay2, NULL);
	if (m_byType == BB_NORMAL || m_byType == BB_FRIEND)
	{
		CCSprite* sp = (CCSprite*) getChildByTag(BRIGHT_TAG);
		if (sp)
			sp->removeFromParent();
		sp = CCSprite::create();
		sp->setZOrder(10);
		sp->setPosition(ccp(64, 64));
		sp->setTag(BRIGHT_TAG);
		addChild(sp);
		sp->runAction(CCRepeatForever::create(seq));
	}
	else
	{
		runAction(CCRepeatForever::create(seq));
	}
}

void CBubble::preloadAnimates()
{
	preloadAnimate("Image/animation/explode/bomb_center", 21, 0.03f);
	preloadAnimate("Image/animation/explode/bomb_side", 23, 0.03f);
	preloadAnimate("Image/animation/explode/zombie", 20);
	preloadAnimate("Image/animation/explode/normal", 20);
	preloadAnimate("Image/animation/explode/rainbow_exp", 35);
	preloadAnimate("Image/animation/explode/diffusion", 28);
	preloadAnimate("Image/animation/fireball/normal", 12, 0.07);
	preloadAnimate("Image/animation/fireball/burn", 28);

	for(int i = COL_PURPLE; i <= COL_RED; i++)
		CBubble::createAnimate(CCString::createWithFormat("Image/animation/bubbles/%d_%d", BB_NORMAL, i)->getCString(), 9);

	/*for (int i = 0; i < WORLD_COUNT; i++)
		preloadAnimate(CCString::createWithFormat("Image/animation/bubbles/friend_%d", i + 1)->getCString(), 40);*/
}

void CBubble::preloadAnimate(const char* folder, int nAnimCount, float fPeriod)
{
	CCAnimation* anime = createAnimate(folder, nAnimCount, fPeriod);
//	anime->release();
}

//Checked
CCAnimation* CBubble::createAnimate(const char* folder, int nAnimCount, float fPeriod/* = ANIM_PERIOD*/)
{
	std::string keyStr = folder;
	const char* animFormat = nAnimCount < 100? "%s/anim_%02d.png" : "%s/anim_%d.png";
	CCAnimation* animation;
	CCRect rcTexture = CCRectZero;
	if (gAnimateCache.end() != gAnimateCache.find(keyStr))
	{
		animation = gAnimateCache.at(keyStr);
	}
	else
	{
		animation = CCAnimation::create();
		for (int i = 0; i < nAnimCount; i++)
		{
			const char* str = CCString::createWithFormat(animFormat, folder, i)->getCString();
			CCTexture2D *playerRunTexture = CCTextureCache::sharedTextureCache()->addImage(str);   
			rcTexture.size = playerRunTexture->getContentSize();
			animation->addSpriteFrame(CCSpriteFrame::createWithTexture(playerRunTexture, rcTexture));    
		}

		animation->setDelayPerUnit(fPeriod);

		gAnimateCache.insert(std::make_pair(folder, animation));
	}

	animation->retain();

	return animation;
}

//
//  GrowButton.h
//  Game
//
//  Created by hrh on 9/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#ifndef __GrowButton_H__
#define __GrowButton_H__

#include "cocos2d.h"

class GrowButton : public cocos2d::CCMenu 
{    
public:

	static GrowButton* buttonWithSprite(const char* normalImage, const char* selectImage, 
					  cocos2d::CCObject* target, cocos2d::SEL_MenuHandler sel, int tag);

	static GrowButton* buttonWithSpriteFrame(const char* frameName, const char* selectframeName, cocos2d::CCObject* target, cocos2d::SEL_MenuHandler sel);
	static GrowButton* create(cocos2d::CCMenuItem* item);

	virtual bool ccTouchBegan(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
	virtual void ccTouchEnded(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
	virtual void ccTouchCancelled(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
	virtual void ccTouchMoved(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);

protected:
	void animateFocusMenuItem(cocos2d::CCMenuItem* menuItem);
	void animateFocusLoseMenuItem(cocos2d::CCMenuItem* menuItem);
};


#endif //__GrowButton_H__

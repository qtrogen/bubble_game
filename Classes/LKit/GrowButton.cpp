//
//  GrowButton.m
//  Game
//
//  Created by hrh on 9/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "GrowButton.h"

USING_NS_CC;

GrowButton* GrowButton::buttonWithSprite(const char* normalImage, const char* selectImage, 
	CCObject* target, SEL_MenuHandler sel, int tag)
{
	CCSprite *normalSprite = CCSprite::create(normalImage);
	CCSprite *selectSprite = CCSprite::create(selectImage);
	assert(normalSprite);
	assert(selectSprite);

	CCMenuItem *menuItem = CCMenuItemSprite::create(normalSprite, selectSprite, target, sel);
	menuItem->setTag(tag);
	GrowButton *menu = (GrowButton*) GrowButton::create(menuItem);
	menu->setTag(tag);
	return menu;	
}

GrowButton* GrowButton::create(CCMenuItem* item)
{
	GrowButton* btn = new GrowButton();
	btn->autorelease();
	CCArray* pArray = CCArray::create(item, NULL);
	btn->initWithArray(pArray);
	return btn;
}

GrowButton* GrowButton::buttonWithSpriteFrame(const char* frameName, const char* selectframeName, CCObject* target, SEL_MenuHandler sel)
{
	CCSprite *normalSprite = CCSprite::createWithSpriteFrameName(frameName);
	CCSprite *selectSprite = CCSprite::createWithSpriteFrameName(selectframeName);

	assert(normalSprite);
	assert(selectSprite);

	CCMenuItem *menuItem = CCMenuItemSprite::create(normalSprite, selectSprite, target, sel);
	GrowButton *menu = (GrowButton*) GrowButton::create(menuItem);
	return menu;
}

void GrowButton::animateFocusMenuItem(CCMenuItem* menuItem)
{
	if (menuItem == NULL) return;

	CCScaleTo* movetozero = CCScaleTo::create(0.1f, 1.2f);
	CCEaseBackOut* ease = CCEaseBackOut::create(movetozero);
	CCScaleTo* movetozero1 = CCScaleTo::create(0.1f, 1.15f);
	CCEaseBackOut* ease1 = CCEaseBackOut::create(movetozero1);
	CCScaleTo* movetozero2 = CCScaleTo::create(0.1f, 1.2f);
	CCEaseBackOut* ease2 = CCEaseBackOut::create(movetozero2);
	CCSequence* sequence = CCSequence::create(ease, ease1, ease2, NULL);
	menuItem->runAction(sequence);	
}

void GrowButton::animateFocusLoseMenuItem(CCMenuItem* menuItem)
{
	if (menuItem == NULL) return;

	CCScaleTo* movetozero = CCScaleTo::create(0.1f, 1.0f);
	CCEaseBackOut* ease = CCEaseBackOut::create(movetozero);
	CCScaleTo* movetozero1 = CCScaleTo::create(0.1f, 1.05f);
	CCEaseBackOut* ease1 = CCEaseBackOut::create(movetozero1);
	CCScaleTo* movetozero2 = CCScaleTo::create(0.1f, 1.0f);
	CCEaseBackOut* ease2 = CCEaseBackOut::create(movetozero2);
	CCScaleTo* movetozero3 = CCScaleTo::create(0.1f, 1.0f);
	CCEaseBackOut* ease3 = CCEaseBackOut::create(movetozero3);
	CCSequence* sequence = CCSequence::create(ease,ease1, ease2, ease3, NULL);
	menuItem->runAction(sequence);
}

bool GrowButton::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	bool bRet = CCMenu::ccTouchBegan(touch, event);
	if (bRet)
	{
		animateFocusMenuItem(m_pSelectedItem);
	}
	return bRet;
}

void GrowButton::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	CCMenu::ccTouchEnded(touch, event);

	animateFocusLoseMenuItem(m_pSelectedItem);
}

void GrowButton::ccTouchCancelled(CCTouch* touch, CCEvent* event)
{
	CCMenu::ccTouchCancelled(touch, event);
	animateFocusLoseMenuItem(m_pSelectedItem);
}

void GrowButton::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	CCMenuItem* pOldItem = m_pSelectedItem;
	CCMenu::ccTouchMoved(touch, event);

	if (pOldItem != m_pSelectedItem) {
		if (pOldItem)
			animateFocusLoseMenuItem(pOldItem);
		if (m_pSelectedItem)
			animateFocusMenuItem(m_pSelectedItem);
	}
}

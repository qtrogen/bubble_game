//
//  BsToggleButton.m
//  StickWars - Siege
//
//  Created by EricH on 8/3/09.
//
 
#include "BsToggleButton.h"

USING_NS_CC;

BsToggleButton* BsToggleButton::buttonWithImage(const char* normalImage, const char* selectedImage, cocos2d::CCObject* target, cocos2d::SEL_MenuHandler handler)
{
	return buttonWithImage(normalImage, selectedImage, target, handler, 0);
}
BsToggleButton* BsToggleButton::buttonWithImage(const char* imageOn, const char* imageOff, cocos2d::CCObject* target, cocos2d::SEL_MenuHandler sel, int tag)
{
	CCSprite *onSprite = CCSprite::create(imageOn);
	CCSprite *offSprite = CCSprite::create(imageOff);
    onSprite->setOpacity(150);
    offSprite->setOpacity(200);
	
	onSprite->setAnchorPoint(ccp(0,0));
	offSprite->setAnchorPoint(ccp(0,0));
	
	assert(onSprite);
	assert(offSprite);
	
	CCMenuItem *itemOn = CCMenuItemSprite::create(onSprite, NULL, target, sel);
	CCMenuItem *itemOff = CCMenuItemSprite::create(offSprite, NULL, target, sel);
	CCMenuItemToggle* menuItem = CCMenuItemToggle::create();
	menuItem->setTarget(target, sel);
	menuItem->addSubItem(itemOn);
	menuItem->addSubItem(itemOff);
    menuItem->setTag(tag);
	
	BsToggleButton *menu = (BsToggleButton *) BsToggleButton::create(menuItem);
    menu->setTag(tag);
	return menu;
}

BsToggleButton* BsToggleButton::create(CCMenuItem* item)
{
	BsToggleButton* btn = new BsToggleButton();
	btn->autorelease();
	CCArray* pArray = CCArray::create(item, NULL);
	btn->initWithArray(pArray);
	return btn;
}

void BsToggleButton::setEnable(bool bEnable)
{
	setTouchEnabled(bEnable);
	
	if (bEnable)
		setOpacity(255);
	else 
		setOpacity(80);	
}

void BsToggleButton::setState(bool bState)
{
	CCMenuItemToggle* item = (CCMenuItemToggle*) getChildren()->objectAtIndex(0);
	if ( item->isVisible() && item->isEnabled() ) {
		if(bState)
			item->setSelectedIndex(0);
		else
			item->setSelectedIndex(1);
		item->selected();
	}
}

bool BsToggleButton::getState()
{
	CCMenuItemToggle* item = (CCMenuItemToggle*) getChildren()->objectAtIndex(0);
	if(item->getSelectedIndex() == 0)
		return true;
	return false;
}

//
//  LCScrollLayer.h
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//

#ifndef __LCScrollLayer_H__
#define __LCScrollLayer_H__

#include "cocos2d.h"

#define FRAME_RATE 60
#define BOUNCE_TIME 0.2f

typedef enum
{
	BounceDirectionGoingUp = 1,
	BounceDirectionStayingStill = 0,
	BounceDirectionGoingDown = -1,
	BounceDirectionGoingLeft = 2,
	BounceDirectionGoingRight = 3
} BounceDirection;

class LCScrollLayer : public cocos2d::CCLayer 
{

public:
	LCScrollLayer();
	~LCScrollLayer();

	cocos2d::CCRect viewPort;
	bool enableScroll;

	void reset();

protected:
	BounceDirection direction;
	bool isDragging;
	float lasty;
	float yvel;

protected:
	virtual void update(float delta);
	virtual bool ccTouchBegan(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
	virtual void ccTouchMoved(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
	virtual void ccTouchEnded(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
	virtual void registerWithTouchDispatcher();
};

#endif //__LCScrollLayer_H__


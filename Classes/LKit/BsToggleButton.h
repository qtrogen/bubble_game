//
//  BsToggleButton.h
//  StickWars - Siege
//
//  Created by EricH on 8/3/09.
//
#ifndef __BSTOGGLEBUTTON_H__
#define __BSTOGGLEBUTTON_H__
#include "cocos2d.h"


class BsToggleButton: public cocos2d::CCMenu {
public:
	static BsToggleButton* buttonWithImage(const char* normalImage, const char* selectedImage, cocos2d::CCObject* target, cocos2d::SEL_MenuHandler handler);
	static BsToggleButton* buttonWithImage(const char* normalImage, const char* selectedImage, cocos2d::CCObject* target, cocos2d::SEL_MenuHandler handler, int tag);

	static BsToggleButton* create(cocos2d::CCMenuItem* item);

	void setEnable(bool bEnable);
	void setState(bool state);
	bool getState();
};
 
#endif //__BSTOGGLEBUTTON_H__

//
//  LCScrollLayer.m
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copybottom 2013 lion. All bottoms reserved.
//

#include "LCScrollLayer.h"
USING_NS_CC;
LCScrollLayer::LCScrollLayer()
{
	setTouchEnabled(true);
    reset();
	scheduleUpdate();
}

LCScrollLayer::~LCScrollLayer()
{

}

void LCScrollLayer::reset()
{
    isDragging = false;
    lasty = 0.0f;
    yvel = 0.0f;
    direction = BounceDirectionStayingStill;
    enableScroll = false;
}

void LCScrollLayer::update(float delta)
{
    if (m_pChildren == NULL || m_pChildren->count() == 0) return;
    if (!enableScroll) return;
    
	CCPoint pos = getPosition();
	// positions for scrollLayer
	
	float top = pos.y + getContentSize().height / 2;
	float bottom = pos.y - getContentSize().height / 2;
	// Bounding area of scrollview
	float minY = viewPort.origin.y;
	float maxY = viewPort.origin.y + viewPort.size.height;
	
	if(!isDragging) {
		static float friction = 0.96f;
		
		if(top < maxY && direction != BounceDirectionGoingUp) {
			
			yvel = 0;
			direction = BounceDirectionGoingUp;
			
		}
		else if(bottom > minY && direction != BounceDirectionGoingDown)	{
			
			yvel = 0;
			direction = BounceDirectionGoingDown;
		}
		
		if(direction == BounceDirectionGoingDown)
		{
			
			if(yvel <= 0)
			{
				float delta = (minY - bottom);
				float yDeltaPerFrame = (delta / (BOUNCE_TIME * FRAME_RATE));
				yvel = yDeltaPerFrame;
			}
			
			if((bottom - 0.5f) <= minY)
			{
				
				pos.y = top - getContentSize().height / 2;
				yvel = 0;
				direction = BounceDirectionStayingStill;
			}
		}
		else if(direction == BounceDirectionGoingUp)
		{
			
			if(yvel >= 0)
			{
				float delta = (maxY - top);
				float yDeltaPerFrame = (delta / (BOUNCE_TIME * FRAME_RATE));
				yvel = yDeltaPerFrame;
			}
			
			if((top + 0.5f) >= maxY) {
				
				pos.y = bottom + getContentSize().height / 2;
				yvel = 0;
				direction = BounceDirectionStayingStill;
			}
		}
		else
		{
			yvel *= friction;
		}
		
		pos.y += yvel;
		
		setPosition(pos);
	}
	else
	{
		if(top >= maxY || bottom <= minY) {
			
			direction = BounceDirectionStayingStill;
		}
		
		if(direction == BounceDirectionStayingStill) {
			yvel = (pos.y - lasty)/2;
			lasty = pos.y;
		}
	}
}

bool LCScrollLayer::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	isDragging = true;
	
	return true;
}

void LCScrollLayer::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	
	CCPoint preLocation = touch->getPreviousLocation();
	CCPoint curLocation = touch->getLocation();
	
	CCPoint nowPosition = getPosition();
	nowPosition.y += ( curLocation.y - preLocation.y );
	setPosition(nowPosition);
}

void LCScrollLayer::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	isDragging = false;
}

void LCScrollLayer::registerWithTouchDispatcher()
{
	CCTouchDispatcher* touchDispatcher = CCDirector::sharedDirector()->getTouchDispatcher();
	touchDispatcher->addTargetedDelegate(this, 0, true);
}

//
//  LCommon.cpp
//  IcecreamCandyBlast
//
//  Created by Chen on 7/10/13.
//  Copyright (c) 2013 lion. All rights reserved.
//

#include "LCommon.h"
#include "MathUtil.h"
#include "curl/curl.h"
#include <algorithm>
#include "AppDelegate.h"
#include "SaveUtil.h"

USING_NS_CC;

float SC_WIDTH = 320;
float SC_HEIGHT = 480;

float SC_WIDTH_HALF = 160;
float SC_HEIGHT_HALF = 240;

float SC_RATIO = 1.0f;
float SC_FACTOR = 1.0f;
float SC_IF = 1.0f;

float SC_BG_SCALE;
float SC_DLG_SCALE;
int	SC_BUBBLES_DIST;

bool IS_LOWMEMORY = true;

bool IS_ANIMAL_SELECTED = false;

float SC_ADHEIGHT = 0;
float SC_BANNERHEIGHT = 0;

std::string CHARA_NAME[] = { 
	"Kali", "Piggles", "Roo", "Jack", "Pingu", "Momo" 
};

std::string BOOST_NAME[] = { 
	"", "Sniper Aim", "Extra Bubble Swap", "Zombie Repellent", "Nuke", "Color bomb", "Pin Popper", "Camera screen", "Undo Move", "Extra Bubbles"
};

std::string LOCAL_FRIEND[] = {
	"Kali", "Pingu", "Piggles", "Roo", "Jack", "Momo"
};

std::string LEVELS_NAME[] = {
	"C1", /*"M3",*/ /*"B9", */ "A1", "C3", "M1", "A3", "M2", "B1", "B3", "A4", "C5",
	"C7", "M4", "B4", "C4", "C9", "M3", "M5", "A5", "A6", "B2",
	"M7", "C6", "C8", "A8", "A9", "B5", "C10", "A7", "B7", "C2",
	"A2", "M10", "B6", "A10", "M6", "B7", "M8", "B8", "M9", "B10",
};


const float BUBBLE_SPEED = 1500;

const float BEE_ROTATION_ANGLE = 30.0f;

const int STAR_COND[] = {0, 14000, 22001};
const int UNLOCKWORLD_STAR[] = {0, 38, 75, 115, 155, 194};

const int DROP_SCORE_MIN  = 50;
const int DROP_SCORE_PLUS = 50;

const int UNUSED_SCORE_MIN = 300;
const int UNUSED_SCORE_MAX = 550;

const int EXPLODE_SCORE = 10;

const int LIVE_COST = 15;
const int LIVE_BUY_COUNT = 5;

const int FREE_SPIN_PERIOD = 24 * 3600; // 24 hour
const int FREE_LIVE_PERIOD = 30 * 60; // 30 min

const int UNLOCK_CHARA_STAR[] = {0, 44, 44, 38, 35, 34};
const int UNLOCK_CHARA_MAGIC[] = {0, 28, 28, 35, 45, 50};

const int BONUS_MAGIC[] = {2, 12, 3, 27, 4, 41, 4, 60, 3, 72, 2, 87};
const int BOOST_APPEAR_LEVEL[] = {0, 9, 19, 29, 34, 20, 16, 44, 11, 999, 999, 999, 999, 999};
const int BUY_BOOST_PACK = 3;

const int BUY_MAGIC_CNT[] = {120, 250, 530, 1400, 3000};
const int BUY_SPIN_CNT[] = {3, 7, 10, 15};
const int BUY_SPIN_COST[] = {50, 80, 100, 125};

const int MIN_LEVEL_CONTINUE_6MAGIC = 33;
const int MIN_LEVEL_CONTINUE_8MAGIC = 43;

//const int PUZZLE_PIECE_PRICE = 500;
const int PUZZLE_PIECE_PRICE[] = {0, 200, 150, 300, 400, 450};

BOOST_COST boost_cost[] = 
{
	// Sniper Aim
	{1, IT_MAGIC, 24},

	// Extra bubble swap
	{1, IT_MAGIC, 9},

	// Zombie Repellent
	{3, IT_MAGIC, 9},
	
	// Bubble Nukes
	{3, IT_MAGIC, 100},
	
	// Color Bomb
	{3, IT_MAGIC, 10},
	
	// Pin Popper
	{3, IT_MAGIC, 4},
	
	// Camera Screen viewer
	{3, IT_MAGIC, 9},
	
	// Undo Move
	{3, IT_MAGIC, 12},

	// Foresight
	{1, IT_MAGIC, 12},
	
	// Extra Bubble
	{3, IT_MAGIC, 50},

	// Fireball
	{3, IT_MAGIC, 15},

	// Renew
	{3, IT_MAGIC, 20},

	// Diffusion
	{3, IT_MAGIC, 40}
};

const SPECIAL_BOOST_INFO special_boost_info[] = 
{
	// None for Kali(Cat)
	{BOOST_NONE,				"Has no special skill", "",	0, 0, 0.0f, 0, 0, 0, false},

	// Sniper Aim for Piggles(Pig)
	{BOOST_SNIPER_AIM,			"Sniper", "Extends the aim and accuracy", 3, 20, 19.99, 9, 9, 60, false},

	// Foresight for Roo(Rabbit)
	{BOOST_FORESIGHT,			"Foresight", "Makes two forthcoming bubbles visible", 3, 15, 14.99, 9, 6, 45, false},

	// UndoMove for Jack(Monkey)
	{BOOST_UNDO_MOVE,			"Recall", "Allows one bubble recall per 5 tosses", 3, 25, 24.99, 9, 6, 75, false},

	// CameraScreen for Pingu(Penguin)
	{BOOST_CAMERA_SCREEN,		"Director", "Grants camera scrolling", 3, 30, 29.99, 9, 6, 90, false},

	// ExtraBubbleSwap for Momo(Panda Bear)
	{BOOST_EXTRA_BUBBLE_SWAP,	"Overload", "Stacks three bubbles to swap through", 3, 30, 29.99, 9, 6, 90, false}
};

const char animalsEquippedPowerups[] = { BOOST_NONE, BOOST_SNIPER_AIM, BOOST_FORESIGHT, BOOST_UNDO_MOVE, BOOST_CAMERA_SCREEN, BOOST_EXTRA_BUBBLE_SWAP };

const BUBBLE_POINTS bubble_points_info[] = 
{
	// None
	{0, 0.0f},

	// Purple
	{100, 0.05f},

	// Blue
	{70, 0.06f},

	// Green
	{50, 0.07f},

	// Yellow
	{20, 0.10f},

	// Orange
	{30, 0.09f},

	// Red
	{40, 0.08f},
};

const int STREAKS_PLUS_COUNT = 18;
const float STREAKS_PLUS[] = { 1.00f, 1.25f, 1.50f, 2.00f, 2.20f, 2.40f, 2.60f, 3.00f, 3.20f, 3.40f, 3.60f, 3.80f, 4.00f, 4.20f, 4.40f, 4.60f, 4.80f, 5.00f };

const int STREAKS_MINUS_COUNT = 6;
const float STREAKS_MINUS[] = { 0.20f, 0.40f, 0.80f, 1.60f, 3.00f, 5.00f };


//int TUTO_LEVELS[] = {0, 4, 6, 9, 13, 14, 20, 21, 28, 34, 38, 44, 49}; //old
int TUTO_LEVELS[] = {0, 9, 19, 11, 16, 13, 20, 21, 28, 29, 34, 38, 44, 49};


const char* ITEM_IMAGES[] = {"", "Image/common/item_puzzle.png", "Image/common/item_live.png", "Image/common/item_magic.png", "Image/common/item_map.png"};

std::string BOOST_DESC[] = { 
	"",
	"",
	"Extends your aim and",
	"accuracy",
	"Can swap through",
	"3 bubbles",
	"Respells the",
	"Zombie bubbles",
	"Blows up 8 bubbles,",
	"/obstacles it touches",
	"Blows up 6 bubbles",
	"of the same color",
	"They allow users to", 
	"pop any one bubble",
	"Camera scrolls up ",
	"",
	"Allows player to",
	"undo a previous move",
	"Gain extra bubbles",
	"to throw",
	"",
	"",
	"Fireball",
	"",
	"Renew",
	"",
	"Diffusion",
	""
};

LCommon::LCommon()
{
	isRemoveAds = false;
    isFBConnected = false;
	curBoosts = 0;
	curChara = 0;
	needSeePassLoseLiftTuto = false;
	needSeeSpinTuto = false;
    currentAppVersion = 0;

	isGameDataLoaded = false;

	isViaMoregame = false;


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#endif // (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
}

LCommon* LCommon::sharedInstance()
{
    static LCommon* instance = NULL;
    if (instance == NULL)
        instance = new LCommon();
    
	return instance;
}

void LCommon::initConstValues()
{
	SC_BUBBLES_DIST = 250;
	if (SC_RATIO > 1) 
		SC_BUBBLES_DIST += (SC_RATIO - 1) * 800;
}

int LCommon::curLevel()
{
	return nCurLevel;
}
int LCommon::maxLevel()
{
	return nMaxLevel;
}
int LCommon::stars(int nLevel)
{
	return charaInfo[curChara].starCount[nLevel];
}
bool LCommon::setMaxLevel(int level)
{
	if (nMaxLevel < level)
	{
		nMaxLevel = level;
		for (int i = 0; i < MAX_LOCAL_FRIENDS; i++)
		{
			int localLevel = localFriendMaxLevels[i][curChara];
			if (localLevel <= level)
			{
				int nRand = getRandRange(0, 100);
				if (nRand > 90)
					localLevel += 2;
				else if (nRand > 75) localLevel += 1;

				do 
				{
					int nSameCount = 0;
					for (int j = 0; j < i; j++)
						if (localFriendMaxLevels[j][curChara] == localLevel)
							nSameCount++;
					if (nSameCount > 2)
						localLevel++;
					else
						break;
				} while (true);

				localFriendMaxLevels[i][curChara] = MIN(localLevel, MAX_LEVEL - 1);
			}
		}
		return true;
	}
	
	return false;
}
bool LCommon::setStarCount(int nLevel, int nCnt)
{
	if (charaInfo[curChara].starCount[nLevel] < nCnt)
	{
		charaInfo[curChara].starCount[nLevel] = nCnt;
		return true;
	}

	return false;
}
void LCommon::setCurLevel(int level)
{
	nCurLevel = level;
	curWorld = getWorldByLevel(level);
}

LEVEL_TYPE LCommon::getCurLevelType()
{
	return curLevelType;
}


void LCommon::setCurLevelType(LEVEL_TYPE type)
{
	curLevelType = type;
}


std::string LCommon::getSavedData()
{
	return CSaveUtil::sharedInstance()->getSavedData();
}

void LCommon::loadFromString(const char* strData)
{
    std::string fbId = myFBID;
	CSaveUtil::sharedInstance()->loadFromString(strData);
    CSaveUtil::sharedInstance()->setStringForKey("myFBID", myFBID);
	loadState();
}

void LCommon::loadState()
{
	CSaveUtil* defaults = CSaveUtil::sharedInstance();
	liveCount = defaults->getIntegerForKey("liveCount", 5);
	magicCount = defaults->getIntegerForKey("magicCount", 20);
    
    // added by KUH for debug
    //liveCount = 20;
    //magicCount = 5000;
    
    // modified by KUH in 2014-09-25 ( from 20 ==> To 1)
	spinCount = defaults->getIntegerForKey("spinCount", 1);
    
    // added by KUH in 2014-09-26
    parseMainMenu = defaults->getIntegerForKey("parseMainMenu", false);
    parseSelChar = defaults->getIntegerForKey("parseSelChar", false);
    parseLevelWorld = defaults->getIntegerForKey("parseLevelWorld", false);
    
	payoutCount = defaults->getIntegerForKey("payoutCount", 0);
	
	totalPopped = defaults->getIntegerForKey("totalPopped", 0);
	totalDropped = defaults->getIntegerForKey("totalDropped", 0);
	totalSaved = defaults->getIntegerForKey("totalSaved", 0);
	nGoalAchievemens = defaults->getIntegerForKey("nGoalAchievemens", 0);

	isTapToShoot = defaults->getBoolForKey("isTapToShoot", true);
	isSoundOn = defaults->getBoolForKey("isSoundOn", true);
	isNotification = defaults->getBoolForKey("isNotification", true);
	is3StarsPointHint = defaults->getBoolForKey("is3StarsPointHint", true);

 	isRemoveAds = defaults->getBoolForKey("isRemoveAds", false);

	myFBID = defaults->getStringForKey("myFBID", MYFBID_DEFAULT);

	curChara = defaults->getIntegerForKey("curChara", 0);

	nLastRatedVersion = defaults->getIntegerForKey("nLastRatedVersion", 0);

	nCurLevel = defaults->getIntegerForKey("nCurLevel", 0);
	nMaxLevel = defaults->getIntegerForKey("nMaxLevel", 0);
	for (int i = 0; i < CHARA_COUNT; i++)
	{
		TCharaInfo& info = charaInfo[i];
		info.byPieceCount = defaults->getIntegerForKey(CCString::createWithFormat("pieceCount_%d", i)->getCString(), 0);

		defaults->getByteArray(CCString::createWithFormat("stars_ch_%d", i)->getCString(), info.starCount, MAX_LEVEL, 0);
	}

	for (int i = 0; i < MAX_LOCAL_FRIENDS; i++)
		for (int ch = 0; ch < CHARA_COUNT; ch++)
			localFriendMaxLevels[i][ch] = defaults->getIntegerForKey(CCString::createWithFormat("localFriendMaxLevels_%d_%d", i, ch)->getCString(), i);

	loadIntArray(highScore, "highscores", 0, MAX_LEVEL, defaults);

	for (int i = 0; i < BOOST_COUNT; i++)
	{
		boostUnlocked[i] = defaults->getBoolForKey(CCString::createWithFormat("boost_unlock_%d", i)->getCString(), false);
		boostCount[i] = defaults->getIntegerForKey(CCString::createWithFormat("boostCount_%d", i)->getCString(), 0);
	}

	serialLevelGoalData(true);
	serialDailyChal(true);

	serialAchievementData(true);

	serialFreeValues(true);

	for (int i = 0; i < MAX_LEVEL; i++)
	{
		shiftColor[i] = getRandRange(1, COLOR_COUNT);
	}

	isPassLoseLifeTuto = defaults->getBoolForKey("isPassLoseLifeTuto", false);
	isPassSpinTuto = defaults->getBoolForKey("isPassSpinTuto", false);

	TFriendInfo* info = new TFriendInfo;
	info->init();
	info->fbid = myFBID;
	info->bLocalFriend = false;
	friendList.insert(std::make_pair(myFBID, info));

#ifdef TEST_MODE

		magicCount = 1000;

		nCurLevel = 0;
		nMaxLevel = 127;

		for (int i = 0; i < CHARA_COUNT; i++)
		{
			for (int j = 0; j < MAX_LEVEL; j++)
				charaInfo[i].starCount[j] = j % 3 + 1;

			charaInfo[i].byPieceCount = 1 + i;
			//charaInfo[i].byPieceCount = PIECE_PACK_NUM;
			//charaInfo[i].byPieceCount = 3;
		}

		//for (int i = 0; i < BOOST_COUNT; i++)
		//	boostUnlocked[i] = true;

#endif //TEST_MODE
}

void LCommon::saveState()
{
	CSaveUtil* defaults = CSaveUtil::sharedInstance();
	defaults->setIntegerForKey("liveCount", liveCount);
	defaults->setIntegerForKey("magicCount", magicCount);
	defaults->setIntegerForKey("spinCount", spinCount);
	defaults->setIntegerForKey("payoutCount", payoutCount);

	defaults->setIntegerForKey("totalPopped", totalPopped);
	defaults->setIntegerForKey("totalDropped", totalDropped);
	defaults->setIntegerForKey("totalSaved", totalSaved);
	defaults->setIntegerForKey("nGoalAchievemens", nGoalAchievemens);
	
	defaults->setBoolForKey("isTapToShoot", isTapToShoot);
	defaults->setBoolForKey("isSoundOn", isSoundOn);
	defaults->setBoolForKey("isNotification", isNotification);
	defaults->setBoolForKey("is3StarsPointHint", is3StarsPointHint);

    defaults->setBoolForKey("isRemoveAds", isRemoveAds);

	defaults->setStringForKey("myFBID", myFBID);

	defaults->setIntegerForKey("curChara", curChara);

	defaults->setIntegerForKey("nLastRatedVersion", nLastRatedVersion);

	defaults->setIntegerForKey("nCurLevel", nCurLevel);
	defaults->setIntegerForKey("nMaxLevel", nMaxLevel);

    // added by KUH in 2014-09-26
	defaults->setIntegerForKey("parseMainMenu", parseMainMenu);
	defaults->setIntegerForKey("parseSelChar", parseSelChar);
	defaults->setIntegerForKey("parseLevelWorld", parseLevelWorld);

	for (int i = 0; i < CHARA_COUNT; i++)
	{
		TCharaInfo& info = charaInfo[i];
		defaults->setIntegerForKey(CCString::createWithFormat("pieceCount_%d", i)->getCString(), info.byPieceCount);

		defaults->setByteArray(CCString::createWithFormat("stars_ch_%d", i)->getCString(), info.starCount, MAX_LEVEL);
	}

	saveIntArray(highScore, "highscores", MAX_LEVEL, defaults);

	for (int i = 0; i < MAX_LOCAL_FRIENDS; i++)
		for (int ch = 0; ch < CHARA_COUNT; ch++)
			defaults->setIntegerForKey(CCString::createWithFormat("localFriendMaxLevels_%d_%d", i, ch)->getCString(), localFriendMaxLevels[i][ch]);

	for (int i = 0; i < BOOST_COUNT; i++)
	{
		defaults->setBoolForKey(CCString::createWithFormat("boost_unlock_%d", i)->getCString(), boostUnlocked[i]);
		defaults->setIntegerForKey(CCString::createWithFormat("boostCount_%d", i)->getCString(), boostCount[i]);
	}

	defaults->setBoolForKey("isPassLoseLifeTuto", isPassLoseLifeTuto);
	defaults->setBoolForKey("isPassSpinTuto", isPassSpinTuto);

	serialLevelGoalData(false);
	serialDailyChal(false);
	serialAchievementData(false);
    
	serialFreeValues(false);

    defaults->flush();
    
    if (isFBConnected)
        AppDelegate::app->uploadGameDataTonet();
}

void LCommon::loadIntArray(int* dst, const char* key, int nDefaultVal, int maxLength, CSaveUtil* saveUtil)
{
	std::string strList = saveUtil->getStringForKey(key, "");
	std::vector<std::string> retList;
	splitString(strList, VALUE_SEPERATOR, retList);

	int i = 0;
	for (i = 0; i < retList.size(); i++)
	{
		std::string& strVal = retList.at(i);
		dst[i] = atoi(strVal.c_str());
	}
	for (; i < maxLength; i++)
		dst[i] = nDefaultVal;

}

void LCommon::saveIntArray(int* src, const char* key, int maxLength, CSaveUtil* saveUtil)
{
	std::string dstStr;
	for (int i = 0; i < maxLength; i++)
	{
		dstStr += CCString::createWithFormat("%d", src[i])->getCString();
		if (i < maxLength - 1)
			dstStr += VALUE_SEPERATOR;
	}
	saveUtil->setStringForKey(key, dstStr);
}

bool LCommon::isTutoPassed(int nLevel)
{
	CSaveUtil* defaults = CSaveUtil::sharedInstance();
	return defaults->getBoolForKey(CCString::createWithFormat("tutopass_%d", nLevel)->getCString(), false);
}
void LCommon::setTutoPassed(int nLevel)
{
	CSaveUtil* defaults = CSaveUtil::sharedInstance();
	defaults->setBoolForKey(CCString::createWithFormat("tutopass_%d", nLevel)->getCString(), true);
}

void LCommon::setItemBuy(int nType, int index)
{
    std::string key = keyForType(nType, index);
	CSaveUtil* defaults = CSaveUtil::sharedInstance();
    defaults->setBoolForKey(key.c_str(), true);
    defaults->flush();
}

bool LCommon::isItemBuy(int nType, int index)
{
    std::string key = keyForType(nType, index);
	CSaveUtil* defaults = CSaveUtil::sharedInstance();
    return defaults->getBoolForKey(key.c_str(), false);
}

std::string LCommon::keyForType(int nType, int index)
{
	char tmp[256];
	sprintf(tmp, "item_%d_%d", nType, index);
    return tmp;
}

void LCommon::addLiveCount(int nLiveCount)
{
	int prevLive = liveCount;
	liveCount += nLiveCount;

	if (liveCount > MAX_LIVE) liveCount = MAX_LIVE;
	if (liveCount < 0) liveCount = 0;

	if (prevLive == MAX_LIVE && liveCount < MAX_LIVE)
	{
		freeLiveSecCount = FREE_LIVE_PERIOD;
		lastFreeLiveSec = getTimeSec();
	}
}

void LCommon::decreateSpinCount()
{
	spinCount -= 1;
	if (spinCount <= 0)
	{
		freeSpinSecCount = FREE_SPIN_PERIOD;
		lastFreeSpinSec = getTimeSec();
	}
}

bool LCommon::checkFirstRun()
{
    const char* firstKey = "isFirstRun";
	CSaveUtil* defaults = CSaveUtil::sharedInstance();
    
    bool isPrevRun = defaults->getBoolForKey(firstKey, false);
    if (!isPrevRun)
    {
        defaults->setBoolForKey(firstKey, true);
		defaults->flush();
    }
    return !isPrevRun;
}

//Checked
int LCommon::getStarsForWorld(int nChara, int nWorld)
{
	int nStart = nWorld * LEVEL_COUNT_PER_WORLD;
	int nLevels = getLevelsPerWorld(nWorld);
	int nCnt = 0;
	for (int i = 0; i < nLevels; i++)
		nCnt += charaInfo[nChara].starCount[nStart + i];
	return nCnt;
}

void LCommon::serialFreeValues(bool bLoad)
{
	CSaveUtil* defaults = CSaveUtil::sharedInstance();
	if (bLoad)
	{
		int curTime = getTimeSec();
		lastFreeSpinSec = defaults->getIntegerForKey("lastFreeSpinSec", curTime);
		lastFreeLiveSec = defaults->getIntegerForKey("lastFreeLiveSec", curTime);

		if (lastFreeLiveSec > curTime) lastFreeLiveSec = curTime;
		while (lastFreeLiveSec + FREE_LIVE_PERIOD < curTime)
		{
			if (liveCount < MAX_LIVE)
				liveCount++;
			lastFreeLiveSec += FREE_LIVE_PERIOD;
		}
		freeLiveSecCount = FREE_LIVE_PERIOD - (curTime - lastFreeLiveSec);

		if (lastFreeSpinSec > curTime) lastFreeSpinSec = curTime;
		while (lastFreeSpinSec + FREE_SPIN_PERIOD < curTime)
		{
			spinCount++;
			lastFreeSpinSec += FREE_SPIN_PERIOD;
		}
		freeSpinSecCount = FREE_SPIN_PERIOD - (curTime - lastFreeSpinSec);
	}
	else
	{
		defaults->setIntegerForKey("lastFreeSpinSec", lastFreeSpinSec);
		defaults->setIntegerForKey("lastFreeLiveSec", lastFreeLiveSec);
	}
}

static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;
    mem->memory = (char *)realloc(mem->memory, mem->size + realsize + 1);
    if (mem->memory == NULL) {
        /* out of memory! */
        printf("not enough memory (realloc returned NULL)\n");
        exit(EXIT_FAILURE);
    }
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
    return realsize;
}

CCTexture2D* LCommon::getImageFromURL(const char* url)
{
    CURL *curl;       // CURL objects
    CURLcode res;
    MemoryStruct buffer; // memory buffer
    
    curl = curl_easy_init(); // init CURL library object/structure
    
    if(curl) {
        
        // set up the write to memory buffer
        // (buffer starts off empty)
        
        buffer.memory = NULL;
        buffer.size = 0;
        
        // (N.B. check this URL still works in browser in case image has moved)
        
        CCLog("%s", url);
        
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1); // tell us what is happening
        
        // tell libcurl where to write the image (to a dynamic memory buffer)
        
        curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
        curl_easy_setopt(curl,CURLOPT_WRITEDATA, (void *) &buffer);
        
        // get the image from the specified URL
        
        res = curl_easy_perform(curl);
        CCLog("Curl code: %i", res);
        
        CCImage* img = new CCImage();
        img->initWithImageData((void*)buffer.memory, (long)buffer.size, CCImage::kFmtPng);
        cocos2d::CCTexture2D* texture = new cocos2d::CCTexture2D(); //TODO:: leak
        texture->initWithImage(img);
        
        curl_easy_cleanup(curl);
        free(buffer.memory);

        return texture;
    }
    return NULL;
}

bool LCommon::getDataFromURL(const char* url, MemoryStruct& buffer)
{
	CURL *curl;       // CURL objects
	CURLcode res;
//	MemoryStruct buffer; // memory buffer

	curl = curl_easy_init(); // init CURL library object/structure

	if(curl == NULL) return false;

	// set up the write to memory buffer
	// (buffer starts off empty)

	buffer.memory = NULL;
	buffer.size = 0;

	// (N.B. check this URL still works in browser in case image has moved)

	CCLog("%s", url);

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1); // tell us what is happening

	// tell libcurl where to write the image (to a dynamic memory buffer)

	curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
	curl_easy_setopt(curl,CURLOPT_WRITEDATA, (void *) &buffer);

	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
	// get the image from the specified URL

	res = curl_easy_perform(curl);
	CCLog("Curl code: %i", res);

	curl_easy_cleanup(curl);
	return true;
}

void LCommon::addFriendPicture(const char* key, void* buf, int nSize)
{
    CCImage* img = new CCImage();
	img->initWithImageData(buf, nSize, CCImage::kFmtPng);
	CCLog("initWithImageData: %s: %d:%c,%c,%c,%c", key, nSize, ((char*)buf)[0], ((char*)buf)[1], ((char*)buf)[2], ((char*)buf)[3]);
	CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addUIImage(img, key);

	/*img->initWithImageData(buf, nSize, CCImage::kFmtRawData, 128, 128);
	CCLog("initWithImageData: %d:%d,%d,%d,%d", nSize, ((char*)buf)[0+0], ((char*)buf)[0+1], ((char*)buf)[0+2], ((char*)buf)[0+3]);
	CCLog("initWithImageData: %d:%d,%d,%d,%d", nSize, ((char*)buf)[10000+0], ((char*)buf)[10000+1], ((char*)buf)[10000+2], ((char*)buf)[10000+3]);
	CCLog("initWithImageData: %d:%d,%d,%d,%d", nSize, ((char*)buf)[65532+0], ((char*)buf)[65532+1], ((char*)buf)[65532+2], ((char*)buf)[65532+3]);*/  
	//CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addUIImage(img, key);

	/*MemoryStruct mem;
	LCommon::sharedInstance()->getDataFromURL("http://192.168.2.117:3000/test.png", mem);
	CCImage* img = new CCImage();
	img->initWithImageData(mem.memory, mem.size, CCImage::kFmtPng);*/
	//CCTexture2D* tex = CCTextureCache::sharedTextureCache()->addUIImage(img, key);
	//CCLog("initWithImageData: %d:%c,%c,%c,%c", mem.size, mem.memory[0], mem.memory[1], mem.memory[2], mem.memory[3]);


    TFriendMap::iterator it = friendList.find(key);
    if (it != friendList.end())
    {
        it->second->texture = tex;

		//it->second->imgBuff.memory = new char[nSize];
		//memcpy(it->second->imgBuff.memory, buf, nSize);
		//it->second->imgBuff.size = nSize;
    }


	MemoryStruct* friendPhoto = new MemoryStruct();
	friendPhoto->memory = new char[nSize];
	memcpy(friendPhoto->memory, buf, nSize);
	friendPhoto->size = nSize;

	TFriendPhotoMap::iterator old = friendPhotoList.find(key);
	if (old == friendPhotoList.end()) {
		friendPhotoList.insert(std::make_pair(key, friendPhoto));
	} else {
		MemoryStruct* oldInfo = (*old).second;
		if(oldInfo->memory) delete oldInfo->memory;
		oldInfo->memory = friendPhoto->memory;
		oldInfo->size = friendPhoto->size;
	}
	
}

bool LCommon::checkFreeTimer()
{
	bool bNeedSave = false;
	int curTime = getTimeSec();

	freeSpinSecCount--;
	if (freeSpinSecCount <= 0)
	{
		freeSpinSecCount = FREE_SPIN_PERIOD;
		if (spinCount < 5)
		{
			lastFreeSpinSec = curTime;
			spinCount += 1;
			bNeedSave = true;
		}
	}

	freeLiveSecCount--;
	if (freeLiveSecCount <= 0)
	{
		freeLiveSecCount = FREE_LIVE_PERIOD;
		if (liveCount < MAX_LIVE)
		{
			addLiveCount(1);
			lastFreeLiveSec = curTime;
			bNeedSave = true;
		}
	}

	if (bNeedSave)
	{
		saveState();
	}

	return bNeedSave;
}

#define FMT_BOOST_DIC	"bst_%d"
#define FMT_MAP_DATA	"map_%d"
#define DIC_BOOST_START	2000
#define DIC_MAP_START	3000
#define METADATA_FILANAME "ebbmetadata.plist"

void LCommon::loadFromMetaData(std::string& metadata)
{
	std::string rec_sep = "\n";
	std::string val_sep = ",";
	std::string head_boost = "bst";
	std::string head_map = "map";
	std::string head_magic = "mgc";

	magicCostList.clear();
	std::vector<std::string> records;

	splitString(metadata, rec_sep, records);

	for (std::vector<std::string>::iterator it = records.begin(); it != records.end(); it++)
	{
		std::string& rec = (*it);
		if (rec.empty()) continue;

		//		rec.erase(std::remove_if(rec.begin(), rec.end(), std::isspace), rec.end());

		if (rec.at(0) == '#') continue;

		int pos = rec.find('=');
		if (pos < 0) continue;

		std::string header = rec.substr(0, pos);
		std::string val = rec.substr(pos + 1, rec.length() - pos);

		std::string headmark = header.substr(0, 3);
		std::string headval = header.substr(3);
		int nHeadVal = atoi(headval.c_str());

		std::vector<std::string> valList;
		splitString(val, val_sep, valList);

		int nIndex = nHeadVal - 1;
		if (headmark == head_boost)
		{
			boost_cost[nIndex].count = atoi(valList[0].c_str());
			boost_cost[nIndex].magicCost = atoi(valList[1].c_str());
		}
		else if (headmark == head_map)
		{
			TMapInfo& info = mapInfo[nIndex];

			info.m_byStar3 = atoi(valList[0].c_str());
			info.m_byStar2 = atoi(valList[1].c_str());
			info.m_byStar1 = atoi(valList[2].c_str());
			info.m_wLimitTime = atoi(valList[3].c_str()) * 60;
		}
		else if (headmark == head_magic)
		{
			magicCostList.push_back(TMagicCost(nHeadVal, valList[0]));
		}
	}

	isGameDataLoaded = true;
}

bool LCommon::loadGameDataViaNet()
{
	MemoryStruct mem;
	// https://dl.dropboxusercontent.com/u/149818497/metadata.txt  -chen folder
	// https://dl.dropboxusercontent.com/u/22394231/metadata.txt - womack folder
	bool bNetSuc = false;//LCommon::sharedInstance()->getDataFromURL("https://dl.dropboxusercontent.com/u/22394231/metadata.txt", mem);

	std::string dataPath = CCFileUtils::sharedFileUtils()->getWritablePath() + METADATA_FILANAME;

	std::string strData;
	if (bNetSuc)
	{
		strData.assign(mem.memory, mem.size);

		std::string header = strData.substr(0, 11);
		if (header.find("#EBBGD") != std::string::npos)
		{
			// write content
            CSaveUtil::saveToFile(strData, dataPath);
		}
		else
		{
			strData.clear();
			bNetSuc = false;
		}
	} 

	if (bNetSuc == false)
	{
		//load from file
		CCString* str = CCString::createWithContentsOfFile(dataPath.c_str());
		if (str != NULL)
        {
			strData.assign(str->getCString());
        }
        else
        {
            CCString* str = CCString::createWithContentsOfFile("map/metadata.txt");
			strData.assign(str->getCString());
            CSaveUtil::saveToFile(strData, dataPath);
        }
	}

// 	free(mem.memory);

	if (!strData.empty())
	{
		loadFromMetaData(strData);
	}

	return true;
}

void LCommon::randomizeColor()
{
	int nCurLevel = curLevel();
	int nextP = shiftColor[nCurLevel] + getRandRange(1, COLOR_COUNT - 1);
	shiftColor[nCurLevel] = ((nextP - COLOR_MIN) % COLOR_COUNT) + COLOR_MIN;
}

char LCommon::getShiftColor(char color)
{
	int nShiftColor = LCommon::sharedInstance()->shiftColor[LCommon::sharedInstance()->curLevel()];
	if (color == COL_NONE || color == COL_ANY) return color;
	return (color + nShiftColor - COLOR_MIN) % COLOR_COUNT + COLOR_MIN;
}

void LCommon::clearFriendList()
{
	for (TFriendMap::iterator it = friendList.begin(); it != friendList.end(); it++)
	{
		TFriendInfo* info = it->second;
		delete info;
	}

	friendList.clear();
}

bool LCommon::isGotFreeMagic()
{
    return currentAppVersion <= nLastRatedVersion;
}

TReqInfo LCommon::popCollectAtIndex(int nIndex)
{
    TReqInfo info = collectList.at(nIndex);
    collectList.erase(collectList.begin() + nIndex);
    return info;
}

TReqInfo LCommon::popLiveReqAtIndex(int nIndex)
{
    TReqInfo info = liveReqList.at(nIndex);
    liveReqList.erase(liveReqList.begin() + nIndex);
    return info;
}


int getTimeSec()
{ 
	time_t timep; 
	struct cc_timeval now;  
	CCTime::gettimeofdayCocos2d(&now, NULL);  
	timep = now.tv_sec; 
	return timep;
}

int getToday()
{
	return getTimeSec() / (3600 * 24);
}

int getTimeMilis()
{ 
	time_t timep; 
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)  
	time(&timep); 
#else  
	struct cc_timeval now;  
	CCTime::gettimeofdayCocos2d(&now, NULL);  
	timep = now.tv_sec; 

	now.tv_sec * 1000 + now.tv_usec;
#endif  
	return timep;
}

void splitString(const std::string &str, const std::string &separator, std::vector<std::string> &retList)
{
	int	pos=0;
	int	newPos=0;
	retList.clear();
	while( (newPos= str.find(separator,pos)) != std::string::npos)
	{
		// if not empty sub str. (skip repetition of separator )
		if(newPos-pos>0)
			retList.push_back(str.substr(pos, newPos-pos));
		// skip token
		pos= newPos+separator.size();
	}
	// copy the last substr
	if( pos<str.size() )
		retList.push_back(str.substr(pos, str.size()-pos));
}

//Checked
BOOST_COST getBoostCost(int boostType)
{
	BOOST_COST cost;
	cost.count = 0;
	cost.payType = IT_NONE;
	cost.magicCost = 0;

	if (boostType == BOOST_NONE)
	{
		CCAssert(false, "Boost request for non-boost");
	} else {

		int nBoostId = boostType - 1;

		//if ((boostType == BOOST_EXTRA_BUBBLES) || LCommon::sharedInstance()->boostUnlocked[boostType])
			cost = boost_cost[nBoostId];
	}

	return cost;
}

int	getMagicBonus(int nLevel)
{
	int nMagic = 0;
	int nCnt = sizeof(BONUS_MAGIC) / sizeof(int) / 2;
	for (int i = 0; i < nCnt; i++)
		if (nLevel == BONUS_MAGIC[2 * i + 1])
			nMagic = BONUS_MAGIC[2 * i];

	return nMagic;
}

std::string toString(int n)
{
	return CCString::createWithFormat("%d", n)->getCString();
}

//Checked
std::string getDotDigit(int nDigit)
{
	int nScore = nDigit;
	std::string strScore;
	do 
	{
		int s3 = nScore % 1000;
		nScore /= 1000;
		if (nScore > 0) strScore = CCString::createWithFormat(",%03d", s3)->getCString() + strScore;
		else strScore = CCString::createWithFormat("%d", s3)->getCString() + strScore;
	} while (nScore > 0);

	return strScore;
}

//Checked
int getWorldByLevel(int nLevel)
{
	int nWorld = nLevel / LEVEL_COUNT_PER_WORLD;
	if (nWorld > 6) nWorld = 6;
	return nWorld;
}

//Checked
int getLevelsPerWorld(int nWorld)
{
	return (nWorld == 6)? 32 : 16;
}

const char* getTimeStr(int nTime)
{
	int nSec = nTime % 60; nTime /= 60;
	int nMin = nTime % 60; nTime /= 60;
	int nHour = nTime % 100;;
	return CCString::createWithFormat("%02d:%02d:%02d", nHour, nMin, nSec)->getCString();
}


bool isTutorialLevel(int nLevel)
{
	int nCnt = sizeof(TUTO_LEVELS) / sizeof(int);
	for (int i = 0; i < nCnt; i++)
	{
		if (TUTO_LEVELS[i] == nLevel) return true;
	}
    
	return false;
}

bool compScore(TFriendInfo* left, TFriendInfo* right)
{
	return left->nCurScore > right->nCurScore;
}
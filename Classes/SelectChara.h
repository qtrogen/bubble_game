//
//  SelCharaSecene.h
//  IcecreamCandyBlast
//
//  Created by Chen on 7/21/13.
//  Copyright 2013 lion. All rights reserved.
//
#ifndef __SelCharaSecene_H__
#define __SelCharaSecene_H__

#include "cocos2d.h"
#include <vector>
#include "cocos-ext.h"
#include "BaseLayer.h"

USING_NS_CC_EXT;

struct TSelCharaInfo
{
	int state;	//CS_LOCKED, CS_UNLOCKABLE, CS_UNLOCKED
	int maxLevel;	//maxLevel
	int totalStars;	//TotalStars
	std::string name;	//"Kali", "Piggles", "Roo", "Jack", "Pingu", "Momo"
	std::string imgName; //Kali_n.png, Kali_l.png, Kali_d.png, Piggles_l.png,,,,,
	char starRate[WORLD_COUNT];	//starRate
};

class CSelCharaScene : public CBaseLayer, public cocos2d::extension::CCTableViewDataSource, public cocos2d::extension::CCTableViewDelegate {
public:
	CSelCharaScene();
	~CSelCharaScene();

	virtual void refreshSelectChara();

	CREATE_SCENE_METHOD(CSelCharaScene);
    
protected:
	void onHome(cocos2d::CCObject* obj);
	void onDone(cocos2d::CCObject* obj);
	void onFBConnect(cocos2d::CCObject* obj);
	void onUnlocknow(cocos2d::CCObject* obj);
	void onInfoLeft(cocos2d::CCObject* obj);
	void onInfoRight(cocos2d::CCObject* obj);

	void pageViewEvent(CCObject *pSender, PageViewEventType type);

	void onMoreApp(cocos2d::CCObject* obj);
	void refreshState();
	virtual void onEnter();
    void moveToMap(float dt);
	void initUI();
	void createInfo();
	void createFriends();
	const char* getCharImgName(int nChara, int nState);

	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView* view) {};
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view) {}
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	void showAnimation(const char* strJsonName, float fScale);

protected:

	TSelCharaInfo m_pCharaInfo[CHARA_COUNT];
	//int			m_nCurChara;
	UIWidget*	m_pMainPanel;
	UIDragPanel*	m_pCharaPanel;
	UILabel*	m_pCharaName;
	cocos2d::extension::CCTableView* m_pFriends;

	UIWidget* m_btnDone;
	UIWidget* m_btnPuzzle;
	UIWidget* m_btnBuy;
};

#endif //__SelCharaSecene_H__

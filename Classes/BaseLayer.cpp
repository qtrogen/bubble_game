//
//  CBaseLayer.m
//  IcecreamCandyBlast
//
//  Created by Chen on 11/29/13.
//  Copyright 2013 lion. All rights reserved.
//

#include "LCommon.h"
#include "BaseLayer.h"
#include "AppDelegate.h"
#include "TutoLayer.h"
#include "DailySpin.h"

USING_NS_CC;

enum
{
	DLG_MAIN_WIDGET = 2000
};

int nPiecePos[] = {
	0, 0,
	164, 0,
	324, 0,
	0, 162,
	164, 216,
	282, 216
};

std::string CBaseLayer::curBGMName = "";
std::string BUY_MAGIC_IAPS[] = {"", PD_BUY_MAGIC1, PD_BUY_MAGIC2, PD_BUY_MAGIC3, PD_BUY_MAGIC4, PD_BUY_MAGIC5, PD_BUY_MAGIC6};
int SPIN_COUNTS[] = {1, 6, 10, 16};
#define BUY_SPIN_COUNT 4
#define BUY_MAGIC_COUNT 7

extern TGoalInfo g_GoalList[];

CBaseLayer::CBaseLayer() :
    bgmName(""), m_pActionBar(NULL), m_lblOutLiveCnt(NULL)
{
	LStoreManager::sharedInstance()->setDelegate(this);
    
	m_nActionBarHeight = 80 + (SC_RATIO - 1) * 90;

	m_pMainLayer = UILayer::create();
	addChild(m_pMainLayer, 20);

	m_pPopupLayer = UILayer::create();
	addChild(m_pPopupLayer, 30);

	m_bGivePopupShowing = m_bCollectPopupShowing = false;

}

CBaseLayer::~CBaseLayer()
{
}

void CBaseLayer::setSceneID(int sceneID)
{
	m_SceneID = sceneID;

	AppDelegate::app->setSceneType(m_SceneID);
}

void CBaseLayer::orientationChangedCallback()
{
	CCLog("orientationChangedCallback");
}

//Checked
void CBaseLayer::showPopup(UIWidget* widget, UIWidget* closeBtn, int nPopupTag)
{
	if (closeBtn != NULL)
		addButtonEvent(closeBtn, BTN_ID_CLOSE);

	showPopup(widget, nPopupTag);
}

//Checked
void CBaseLayer::showPopup(UIWidget* widget, int nPopupTag, ccColor3B color/* = ccBLACK*/, int opacity/* = 200*/)
{
	UIPanel* layer = UIPanel::create();
	layer->setSize(CCSizeMake(SC_WIDTH, SC_HEIGHT));

	layer->setTouchEnable(true);
	if(m_pPopupStack.size() == 0)
	{
		layer->setBackGroundColorType(LAYOUT_COLOR_SOLID);
		layer->setBackGroundColor(color);
		layer->setBackGroundColorOpacity(opacity);
	}

	layer->setZOrder(10);
	layer->addChild(widget);
	layer->setTag(nPopupTag);
	widget->setTag(DLG_MAIN_WIDGET);

	m_pPopupLayer->addWidget(layer);
	m_pPopupStack.push_front(layer);
}

void CBaseLayer::showPopup(UIWidget* widget, const char* closeBtnName, int nPopupTag)
{
	UIWidget* closeWidget;
	
	if (closeBtnName != NULL)
		closeWidget = widget->getChildByName(closeBtnName);
	showPopup(widget, closeWidget, nPopupTag);
}

void CBaseLayer::showGiftingPopup(UIWidget* widget, const char* closeBtnName, bool bCollect)
{
	UIWidget* closeWidget = widget->getChildByName(closeBtnName);

	if(bCollect)
	{
		addButtonEvent(closeWidget, BTN_ID_COLLECT_POPUP_CLOSE);
		m_bCollectPopupShowing = true;
	}else{
		addButtonEvent(closeWidget, BTN_ID_GIVE_POPUP_CLOSE);
		m_bGivePopupShowing = true;
	}

	showPopup(widget, 0);
}

void CBaseLayer::blankTouch(CCObject* sender, TouchEventType eventType)
{
// 	CCLog("blankTouch");
}

void CBaseLayer::onBlankButtonEvent(cocos2d::CCObject* obj)
{
//	CCLog("onBlankButtonEvent");
}

//Checked
bool CBaseLayer::closeTopPopup(bool bClean /*= true*/)
{
	if (m_pPopupStack.empty()) return false;

	UIWidget* layer = m_pPopupStack.front();

	int nPopupTag = layer->getTag();
	m_pPopupStack.pop_front();

	if (!bClean)
	{
		UIWidget* widget = layer->getChildByTag(DLG_MAIN_WIDGET);
		widget->retain();
	}

	layer->removeFromParent();

	onPopupClosed(nPopupTag);

	return true;
}

UIWidget* CBaseLayer::topPopupWidget()
{
	if (m_pPopupStack.empty()) return NULL;
	return m_pPopupStack.front();
}

//Checked
void CBaseLayer::delayProcPopup(float dt)
{
	bool bNeedClose = onProcBeforeClose(m_nSelectedTag);

	if (!bNeedClose) return;

	closeTopPopup();

	onProcAfterClose(m_nSelectedTag, m_nAdditionalParam);
}

void CBaseLayer::onButtonEvent(CCObject* obj)
{
	UIWidget* btn = (UIWidget*) obj;
	m_nSelectedTag = btn->getTag();
	m_nAdditionalParam = btn->getActionTag();

	scheduleOnce(schedule_selector(CBaseLayer::delayProcPopup), 0);
}

bool CBaseLayer::onProcAfterClose(int nTag, int nParam)
{
	switch (nTag)
	{
	case BTN_ID_GIVE_POPUP_CLOSE:
		m_bGivePopupShowing = false;
		break;
	case BTN_ID_COLLECT_POPUP_CLOSE:
		m_bCollectPopupShowing = false;
		break;
	case BTN_ID_SHOW_BUY_MAGIC:
		showBuyMagic();
		break;
	case BTN_ID_CONTINUE_BUYING_LIVE:
		if (LCommon::sharedInstance()->magicCount < LIVE_COST)
			showNoEnoughMagic();
		else
		{
			AppDelegate::app->sendFuseEvent(REFILL_LIVES);
			LCommon::sharedInstance()->magicCount -= LIVE_COST;
			LCommon::sharedInstance()->addLiveCount(LIVE_BUY_COUNT);
            LCommon::sharedInstance()->saveState();
            refreshInfo();
		}
		break;
    case BTN_ID_ASK_LIVE:
        AppDelegate::app->askLive();
        break;
	case BTN_ID_BUY_MAGIC_1:
        AppDelegate::app->showRateApp();
        break;
	case BTN_ID_BUY_MAGIC_2:
	case BTN_ID_BUY_MAGIC_3:
	case BTN_ID_BUY_MAGIC_4:
	case BTN_ID_BUY_MAGIC_5:
	case BTN_ID_BUY_MAGIC_6:
	case BTN_ID_BUY_MAGIC_7:
		LStoreManager::sharedInstance()->buyProduct(BUY_MAGIC_IAPS[nTag - BTN_ID_BUY_MAGIC_1]);
		break;
	case BTN_ID_BUY_SPIN_1:
	case BTN_ID_BUY_SPIN_2:
	case BTN_ID_BUY_SPIN_3:
	case BTN_ID_BUY_SPIN_4:
		{
			int id = nTag - BTN_ID_BUY_SPIN_1;
			if (LCommon::sharedInstance()->magicCount < BUY_SPIN_COST[id])
			{
				showNoEnoughMagic();
			}
			else
			{
				AppDelegate::app->sendFusePurchase(CCString::createWithFormat(HOW_MANY_USERS_PURCHASE_SPINS_FMT, BUY_SPIN_CNT[id])->getCString(), BUY_SPIN_CNT[id]);

				LCommon::sharedInstance()->magicCount -= BUY_SPIN_COST[id];
				LCommon::sharedInstance()->spinCount += BUY_SPIN_CNT[id];
				refreshInfo();
				LCommon::sharedInstance()->saveState();
			}
		}
		break;
	case BTN_ID_BUY_POWERUP_1:
		{
			int nSelectedBoostIndex = nParam;

			if(special_boost_info[nSelectedBoostIndex].item1_cost > LCommon::sharedInstance()->magicCount)
			{
				showNoEnoughMagic();
			}else{
				LCommon::sharedInstance()->magicCount -= special_boost_info[nSelectedBoostIndex].item1_cost;
				LCommon::sharedInstance()->boostCount[special_boost_info[nSelectedBoostIndex].boostId] += special_boost_info[nSelectedBoostIndex].item1_count;

				refreshInfo();
				LCommon::sharedInstance()->saveState();

				CBaseLayer* curScene = dynamic_cast<CBaseLayer*> (CCDirector::sharedDirector()->getRunningScene());
				if (curScene != NULL)
					curScene->onProcEvent(BTN_ID_BUY_POWERUP_1, 0, "");
			}
		}
		break;
	case BTN_ID_BUY_POWERUP_2:
		{

		}
		break;
	case BTN_ID_BUY_POWERUP_3:
		{
			int nSelectedBoostIndex = nParam;

			if(special_boost_info[nSelectedBoostIndex].item3_cost > LCommon::sharedInstance()->magicCount)
			{
				showNoEnoughMagic();
			}else{
				LCommon::sharedInstance()->magicCount -= special_boost_info[nSelectedBoostIndex].item3_cost;
				LCommon::sharedInstance()->boostCount[special_boost_info[nSelectedBoostIndex].boostId] += special_boost_info[nSelectedBoostIndex].item3_count + special_boost_info[nSelectedBoostIndex].item3_free_count;

				refreshInfo();
				LCommon::sharedInstance()->saveState();

				CBaseLayer* curScene = dynamic_cast<CBaseLayer*> (CCDirector::sharedDirector()->getRunningScene());
				if (curScene != NULL)
					curScene->onProcEvent(BTN_ID_BUY_POWERUP_3, 0, "");
			}
		}
		break;
	case BTN_ID_DAILY_SPIN:
		{
			DailySpin_PreviousScreen = m_SceneID;
			LCommon::sharedInstance()->curChara = m_nCurChara;
			CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CDailySpin::scene(), ccWHITE));
		}
		break;
	case BTN_ID_BUY_PIECE:
		{
			if(LCommon::sharedInstance()->magicCount >= PUZZLE_PIECE_PRICE[m_nCurChara])
			{
				LCommon::sharedInstance()->charaInfo[m_nCurChara].byPieceCount = PIECE_PACK_NUM;
				LCommon::sharedInstance()->magicCount -= PUZZLE_PIECE_PRICE[m_nCurChara];
				LCommon::sharedInstance()->saveState();

				refreshInfo();
				refreshSelectChara();

			}else{
				showNoEnoughMagic();		
			}
		}
		break;
	case BTN_ID_SETTING_LEADERBOARD:
		{
            AppDelegate::app->showLeaderboard();
		}
		break;
	case BTN_ID_SETTING_TUTORIAL:
		{

		}
		break;
	case BTN_ID_SETTING_DONE:
		{

		}
		break;
	default:
		return false;
	}
	return true;
}

bool CBaseLayer::onProcBeforeClose(int nButtonTag)
{
	switch (nButtonTag)
	{
	case BTN_ID_SETTING_SOUND:
		{
			LCommon::sharedInstance()->isSoundOn = !LCommon::sharedInstance()->isSoundOn;
			if (LCommon::sharedInstance()->isSoundOn) {

                if (!bgmName.empty())
                {
                    playBGM(bgmName);
                }
            } else
				stopBgm();
			updateSettingInfo();
		}
		return false;
	case BTN_ID_SETTING_NOTIF:
		{
			LCommon::sharedInstance()->isNotification = !LCommon::sharedInstance()->isNotification;
			updateSettingInfo();
		}
		return false;
	case BTN_ID_OPEN_TREASURE:
		{
			if (LCommon::sharedInstance()->nGoalAchievemens >= MAPPIECE_PACK_CNT)
			{
				LCommon::sharedInstance()->nGoalAchievemens -= MAPPIECE_PACK_CNT;
				LCommon::sharedInstance()->magicCount += 2;

				UIWidget* treasurePopup = topPopupWidget();
				refreshTreasureInfo(treasurePopup);
				refreshInfo();
				LCommon::sharedInstance()->saveState();
			}
		}
		return false;
	};

	return true;
}

void CBaseLayer::refreshTreasureInfo(UIWidget* popupWidget)
{
	if (popupWidget == NULL) return;

	for (int i = 0; i < MAPPIECE_PACK_CNT; i++)
	{
		UIWidget* mapPiece = popupWidget->getChildByName(CCString::createWithFormat("btnMap_%d", i)->getCString());
		if (i < LCommon::sharedInstance()->nGoalAchievemens)
			mapPiece->active();
		else
			mapPiece->disable();
	}

	UILabelBMFont* lblPieceCount = (UILabelBMFont*) popupWidget->getChildByName("lblPieceCount");

	UIWidget* btnOpenTreasure = popupWidget->getChildByName("btnChest");
	if (LCommon::sharedInstance()->nGoalAchievemens >= MAPPIECE_PACK_CNT)
	{
		btnOpenTreasure->active();
		lblPieceCount->setVisible(true);
	} else
	{
		btnOpenTreasure->disable();
		lblPieceCount->setVisible(false);
	}

	addButtonEvent(btnOpenTreasure, BTN_ID_OPEN_TREASURE);
}

//Checked
void CBaseLayer::onPopupClosed(int nPopupTag)
{
	if (nPopupTag == POPUP_ID_BUY_LIVE)
	{
		m_lblOutLiveCnt = NULL;
	} else if (nPopupTag == POPUP_ID_BUY_MAGIC || nPopupTag == POPUP_ID_BUY_SPIN)
	{
		AppDelegate::app->sendFuseEvent(GOES_TO_PURCHASE_CREDIT_PAGE_AND_THEN_BACKS_OUT_OF_PURCHASE);
	}
}

//Checked
void CBaseLayer::addButtonEvent(UIWidget* btn, int nTag, int nActionTag)
{
	btn->setTag(nTag);
	btn->setActionTag(nActionTag);
	btn->setTouchEnabled(true);
	btn->addReleaseEvent(this, coco_releaseselector(CBaseLayer::onButtonEvent));
}

void CBaseLayer::addButtonEvent(UIWidget* parent, const char* childName, int nTag, int nActionTag)
{
	UIWidget* pWidget = parent->getChildByName(childName);
	addButtonEvent(pWidget, nTag, nActionTag);
}

//Checked
void CBaseLayer::showNoEnoughMagic()
{
	UIWidget* magicWidget = (GUIReader::shareReader()->widgetFromJsonFile("Image/out_magic.json"));

	UIWidget* pBGImage = magicWidget->getChildByName("imageBG");
	pBGImage->setScale(SC_DLG_SCALE);
	pBGImage->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));

	addButtonEvent(magicWidget, "btnBuyMagic", BTN_ID_SHOW_BUY_MAGIC);

	UIWidget* closeBtn = magicWidget->getChildByName("btnClose");
	showPopup(magicWidget, closeBtn);
}

void CBaseLayer::showBuyMagic()
{
	UIWidget* magicWidget = (GUIReader::shareReader()->widgetFromJsonFile("Image/buy_magic.json"));

	UIWidget* pBGImage = magicWidget->getChildByName("imageBG");
	pBGImage->setScale(SC_DLG_SCALE);
	pBGImage->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));

	for (int i = 0; i < BUY_MAGIC_COUNT; i++)
	{
		UIButton* btn = (UIButton*) magicWidget->getChildByName(CCString::createWithFormat("btnBuy1_%d", i)->getCString());
		UILabelBMFont* btnLabel = (UILabelBMFont*) btn->getChildByName(CCString::createWithFormat("lblBuy1_%d", i)->getCString());
		TMagicCost& cost = LCommon::sharedInstance()->magicCostList[i];
		btnLabel->setText(cost.buyTitle.c_str());

		UILabelBMFont* lblCount = (UILabelBMFont*) magicWidget->getChildByName(CCString::createWithFormat("lblBuyMagic1_%d", i)->getCString());
		lblCount->setText(toString(cost.nMagicCount).c_str());
		addButtonEvent(btn, BTN_ID_BUY_MAGIC_1 + i);
	}
    
    TMagicCost& cost = LCommon::sharedInstance()->magicCostList[0]; // first free magic
    
    if (cost.nMagicCount == 0 || LCommon::sharedInstance()->isGotFreeMagic())
    {
		UIWidget* freePane = magicWidget->getChildByName("Panel_1_0");
        freePane->setVisible(false);
        freePane->disable();
        
        for (int i = 1; i < BUY_MAGIC_COUNT; i++)
        {
            UIWidget* pane = magicWidget->getChildByName(CCString::createWithFormat("Panel_1_%d", i)->getCString());
            CCPoint pos = pane->getPosition();
            pos.y += (BUY_MAGIC_COUNT - i - 1) * 20 + 0;
            pane->setPosition(pos);
        }
    }


	UIButton* closeBtn = (UIButton*) magicWidget->getChildByName("btnClose");
	showPopup(magicWidget, closeBtn, POPUP_ID_BUY_MAGIC);
}

void CBaseLayer::showBuySpin()
{
	UIWidget* pWidget = dynamic_cast<UIWidget*>(GUIReader::shareReader()->widgetFromJsonFile("Image/buy_spin.json"));

	UIWidget* imgView = pWidget->getChildByName("imageBG");
	imgView->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));
	imgView->setScale(SC_DLG_SCALE);

	UIWidget* closeBtn = pWidget->getChildByName("btnClose");

	for (int i = 0; i < BUY_SPIN_COUNT; i++)
	{
		UIWidget* buyBtn = pWidget->getChildByName(CCString::createWithFormat("buyBtn_%d", i)->getCString());
		addButtonEvent(buyBtn, BTN_ID_BUY_SPIN_1 + i);
	}

	showPopup(pWidget, closeBtn, POPUP_ID_BUY_SPIN);
}

//Checked
void CBaseLayer::showNoEnoughLive()
{
	UIWidget* magicWidget = (GUIReader::shareReader()->widgetFromJsonFile("Image/out_live.json"));

	UIWidget* pBGImage = magicWidget->getChildByName("imageBG");
	pBGImage->setScale(SC_DLG_SCALE);
	pBGImage->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));

	UIWidget* continueBtn = magicWidget->getChildByName("btnContinue");
	addButtonEvent(continueBtn, BTN_ID_CONTINUE_BUYING_LIVE);
    
	UIWidget* askBtn = magicWidget->getChildByName("btnAskFriend");
	addButtonEvent(askBtn, BTN_ID_ASK_LIVE);
    
	UILabel* curCount = (UILabel*) magicWidget->getChildByName("labelLives");
	curCount->setText(toString(LCommon::sharedInstance()->liveCount).c_str());

	m_lblOutLiveCnt = (UILabel*) magicWidget->getChildByName("labelTime");

	UIWidget* closeBtn = magicWidget->getChildByName("btnClose");
	showPopup(magicWidget, closeBtn, POPUP_ID_BUY_LIVE);
}

void CBaseLayer::showBuyPowerup(int nBoostIndex)
{
	UIWidget* magicWidget = (GUIReader::shareReader()->widgetFromJsonFile("Image/buy_boost.json"));

	UIWidget* pBGImage = magicWidget->getChildByName("imageBG");
	pBGImage->setScale(SC_DLG_SCALE);
	pBGImage->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));

	//Boost Icon, Title, Description
	UIImageView* pBoostIcon = (UIImageView*)pBGImage->getChildByName("ImageView_boost");
	pBoostIcon->loadTexture(CCString::createWithFormat("Image/boosts/boost_%d.png", special_boost_info[nBoostIndex].boostId)->getCString());

	UIImageView* pBoostName = (UIImageView*)pBGImage->getChildByName("ImageView_BoostName");
	pBoostName->loadTexture(CCString::createWithFormat("Image/boosts/boost_effect_desc_%d.png", special_boost_info[nBoostIndex].boostId)->getCString());

	UILabelBMFont* lblBoostDesc = (UILabelBMFont*) pBGImage->getChildByName("lbl_BoostDesc");
	lblBoostDesc->setText(special_boost_info[nBoostIndex].boostDesc.c_str());

	//Item1
	UIImageView* pItem1Icon = (UIImageView*)pBGImage->getChildByName("ImageView_boost_1");
	pItem1Icon->loadTexture(CCString::createWithFormat("Image/boosts/boost_%d.png", special_boost_info[nBoostIndex].boostId)->getCString());

	UILabelBMFont* lblItem1Count = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Count_1");
	lblItem1Count->setText(CCString::createWithFormat("x %d", special_boost_info[nBoostIndex].item1_count)->getCString());

	UILabelBMFont* lblItem1Price = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Price_1");
	lblItem1Price->setText(CCString::createWithFormat("x %d", special_boost_info[nBoostIndex].item1_cost)->getCString());

	UIButton* btnBuy1 = (UIButton*) pBGImage->getChildByName("btnBuy1");
	addButtonEvent(btnBuy1, BTN_ID_BUY_POWERUP_1, nBoostIndex);
	//btnBuy1->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onBuyBoostItem1));


	//Item2
	UIImageView* pItem2Icon = (UIImageView*)pBGImage->getChildByName("ImageView_boost_2");
	pItem2Icon->loadTexture(CCString::createWithFormat("Image/boosts/boost_%d.png", special_boost_info[nBoostIndex].boostId)->getCString());

	UILabelBMFont* lblItem2Count = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Count_2");
	lblItem2Count->setText(CCString::createWithFormat("PERMANENT")->getCString());

	UILabelBMFont* lblItem2Price = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Price_2");
	lblItem2Price->setText(CCString::createWithFormat("$%.2f", special_boost_info[nBoostIndex].item2_cost)->getCString());

	UIButton* btnBuy2 = (UIButton*) pBGImage->getChildByName("btnBuy2");
	addButtonEvent(btnBuy2, BTN_ID_BUY_POWERUP_2, nBoostIndex);
	//btnBuy2->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onBuyBoostItem2));


	//Item3
	UIImageView* pItem3Icon = (UIImageView*)pBGImage->getChildByName("ImageView_boost_3");
	pItem3Icon->loadTexture(CCString::createWithFormat("Image/boosts/boost_%d.png", special_boost_info[nBoostIndex].boostId)->getCString());

	UILabelBMFont* lblItem3Count = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Count_3");
	lblItem3Count->setText(CCString::createWithFormat("x %d", special_boost_info[nBoostIndex].item3_count)->getCString());

	UILabelBMFont* lblItem3AdditionCount = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Count_3_Addition");
	lblItem3AdditionCount->setText(CCString::createWithFormat("+%d FREE", special_boost_info[nBoostIndex].item3_free_count)->getCString());

	UILabelBMFont* lblItem3Price = (UILabelBMFont*) pBGImage->getChildByName("lbl_Boost_Price_3");
	lblItem3Price->setText(CCString::createWithFormat("x %d", special_boost_info[nBoostIndex].item3_cost)->getCString());

	UIButton* btnBuy3 = (UIButton*) pBGImage->getChildByName("btnBuy3");
	addButtonEvent(btnBuy3, BTN_ID_BUY_POWERUP_3, nBoostIndex);
	//btnBuy3->addReleaseEvent(this, coco_releaseselector(CSelBoostScene::onBuyBoostItem3));

	if(m_SceneID == SCENE_GAME)
	{
		UIPanel* panel1 = (UIPanel*)pBGImage->getChildByName("Panel_1");
		panel1->setTouchEnable(false);
		panel1->setVisible(false);
		btnBuy1->setTouchEnable(false);

		UIPanel* panel3 = (UIPanel*)pBGImage->getChildByName("Panel_3");
		panel3->setTouchEnable(false);
		panel3->setVisible(false);
		btnBuy3->setTouchEnable(false);
	}

	//Close Button
	UIButton* closeBtn = (UIButton*) magicWidget->getChildByName("btnClose");

	showPopup(magicWidget, closeBtn);
}

//Checked
void CBaseLayer::showPuzzleInfo(int nCharaId, int nPuzzleCnt, CCPoint pos)
{
	m_nCurChara = nCharaId;

	UIWidget* puzzleWidget = (GUIReader::shareReader()->widgetFromJsonFile("Image/puzzle_dlg.json"));

	UIWidget* pBGImage = puzzleWidget->getChildByName("imageBG");
	pBGImage->setScale(SC_DLG_SCALE);
	pBGImage->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));

	UIImageView* img = (UIImageView*)pBGImage->getChildByName("puzzle_back");

	/*UIImageView* img = UIImageView::create();
	img->loadTexture("Image/puzzle/puzzle_back.png");
	img->setPosition(ccp(-300, -300));
	pBGImage->addChild(img);*/

	//UIImageView* imgCloseBack = UIImageView::create();
	//imgCloseBack->loadTexture("Image/chara/close_back.png");
	//imgCloseBack->setPosition(ccp(280, 260));
	//img->addChild(imgCloseBack);

	//UIButton* btnClose = UIButton::create();
	//btnClose->loadTextures("Image/popup/btn_close_n.png", "Image/popup/btn_close_p.png", "");
	//imgCloseBack->addChild(btnClose);

	for (int i = 0; i < nPuzzleCnt; i++)
	{
		int xx = - img->getContentSize().width / 2 + nPiecePos[2 * i] + 8;
		int yy = img->getContentSize().height / 2 - nPiecePos[2 * i + 1] - 8;
		UIImageView* imgPuzzle = UIImageView::create();
		imgPuzzle->setAnchorPoint(ccp(0, 1));
		imgPuzzle->loadTexture(CCString::createWithFormat("Image/puzzle/%s_%d.png", CHARA_NAME[nCharaId].c_str(), i)->getCString());
		imgPuzzle->setPosition(ccp(xx, yy));
		img->addChild(imgPuzzle);
	}

	UILabelBMFont* lblPrice = (UILabelBMFont*) pBGImage->getChildByName("lbl_Price");
	lblPrice->setText(CCString::createWithFormat("%d", PUZZLE_PIECE_PRICE[m_nCurChara])->getCString());

	UIButton* btnDailySpin = (UIButton*) pBGImage->getChildByName("btnDailySpin");
	addButtonEvent(btnDailySpin, BTN_ID_DAILY_SPIN);
	//btnDailySpin->addReleaseEvent(this, coco_releaseselector(CBaseLayer::onDailySpin));

	UIButton* btnBuy = (UIButton*) pBGImage->getChildByName("btnBuy");
	addButtonEvent(btnBuy, BTN_ID_BUY_PIECE);
	//btnBuy->addReleaseEvent(this, coco_releaseselector(CBaseLayer::onBuyPuzzlePiece));

	//img->setPosition(pos);

	//Close Button
	UIButton* closeBtn = (UIButton*) puzzleWidget->getChildByName("btnClose");

	showPopup(puzzleWidget, closeBtn);
}

void CBaseLayer::refreshSelectChara()
{

}

//void CBaseLayer::onDailySpin(cocos2d::CCObject* obj)
//{
//	DailySpin_PreviousScreen = 0;
//	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CDailySpin::scene(), ccWHITE));
//}
//
//void CBaseLayer::onBuyPuzzlePiece(cocos2d::CCObject* obj)
//{
//	if(LCommon::sharedInstance()->magicCount >= PUZZLE_PIECE_PRICE)
//	{
//		LCommon::sharedInstance()->charaInfo[m_nCurChara].byPieceCount = PIECE_PACK_NUM;
//		LCommon::sharedInstance()->saveState();
//
//		refreshInfo();
//	
//	}else{
//		showNoEnoughMagic();		
//	}
//}

void CBaseLayer::createActionBar()
{
	m_pActionBar = dynamic_cast<UIPanel*>(GUIReader::shareReader()->widgetFromJsonFile("Image/actionbar.json"));
	m_pMainLayer->addWidget(m_pActionBar);

	UIWidget* actionBarBG = m_pActionBar->getChildByName("actionBar_BG");
	actionBarBG->setAnchorPoint(ccp(0.5f, 1.0f));
	actionBarBG->setScaleY(m_nActionBarHeight / 106.f);
	actionBarBG->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT));

	UIWidget* actionBar = m_pActionBar->getChildByName("actionBar");
	actionBar->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT - m_nActionBarHeight / 2 + 5));

	UIButton* btnSpin = (UIButton*) m_pActionBar->getChildByName("btn_spin_plus");
	btnSpin->addReleaseEvent(this, coco_releaseselector(CBaseLayer::buySpin));

	UIButton* btnMagic = (UIButton*) m_pActionBar->getChildByName("btn_magic_plus");
	btnMagic->addReleaseEvent(this, coco_releaseselector(CBaseLayer::buyMagic));

	UIButton* btnLive = (UIButton*) m_pActionBar->getChildByName("btn_live_plus");
	btnLive->addReleaseEvent(this, coco_releaseselector(CBaseLayer::buyLive));

	refreshInfo();
}

void CBaseLayer::createActionBar1()
{
	m_pActionBar = dynamic_cast<UIPanel*>(GUIReader::shareReader()->widgetFromJsonFile("Image/actionbar1.json"));
	m_pMainLayer->addWidget(m_pActionBar);

	UIWidget* actionBarBG = m_pActionBar->getChildByName("actionBar_BG");
	actionBarBG->setAnchorPoint(ccp(0.5f, 1.0f));
	actionBarBG->setScaleY(m_nActionBarHeight / 106.f);
	actionBarBG->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT));

	UIWidget* actionBar = m_pActionBar->getChildByName("actionBar");
	actionBar->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT - m_nActionBarHeight / 2 + 5));

	UIButton* btnSpin = (UIButton*) m_pActionBar->getChildByName("btn_spin_plus");
	btnSpin->addReleaseEvent(this, coco_releaseselector(CBaseLayer::buySpin));

	UIButton* btnMagic = (UIButton*) m_pActionBar->getChildByName("btn_magic_plus");
	btnMagic->addReleaseEvent(this, coco_releaseselector(CBaseLayer::buyMagic));

	UIButton* btnLive = (UIButton*) m_pActionBar->getChildByName("btn_live_plus");
	btnLive->addReleaseEvent(this, coco_releaseselector(CBaseLayer::buyLive));

	refreshInfo();
}

void CBaseLayer::refreshInfo()
{
	if (NULL == m_pActionBar) return;

	UILabelBMFont* lblSpin = (UILabelBMFont*) m_pActionBar->getChildByName("label_spin");
	std::string spins = getDotDigit(LCommon::sharedInstance()->spinCount);
	lblSpin->setText(spins.c_str());

	UILabelBMFont* lblMagic = (UILabelBMFont*) m_pActionBar->getChildByName("label_magic");
	std::string magics = getDotDigit(LCommon::sharedInstance()->magicCount);
	lblMagic->setText(magics.c_str());

	UILabelBMFont* lblLive = (UILabelBMFont*) m_pActionBar->getChildByName("label_live");
	std::string lives = getDotDigit(LCommon::sharedInstance()->liveCount);
	lblLive->setText(lives.c_str());
}

void CBaseLayer::buyMagic(CCObject* obj)
{
	showBuyMagic();
}
void CBaseLayer::buyLive(CCObject* obj)
{
	if (LCommon::sharedInstance()->liveCount < MAX_LIVE)
		showNoEnoughLive();
}
void CBaseLayer::buySpin(CCObject* obj)
{
	if(m_SceneID == SCENE_DAILYSPIN)
		return;

	DailySpin_PreviousScreen = m_SceneID;

	if(LCommon::sharedInstance()->spinCount > 0)
		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1, CDailySpin::scene(), ccWHITE));
	else
		showBuySpin();
}

void CBaseLayer::showSettings()
{
	m_pSettingPane = (GUIReader::shareReader()->widgetFromJsonFile("Image/setting_dlg.json"));

	UIWidget* pWidget = m_pSettingPane->getChildByName("imageBG");
	pWidget->setScale(SC_DLG_SCALE);
	pWidget->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF + 25));
	pWidget->setTouchEnable(true);

	UIWidget* pTapShoot = pWidget->getChildByName("btnTapShoot");
	pTapShoot->addPushDownEvent(this, coco_pushselector(CBaseLayer::onSettingTapShoot));

	UIWidget* pAimShoot = pWidget->getChildByName("btnAimShoot");
	pAimShoot->addPushDownEvent(this, coco_pushselector(CBaseLayer::onSettingAimShoot));

	addButtonEvent(pWidget, "btnLeaderboard", BTN_ID_SETTING_LEADERBOARD);
//	addButtonEvent(pWidget, "btnTutorial", BTN_ID_SETTING_TUTORIAL);
	addButtonEvent(pWidget, "imgSound", BTN_ID_SETTING_SOUND);
	addButtonEvent(pWidget, "imgNotif", BTN_ID_SETTING_NOTIF);
	addButtonEvent(pWidget, "btnDone", BTN_ID_SETTING_DONE);

	updateSettingInfo();
	UIWidget* closeBtn = pWidget->getChildByName("btnClose");
	showPopup(m_pSettingPane, closeBtn);
}

void CBaseLayer::updateSettingInfo()
{
	LCommon* pCommon = LCommon::sharedInstance();

	UIImageView* pSound = (UIImageView*)m_pSettingPane->getChildByName("imgSound");
	pSound->loadTexture(pCommon->isSoundOn? "Image/popup/switch_on.png" : "Image/popup/switch_off.png");

	UIImageView* pNotif = (UIImageView*)m_pSettingPane->getChildByName("imgNotif");
	pNotif->loadTexture(pCommon->isNotification? "Image/popup/switch_on.png" : "Image/popup/switch_off.png");

	UIButton* pTapToShoot = (UIButton*)m_pSettingPane->getChildByName("btnTapShoot");
	UIButton* pAimShoot = (UIButton*)m_pSettingPane->getChildByName("btnAimShoot");

	if (pCommon->isTapToShoot)
	{
		pTapToShoot->disable();
		pAimShoot->active();
	}
	else
	{
		pTapToShoot->active();
		pAimShoot->disable();
	}
}

void CBaseLayer::onSettingTapShoot(CCObject* obj)
{
	LCommon::sharedInstance()->isTapToShoot = true;

	updateSettingInfo();
}

void CBaseLayer::onSettingAimShoot(CCObject* obj)
{
	LCommon::sharedInstance()->isTapToShoot = false;

	updateSettingInfo();
}

void CBaseLayer::onEnter()
{
	CCScene::onEnter();

	CCLayerColor* adsLayer = CCLayerColor::create(ccc4(0, 0, 0, 255), SC_WIDTH, SC_ADHEIGHT);
	adsLayer->setPosition(ccp(0, -SC_ADHEIGHT));
	addChild(adsLayer, 100);

	setPosition(ccp(0, SC_ADHEIGHT));
	//setAnchorPoint(CCPointZero);

	if (!bgmName.empty())
	{
		playBGM(bgmName);
	}

	schedule(schedule_selector(CBaseLayer::onOneSecTimer), 1.0f);
}

void CBaseLayer::onExit()
{
	unschedule(schedule_selector(CBaseLayer::onOneSecTimer));
    CCScene::onExit();
//    CCTextureCache::sharedTextureCache()->removeUnusedTextures();
}

bool CBaseLayer::isPlayingBGM()
{
	return CocosDenshion::SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying();
}

void CBaseLayer::playBGM(std::string bgmFileName, float fVolume, bool bLoop)
{
//#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
//	return;
//#endif
    
	if (!LCommon::sharedInstance()->isSoundOn) return;
    if (curBGMName.compare(bgmFileName) == 0)
	{
		if (CocosDenshion::SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying()) return;
	}

	CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(fVolume);
    
    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(bgmFileName.c_str(), bLoop);

	curBGMName = bgmFileName;
    CCLog("PlayBGM %s", bgmFileName.c_str());
}
void CBaseLayer::playEffect(const char* sfxFileName, bool bLoop)
{
//#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
//	return;
//#endif
	if (!LCommon::sharedInstance()->isSoundOn) return;
    
    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopAllEffects();
	CocosDenshion::SimpleAudioEngine::sharedEngine()->setEffectsVolume(0.2f);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(sfxFileName, bLoop);
    CCLog("playEffect %s", sfxFileName);
}

void CBaseLayer::playEffect(const char* sfxFileName, float fVolume)
{
//#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
//	return;
//#endif
	if (!LCommon::sharedInstance()->isSoundOn) return;
    
    //CCLog("playEffect2 %s", sfxFileName);

//    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopAllEffects();
	CocosDenshion::SimpleAudioEngine::sharedEngine()->setEffectsVolume(fVolume);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(sfxFileName, false);
}

void CBaseLayer::stopAllEffects()
{
    CCLog("stopAllEffects");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopAllEffects();
}

void CBaseLayer::stopBgm()
{
    CCLog("stopBgm");
    curBGMName.clear();
	CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
}

void CBaseLayer::setBGMName(const char* bgmFileName)
{
	//bgmName = "";
	bgmName = bgmFileName;
}


void CBaseLayer::purchaseSuccessed(std::string& productId) 
{
	int nCnt = 0;
	for (int i = 0; i < BUY_MAGIC_COUNT; i++)
	{
		if (productId == BUY_MAGIC_IAPS[i])
		{
			nCnt = LCommon::sharedInstance()->magicCostList[i].nMagicCount;
			AppDelegate::app->sendFusePurchase(CCString::createWithFormat(HOW_MANY_USERS_PURCHASE_CREDITS_FMT, nCnt)->getCString(), 0);
			break;
		}
	}
	if (nCnt == 0) return;
	LCommon::sharedInstance()->magicCount += nCnt;
	LCommon::sharedInstance()->saveState();
	refreshInfo();
}

void CBaseLayer::purchaseFailed(std::string& productId, std::string& errMsg)
{
    CCMessageBox("Purchase Failed.", "Error");
}

//Checked
void CBaseLayer::showIndicator(std::string msg)
{
	UIWidget* blankLayer = UIWidget::create();
	showPopup(blankLayer);
	AppDelegate::app->showIndicator(msg);
}

//Checked
void CBaseLayer::showIndicator()
{
	showIndicator("Please wait");
}

//Checked
void CBaseLayer::hideIndicator()
{
	closeTopPopup();
	AppDelegate::app->hideIndicator();
}

void CBaseLayer::onOneSecTimer(float dt)
{
	if (LCommon::sharedInstance()->checkFreeTimer())
	{
		refreshInfo();
	}

	if (m_lblOutLiveCnt != NULL)
	{
		m_lblOutLiveCnt->setText(getTimeStr(LCommon::sharedInstance()->freeLiveSecCount));
	}

	onTimer();
}

void CBaseLayer::onTimer()
{
}

void CBaseLayer::showTreasure()
{
	UIWidget* mainWidget = (GUIReader::shareReader()->widgetFromJsonFile("Image/goal_dlg.json"));

	UIWidget* pBGImage = mainWidget->getChildByName("imageBG");
	pBGImage->setScale(SC_DLG_SCALE);
	pBGImage->setPosition(ccp(SC_WIDTH_HALF, SC_HEIGHT_HALF));

	if (LCommon::sharedInstance()->nGoalAchievemens > MAPPIECE_PACK_CNT)
	{
		int nCnt = LCommon::sharedInstance()->nGoalAchievemens / MAPPIECE_PACK_CNT;
		LCommon::sharedInstance()->nGoalAchievemens -= MAPPIECE_PACK_CNT * nCnt;
		LCommon::sharedInstance()->magicCount += 2 * nCnt;

		LCommon::sharedInstance()->saveState();
	}

	refreshTreasureInfo(mainWidget);

	int nCurLevel = LCommon::sharedInstance()->curLevel();
	int nSize = LCommon::getOrgTreasureCount();
	
	int showPos = 0;
	for (int i = 0; i < 2; i++) {
		TGoalInfo* goalInfo = NULL;

		for (int j = showPos; j < nSize; j++)
		{
			if (g_GoalList[j].level < nCurLevel || g_GoalList[j].isDone) continue;
			showPos = j + 1;
			goalInfo = &g_GoalList[j];
			break;
		}

		UIWidget* imgGoal = mainWidget->getChildByName(CCString::createWithFormat("imgGoal%d", i)->getCString());
		if (goalInfo != NULL)
		{
			imgGoal->setVisible(true);

			UILabelBMFont* lblLevel = (UILabelBMFont*) imgGoal->getChildByName(CCString::createWithFormat("lblLevel%d", i)->getCString());
			lblLevel->setText(toString(goalInfo->level + 1).c_str());
			UITextArea* txtDesc = (UITextArea*) imgGoal->getChildByName(CCString::createWithFormat("txtDesc%d", i)->getCString());

			int nShiftColor = LCommon::sharedInstance()->shiftColor[goalInfo->level];
			std::string strNewDesc = getChallengeDescShift(goalInfo->desc, nShiftColor);
			txtDesc->setText(strNewDesc.c_str());
		}
		else 
			imgGoal->setVisible(false);
	}

	UIWidget* closeBtn = mainWidget->getChildByName("btnClose");
	showPopup(mainWidget, closeBtn);
}

void CBaseLayer::showTutorial(const char* strTuto, cocos2d::CCPoint pos, char byType, bool bClip, bool isTouchConsumed)
{
	beforeShowTutorial();
	CTutoLayer* tutoLayer = CTutoLayer::create(isTouchConsumed);
	tutoLayer->setCallback(this, callfunc_selector(CBaseLayer::tutorialTapped));
	tutoLayer->showTutorial(strTuto, pos, byType, bClip);
	addChild(tutoLayer, 100);
}

void CBaseLayer::showTutorial1(const char* strTuto, cocos2d::CCPoint pos, char byType, bool bClip, bool isTouchConsumed)
{
	//beforeShowTutorial();
	CTutoLayer* tutoLayer = CTutoLayer::create(isTouchConsumed);
	tutoLayer->setCallback(this, callfunc_selector(CBaseLayer::tutorialTapped));
	tutoLayer->showTutorial(strTuto, pos, byType, bClip, TUTO_BIG_HOLE);
	addChild(tutoLayer, 100);
}

void CBaseLayer::showTutorial(const char* strTuto, cocos2d::CCPoint holePos, TBubbleList& bblList, char byType, bool isTouchConsumed, bool isCenter)
{
	beforeShowTutorial();
	CTutoLayer* tutoLayer = CTutoLayer::create(isTouchConsumed);
	tutoLayer->setCallback(this, callfunc_selector(CBaseLayer::tutorialTapped));
	tutoLayer->showTutorial(strTuto, holePos, bblList, byType, isCenter);
	addChild(tutoLayer, 100);
}

void CBaseLayer::showTutorial(const char* strTuto, TBubbleList& posList, char byType, bool isShowHand)
{
	beforeShowTutorial();
	CTutoLayer* tutoLayer = CTutoLayer::create(isShowHand);
	tutoLayer->setCallback(this, callfunc_selector(CBaseLayer::tutorialTapped));
	tutoLayer->showTutorial(strTuto, posList, byType, isShowHand);
	addChild(tutoLayer, 100);
}

void CBaseLayer::showTutorial(const char* strTuto, char byType, bool isTouchConsumed)
{
	beforeShowTutorial();

	CTutoLayer* tutoLayer = CTutoLayer::create(isTouchConsumed);
	tutoLayer->setCallback(this, callfunc_selector(CBaseLayer::tutorialTapped));
	tutoLayer->showTutorial(strTuto, byType);
	addChild(tutoLayer, 100);
}

void CBaseLayer::FBConnect()
{
	if (LCommon::sharedInstance()->isFBConnected)
		AppDelegate::app->logoutFacebook();
	else
	{
		AppDelegate::app->connectFacebook();

		AppDelegate::app->sendFuseEvent(USERS_WHO_CONNECT_TO_FACEBOOK);
	}
}


// added by KUH in 2014-09-20
#ifdef GLOW_ADD
#endif

//Checked
void CBaseLayer::beforeShowTutorial()
{
	m_bNeedCalcTime = false;
	while (closeTopPopup());
}

/*
 * return value: bool: is processed.
 */
bool CBaseLayer::onProcEvent(int nEventId, int nParam, std::string strParam)
{
    if (nEventId == EVT_GOT_FREE_MAGIC)
    {
        refreshInfo();
    }
    return false;
}
